#!/usr/bin/make -f
# Debhelper rules for lyskom-server.
# Based on templates copyright 1997-1999 Joey Hess.
# Edited in 2001-2009 by Peter Krefting <peterk@debian.org>

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

configure: configure-stamp
configure-stamp:
	dh_testdir
	dh_autoreconf
	./configure --prefix=/usr \
	  --mandir=/usr/share/man \
	  --infodir=/usr/share/info \
	  --sysconfdir=/etc/lyskom-server \
	  --localstatedir=/var/lib/lyskom-server --enable-ipv6 \
	  $(shell dpkg-buildflags --export=configure)
	touch configure-stamp

build-indep:
build-arch: build
build: configure-stamp build-stamp
build-stamp:
	dh_testdir
	$(MAKE)
	touch build-stamp

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp configure-stamp
	[ ! -f Makefile ] || $(MAKE) -C db-crypt distclean
	[ ! -f Makefile ] || $(MAKE) distclean
	dh_autoreconf_clean
	dh_clean

install: build
	dh_testdir
	dh_testroot
	dh_prep
	$(MAKE) install DESTDIR=$(CURDIR)/debian/lyskom-server

# Build architecture-independent files here.
binary-indep: build install
# We have nothing to do by default.

# Build architecture-dependent files here.
binary-arch: build install
	dh_testdir
	dh_testroot
	dh_installdirs
	dh_install db-crypt/db/lyskomd-* db-crypt/db/number.txt usr/share/lyskom-server/default/
	dh_installdebconf	
	dh_installdocs
	dh_installexamples
	dh_installmenu
	dh_installlogrotate
	dh_installcron
	dh_installman
	dh_systemd_enable
	dh_installinit
	dh_installchangelogs -k ChangeLog
	dh_link
	dh_strip
	dh_compress
	dh_fixperms
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build-indep build-arch build clean binary-indep binary-arch binary install configure
