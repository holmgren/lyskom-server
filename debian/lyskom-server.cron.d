# /etc/cron.d/lyskom-server: crontab entries for the lyskom-server package

# Not activated by default, to make sure server administrator logs in
# manually and changes administrator password first.

# This cron job should not be active when systemd is in use.

# To activate, please remove the comment signs from the following
# line.
# 0,10,20,30,40,50 * * * * root test -x /usr/sbin/updateLysKOM && su lyskom -c /usr/sbin/updateLysKOM

