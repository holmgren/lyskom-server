dnl $Id: configure.in,v 1.113 2003/10/03 08:40:23 ceder Exp $
dnl Configuration for LysKOM
dnl Copyright (C) 1993-2003  Lysator Academic Computer Association.
dnl
dnl This program is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 2, or (at your option)
dnl any later version.
dnl
dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License
dnl along with this program; if not, write to the Free Software
dnl Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
dnl
dnl Please report bugs at http://bugzilla.lysator.liu.se/. 
dnl
AC_REVISION($Revision: 1.113 $)
AC_PREREQ(2.57)
AC_INIT([lyskom-server], [2.1.2])
AC_CONFIG_SRCDIR([src/server/lyskomd.h])
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_AUX_DIR(scripts)
AM_INIT_AUTOMAKE

AC_ARG_WITH([debug-calls],
        AC_HELP_STRING([--with-debug-calls], 
	               [compile debug protocol requests]),
        [use_debug_calls=$withval],
        [use_debug_calls=no])

AC_ARG_WITH([efence],
        AC_HELP_STRING([--with-efence], [link with ElectricFence]),
        [use_efence=$withval],
        [use_efence=no])

AC_ARG_WITH([insure++],
        AC_HELP_STRING([--with-insure++], [compile with ParaSoft Insure++]),
        [use_insure=$withval],
        [use_insure=no])

AC_ARG_WITH([checker],
        AC_HELP_STRING([--with-checker], [compile with Gnu Checker]),
        [use_checker=$withval],
        [use_checker=no])

AC_ARG_WITH([valgrind],
	AC_HELP_STRING([--with-valgrind[=PATH]], 
	               [use valgrind when running tests]),
	[use_valgrind=$withval],
	[use_valgrind=no])

AC_ARG_ENABLE([malloc-guards],
	AC_HELP_STRING([--disable-malloc-guards], 
	               [disable defensive guard areas (may crash lyskomd)]),
	[use_malloc_guards=$enableval],
	[use_malloc_guards=yes])

dnl The IPv6 support is currently only handled in ISC.  The only reason
dnl to include this AC_ARG_ENABLE statement in this configure.in is to
dnl display the help text when "./configure --help" is run.
AC_ARG_ENABLE([ipv6],
        AC_HELP_STRING([--enable-ipv6], [enable IPv6 support]),
        [use_inet6=yes],
        [use_inet6=no])

AC_ARG_WITH([gcov],
        AC_HELP_STRING([--with-gcov], [instrument for gcov (requires gcc)]),
        [use_gcov=$withval],
        [use_gcov=no])

AC_ARG_WITH([traced-allocations],
        AC_HELP_STRING([--with-traced-allocations], 
	               [trace allocations (see src/server/ram-smalloc.c)]),
        [use_traced_allocations=$withval],
        [use_traced_allocations=no])

AC_ARG_WITH([optimization],
        AC_HELP_STRING([--with-optimization], [select level of optimization]),
        [opt_level=$withval],
        [opt_level=""])

AC_ARG_WITH([language],
	AC_HELP_STRING([--with-language=sv],
		       [select Swedish database (default English)]),
	[language=$withval],
	[language=en])

AC_SUBST(LANGUAGE_SUFFIX)
[case "$language" in
	en) LANGUAGE_SUFFIX=-en;;
	sv) LANGUAGE_SUFFIX=;;]
	*)  AC_MSG_ERROR(bad value $language for --with-language)[;;
esac]

AC_SUBST(VALGRIND)
[if test "$use_valgrind" = "yes"
then
	VALGRIND=valgrind
elif test "$use_valgrind" = "no"
then
	VALGRIND=
else
    case "$use_valgrind" in
      /*) VALGRIND="$use_valgrind";;
      *) ] AC_MSG_ERROR([invalid --with-valgrind argument $use_valgrind]) [ ;;
    esac
fi]

[if test "$use_malloc_guards" = "yes"
then]
	AC_DEFINE([USE_MALLOC_GUARDS], 1, 
		  [Insert magic number before and after malloced areas.
		   This can help detect buffer overruns.])
[fi]

AC_PREFIX_DEFAULT(/usr/lyskom)
AC_PATH_PROG([AR], [ar], [notfound], [$PATH$PATH_SEPARATOR/usr/ccs/bin])
AC_ARG_VAR([AR], [ar program to use])
[if test "$AR" = "notfound"; then]
    AC_MSG_ERROR([cannot find ``ar''])
[fi]
AC_PATH_PROG([SENDMAIL], [sendmail], [no], 
    [$PATH$PATH_SEPARATOR/usr/lib$PATH_SEPARATOR/usr/sbin$PATH_SEPARATOR/etc])
[if test "$SENDMAIL" = "no"; then
    SENDMAIL=:]
    AC_MSG_WARN([updateLysKOM will not send mail since no sendmail was found])
[fi]
AC_PROG_CC
AC_AIX
AC_ISC_POSIX
AC_MINIX
AM_PATH_PYTHON
AM_CONDITIONAL(HAVE_PYTHON, test -n "$PYTHON")

[if test -n "$GCC"; then]
	dnl "-Wshadow" could be useful, but it gives too many false
	dnl warnings.
	dnl "-Wtraditional" isn't really useful: we don't support
	dnl pre-c89-compilers.
	[CFLAGS="$CFLAGS -Wall -W"]
	dnl AC_DEFINE(_GNU_SOURCE)
	CMOD_CHECK_CC_OPT([-Wbad-function-cast], [bad_function_cast])
	dnl
	dnl This gives too many false warnings, but it may be proper
	dnl to use this once in a while.
	dnl CMOD_CHECK_CC_OPT([-Wcast-qual], [cast_qual])
	dnl
	CMOD_CHECK_CC_OPT([-Wcast-align], [cast_align])
	CMOD_CHECK_CC_OPT([-Wwrite-strings], [write_strings])
	CMOD_CHECK_CC_OPT([-Wstrict-prototypes], [strict_prototypes])
	CMOD_CHECK_CC_OPT([-Wmissing-prototypes], [missing_prototypes])
	CMOD_CHECK_CC_OPT([-Wmissing-declarations], [missing_declarations])
	CMOD_CHECK_CC_OPT([-Wfloat-equal], [float_equal])
	dnl
	dnl This gives too many false warnings, especially in dbck,
	dnl but it may be proper to use it once in a while.  dbck
	dnl should be fixed so that we get rid of the warnings there.
	dnl CMOD_CHECK_CC_OPT([-Wnested-externs], [nested_externs])
	dnl
	CMOD_CHECK_CC_OPT([-pipe], [pipe]) [
fi]

AC_SUBST(EFENCE)
[if test "$use_efence" = "yes"
then]
        AC_CHECK_LIB(efence,malloc)
        EFENCE=1
[fi]

AC_SUBST(TRACED_ALLOCATIONS)
[if test "$use_traced_allocations" = "yes"
then]
	AC_DEFINE([TRACED_ALLOCATIONS], 1,
	[Should all allocations be traced?  See src/server/ram-smalloc.c.])
[fi]

AC_SUBST(DEBUG_CALLS)
[if test "$use_debug_calls" = "yes"
then]
        AC_DEFINE([DEBUG_CALLS], 1, 
	          [Include special debug requests.  Don't use this on
		   a production server.  The debug requests are
		   insecure.  The test suite uses these calls if they
		   are available to increase the coverage.])
        AC_CHECK_FUNCS(mallinfo)
[fi]

[if test "$use_gcov" = "yes" && test -n "$GCC"; then]
        CMOD_CHECK_CC_OPT([-ftest-coverage], [test_coverage])
        CMOD_CHECK_CC_OPT([-fprofile-arcs], [profile_arcs])
        AC_DEFINE([AVOID_ABORTS], 1, 
	          [This should be defined if you want gcov
		  statistics. It replaces some aborts with exits so
		  coverage data is output.])
[fi]

[if test -n "$opt_level" -a "$opt_level" != "yes" ; then
    CFLAGS=`echo "$CFLAGS" | sed "s/-O[0-9]*//"`
    if test "$opt_level" != "no" ; then
        CFLAGS="$CFLAGS -O$opt_level"
    fi
fi]

# Do this test early since it may define _POSIX_SOURCE, which may
# affect future tests.
# FIXME (bug 210): is this needed, now that we use AM_C_PROTOTYPES?
AC_MSG_CHECKING([if <signal.h> defines struct sigaction])
AC_CACHE_VAL([kom_cv_header_posix_source_needed],
	[# This default may be overridden below.
	kom_cv_header_posix_source_needed=no])
AC_CACHE_VAL([kom_cv_struct_sigaction],
AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[#include <signal.h>]],
				   [[struct sigaction foosig;]])],
[kom_cv_struct_sigaction=yes],
[kom_cv_struct_sigaction=no])
[if test $kom_cv_struct_sigaction = no ; then]
  AC_COMPILE_IFELSE(
  [AC_LANG_PROGRAM([[#define _POSIX_SOURCE
                     #include <signal.h>]],
		   [[struct sigaction barsig;]])],
  [kom_cv_struct_sigaction=yes
  kom_cv_header_posix_source_needed=yes],
  [kom_cv_struct_sigaction=no])
[fi])
[if test $kom_cv_header_posix_source_needed = yes ; then]
  AC_MSG_RESULT([yes, but _POSIX_SOURCE was needed])
  AC_DEFINE(_POSIX_SOURCE)
[else]
  AC_MSG_RESULT($kom_cv_struct_sigaction)
[fi]
[if test $kom_cv_struct_sigaction = yes ; then]
  AC_DEFINE([HAVE_STRUCT_SIGACTION], 1, 
            [Define if struct sigaction is available.])
[fi]

AC_C_CONST
AC_C_VOLATILE
AC_C_INLINE
CMOD_C_WORKING_ATTRIBUTE_UNUSED
AC_HEADER_STDC
AC_TYPE_PID_T
dnl
dnl Check for sig_atomic_t
dnl
AC_CACHE_CHECK([if sig_atomic_t exists], kom_cv_type_sig_atomic_t,
[AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[#include <signal.h>]],
				    [[sig_atomic_t t;]])],
	[kom_cv_type_sig_atomic_t=yes], 
	[kom_cv_type_sig_atomic_t=no])])
[if test $kom_cv_type_sig_atomic_t = no
then]
    AC_DEFINE([sig_atomic_t], [int], 
              [Define this to a type that can be changed atomically
	      from a signal handler if your OS lacks sig_atomic_t.])
[fi]
dnl
# FIXME (bug 209): is this needed now that we use AM_C_PROTOTYPES?
AC_MSG_CHECKING([if _HPUX_SOURCE needs to be defined])
AC_CACHE_VAL([kom_cv_sys_hpux_source],
[AC_LINK_IFELSE([AC_LANG_PROGRAM([[#include <sys/types.h>
				   #include <sys/socket.h>]],
				 [[struct sockaddr foo;]])],
[kom_cv_sys_hpux_source=no],
[AC_LINK_IFELSE([AC_LANG_PROGRAM([[#define _HPUX_SOURCE
				   #include <sys/types.h>
				   #include <sys/socket.h>]],
				 [[struct sockaddr foo;]])],
[kom_cv_sys_hpux_source=yes],
[kom_cv_sys_hpux_source=no])])])
AC_MSG_RESULT($kom_cv_sys_hpux_source)
[if test $kom_cv_sys_hpux_source = yes ; then]
  AC_DEFINE([_HPUX_SOURCE], 1, [Define if on HPUX])

[fi]
dnl
dnl
AC_MSG_CHECKING([if the compiler understands __attribute__ ((format))])
AC_CACHE_VAL([kom_cv_c_attribute_format_printf],
[AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[extern void log (const char *f, ...) 
__attribute__ ((format (printf, 1, 2)));]], [[]])],
[kom_cv_c_attribute_format_printf=yes],
[kom_cv_c_attribute_format_printf=no])])
AC_MSG_RESULT($kom_cv_c_attribute_format_printf)
[if test $kom_cv_c_attribute_format_printf = yes ; then]
  AC_DEFINE([HAVE_ATTRIBUTE_FORMAT_PRINTF], 1, 
            [Define if your compiler supports __attribute__ in printf.])
[fi]
AC_MSG_CHECKING([if the compiler understands __attribute__ ((__noreturn__))])
AC_CACHE_VAL([kom_cv_c_attribute_noreturn],
[AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[extern void log (const char *f, ...) 
__attribute__ ((__noreturn__));]], [[]])],
[kom_cv_c_attribute_noreturn=yes],
[kom_cv_c_attribute_noreturn=no])])
AC_MSG_RESULT($kom_cv_c_attribute_noreturn)
[if test $kom_cv_c_attribute_noreturn = yes ; then]
  AC_DEFINE([HAVE_ATTRIBUTE_NORETURN], 1,
            [Define if your compiler supports __attribute__ ((noreturn)).])
[fi]
AC_PROG_INSTALL
AC_PROG_RANLIB
AC_PROG_AWK
AC_PROG_LN_S
AC_PROG_YACC
AM_PROG_LEX
AC_CHECK_PROGS(BISON, bison)
AC_CHECK_PROGS(FLEX, flex)
AC_CHECK_PROGS(SED, sed)
AC_CHECK_HEADERS(string.h memory.h strings.h sys/param.h sys/time.h)
AC_CHECK_HEADERS(stdarg.h stdlib.h stddef.h locale.h)
AC_CHECK_HEADERS(crypt.h unistd.h)
AC_CHECK_HEADERS(values.h) dnl libmisc/pom.c
AC_CHECK_HEADERS(stdint.h)
AC_CHECK_HEADERS(limits.h)
AC_CHECK_HEADERS(sys/mount.h)
AC_CHECK_HEADERS(sys/vfs.h)
AC_CHECK_HEADERS(sys/fs/s5param.h)
AC_CHECK_HEADERS(sys/filsys.h)
AC_CHECK_HEADERS(sys/statfs.h)
AC_CHECK_HEADERS(sys/statvfs.h)
AC_CHECK_HEADERS(sys/dustat.h)
AC_CHECK_HEADERS(fcntl.h)
jm_AC_TYPE_UINTMAX_T
jm_FILE_SYSTEM_USAGE([space=yes], [space=no])
AM_CONDITIONAL(HAVE_FSUSAGE, test $space = yes)
AC_HEADER_TIME
AC_FUNC_ALLOCA
dnl
AC_CHECK_HEADERS(sys/resource.h,,,
[#ifdef TIME_WITH_SYS_TIME
#  include <sys/time.h>
#  include <time.h>
#else
#  ifdef HAVE_SYS_TIME_H
#    include <sys/time.h>
#  else
#    include <time.h>
#  endif
#endif])
dnl
AC_CHECK_TYPE([rlim_t],
AC_DEFINE([HAVE_RLIM_T], [1], [Define if rlimt_t is available.]),,[
#ifdef HAVE_SYS_RESOURCE_H
#  ifdef TIME_WITH_SYS_TIME
#    include <sys/time.h>
#    include <time.h>
#  else
#    ifdef HAVE_SYS_TIME_H
#      include <sys/time.h>
#    else
#      include <time.h>
#    endif
#  endif
#  include <sys/resource.h>
#endif])
AC_CHECK_TYPE(intptr_t)
AC_CHECK_TYPE(intmax_t)
AC_CHECK_TYPE(size_t)
dnl
AC_CHECK_LIB(resolv,main) dnl Host name lookup.
AC_CHECK_LIB(authuser,auth_tcpuser3) dnl User authentication according to RFC 931.
dnl Low-level networking code on Solaris 2.
AC_CHECK_FUNC(gethostbyname)
[if test $ac_cv_func_gethostbyname = no
then]
      AC_CHECK_LIB(nsl, gethostbyname)
[fi]
dnl socket() et c on Solaris 2.
AC_CHECK_FUNC(socket)
[if test $ac_cv_func_socket = no
then]
  AC_CHECK_LIB(socket, socket)
[fi]
AC_CHECK_LIB(i,setlocale) dnl For setlocale() on Ultrix.
AC_CHECK_FUNC(crypt)
[if test $ac_cv_func_crypt = no
then]
  AC_CHECK_LIB(crypt,crypt) dnl For crypt() on Sparc NetBSD 1.1
[fi]
AC_CHECK_FUNCS(difftime getdtablesize sysconf strchr getcwd vfprintf)
AC_CHECK_FUNCS(setrlimit)
AC_CHECK_FUNCS(snprintf)
AC_REPLACE_FUNCS(memcpy strerror remove memset memchr memcmp setsid)
dnl
dnl On AIX 4.2, setrlimit(RLIMIT_NOFILE) doesn't work.  The
dnl RLIMIT_NOFILE limit is ignored, and getrlimit(RLIMIT_NOFILE)
dnl always returns 2147483647.  Not very helpful.
[if test $ac_cv_func_setrlimit = yes
then]
    AC_CACHE_CHECK([for working setrlimit(RLIMIT_NOFILE, ...)], 
        kom_cv_func_rlimit_nofile,
	AC_RUN_IFELSE([AC_LANG_SOURCE([[
#include <stdio.h>
#ifdef HAVE_SYS_RESOURCE_H
#  ifdef TIME_WITH_SYS_TIME
#    include <sys/time.h>
#    include <time.h>
#  else
#    ifdef HAVE_SYS_TIME_H
#      include <sys/time.h>
#    else
#      include <time.h>
#    endif
#  endif
#  include <sys/resource.h>
#else
#  include <time.h>
#endif
#include <fcntl.h>

#if defined(HAVE_SETRLIMIT) && defined(RLIMIT_OFILE) && !defined(RLIMIT_NOFILE)
#  define RLIMIT_NOFILE RLIMIT_OFILE
#endif

#if !HAVE_RLIM_T
typedef int rlim_t;
#endif

int main()
{
#if defined(HAVE_SETRLIMIT) && defined(RLIMIT_NOFILE)
    struct rlimit rlim;
    int i;
    int fd;

    if (getrlimit(RLIMIT_NOFILE, &rlim) < 0)
    {
	perror("getrlimit(RLIMIT_NOFILE) failed");
	return 1;
    }
    rlim.rlim_cur = 20;
    if (setrlimit(RLIMIT_NOFILE, &rlim) < 0)
    {
	perror("setrlimit(RLIMIT_NOFILE) failed");
	return 1;
    }
    if (getrlimit(RLIMIT_NOFILE, &rlim) < 0)
    {
	perror("second getrlimit(RLIMIT_NOFILE) failed");
	return 1;
    }
    if (rlim.rlim_cur != 20)
    {
        fprintf(stderr, "NOFILE set to %ld, not 20\n", (long)rlim.rlim_cur);
	return 1;
    }
    for (i = 0; i < 25; i++)
        if ((fd = open("/dev/null", O_WRONLY)) > 19)
	{
	    fprintf(stderr, "NOFILE doesn't limit files; got fd %d\n", fd);
	    return 1;
        }
    return 0;
#else
    fprintf(stderr, "setrlimit not available\n");
    return 1;
#endif
}]])],
        [kom_cv_func_rlimit_nofile=yes],
        [kom_cv_func_rlimit_nofile=no],
        [kom_cv_func_rlimit_nofile=no]))
    [if test $kom_cv_func_rlimit_nofile = no
    then]
        AC_DEFINE([HAVE_BROKEN_NOFILE], [1], 
            [Define if setrlimit(RLIMIT_NOFILE) does not work.])
    [fi
fi]
dnl
dnl strdup() is not currently used, but we have a replacement function
dnl since it was used a while ago.  No need to take up time looking
dnl for it, though.  Remove strdup.c from EXTRA_DIST in
dnl src/libraries/libansi/Makefile.am if strdup is ever used again.
dnl AC_REPLACE_FUNCS(strdup)
dnl
dnl I don't know how to test for buggy inet_ntoa functions
dnl programmatically, but the supplied substitute should always work,
dnl so we define BUGGY_INET_NTOA unconditionally.
AC_DEFINE([BUGGY_INET_NTOA], 1, 
          [Define if your OS has buggy inet_ntoa. If you don't know,
	   define this.])
dnl
dnl Unencrypted passwords are no longer supported.
AC_DEFINE([ENCRYPT_PASSWORDS], 1, [Define if you want encrypted passwords])
dnl
dnl Tests for GNU getopt (used in dbck)
AC_CHECK_HEADERS(alloca.h)
dnl
dnl Check for use of Gnu checker
dnl

[if test "$use_insure" = "yes"
then]
        AC_CHECK_PROGS(INSURE, insure)
        [ CC="insure" 
          LDFLAGS="-Zsl $LDFLAGS" ]
[fi]

[if test "$use_checker" = "yes"
then]
        AC_CHECK_PROGS(CHECKER, checker)
        [ CC="checker $CC"
          LIBS="-lchkr_m $LIBS" ]
[fi]

AC_CONFIG_SUBDIRS(src/libraries/libisc-new)
AC_CONFIG_SUBDIRS(src/libraries/liboop)
AC_CONFIG_SUBDIRS(src/libraries/adns)
AC_CONFIG_FILES([Makefile
db-crypt/Makefile
db-crypt/db/Makefile
doc/Makefile
doc/man/Makefile
scripts/Makefile
src/Makefile
src/include/Makefile
src/include/server/Makefile
src/libraries/Makefile
src/libraries/libansi/Makefile
src/libraries/libcommon/Makefile
src/libraries/libeintr/Makefile
src/libraries/libmisc/Makefile
src/libraries/regex/Makefile
src/libraries/regex/doc/Makefile
src/libraries/regex/test/Makefile
src/server/Makefile
src/server/testsuite/Makefile
src/server/testsuite/config/Makefile
src/server/testsuite/lyskomd.0/Makefile
m4/Makefile
run-support/Makefile])
AC_CONFIG_COMMANDS([default],[[echo
echo "  Selected language: $language"
echo]],[[language=$language]])
AC_OUTPUT
