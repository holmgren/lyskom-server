# Generate wrapper functions that check for errno==EINTR.
# Copyright (C) 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 
{
    incl=$1
    rettype=$2
    errval=$3
    fnc=$4
    fn=fnc ".c"
    print "#include <errno.h>" > fn
    printf "#include <%s>\n\n", incl >> fn
    printf "#include \"eintr.h\"\n\n" >> fn
    print rettype >> fn
    printf "i_%s(", fnc >> fn
    for (i = 5; i <= NF; i++) {
	printf "%s %c", $i, 60+i >> fn
	if (i <= NF - 1) printf ", " >> fn
    }
    print ")" >> fn
    print "{" >> fn
    printf "    %s ret;\n\n", rettype >> fn
    print "    do {" >> fn
    print "\terrno = 0;" >> fn
    printf "\tret = %s(", fnc >> fn
    for (i = 5; i <= NF; i++) {
	printf "%c", 60+i >> fn
	if (i <= NF - 1) printf ", " >> fn
    }
    print ");" >> fn
    printf "    } while (ret == %s && errno == EINTR);\n\n", errval >> fn
    print "    return ret;" >> fn
    print "}" >> fn

    printf "%s i_%s(", rettype, fnc >> "eintr.h"
    for (i = 5; i <= NF; i++) {
	printf "%s", $i >> "eintr.h"
	if (i <= NF - 1) printf ", " >> "eintr.h"
    }
    print ");" >> "eintr.h"
}

