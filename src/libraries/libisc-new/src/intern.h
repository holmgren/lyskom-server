/*
** intern.h                Definitions and prototypes used internally
**
** Copyright (C) 1991,1999 by Peter Eriksson and Per Cederqvist of the
**                         Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 910305 pen      initial coding
** (See ChangeLog for recent history)
*/

#ifndef __ISC_INTERNALS_H__
#define __ISC_INTERNALS_H__

/*
** Some nice defaults
*/
#define ISC_DEFAULT_MAX_MSG_SIZE      8176
#define ISC_DEFAULT_MAX_QUEUED_SIZE   50
#define ISC_DEFAULT_MAX_DEQUEUE_LEN   30
#define ISC_DEFAULT_MAX_OPEN_RETRIES  10
#define ISC_DEFAULT_MAX_BACKLOG       50

/*
** The Master Control Block
*/
struct isc_mcb
{
  struct isc_session_cfg  * scfg;
  struct isc_scb_entry    * sessions;
  oop_source              * event_source;
  oop_adapter_adns        * adns;
  isc_write_queue_change_cb *write_change_cb;
};


/*
** The Client Control Block
*/
struct isc_scb_internal
{
  struct isc_scb pub;

  enum isc_session_state state;

  struct isc_msgqueue *wr_msg_q;

  struct isc_session_cfg * cfg;
  
  /* Cheap transmit buffer */
  /* Should be dynamically allocated - into an struct isc_msg */
  char                sendbuf[ISC_DEFAULT_MAX_MSG_SIZE];
  int                 sendindex;
  
  isc_accept_callback *accept_cb;
  isc_write_error_cb  *write_err_cb;
  isc_stale_output_cb *stale_output_cb;
  isc_stale_output_cb *idle_cb;

  oop_call_fd         *data_available_callback;
  int                  data_available_registered;

  int                  write_cb_registered;

  int                  stale_output_cb_registered;
  struct timeval       stale_output_tv;

  int                  idle_cb_registered;
  struct timeval       idle_tv;
  struct timeval       acceptable_idle_time;

  isc_resolve_done_cb  *resolve_callback; /* Non-NULL when lookup pending. */
  oop_adns_query       *adns_query;
};

/*
** Generic message type
*/
struct isc_msg
{
  int               size;      	/* Maximum buffer size */
  int               length;    	/* Length of used buffer */
  char            * buffer;  	/* Pointer to buffer */
};

/*
** Entry in a message queue
*/
struct isc_msg_q_entry
{
  struct isc_msg_q_entry    * prev;
  struct isc_msg_q_entry    * next;
  struct isc_msg * msg;
};



/*
** The generic message queue
*/
struct isc_msgqueue
{
  struct isc_msg_q_entry  * head;
  struct isc_msg_q_entry  * tail;
  int         entries;
  int         bytes;
};

/*
** Free an allocated struct isc_msg
*/
extern void
isc_freemsg(struct isc_msg  * msg);

/*
** Configuration structure for sessions
*/
struct isc_session_cfg
{
  struct
  {
    int msgsize;		/* Size of a single message. */
    int queuedsize;		/* Number of messages. */
    int queuedsize_bytes;	/* Total number of bytes. */
    int dequeuelen;		/* Number of messages to write at once. */
    int openretries;
    int backlog;
  } max;
  int fd_relocate;
  struct timeval stale_timeout;
  struct timeval idle_timeout;
};

/*
** Session list entry
*/
struct isc_scb_entry
{
  struct isc_scb_entry  * prev;
  struct isc_scb_entry  * next;
  
  struct isc_scb_internal * scb;
};

/* isc_abort.c */
extern void
isc_abort(const char *message);


/* isc_alloc.c */
extern void *
isc_malloc(size_t length);

extern void *
isc_realloc(void *buf, size_t length);

extern void
isc_free(void *buf);

/* isc_queue.c */
extern struct isc_msgqueue *
isc_newqueue(void);

/* Return TRUE if the queue is not empty. */
#define isc_pollqueue(qp)	((qp) != NULL && ((qp)->head != NULL))

extern int
isc_killqueue(struct isc_msgqueue *queue);

extern void
isc_pushqueue(struct isc_msgqueue *queue, struct isc_msg *msg);

extern struct isc_msg *
isc_topqueue(struct isc_msgqueue *queue);

extern int
isc_sizequeue(struct isc_msgqueue *queue);

extern struct isc_msg *
isc_popqueue(struct isc_msgqueue *queue);


extern void
isc_oflush(struct isc_scb_internal  * scb);


/* isc_session.c */
extern struct isc_scb_internal *
isc_create(struct isc_mcb *mcb,
	   struct isc_session_cfg *cfg,
	   enum isc_session_state initial_state);

extern void
isc_insert(struct isc_mcb *mcb, struct isc_scb_internal *scb);

extern int
isc_remove(struct isc_mcb *mcb, struct isc_scb_internal *scb);

/* isc_socket.c */
extern union isc_address *
isc_copyaddress(union isc_address *addr);

extern oop_call_time isc_dns_resolve_cb;

/* isc_tcp.c */
extern struct isc_scb_internal *
isc_createtcp(struct isc_mcb *mcb,
	      struct isc_session_cfg *cfg,
	      int fd,
	      enum isc_session_state initial_state);

extern int
isc_bindtcp(struct isc_scb_internal *scb,
	    const char *address, const char *port);

/* isc_message.c. */
/*
** Allocate a new struct isc_msg of specified size
*/
extern struct isc_msg *
isc_allocmsg(size_t  size);


#if 0
/*
** Setup a function to handle fatal abort requests
*/
extern void
isc_setabortfn(void (*abortfn)(const char  * msg));
#endif


/*
 * Move a file descriptor FD the first unused file descriptor higher
 * than or equal to LIMIT.  Return the new file descriptor.  Close FD.
 *
 * On failure, the old FD will be returned, and errno will be set.
 *
 * Do nothing (and return FD) if LIMIT is 0.
 */
extern int
isc_relocate_fd(int fd, int limit);

extern void
isc_check_read_callback(struct isc_scb_internal *session, int any_written);

extern void
isc_cancel_read_callback(struct isc_scb_internal *session);

extern void
isc_cancel_write_callback(struct isc_scb_internal *session);

#define ISC_XNEW(var)   (var = isc_malloc(sizeof(*var)))
	
#if !HAVE_INET_ATON
struct in_addr;
int inet_aton (const char *, struct in_addr *);
#endif

#if !HAVE_SOCKLEN_T
/* The configure script attempts to find the appropriate socklen_t
   type to use.  This can be size_t, unsigned long or unsigned int. */
typedef SOCKLEN_TYPE socklen_t;
#endif

#if !HAVE_UINT32_T
#  if SIZEOF_INT == 4

typedef unsigned int uint32_t;

#  elif SIZEOF_LONG == 4

typedef unsigned long uint32_t;

#  else
#    error Cannot find a 32-bit type on this platform
#  endif
#endif


/* IPv4/IPv6 compatibility macros:
 *
 * These are used as wrappers around IPv4/IPv6 specific socket
 * handling code. When USE_INET6 is not defined, IPv6 specific
 * parts will not be compiled.
 *
 * CHOOSE_IP4OR6:    Takes 3 args:
 *                     evaluates to second arg if first arg (of type
 *                     enum sockaddrs) contains an AF_INET address or
 *                     IPv6 is not used, otherwise to third arg.
 * FOR_EACH_AF:      Takes 2 args; first is an int lvalue, second
 *                     an int expression. For each available address
 *                     family (from most preferred to last preferred),
 *                     the second arg is evaluated with the address
 *                     family assigned to the first arg, until it
 *                     evaluates to false (in which case the entire
 *                     macro expansion evaluates to false), or until
 *                     there are no more address families, in which
 *                     case the entire macro expansion evaluates to
 *                     true.
 * FOR_EACH_PF:      As FOR_EACH_AF, but for protocol families
 *                     instead of address families.
 * STORE_ADDR:         First arg (enum sockaddrs) is checked for
 *                     address family (if several address families
 *                     are supported), and its address part
 *                     assigned the value of the second arg
 *                     (of type in_addr) for IPv4, otherwise
 *                     the third arg (of type in6_addr).
 */

#ifdef USE_INET6

#define CHOOSE_IP4OR6(saddrs, ifip4, ifip6) \
 ((((saddrs).sa.sa_family) == AF_INET) ? (ifip4) : (ifip6))
#define FOR_EACH_AF(af,expr) \
 (((af)=AF_INET6, (expr)) && ((af)=AF_INET, (expr)))
#define FOR_EACH_PF(pf,expr) \
 (((pf)=PF_INET6, (expr)) && ((pf)=PF_INET, (expr)))
#define STORE_ADDR(addr, IPv4addr, IPv6addr) \
 CHOOSE_IP4OR6((addr), \
   (addr).sa_in.sin_addr.s_addr = (IPv4addr), \
   (memcpy(&(addr).sa_in6.sin6_addr, \
	   &(IPv6addr), \
	   sizeof((addr).sa_in6.sin6_addr)), 0))

#else

#define CHOOSE_IP4OR6(saddrs, ifip4, ifip6) (ifip4)
#define FOR_EACH_AF(af,expr) ((af)=AF_INET, (expr))
#define FOR_EACH_PF(pf,expr) ((pf)=PF_INET, (expr))
#define STORE_ADDR(addr, IPv4addr, IPv6addr) \
 ((addr).sa_in.sin_addr.s_addr = (IPv4addr));

#endif /* USE_INET6 */

#endif
