/*
** isc_master.c                               IscMaster control functions
**
** Copyright (C) 1991-1992, 1998-1999 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 910305 pen      moved to separate file
** (See ChangeLog for recent history)
*/

#include <time.h>
#include <errno.h>
#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#endif
#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#include <sys/types.h>
#include <sys/file.h>
#include <sys/socket.h>
#ifndef NULL
#  include <stdio.h>
#endif

#include "oop.h"
#include "adns.h"
#include "oop-adns.h"

#include "s-string.h"
#include "timeval-util.h"

#include "isc.h"
#include "intern.h"



/*
** This routine will initialize the ISC subsystem
*/
struct isc_mcb *
isc_initialize(oop_source *event_source,
	       isc_write_queue_change_cb *write_change_cb)
{
  struct isc_mcb    * mcb;
  oop_adapter_adns  * adns;

  adns = oop_adns_new(event_source,
		      0
		      |adns_if_logpid
		      |adns_if_checkc_entex
		      |adns_if_nosigpipe,
		      stderr);
  if (adns == NULL)
      return NULL;

  ISC_XNEW(mcb);
  ISC_XNEW(mcb->scfg);
  mcb->sessions = NULL;
  mcb->adns = adns;
  
  /* Setup default values */
  mcb->scfg->max.msgsize     = ISC_DEFAULT_MAX_MSG_SIZE;
  mcb->scfg->max.queuedsize  = ISC_DEFAULT_MAX_QUEUED_SIZE;
  mcb->scfg->max.dequeuelen  = ISC_DEFAULT_MAX_DEQUEUE_LEN;
  mcb->scfg->max.openretries = ISC_DEFAULT_MAX_OPEN_RETRIES;
  mcb->scfg->max.backlog     = ISC_DEFAULT_MAX_BACKLOG;
  mcb->scfg->fd_relocate     = 0;
  mcb->scfg->stale_timeout   = timeval_ctor(3600, 0);

  /* Default to no byte limit. */
  mcb->scfg->max.queuedsize_bytes = (ISC_DEFAULT_MAX_QUEUED_SIZE
				     * ISC_DEFAULT_MAX_MSG_SIZE);

  mcb->event_source = event_source;
  mcb->write_change_cb = write_change_cb;

  return mcb;
}


void
isc_cfg_fd_relocate(struct isc_mcb *mcb,
		    int fd_relocate)
{
  mcb->scfg->fd_relocate = fd_relocate;
}
  

void
isc_cfg_stale_timeout(struct isc_mcb *mcb,
		      struct timeval stale,
		      struct timeval default_idle)
{
  mcb->scfg->stale_timeout = stale;
  mcb->scfg->idle_timeout = default_idle;
}

void
isc_cfg_queue_size(struct isc_mcb *mcb,
		   int max_queue_size_bytes,
		   int max_msg_size,
		   int max_queue_size_msgs,
		   int max_dequeue_msgs)
{
  mcb->scfg->max.msgsize = max_msg_size;
  mcb->scfg->max.queuedsize = max_queue_size_msgs;
  mcb->scfg->max.queuedsize_bytes = max_queue_size_bytes;
  mcb->scfg->max.dequeuelen = max_dequeue_msgs;
}


/*
** Close and destroy all sessions associated with a MCB, then
** destroy the MCB too.
*/
void
isc_shutdown(struct isc_mcb  * mcb)
{
  while (mcb->sessions)
    isc_destroy(mcb, &mcb->sessions->scb->pub);

  oop_adns_delete(mcb->adns);
  isc_free(mcb->scfg);
  isc_free(mcb);
}
