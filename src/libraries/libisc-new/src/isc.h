/*
** isc.h                        structures and defines used in a ISC server
**
** Copyright (C) 1991, 1996, 1998-1999, 2001 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 910306 pen      major overhaul
** 910307 pen      type name changes, changes in structs..
** 920207 pen      updated
** (See ChangeLog for recent history)
*/

#ifndef __ISC_H__
#define __ISC_H__

/*
** Give the poor user a chance to change it
*/
#ifndef ISC_UDGTYPE
#  define ISC_UDGTYPE void
#endif

/* Forward declarations. */
struct isc_mcb;
struct isc_scb;

/*
 * Callback function types.
 */

/* When a new TCP client conntects.  */
typedef void *isc_accept_callback(struct isc_scb *cb_accepting_session,
				  struct isc_scb *cb_new_session);

typedef void isc_write_error_cb(struct isc_scb *cb_session,
				int saved_errno);

typedef void isc_stale_output_cb(struct isc_scb *cb_session);

typedef void isc_write_queue_change_cb(int delta_bytes);

enum isc_resolve_status
{
    isc_resolve_ok,		/* Name successfully looked up. */
    isc_resolve_h_errno,	/* Failed.  h_errno value passed in errcode. */
    isc_resolve_adns_error,	/* Failed.  status value passed in errcode. */
    isc_resolve_aborted,	/* Session is being killed and no reply yet. */
};

/* The errcode is valid when res indicates so. */
typedef void *isc_resolve_done_cb(struct isc_scb *scb,
				  enum isc_resolve_status res,
				  long errcode);


/*
** The different session states
*/
enum isc_session_state
{
  ISC_STATE_RUNNING,
  ISC_STATE_DISABLED,
  ISC_STATE_CLOSING,
  ISC_STATE_LISTENING,
};


/*
** Session control structure
*/
union isc_address;
struct isc_scb
{
  int                 fd;
  union isc_address *raddr;
  union isc_address *laddr;
  String             remote;	/* See isc_resolve_remote(). */
  
  ISC_UDGTYPE       * udg;   /* User defined garbage :-) */

  struct isc_mcb      *master;
};




/*
** Setup a set of functions to handle memory allocation
*/
extern void
isc_setallocfn(void  * (*mallocfn)(size_t size),
               void  * (*reallocfn)(void  * buf, size_t size),
	       void    (*freefn)(void  * buf));

/*
** This routine will setup the ISC subsystem
*/
extern struct isc_mcb *
isc_initialize(oop_source *,
	       isc_write_queue_change_cb *);

extern void
isc_cfg_fd_relocate(struct isc_mcb *, int fd_relocate);

/*
** Configure timeouts for when a client will be disconnected.  If the
** output buffer is full and nothing can be written to the cilent for
** a period longer than STALE the client will be disconnected.  If
** nothing is written to it for a period longer than DEFAULT_IDLE, it
** will be disconnected.  DEFAULT_IDLE must always be larger than
** STALE.
**
** The client is actually not disconnected by ISC.  Instead, one of
** the callback functions stale_output_cb and idle_cb, registered with
** isc_set_read_callback(), will be called when the condition occurs.
** It should make sure that the client is closed (but it need not do
** so right away).
*/
extern void
isc_cfg_stale_timeout(struct isc_mcb *,
		      struct timeval stale,
		      struct timeval default_idle);

extern void
isc_cfg_queue_size(struct isc_mcb *,
		   int max_queue_size_bytes,
		   int max_msg_size,
		   int max_queue_size_msgs,
		   int max_dequeue_msgs);


/*
** Shut down all sessions associated with an ISC Master Control Block
** and deallocate all storage used by the MCB.
*/
extern void
isc_shutdown(struct isc_mcb  * mcb);



/*
** Establish a TCP port to listen for connections at
*/

extern struct isc_scb *
isc_listentcp(struct isc_mcb   * mcb,
	      const char  * address,
	      const char  * service,
	      isc_accept_callback *cb);

/*
** The callback function supplied to isc_listentcp can call this
** function to install a callback that will be called when data is
** available on the new socket.
*/
extern void
isc_set_read_callback(struct isc_scb *session,
		      oop_call_fd *data_available_cb,
		      isc_write_error_cb *write_error_cb,
		      isc_stale_output_cb *stale_output_cb,
		      isc_stale_output_cb *idle_cb);

/*
** Dynamically change the acceptable idle timeout of a session.  This
** can be used to increase the timeout once the initial handshake is
** complete, or once the user has logged on.  (It could even be used
** to lower it if a certain person logs on... -- I'm talking about
** robots whose sessions should be short-lived, of course.)
*/
extern void
isc_set_acceptable_idle(struct isc_scb *session,
			struct timeval acceptable);

extern int
isc_destroy(struct isc_mcb  * mcb, struct isc_scb  * scb);

/*
** Read data from a session into the end of a string.  The first
** UNUSED bytes of the string are unused, and may be reused.
*/
enum isc_read_result {
    ISC_READ_DATA,
    ISC_READ_LOGOUT,
    ISC_READ_WOULDBLOCK,
    ISC_READ_ERROR,		/* read() failed unexpectedly -- see errno. */
    ISC_READ_NOMEM,		/* out of memory. */
};

extern enum isc_read_result
isc_read_data(struct isc_scb *scb,
	      String *result,
	      String_size *unused);


 
/*
** Flush the transmit queue for a specific session.
** Unless this function is called, the output might remain in the
** output buffer forever.
*/
extern void
isc_flush(struct isc_scb  * scb);

/*
** Put a buffer on the transmit queue for a session
*/
extern int
isc_write(struct isc_scb *scb,
	  const void  * buffer,
	  size_t        length);



/*
** Put a single character on the transmit queue
*/
extern int
isc_putc(int           chr,
	 struct isc_scb *scb);

/*
** Put a NUL-terminated string on the transmit queue (the NUL is not sent).
*/
extern int
isc_puts(const char *str,
	 struct isc_scb *scb);

/*
** Put a decimal representation of ``nr'' on the transmit queue.
*/
extern int isc_putul(unsigned long nr, struct isc_scb *scb);

/*
** Enable a previously disabled session
*/
extern int
isc_enable(struct isc_scb *scb);

/*
** Disable a session. The session will not generate read events, but
** any enqueued output will still be sent to the remote host.
*/
extern int
isc_disable(struct isc_scb *scb);

union isc_address *
isc_mktcpaddress(const char *address,
		 const char *service);

int
isc_addressfamily(union isc_address *);

int
isc_addresssize(union isc_address *);

struct sockaddr *
isc_addresspointer(union isc_address *ia);

extern void
isc_freeaddress(union isc_address *addr);

extern char *
isc_getipnum(union isc_address *ia,
	     char *address,
	     int len);


/* Start to make a reverse DNS lookup on the remote host.  Once the
   lookup completes, store the result in scb->remote and call the
   callback.  If the lookup fails, a printable representation of the
   IP address will be stored in scb->remote and the callback will be
   called with res set to something other than isc_resolve_ok.

   Returns 0 on success, or an error code defined by adns on error. */
extern int
isc_resolve_remote(struct isc_scb *scb,
		   isc_resolve_done_cb *callback);

extern int
isc_getportnum(union isc_address *ia);

extern oop_source *
isc_getoopsource(struct isc_scb *scb);

#endif /* __ISC_H__ */
