/*
** isc_tcp.c                          Routines to handle TCP ISC sessions
**
** Copyright (C) 1992, 1998-1999, 2001 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 920209 pen      code extracted from isc_session.c
** (See ChangeLog for recent history)
*/

#include <errno.h>
#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#endif
#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#include <ctype.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netdb.h>
#ifdef HAVE_STRING_H
#  include <string.h>
#endif
#include <fcntl.h>
#ifndef NULL
#  include <stdio.h>
#endif
#ifdef HAVE_UNISTD_H
#  include <unistd.h>
#endif
#include <time.h>
#include <assert.h>

#include "oop.h"
#include "adns.h"
#include "oop-adns.h"

#include "s-string.h"

#include "isc.h"
#include "intern.h"
#include "isc_addr.h"
#include "unused.h"

static oop_call_fd isc_accept_cb;

static struct isc_scb_internal *
isc_tcp_accept(struct isc_scb_internal *scb)
{
  struct isc_scb_internal *new_scb;
  int fd;
  SOCKADDR_STORAGE addr;
  socklen_t len;
  

  len = sizeof(addr);
  if ((fd = accept(scb->pub.fd,
		   (struct sockaddr *)&addr,
		   &len)) < 0)
  {
    /* FIXME (bug 106): Log a message. */
    return NULL;
  }
  
  new_scb = isc_createtcp(scb->pub.master, scb->cfg, fd, ISC_STATE_RUNNING);
  if (!new_scb)
    return NULL;
  
  /* Fill in the session info structure */
  new_scb->pub.raddr = isc_mkipaddress(&addr);
  new_scb->pub.laddr = isc_copyaddress(scb->pub.laddr);
  
  return new_scb;
}



/*
** Create a TCP Session Address
*/
static union isc_address *
isc_mktcpaddress_internal(const char *address,
			  const char *service,
			  int address_family)
{
  union sockaddrs addr;
  struct hostent *hp = NULL;
  struct servent *sp;
#ifdef USE_GETIPNODEBYNAME
  int error_num;
#endif


  memset(&addr, 0, sizeof(addr));
  
  /* Any local address? */
  if (address == NULL)
  {
    addr.sa.sa_family = address_family;
    STORE_ADDR(addr, htonl(INADDR_ANY), in6addr_any);
  }
  else if (isdigit((int)(unsigned char)address[0])
	   || (address[0] == ':'))
  {
#ifdef HAVE_INET_PTON
    if (FOR_EACH_AF(addr.sa.sa_family,
		    (address_family != addr.sa.sa_family)
		    || (inet_pton(address_family,
				  address,
				  CHOOSE_IP4OR6(addr,
						(void *) &addr.sa_in.sin_addr,
						(void *) &addr.sa_in6.sin6_addr)
				  ) <= 0)))
      return NULL;
#else
    /* No inet_pton; we have to do with inet_aton which
       only handles AF_INET addresses */
    if (address_family != AF_INET)
      return NULL;
    addr.sa.sa_family = AF_INET;
    if (!inet_aton(address, &addr.sa_in.sin_addr))
      return NULL;
#endif
  }
#ifdef USE_GETIPNODEBYNAME
  else if ((hp = getipnodebyname(address,
				 address_family,
				 /* Use AI_ADDRCONFIG and not
				    AI_DEFAULT, since this address is
				    only used for local addresses of
				    this host. */
				 AI_ADDRCONFIG,
				 &error_num)) == NULL)
    return NULL;
#elif defined(HAVE_GETHOSTBYNAME2)
  else if ((hp = gethostbyname2(address, address_family)) == NULL)
    return NULL;
#else
  else if ((hp = gethostbyname(address)) == NULL)
    return NULL;
#endif
  else 
  {
    addr.sa.sa_family = hp->h_addrtype;
    memcpy(CHOOSE_IP4OR6(addr,
			 (void *) &addr.sa_in.sin_addr,
			 (void *) &addr.sa_in6.sin6_addr),
	   hp->h_addr,
	   hp->h_length);
  }
  
#ifdef USE_GETIPNODEBYNAME
  if (hp != NULL)
    freehostent(hp);
#endif

  if (addr.sa.sa_family != address_family)
    return NULL;
    
  if (isdigit((int)(unsigned char)service[0]))
    CHOOSE_IP4OR6(addr,
		  addr.sa_in.sin_port = htons(atoi(service)),
		  addr.sa_in6.sin6_port = htons(atoi(service)));
  else if ((sp = getservbyname(service, "tcp")) == NULL)
    return NULL;
  else
    CHOOSE_IP4OR6(addr,
		  addr.sa_in.sin_port = sp->s_port,
		  addr.sa_in6.sin6_port = sp->s_port);
  
  return isc_mkipaddress(&addr.ssa);
}

union isc_address *
isc_mktcpaddress(const char *address,
		 const char *service)
{
  union isc_address *ia;
  int af;

  if (FOR_EACH_AF(
	  af,
	  (ia = isc_mktcpaddress_internal(address, service, af)) == NULL))
      return NULL;

  return ia;
}


int
isc_addresssize(union isc_address *UNUSED_UNLESS_INET6(ia))
{
    return CHOOSE_IP4OR6(ia->saddr,
			 sizeof(struct sockaddr_in),
			 sizeof(struct sockaddr_in6));
}

int
isc_addressfamily(union isc_address *ia)
{
    return ia->saddr.sa.sa_family;
}


struct sockaddr *
isc_addresspointer(union isc_address *ia)
{
    return (struct sockaddr *) &ia->saddr;
}

		  
/*
** Create a TCP session.
** Will close fd and return NULL if an error occurs.
*/
struct isc_scb_internal *
isc_createtcp(struct isc_mcb *mcb,
	      struct isc_session_cfg *cfg,
	      int fd,
	      enum isc_session_state initial_state)
{
  struct isc_scb_internal *scb;
  int res;
  int flag;
  struct linger ling;
  

  if (fd == -1)
  {
    int protocol_family;
    if (FOR_EACH_PF(protocol_family,
		    (fd = socket(protocol_family, SOCK_STREAM, 0)) < 0))
    {
      /* FIXME (bug 106): Log a message. */
      return NULL;
    }
  }

  fd = isc_relocate_fd(fd, cfg->fd_relocate);
  
  /* Set non blocking write mode */
  if ((res = fcntl(fd, F_GETFL, 0)) == -1)
  {
    /* FIXME (bug 106): Log a message. */
    close(fd);
    return NULL;
  }
    
  /* If compilation fails on the next line, please report it as a bug
     to ceder@lysator.liu.se.  I'd like to talk to you so that you can
     test an autoconf solution to this problem.  As a workaround, you
     can change "O_NONBLOCK" to "FNDELAY". */
  if (fcntl(fd, F_SETFL, res | O_NONBLOCK) == -1)
  {
    /* FIXME (bug 106): Log a message. */
    close(fd);
    return NULL;
  }

  /* This is the modern way to turn off linger and turn on reuseaddr. */
  ling.l_onoff = 0;
  ling.l_linger = 0;
  setsockopt(fd, SOL_SOCKET, SO_LINGER, &ling, sizeof(ling)); 
  flag = 1;
  setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag));
  
  scb = isc_create(mcb, cfg, initial_state);
  if (!scb)
  {
    /* FIXME (bug 106): Log a message. */
    close(fd);
    return NULL;
  }

  scb->pub.fd = fd;
  
  scb->pub.raddr = NULL;
  scb->pub.laddr = NULL;
    
  return scb;
}



/*
** Bind a TCP session to a local port and address
*/
int
isc_bindtcp(struct isc_scb_internal *scb,
	    const char *address,
	    const char *service)
{
  union isc_address *ia;
  int af;


  if (FOR_EACH_AF(af,
		  ! (ia = isc_mktcpaddress_internal(address, service, af))
		  || (bind(scb->pub.fd,
			   isc_addresspointer(ia),
			   isc_addresssize(ia)) < 0)))
    return -1;

  scb->pub.laddr = ia;
  assert(scb->pub.raddr == NULL);
    
  return 0;
}


static void *
isc_accept_cb(oop_source *UNUSED(source),
	      int fd,
	      oop_event event,
	      void *user)
{
  struct isc_scb_internal *scb = (struct isc_scb_internal*)user;
  struct isc_scb_internal *new_scb;

  assert(event == OOP_READ);
  assert(scb->state == ISC_STATE_LISTENING);
  assert(scb->pub.fd == fd);

  new_scb = isc_tcp_accept(scb);
  if (new_scb == NULL)
    return OOP_CONTINUE;

  isc_insert(scb->pub.master, new_scb);
  return scb->accept_cb(&scb->pub, &new_scb->pub);
}


/*
** Establish a port to listen at for new TCP connections
*/
struct isc_scb *
isc_listentcp(struct isc_mcb *mcb,
	      const char *address,
	      const char *service,
	      isc_accept_callback *cb)
{
  struct isc_scb_internal *scb;
  int retries;
  int errcode;
  

  scb = isc_createtcp(mcb, mcb->scfg, -1, ISC_STATE_LISTENING);
  if (!scb)
    return NULL;

  for (retries = 0; retries < scb->cfg->max.openretries; sleep(1), retries++)
  {
    errno = 0;
    if (isc_bindtcp(scb, address, service) >= 0 || errno != EADDRINUSE)
      break;
  }

  if (retries >= scb->cfg->max.openretries || errno != 0)
  {
    errcode = errno;
    isc_destroy(NULL, &scb->pub);
    errno = errcode;
    return NULL;
  }

  if (listen(scb->pub.fd, scb->cfg->max.backlog) < 0)
  {
    errcode = errno;
    isc_destroy(NULL, &scb->pub);
    errno = errcode;
    return NULL;
  }

  (void) isc_insert(mcb, scb);
  scb->accept_cb = cb;
  mcb->event_source->on_fd(mcb->event_source, scb->pub.fd, OOP_READ,
			   isc_accept_cb, scb);
  
  return &scb->pub;
}
