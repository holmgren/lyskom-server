/* unused -- compilation macros for unused arguments
   Copyright (C) 1997-1999 by Per Cederqvist.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this library; if not, write to the Free
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef UNUSED_H
#define UNUSED_H

#define UNUSED_H_CONCAT(a, b) a ## b
#ifdef HAVE_ATTRIBUTE_UNUSED
#  define UNUSED(var) UNUSED_H_CONCAT(qazwsxedc, var) __attribute__((unused))
#else
#  if defined(__cplusplus)
#    define UNUSED(var)
#  else
#    define UNUSED(var) var
#  endif
#endif

#endif
