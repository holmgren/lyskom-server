/*
** isc_event.c            		 definitions of ISC subsystem routines
**
** Copyright (C) 1990-1992, 1996, 1998-1999 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
**    900403 pen       initial coding.
**    900612 pen       rewrote the message buffering.
**    901102 pen       fixed bug in isc_gethostname().
**    910303 ceder     clean up. Removed everything that is lyskom-specific.
**    910304 pen       really removed everything lyskom-specific.. :-)
**    920129 pen       added support for "lazy" connect
**    920805 pen       one unneccessary isc_pollqueue() removed.
** (See ChangeLog for recent history)
*/

#include <stdio.h>
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#ifdef TIME_WITH_SYS_TIME
#  include <sys/time.h>
#  include <time.h>
#else
#  ifdef HAVE_SYS_TIME_H
#    include <sys/time.h>
#  else
#    include <time.h>
#  endif
#endif
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#ifdef HAVE_SYS_SELECT_H
#  include <sys/select.h>
#endif
#ifdef HAVE_UNISTD_H
#  include <unistd.h>	/* Needed on NetBSD1.1/SPARC due to select */
#endif
#ifdef HAVE_STRING_H
#  include <string.h>	/* Needed on NetBSD1.1/SPARC due to bzero */
#endif
#ifdef HAVE_STRINGS_H
#  include <strings.h>  /* Needed on AIX 4.2 due to bzero */
#endif

#include "oop.h"
#include "adns.h"
#include "oop-adns.h"

#include "s-string.h"

#include "isc.h"
#include "intern.h"


enum isc_read_result
isc_read_data(struct isc_scb *scb_pub,
	      String *result,
	      String_size *unused)
{
    void *buf;
    ssize_t status;
    struct isc_scb_internal *scb = (struct isc_scb_internal *)scb_pub;

    /* No output queue?  This could only happen if trying to read from
       a connection that has returned an error, but fail safe.  */
    if (scb->wr_msg_q == NULL)
	return ISC_READ_WOULDBLOCK;

    /* Too many queued blocks?  Don't read any more data, until
       the client has read the output we have already produced. */
    if (isc_sizequeue(scb->wr_msg_q) >= scb->cfg->max.queuedsize)
	return ISC_READ_WOULDBLOCK;

    /* Too many bytes? */
    if (scb->wr_msg_q->bytes >= scb->cfg->max.queuedsize_bytes)
	return ISC_READ_WOULDBLOCK;

    if (*unused * 2 > s_strlen(*result))
    {
	if (s_trim_left(result, *unused) != OK)
	    return ISC_READ_NOMEM;
	*unused = 0;
    }

    buf = s_reserve(result, scb->pub.master->scfg->max.msgsize);
    if (buf == NULL)
	return ISC_READ_NOMEM;

    do {
	status = read(scb->pub.fd, buf, scb->pub.master->scfg->max.msgsize);
    } while (status < 0 && errno == EINTR);
    
    if (status == 0)
    {
	s_reserve_done(result, 0);
	return ISC_READ_LOGOUT;
    }

    if (status < 0)
    {
	s_reserve_done(result, 0);
	if (errno == EAGAIN || errno == EWOULDBLOCK)
	    return ISC_READ_WOULDBLOCK;
	return ISC_READ_ERROR;
    }

    s_reserve_done(result, status);

    return ISC_READ_DATA;
}
