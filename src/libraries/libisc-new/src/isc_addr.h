/*
** isc_addr.h             aggregates and functions that handles IP addresses
**
** Copyright (C) 1991, 1996, 1998-1999, 2001 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * For IPv4/IPv6 compatibility:
 * SOCKADDR_STORAGE: Socket address storage type.
 * struct sockaddr_storage is used when available (RFC2553)
 */

#ifdef HAVE_STRUCT_SOCKADDR_STORAGE
#define SOCKADDR_STORAGE struct sockaddr_storage
#else
#define SOCKADDR_STORAGE struct sockaddr
#endif


/* Socket address
 */

union sockaddrs {
  SOCKADDR_STORAGE ssa;
  struct sockaddr sa;
  struct sockaddr_in sa_in;
#ifdef USE_INET6
  struct sockaddr_in6 sa_in6;
#endif
};

union isc_address
{
    union sockaddrs saddr;
};

extern union isc_address *
isc_mkipaddress(SOCKADDR_STORAGE *addr);


#if (defined(HAVE_GETIPNODEBYNAME) && defined(AI_ADDRCONFIG) \
     && defined(HAVE_FREEHOSTENT))
#  define USE_GETIPNODEBYNAME 1
#else
#  undef USE_GETIPNODEBYNAME
#endif

#ifdef USE_INET6
#  define UNUSED_UNLESS_INET6(x) x
#else
#  define UNUSED_UNLESS_INET6(x) UNUSED(x)
#endif
