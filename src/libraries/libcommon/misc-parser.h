/*
 * $Id: misc-parser.h,v 0.12 2003/08/23 16:38:20 ceder Exp $
 * Copyright (C) 1990-1991, 1993-1995, 1997, 1999, 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: misc-parser.h,v 0.12 2003/08/23 16:38:20 ceder Exp $
 *
 *  misc-parser.h
 *		Parse a list of "misc-item":s for a text in LysKOM
 *
 *
 *  Copyright (C) 1990-1991, 1993-1995, 1997, 1999, 2003	Lysator Computer Club,
 *			Linkoping University,  Sweden
 *
 *  Everyone is granted permission to copy, modify and redistribute
 *  this code, provided the people they give it to can.
 *
 *
 *  Author:	Thomas Bellman
 *		Lysator Computer Club
 *		Linkoping University
 *		Sweden
 *
 *  email:	Bellman@Lysator.LiU.SE
 */



#include <kom-types.h>



typedef  enum  {
	m_recpt,
	m_cc_recpt,
	m_bcc_recpt,
	m_comm_to,
	m_comm_in,
	m_footn_to,
	m_footn_in,
	m_end_of_list,	/* End of list reached	*/
	m_error		/* Bad misc-items found */
}  Misc_struct_type;


typedef  struct  {
	Misc_struct_type	type;
	Conf_no			recipient;
	Conf_no			cc_recipient;
	Conf_no			bcc_recipient;
	Local_text_no		local_no;
	Time			received_at;
	Text_no			comment_to;
	Text_no			commented_in;
	Text_no			footnote_to;
	Text_no			footnoted_in;
	Pers_no			sender;
	Time			sent_at;
	Bool			is_received;
	Bool			is_sent;
}  Misc_info_group;



#define IS_RECEIVED(mstruct)	((mstruct).is_received != FALSE)
#define IS_SENT_BY(mstruct)	((mstruct).sender != 0)
#define IS_SENT(mstruct)	((mstruct).is_sent != FALSE)


/*
 *  Parse out a "group" of misc-items from a list of them pointed
 *  to by *INFO.  *STOP_POINTER must point to the item directly
 *  after the last in the list.  **INFO will be incremented to
 *  point to the item after the recently parsed out group.  The
 *  return value will have the TYPE field set to 'm_end_of_list'
 *  when the end of the list has been reached.
 *
 *  If the has a bad format, the TYPE field will be 'm_error',
 *  and **INFO_POINTER will point to the item before the bad (or
 *  missing) item.
 */
extern  Misc_info_group
parse_next_misc (const Misc_info	** info_pointer,
		 const Misc_info	 * stop_pointer);
