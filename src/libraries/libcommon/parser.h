/*
 * $Id: parser.h,v 0.14 2003/08/23 16:38:20 ceder Exp $
 * Copyright (C) 1990-1991, 1994-1995, 1998-1999, 2002-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: parser.h,v 0.14 2003/08/23 16:38:20 ceder Exp $
 *
 *  client/parser.h  --  Header file for LysKOM command parsing routines
 *
 *
 *  Copyright (C) 1990-1991, 1994-1995, 1998-1999, 2002-2003	Lysator Computer Club,
 *			Linkoping University,  Sweden
 *
 *  Everyone is granted permission to copy, modify and redistribute
 *  this code, provided the people they give it to can.
 *
 *
 *  Author:	Thomas Bellman
 *		Lysator Computer Club
 *		Linkoping University
 *		Sweden
 *
 *  e-mail:	Bellman@Lysator.LiU.SE
 */


#ifndef  PARSER_H_ALREADY_INCLUDED__
#define  PARSER_H_ALREADY_INCLUDED__


#include <misc-types.h>
#include <s-string.h>
#include <s-collat-tabs.h>



/*
 *  Information about one word.  If the field 'word' is
 *  EMPTY_STRING, then the struct is considered to be the last
 *  in an "array".		BUG: "array" is wrong word
 */
typedef	struct {
	String		word;
	String_size	start_in_string;
} Parse_token;


/*
 *  Information about one string to match against during parse().
 *  A list of these should be passed to parse() as the CMD_TABLE
 *  parameter.  The 'tokens' field is set by doing
 *	I.tokens = tokenize(I.name, Separators);
 */
typedef	struct matching_info {
    Conf_no        conf_no;         /* Conference */
    String	   name;	    /* Name to match against */
    Parse_token	 * tokens;          /* Tokenized version of name */
    int		   priority;        /* Normally in interval 1..15 */
} Matching_info;



/*
 *  Remove paranthesized "expressions" from the string STR by
 *  replacing them with the character SEPARATOR.
 *  Superflous close paranthesis are disregarded.
 */
extern  void
remove_parenthesis (String	* str,
		    char	  separator);



/*
 *  Convert a String to a list of tokens (words).  This list is
 *  easier to parse than a string (since the string would have to
 *  be tokenized first anyway).
 *  Returns NULL if any error occured (couldn't allocate memory).
 */
extern  Parse_token *
tokenize (const String	  source,
	  const String	  separators);



/*
 *  Count the number of tokens (words) in TOKEN_LIST.  Used to
 *  set the NUMBER_OF_WORDS field in a 'Matching_info' object.
 */
extern  int
count_words (const Parse_token   * token_list);



/*
 *  Free the list of tokens (// and the strings they are pointing to //).
 *  Free:ing NULL is a no-op.
 */
extern  void
free_tokens (Parse_token   * token_list);



/*
 *  Returns the number of the first word of SOURCE that does
 *  not match PATTERN.  A word "foo" in SOURCE matches "foobar"
 *  in PATTERN, but not vice versa.
 */
extern  int
match (Parse_token	* source,
       Parse_token	* pattern,
       unsigned char      collat_tab[COLLAT_TAB_SIZE]);



/*
 *  Contains the result of a parse().
 */
typedef struct {
	int	  no_of_matches;	/* Number of found matches */
	int	* indexes;		/* List of indexes */
	String	  arguments;		/* The arguments... */
} Parse_info;


/*
 *  Searches for a matching string in the table 'match_table'.
 *  Some weird pattern matching is done.  The 'ARGUMENTS' field of
 *  the result is not a separate String, but points into
 *  SOURCE_STRING.
 *  If ALLOW_TRAILING_WORDS is false, then SOURCE_STRING may not
 *  contain any trailing words.
 *  If NUMBER_OF_WORDS_MUST_MATCH is true, then all the words
 *  in from the entry in MATCH_TABLE must be present (possibly
 *  abbrevated) in SOURCE_STRING.
 *  parse().no_of_matches is -1 if an error occured (out of
 *  memory).
 *  If SOURCE_STRING is empty, i e does not contain any words,
 *  the 'no_of_matches' field is 1, and indexes[0] is -1.
 *
 *  What?  You want a description of how it matches?  Forget it!  BUG!
 *  Try for yourself, and you'll find out!
 */

extern  Parse_info
parse (String		  source_string,
       Matching_info	* match_tbl,
       Bool		  allow_trailing_words,
       Bool		  number_of_words_must_match,
       String		  separators,
       unsigned char      collat_tab[COLLAT_TAB_SIZE]);


#endif	/*  PARSER_H_ALREADY_INCLUDED__  */
