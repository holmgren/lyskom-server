/*
 * $Id: s-collat-tabs.h,v 1.12 2003/08/23 16:38:19 ceder Exp $
 * Copyright (C) 1990-1991, 1993-1995, 1999, 2002-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: s-collat-tabs.h,v 1.12 2003/08/23 16:38:19 ceder Exp $
 *
 *  s-collat-tables.h  --  Declarations for collating tables
 *
 *
 *  Copyright (C) 1990-1991, 1993-1995, 1999, 2002-2003	Lysator Computer Club,
 *			Linkoping University,  Sweden
 *
 *  Everyone is granted permission to copy, modify and redistribute
 *  this code, provided the people they give it to can.
 *
 *
 *  Author:	Thomas Bellman
 *		Lysator Computer Club
 *		Linkoping University
 *		Sweden
 *
 *  email:	Bellman@Lysator.LiU.SE
 *
 *
 *  Any opinions expressed in this code are the author's PERSONAL opinions,
 *  and does NOT, repeat NOT, represent any official standpoint of Lysator,
 *  even if so stated.
 */


#define COLLAT_TAB_SIZE	   (UCHAR_MAX+1)


extern unsigned char	swedish_collate_tab [ COLLAT_TAB_SIZE ];
extern unsigned char	english_collate_tab [ COLLAT_TAB_SIZE ];
#if 0
extern unsigned char	iso8859_1_collate_tab [ COLLAT_TAB_SIZE ];
#endif
