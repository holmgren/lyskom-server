/*
 * $Id: s-collat-tabs.c,v 1.19 2003/08/23 16:38:19 ceder Exp $
 * Copyright (C) 1990-1991, 1993-1995, 1998-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 *  s-collat-tables.c  --  Collating tables used for the s_usr_strcmp()
 *			   routine in s-string.[ch]
 *
 *
 *  Copyright (C) 1990-1991, 1993-1995, 1998-1999, 2001-2003	Lysator Computer Club,
 *			Linkoping University,  Sweden
 *
 *  Everyone is granted permission to copy, modify and redistribute
 *  this code, provided the people they give it to can.
 *
 *
 *  Author:	Thomas Bellman
 *		Lysator Computer Club
 *		Linkoping University
 *		Sweden
 *
 *  email:	Bellman@Lysator.LiU.SE
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <limits.h>
#include "s-collat-tabs.h"


/*
 *  Collating tables:
 *	swedish_collate_tab:	Swedish text.  "][\}{|" in the right
 *				order.  Upper and lower case letters
 *				are equal.
 *	english_collate_tab:	English text.  Upper and lower case
 *				letters are equal.
 */

/*
 * swedish_collate_tab is now in ISO-8859-1.
 */

unsigned char
swedish_collate_tab [ COLLAT_TAB_SIZE ] =
    {
	/*   NU    SH    SX    EX    ET    EQ    AK    BL  */
	   0000, 0001, 0002, 0003, 0004, 0005, 0006, 0007, 

	/*   BS    HT    LF    VT    FF    CR    SO    SI  */
	   0010, 0011, 0012, 0013, 0014, 0015, 0016, 0017, 

	/*   DL    D1    D2    D3    D4    NK    SY    EB  */
	   0020, 0021, 0022, 0023, 0024, 0025, 0026, 0027, 

	/*   CN    EM    SB    EC    FS    GS    RS    US  */
	   0030, 0031, 0032, 0033, 0034, 0035, 0036, 0037, 

	/*		        #     $			   */   
	/*   SP    !     "     Nb    DO    %     &     '   */
	   0040, 0041, 0042, 0043, 0044, 0045, 0046, 0047, 

	/*   (     )     *     +     ,     -     .     /   */
	   0050, 0051, 0052, 0053, 0054, 0055, 0056, 0057, 

	/*   0     1     2     3     4     5     6     7   */
	   0060, 0061, 0062, 0063, 0064, 0065, 0066, 0067, 

	/*   8     9     :     ;     <     =     >     ?   */
	   0070, 0071, 0072, 0073, 0074, 0075, 0076, 0077, 

	/*    @                                            */
	/*   At    A     B     C     D     E     F     G   */
	   0100, 0101, 0102, 0103, 0104, 0105, 0106, 0107, 

	/*   H     I     J     K     L     M     N     O   */
	   0110, 0111, 0112, 0113, 0114, 0115, 0116, 0117, 

	/*   P     Q     R     S     T     U     V     W   */
	   0120, 0121, 0122, 0123, 0124, 0125, 0126, 0127, 

	/*                      [     \     ]     ^        */
	/*   X     Y     Z     <(    //    )>    '>    _   */
	   0130, 0131, 0132, 0134, 0135, 0133, 0136, 0137, 

	/*    `                                            */
	/*   '!    a     b     c     d     e     f     g   */
	/* 0140, 0141, 0142, 0143, 0144, 0145, 0146, 0147, */
	   0140, 0101, 0102, 0103, 0104, 0105, 0106, 0107, 

	/*   h     i     j     k     l     m     n     o   */
	/* 0150, 0151, 0152, 0153, 0154, 0155, 0156, 0157, */
	   0110, 0111, 0112, 0113, 0114, 0115, 0116, 0117, 

	/*   p     q     r     s     t     u     v     w   */
	/* 0160, 0161, 0162, 0163, 0164, 0165, 0166, 0167, */
	   0120, 0121, 0122, 0123, 0124, 0125, 0126, 0127, 

	/*                      {     |     }     ~        */
	/*   x     y     z     (!    !!    !)    '?    DT  */
	/* 0170, 0171, 0172, 0173, 0174, 0175, 0176, 0177, */
	   0130, 0131, 0132, 0134, 0135, 0133, 0176, 0177, 

	/*   PA    HO    BH    NH    IN    NL    SA    ES  */
	   0200, 0201, 0202, 0203, 0204, 0205, 0206, 0207, 

	/*   HS    HJ    VS    PD    PU    RI    S2    S3  */
	   0210, 0211, 0212, 0213, 0214, 0215, 0216, 0217, 

	/*   DC    P1    P2    TS    CC    MW    SG    EG  */
	   0220, 0221, 0222, 0223, 0224, 0225, 0226, 0227, 

	/*   SS    GC    SC    CI    ST    OC    PM    AC  */
	   0230, 0231, 0232, 0233, 0234, 0235, 0236, 0237, 

	/*   NS    !I    Ct    Pd    Cu    Ye    BB    SE  */
	   0240, 0041, 0242, 0243, 0244, 0245, 0246, 0247, 

	/*   ':    Co    -a    <<    NO    --    Rg    '-  */
	   0250, 0251, 0252, 0253, 0254, 0255, 0256, 0257, 

	/*   DG    +-    2S    3S    ''    My    PI    .M  */
	   0260, 0261, 0262, 0263, 0264, 0265, 0266, 0267, 

	/*   ',    1S    -o    >>    14    12    34    ?I  */
	   0270, 0271, 0272, 0273, 0274, 0275, 0276, 0277, 

	/*   A!    A'    A>    A?    A:    AA    AE    C,  */
	/* 0300, 0301, 0302, 0303, 0304, 0305, 0306, 0307, */
	   0101, 0101, 0101, 0101, 0134, 0133, 0134, 0103, 

	/*   E!    E'    E>    E:    I!    I'    I>    I:  */
	/* 0310, 0311, 0312, 0313, 0314, 0315, 0316, 0317, */
	   0105, 0105, 0105, 0105, 0111, 0111, 0111, 0111,

	/*   D-    N?    O!    O'    O>    O?    O:    *X  */
	/* 0320, 0321, 0322, 0323, 0324, 0325, 0326, 0327, */
	   0320, 0116, 0117, 0117, 0117, 0117, 0135, 0052, 

	/*   O/    U!    U'    U>    U:    Y'    TH    ss  */
	/* 0330, 0331, 0332, 0333, 0334, 0335, 0336, 0337, */
	   0135, 0125, 0125, 0125, 0131, 0131, 0336, 0337, 

	/*   a!    a'    a>    a?    a:    aa    ae    c,  */
	/* 0340, 0341, 0342, 0343, 0344, 0345, 0346, 0347, */
	   0101, 0101, 0101, 0101, 0134, 0133, 0134, 0103, 

	/*   e!    e'    e>    e:    i!    i'    i>    i:  */
	/* 0350, 0351, 0352, 0353, 0354, 0355, 0356, 0357, */
	   0105, 0105, 0105, 0105, 0111, 0111, 0111, 0111, 

	/*   d-    n?    o!    o'    o>    o?    o:    -:  */
	/* 0360, 0361, 0362, 0363, 0364, 0365, 0366, 0367, */
	   0360, 0116, 0117, 0117, 0117, 0117, 0135, 0057, 

	/*   o/    u!    u'    u>    u:    y'    th    y:  */
	/* 0370, 0371, 0372, 0373, 0374, 0375, 0376, 0377, */
	   0135, 0125, 0125, 0125, 0131, 0131, 0376, 0377
    };



unsigned char
english_collate_tab [ COLLAT_TAB_SIZE ] =
    {
	'\000', '\001', '\002', '\003', '\004', '\005', '\006', '\007',
	'\010', '\011', '\012', '\013', '\014', '\015', '\016', '\017',
	'\020', '\021', '\022', '\023', '\024', '\025', '\026', '\027',
	'\030', '\031', '\032', '\033', '\034', '\035', '\036', '\037',
	' ',	'!',	'"',	'#',	'$',	'%',	'&',	'\'',
	'(',	')',	'*',	'+',	',',	'-',	'.',	'/',
	'0',	'1',	'2',	'3',	'4',	'5',	'6',	'7',
	'8',	'9',	':',	';',	'<',	'=',	'>',	'?',
	'@',	'a',	'b',	'c',	'd',	'e',	'f',	'g',
	'h',	'i',	'j',	'k',	'l',	'm',	'n',	'o',
	'p',	'q',	'r',	's',	't',	'u',	'v',	'w',
	'x',	'y',	'z',	'[',	'\\',	']',	'~',	'_',
	'`',	'a',	'b',	'c',	'd',	'e',	'f',	'g',
	'h',	'i',	'j',	'k',	'l',	'm',	'n',	'o',
	'p',	'q',	'r',	's',	't',	'u',	'v',	'w',
	'x',	'y',	'z',	'{',	'|',	'}',	'~',	'',
	'\200',	'\201',	'\202',	'\203',	'\204',	'\205',	'\206',	'\207',	
	'\210',	'\211',	'\212',	'\213',	'\214',	'\215',	'\216',	'\217',
	'\220',	'\221',	'\222',	'\223',	'\224',	'\225',	'\226',	'\227',	
	'\230',	'\231',	'\232',	'\233',	'\234',	'\235',	'\236',	'\237',
	'\240',	'\241',	'\242',	'\243',	'\244',	'\245',	'\246',	'\247',
	'\250',	'\251',	'\252',	'\253',	'\254',	'\255',	'\256',	'\257',
	'\260',	'\261',	'\262',	'\263',	'\264',	'\265',	'\266',	'\267',
	'\270',	'\271',	'\272',	'\273',	'\274',	'\275',	'\276',	'\277',
	'\300',	'\301',	'\302',	'\303',	'\304',	'\305',	'\306',	'\307',
	'\310',	'\311',	'\312',	'\313',	'\314',	'\315',	'\316',	'\317',
	'\320',	'\321',	'\322',	'\323',	'\324',	'\325',	'\326',	'\327',
	'\330',	'\331',	'\332',	'\333',	'\334',	'\335',	'\336',	'\337',
	'\340',	'\341',	'\342',	'\343',	'\344',	'\345',	'\346',	'\347',
	'\350',	'\351',	'\352',	'\353',	'\354',	'\355',	'\356',	'\357',
	'\360',	'\361',	'\362',	'\363',	'\364',	'\365',	'\366',	'\367',
	'\370',	'\371',	'\372',	'\373',	'\374',	'\375',	'\376',	'\377'
    };
