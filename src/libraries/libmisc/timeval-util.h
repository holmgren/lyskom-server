/*
 * Copyright (C) 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

extern Bool timeval_nonzero(struct timeval t);
extern Bool timeval_zero(struct timeval t);

extern struct timeval timeval_ctor(time_t sec, int usec);

/* Compute WANTED_INTERVAL - (NOW - START) and store it in REMAINING,
   assuming that it is positive.  Return true if REMAINING is greater
   than zero.  Don't use the value stored in REMAINING when this
   function returns false. */
extern Bool timeval_remaining(struct timeval *remaining,
			      struct timeval wanted_interval,
			      struct timeval start,
			      struct timeval now);

extern Bool timeval_greater(struct timeval a, struct timeval b);
extern Bool timeval_less(struct timeval a, struct timeval b);

/* Return the difference as a number of seconds, properly rounded. */
extern long timeval_diff_sec(struct timeval a, struct timeval b);

/* Return the difference as a number of seconds, as a double. */
extern double timeval_diff_d(struct timeval a, struct timeval b);

/* Set TV to the current time plus INTERVAL.  Return -1 if
   gettimeofday() fails (check errno).  */
extern int setup_timer(struct timeval *tv, struct timeval interval);
