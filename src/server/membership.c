/*
 * $Id: membership.c,v 0.96 2003/08/28 23:13:06 ceder Exp $
 * Copyright (C) 1991-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * membership.c
 *
 * All atomic calls that controlls who is a member in what.
 * (The person/conf relation).
 */



#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#define DEBUG_MARK_AS_READ

#include <assert.h>
#include <stdio.h>
#ifdef HAVE_STRING_H
#  include <string.h>
#endif
#include <stddef.h>
#include "timewrap.h"
#include <setjmp.h>
#include <sys/types.h>

#include "misc-types.h"
#include "s-string.h"
#include "kom-types.h"
#include "services.h"
#include "server/smalloc.h"
#include "lyskomd.h"
#include "com.h"
#include "async.h"
#include "connections.h"
#include "internal-connections.h"
#include "kom-errno.h"
#include "manipulate.h"
#include "cache.h"
#include "send-async.h"
#include "minmax.h"
#include "kom-memory.h"
#include "param.h"
#include "local-to-global.h"
#include "server-time.h"

#ifdef DEBUG_MARK_AS_READ
#  include "log.h"
#  include "ram-output.h"
#endif

static void
set_membership_type_bits(Membership_type *type,
			 Bool invitation,
			 Bool passive,
			 Bool secret,
			 Bool passive_message_invert,
			 Bool reserved2,
			 Bool reserved3, 
			 Bool reserved4,
			 Bool reserved5)
{
    type->invitation = invitation;
    type->passive = passive;
    type->secret = secret;
    type->passive_message_invert = passive_message_invert;
    type->reserved2 = reserved2;
    type->reserved3 = reserved3;
    type->reserved4 = reserved4;
    type->reserved5 = reserved5;
}


static enum memb_visibility
check_unread(Pers_no member)
{
    Person *memb_p;
    GET_P_STAT(memb_p, member, mv_none);
    if (memb_p->flags.unread_is_secret)
	return mv_censor_unread;
    else
	return mv_full;
}


enum memb_visibility
membership_visible(const Connection *viewer_conn,
		   Pers_no member,
		   Conf_no conf_no,
		   Membership_type type,
		   Bool is_supervisor_of_member,
		   Bool is_supervisor_of_conf)

{
    enum access ma = error;
    enum access ca = error;
    
    if (ENA_C(viewer_conn, admin, 2) || ENA_C(viewer_conn, wheel, 8))
	return mv_full;

    if (is_supervisor_of_member
	|| (ma = access_perm(member, viewer_conn, unlimited)) >= unlimited)
    {
	return mv_full;
    }

    if (is_supervisor_of_conf
	|| (ca = access_perm(conf_no, viewer_conn, unlimited)) >= unlimited)
    {
	return check_unread(member);
    }

    if (ma >= read_protected && ca >= read_protected && !type.secret)
	return check_unread(member);
    
    return mv_none;
}

/*
 * Copy all information that ACTPERS is authorized to know about ORIG_P's
 * membership in all conferences to CENSOR_P.
 *
 * This function is used in get_membership().
 */
static void
copy_public_confs (Connection * conn, /* The connection for which we copy */
		   Pers_no    pers_no,  /* The person being censored */
                   Person   * censor_p,	/* The censored Person-struct */
		   Person   * orig_p,	/* The uncensored Person-struct */
		   Bool       keep_read,
		   Bool       want_read,
		   unsigned long max_ranges)
{
    int i;			/* Number of mships lefte in ORIG_P */
    Membership * censor_m;		/* Pointer in CENSOR_P */
    Membership * orig_m;	/* Pointer in ORIG_P */

    /* Copy all information except the secret. */
		    
    censor_p->conferences.confs
	= tmp_alloc( orig_p->conferences.no_of_confs * sizeof(Membership));
    censor_p->conferences.no_of_confs = 0;

    censor_m = censor_p->conferences.confs;
    orig_m   = orig_p->conferences.confs;

    for ( i = 0; i < orig_p->conferences.no_of_confs; i++, orig_m++ )
    {
	enum memb_visibility vis = membership_visible(
	    conn, pers_no, orig_m->conf_no, orig_m->type, FALSE, FALSE);

	if (vis > mv_none)
	{
	    *censor_m = *orig_m;

	    if (!want_read)
	    {
		if (keep_read)
		    censor_m->skip_read_texts = TRUE;
		else
		    censor_m->read_ranges = NULL;
	    }
	    else
	    {
		if (max_ranges > 0 && censor_m->no_of_read_ranges > max_ranges)
		    censor_m->no_of_read_ranges = max_ranges;
	    }

	    if (vis == mv_censor_unread)
	    {
		censor_m->last_time_read = NO_TIME;
		censor_m->no_of_read_ranges = 0;
		censor_m->read_ranges = NULL;
	    }
	    
	    ++censor_m;
	    ++censor_p->conferences.no_of_confs;
	}
    }
}

/*
 * Change the priority of a certain conference in a person.
 */

static void
do_change_priority (Membership * mship,
                    Conf_no conf_no,
                    Conference * conf_c,
		    unsigned char	 priority,
		    unsigned short	 where,
		    Pers_no	 pers_no,
		    Person     * pers_p,
                    Membership_type * type,
                    Bool            fake_passive)
{
    Membership tmp_conf;
    Member *mem;
    
    if (priority == 0 && fake_passive)
    {
        type->passive = 1;
    }
    else
    {
        mship->priority = priority;
    }

    mship->type = *type;
    
    /* Check range of where */
    
    if ( where >= pers_p->conferences.no_of_confs )
    {
	where = pers_p->conferences.no_of_confs - 1;
    }
    
    /* And now move the conference to slot number 'where' */
    
    if ( mship < pers_p->conferences.confs + where )
    {
	tmp_conf = *mship;
	while ( mship < pers_p->conferences.confs + where)
	{
	    *mship = *(mship + 1);
	    mship++;
	}
	*mship = tmp_conf;
    }
    else
    {
	tmp_conf = *mship;
	while ( mship > pers_p->conferences.confs + where)
	{
	    *mship = *(mship - 1);
	    mship--;
	}
	*mship = tmp_conf;
    }

    /* Sync the change with the corresponding member list */
    mem = locate_member(pers_no, conf_c);
    if (mem != NULL)
    {
        mem->type = *type;
        mark_conference_as_changed( conf_no );
    }

    mark_person_as_changed( pers_no );
}


/*
 * Insert a rec_time misc item to a text_status.
 * The item is put at position POS on the list. (0 == first)
 * Take no action if the misc_item at POS is a rec_time.
 * This function is only used when a person marks his letters as read.
 *
 *	Text_stat * text_stat_pointer	Textstatus to modify
 *	int	    pos			Where to insert rec_time
 */

static void
do_add_rec_time (Text_stat    * text_stat_ptr,
		 int		pos)
{
    int i;

    /* Defensive checks */
    if ( pos < 0 || pos > text_stat_ptr->no_of_misc )
    {
	restart_kom("do_add_rec_time() - illegal pos\n");
    }

    /* Check that no rec_time exists */

    if ( pos < text_stat_ptr->no_of_misc
	&& text_stat_ptr->misc_items[ pos ].type == rec_time )
    {
	return;
    }
    
    /* Allocate space */
    text_stat_ptr->misc_items
    = srealloc(text_stat_ptr->misc_items,
	       (++(text_stat_ptr->no_of_misc)) * sizeof(Misc_info));

    /* Move items. */

    for ( i = text_stat_ptr->no_of_misc - 1; i > pos; i-- )
    {
	text_stat_ptr->misc_items[ i ] = text_stat_ptr->misc_items[ i - 1 ];
    }

    /* Set type */
    text_stat_ptr->misc_items[ pos ].type = rec_time;

    /* Set value */
    text_stat_ptr->misc_items[ pos ].datum.received_at = current_time.tv_sec;
}

/*
 * add_rec_time adds a 'rec_time'  misc_item to text number LOC_NO in
 * conference CONF_C. The item will follow a recpt or cc_recpt to pers_no.
 * No action is taken if PERS_NO is not a recipient of the text, or the text
 * no longer exists, or the text has already been received.
 */
static void
add_rec_time(Conference * conf_c,
	     Local_text_no local_no,
             Pers_no pers_no)
{
    Bool 	  found;
    Text_no	  text_no;
    Text_stat 	* t_stat;
    int		  i;
    
    text_no = l2g_lookup(&conf_c->texts, local_no);

    if ( text_no == 0 )
    {
	return;			/* Text is deleted. */
    }
    
    VOID_GET_T_STAT(t_stat, text_no);

    /* locate the misc_item which says that ACTPERS is a recipient */

    for ( found = FALSE, i = 0; !found && i < t_stat->no_of_misc; i++ )
    {
	switch ( t_stat->misc_items[ i ].type )
	{
	case recpt:
	case cc_recpt:
	case bcc_recpt:
	    if ( t_stat->misc_items[ i ].datum.recipient == pers_no )
	    {
		do_add_rec_time( t_stat, i + 2 ); /* Add after loc_no */
		found = TRUE;
	    }
	    break;

	case comm_to:
	case comm_in:
	case footn_to:
	case footn_in:
	case loc_no:
	case rec_time:
	case sent_by:
	case sent_at:
	    break;

        case unknown_info:
        default:
            restart_kom("ERROR: bogus misc_info type detected in text %lu\n", text_no);
	}
    }

    if( found ==  FALSE )
    {
	kom_log("ERROR: add_rec_time(): found==FALSE\n");
    }
    
    mark_text_as_changed( text_no);
    return;
}

/*
 * Check if any read_ranges can be extended due to deleted texts.
 * Merge read_ranges blocks, if possible.
 *
 * This is currently only used from mark_as_read(), but it would be
 * good to have a background task that does this whenever a text has
 * been deleted.
 *
 * Returns non-zero if any change was performed.
 */
Bool
adjust_read(Membership *m,
	    const Conference *conf)
{
    struct read_range *ptr;
    struct read_range *prev;
    struct read_range *next;
    struct read_range *tail;
    struct read_range *begin;
    struct read_range *end;
    Bool change = FALSE;

    Local_text_no   conf_max;	/* Highest used local_text_no in conf */
    Local_text_no   conf_min;	/* Lowest used local_text_no in conf */

    Local_text_no   past_block;	/* The next local_text_no after the current block. */
    Local_text_no   limit;

    /* If nothing is marked as read, there is nothing to adjust.  */
    if (m->no_of_read_ranges == 0)
	return change;

    /* (conf_min <= x < conf_max) if x is an existing local_text_no */
    conf_min = l2g_next_key(&conf->texts, 0);
    if (conf_min == 0)
	conf_min = l2g_first_appendable_key(&conf->texts);
    conf_max = l2g_first_appendable_key(&conf->texts);

    begin = &m->read_ranges[0];
    end = begin + m->no_of_read_ranges;

#ifdef DEBUG_MARK_AS_READ
    
    /* Check that the items in read_texts really ARE sorted in ascending order.
       If not, there is probably a bug in this routine or in mark_as_read. */

    for (ptr = begin; ptr < end; ptr++)
    {
	if (ptr->first_read > ptr->last_read)
	{
	    kom_log("Bad input to adjust_read. Conference %lu, Priority %lu."
		    " Range %lu-%lu is nonsensical.\n",
		    (unsigned long)m->conf_no,
		    (unsigned long)m->priority,
		    (unsigned long)ptr->first_read,
		    (unsigned long)ptr->last_read);
	    return change;
	}

	if (ptr != begin && (ptr-1)->last_read >= ptr->first_read)
	{
	    kom_log("Bad input to adjust_read. Conference %lu, Priority %lu."
		    " Overlapping ranges: %lu-%lu, %lu-%lu.\n",
		    (unsigned long)m->conf_no,
		    (unsigned long)m->priority,
		    (unsigned long)(ptr-1)->first_read,
		    (unsigned long)(ptr-1)->last_read,
		    (unsigned long)ptr->first_read,
		    (unsigned long)ptr->last_read);
	    return change;
	}
    }
#endif

    for (ptr = begin; ptr < end; ptr++)
    {
	if (ptr == begin)
	    prev = NULL;
	else
	    prev = ptr - 1;
	next = ptr + 1;
	if (next == end)
	    next = NULL;

	/* Try to lower first_read. */
	while (ptr->first_read > 1
	       && (prev == NULL || prev->last_read + 1 < ptr->first_read)
	       && l2g_lookup(&conf->texts, ptr->first_read - 1) == 0)
	{
	    change = TRUE;
	    ptr->first_read--;
	    if (ptr->first_read < conf_min)
		ptr->first_read = 1;
	}

	/* Raise last_read past any deleted texts. */
	past_block = l2g_next_key(&conf->texts, ptr->last_read);
	if (next != NULL)
	    limit = next->first_read;
	else
	    limit = conf_max;

	if (past_block == 0 || past_block > limit)
	    past_block = limit;
	if (ptr->last_read != past_block - 1)
	{
	    ptr->last_read = past_block - 1;
	    change = TRUE;
	}
    }

    /* Join any ranges that became adjacent. */
    tail = begin;
    for (ptr = begin + 1; ptr < end; ptr++)
    {
	if (tail->last_read + 1 == ptr->first_read)
	    tail->last_read = ptr->last_read;
	else if (++tail != ptr)
	    *tail = *ptr;
    }
    if (end != tail + 1)
    {
	end = tail + 1;
	m->no_of_read_ranges = end - begin;
	m->read_ranges = srealloc(
	    m->read_ranges,
	    m->no_of_read_ranges * sizeof(m->read_ranges[0]));
	change = TRUE;
    }

#ifndef NDEFENSIVE_CHECKS
    
    /* Check that the items in read_texts really ARE sorted in ascending order.
       If not, there is probably a bug in this routine or in mark_as_read. */

    for (ptr = begin; ptr < end; ptr++)
    {
	if (ptr->first_read > ptr->last_read)
	{
	    kom_log("Bug in adjust_read. Conference %lu, Priority %lu."
		    " Range %lu-%lu is nonsensical (fixed).\n",
		    (unsigned long)m->conf_no,
		    (unsigned long)m->priority,
		    (unsigned long)ptr->first_read,
		    (unsigned long)ptr->last_read);
	    ptr->last_read = ptr->first_read;
	    change = TRUE;
	    return change;
	}

	if (ptr != begin && ptr->first_read <= (ptr-1)->last_read)
	{
	    kom_log("Bug in adjust_read. Conference %lu, Priority %lu."
		    " %lu not greater than %lu (fixed).\n",
		    (unsigned long)m->conf_no,
		    (unsigned long)m->priority,
		    (unsigned long)ptr->first_read,
		    (unsigned long)(ptr-1)->last_read);
	    sfree(m->read_ranges);
	    m->read_ranges = NULL;
	    m->no_of_read_ranges = 0;   
	    change = TRUE;
	    return change;
	}
    }
#endif
    return change;
}

/*
 * insert TEXT in the set of read texts in M.
 *
 * Returns FAILURE if the text is already read.
 *
 * This is only used from mark_as_read().
 */

static Success
insert_loc_no(Local_text_no   text,
	      Membership    * m)
{
    struct read_range *lo;
    struct read_range *hi;
    struct read_range *mid;
    struct read_range *begin;
    struct read_range *end;
    ptrdiff_t save;
    struct read_range *gap;
    struct read_range *move;

    if (m->no_of_read_ranges == 0)
    {
	m->read_ranges = smalloc(sizeof(m->read_ranges[0]));
	m->no_of_read_ranges = 1;
	m->read_ranges[0].first_read = text;
	m->read_ranges[0].last_read = text;
	return OK;
    }

    begin = lo = &m->read_ranges[0];
    end = hi = lo + m->no_of_read_ranges;

    while (hi - lo > 1)
    {
	mid = lo + (hi - lo) / 2;
	if (text < mid->first_read - 1)
	    hi = mid;
	else if (text > mid->last_read + 1)
	    lo = mid;
	else
	{
	    lo = mid;
	    hi = mid + 1;
	}
    }

    if (text >= lo->first_read && text <= lo->last_read)
	return FAILURE;		/* This text was already read. */

    if (text == lo->first_read - 1)
    {
	if (lo > begin && text == (lo-1)->last_read)
	    return FAILURE;

	lo->first_read = text;
	return OK;
    }

    if (text == lo->last_read + 1)
    {
	if (lo < end && (lo+1) < end && text == (lo+1)->first_read)
	    return FAILURE;

	lo->last_read = text;
	return OK;
    }

    /* We have to allocate a new block. */
    if (lo->last_read < text)
	lo++;

    save = lo - begin;
    m->read_ranges = srealloc(
	m->read_ranges,
	++(m->no_of_read_ranges) * sizeof(m->read_ranges[0]));
    begin = &m->read_ranges[0];
    gap = begin + save;
    end = begin + m->no_of_read_ranges;
    for (move = end - 1; move > gap; move--)
	*move = *(move - 1);
    gap->first_read = gap->last_read = text;
    return OK;
}

static void
remove_loc_no(Local_text_no   text,
	      Membership    * m)
{
    struct read_range *lo;
    struct read_range *hi;
    struct read_range *mid;
    struct read_range *begin;
    struct read_range *end;
    ptrdiff_t save;
    struct read_range *gap;
    struct read_range *move;

    if (m->no_of_read_ranges == 0)
	return;

    begin = lo = &m->read_ranges[0];
    end = hi = lo + m->no_of_read_ranges;

    while (hi - lo > 1)
    {
	mid = lo + (hi - lo) / 2;
	if (text < mid->first_read - 1)
	    hi = mid;
	else if (text > mid->last_read + 1)
	    lo = mid;
	else
	{
	    lo = mid;
	    hi = mid + 1;
	}
    }

    if (text < lo->first_read || text > lo->last_read)
	return;			/* This text was already unread. */
    
    if (text == lo->first_read)
    {
	if (lo->first_read != lo->last_read)
	{
	    /* Remove the first text of this range. */
	    lo->first_read++;
	    return;
	}

	/* This range contains a single text.  Remove the entire range.  */
	for (move = lo; move < (end-1); move++)
	    *move = *(move+1);
	m->no_of_read_ranges--;
	if (m->no_of_read_ranges == 0)
	{
	    sfree(m->read_ranges);
	    m->read_ranges = NULL;
	}
	else
	    m->read_ranges = srealloc(
		m->read_ranges,
		m->no_of_read_ranges * sizeof(m->read_ranges[0]));

	return;
    }

    if (text == lo->last_read)
    {
	assert(text != lo->first_read);

	/* Remove the last text of this range.  */
	lo->last_read--;
	return;
    }

    assert(lo->first_read < text && text < lo->last_read);

    /* We have to split this block. */
    save = lo - begin;
    m->read_ranges = srealloc(
	m->read_ranges,
	++(m->no_of_read_ranges) * sizeof(m->read_ranges[0]));
    begin = &m->read_ranges[0];
    gap = begin + save;
    end = begin + m->no_of_read_ranges;
    for (move = end - 1; move > gap; move--)
	*move = *(move - 1);

    assert(gap->first_read == (gap+1)->first_read);
    assert(gap->last_read == (gap+1)->last_read);
    assert(text > gap->first_read);
    assert(text < gap->last_read);

    gap->last_read = text - 1;
    (gap+1)->first_read = text + 1;
    return;
}


/*
 * Send a new-membership message to the affected person
 */

static void
send_async_new_membership(Pers_no pers_no,
			  Conf_no conf_no)
{
    Connection *cptr = NULL;
    Session_no i = 0;

    while ((i = traverse_connections(i)) != 0)
    {
        cptr = get_conn_by_number(i);
        if (cptr->pers_no == pers_no)
        {
            async_new_membership(cptr, pers_no, conf_no);
        }
    }
}

/*
 * Compute the derived last-text-read value.
 */
static inline Local_text_no
last_text_read(const Membership *mship)
{
    if (mship->no_of_read_ranges == 0)
	return 0;
    if (mship->read_ranges[0].first_read > 1)
	return 0;
    return mship->read_ranges[0].last_read;
}


/*
 * End of static functions
 */

/*
 * Functions that are exported to the server.
 */

/*
 * Add a member to a conference. All errorchecking should already
 * be done when this function is called. The person must not already
 * be a member of the conference. It is _not_ an error to make WHERE bigger
 * than the number of conferences the person is a member in.
 */

void
do_add_member(Conf_no	   conf_no,  /* Conference to add a new member to. */
	      Conference * conf_c,   /* Conf. status. Must NOT be NULL.  */
	      Pers_no	   pers_no,  /* Person to be added. */
	      Person	 * pers_p,   /* Pers. status. Must NOT be NULL. */
              Pers_no      added_by, /* Person doing the adding */
	      unsigned char	   priority, /* Prioritylevel to assign to this conf */
	      unsigned short	   where,   /* Sequence number in the list */
              Membership_type      * type, /* Type of the membership */
              Bool              fake_passive
              )
{
    Membership  * mship;
    Member	* mem;


    if (fake_passive && priority == 0)
    {
        type->passive = 1;
    }


    /* First add the conference in the person-struct.
     * Make room for it.
     */
    
    pers_p->conferences.confs = srealloc( pers_p->conferences.confs,
					 ++(pers_p->conferences.no_of_confs)
					 * sizeof(Membership));
    
    /* Fill in the room */

    /* Find last slot */    
    mship = pers_p->conferences.confs + pers_p->conferences.no_of_confs - 1 ;
    
    /* Move all data beyond WHERE */
    while ( mship > pers_p->conferences.confs + where )
    {
	*mship = *(mship - 1);
	mship--;
    }

    init_membership(mship);
    
    mship->added_by = added_by;
    mship->added_at = current_time.tv_sec;
    mship->conf_no = conf_no;
    mship->priority = priority;
    mship->last_time_read = current_time.tv_sec;
    mship->no_of_read_ranges = 0;
    mship->read_ranges = NULL;
    mship->type = *type;
    mship->skip_read_texts = FALSE;

    /* Make room for the person in the conference */
    
    conf_c->members.members = srealloc( conf_c->members.members,
				       ++(conf_c->members.no_of_members)
				       * sizeof(Member));

    /* New members go to the end of the list */
    
    mem = (conf_c->members.members + conf_c->members.no_of_members - 1);
    mem->member = pers_no;
    mem->added_by = added_by;
    mem->added_at = current_time.tv_sec;
    mem->type = *type;
    
    mark_conference_as_changed( conf_no );
    mark_person_as_changed( pers_no );

    return;
}

/*
 * Send an asynchronous message to person pers_no (if he is logged on)
 * and tell him that he is no longer a member of conf_no. Also calls
 * leave_conf(). 
 */
extern void
forced_leave_conf(Pers_no pers_no,
		  Conf_no conf_no)
{
    Connection *real_active_connection;
    Session_no i = 0;
    
    real_active_connection = active_connection;

    while ( (i = traverse_connections(i)) != 0 )
    {
	active_connection = get_conn_by_number(i);

	if ( active_connection->pers_no == pers_no )
	{
	    async_forced_leave_conf(active_connection, conf_no);

	    if ( active_connection->cwc == conf_no )
		leave_conf(active_connection);
	}
    }

    active_connection = real_active_connection;
}

/*
 * Delete a member from a conference.
 * No checks are made on the parameters.
 * The dynamically allocated areas conf_c->members.members and
 * pers_p->confs are NOT reallocated since they will anyhow sooner or later
 * be flushed from core.
 */

Success
do_sub_member(Conf_no	   conf_no, /* Conf to delete member from. */
	      Conference * conf_c,  /* May be NULL */
	      Member     * mbr,     /* May be NULL */
	      Pers_no	   pers_no, /* Person to be deleted. */
	      Person	 * pers_p,  /* May be NULL */
	      Membership * mship)   /* Pointer to the persons membership in
				       conf., or NULL if not known. */
{
    if ( conf_c == NULL )
	GET_C_STAT(conf_c, conf_no, FAILURE);
    
    if ( pers_p == NULL )
	GET_P_STAT(pers_p, pers_no, FAILURE);
    
    if ( mship == NULL && (mship = locate_membership(conf_no, pers_p)) == NULL)
	restart_kom("do_sub_member() - can't find mship\n");

    if ( mbr == NULL && (mbr = locate_member(pers_no, conf_c)) == NULL)
	restart_kom("do_sub_member() - can't find member.\n");

    forced_leave_conf(pers_no, conf_no);
    
    /* Delete from Person */

    sfree(mship->read_ranges);
    --pers_p->conferences.no_of_confs;
    while ( mship
	   < pers_p->conferences.confs + pers_p->conferences.no_of_confs )
    {
	*mship = *(mship + 1);
	++mship;
    }

    /* Delete from Conference */

    --conf_c->members.no_of_members;
    while ( mbr < conf_c->members.members + conf_c->members.no_of_members )
    {
	*mbr = *(mbr + 1);
	++mbr;
    }

    mark_person_as_changed( pers_no );
    mark_conference_as_changed( conf_no );
    
    return OK;    
}


/*
 * VICTIM is a person or a conference.
 * Meaning of return values:
 *	unlimited: ACTPERS is supervisor of VICTIM, or ACTPERS is admin,
 *		   or ACTPERS is VICTIM
 *	none:	   VICTIM is secret, and ACTPERS is not a member
 *	member:	   ACTPERS is a member in VICTIM, but doesn't have unlimited
 *		   access.
 *	read_protected: The conference is rd_prot and ACTPERS is not a member.
 *	limited:   otherwise.
 *	error:	   see kom_errno
 */

static enum access
access_perm_helper(Conf_no victim,
		   const Connection *conn,
		   enum access wanted_access)
{
    Conf_type   victim_type;
    int         victim_type_known = 0;
    static int  maxwarnings = 10;

    if (!cached_conf_exists(victim))
    {
        kom_errno = KOM_UNDEF_CONF;
        err_stat = victim;
	return error;
    }

    if (victim == conn->pers_no)
    	return unlimited;

    if (ENA_C(conn, admin, 2) || ENA_C(conn, wheel, 8))
    	return unlimited;

    if (wanted_access <= limited)
    {
	victim_type = cached_get_conf_type(victim);
	victim_type_known = 1;

	if (victim_type.secret == 0)
	{
	    if (victim_type.rd_prot)
	    {
		if (wanted_access <= read_protected)
		    return read_protected;
	    }
	    else
		return limited;
	}
    }

    if (is_supervisor(victim, conn))
    	return unlimited;

    if (conn->pers_no != 0)
    {
	if (conn->person == NULL)
	{
	    if (maxwarnings > 0)
	    {
		kom_log("WNG: conn->person unexpectedly NULL in "
			"access_perm_helper()\n");
		if (--maxwarnings == 0)
		    kom_log("WNG: Won't log the above warning any more.\n");
	    }
	    kom_errno = KOM_INTERNAL_ERROR;
	    err_stat = 0;
	    return error;
	}

	/* FIXME (bug 155): no need to call the expensive
	   locate_membership if supervisor(victim) == victim. */
	if (locate_membership(victim, conn->person) != NULL)
	    return member;
    }

    if (victim_type_known == 0)
	victim_type = cached_get_conf_type(victim);

    if (victim_type.secret)
        return none;

    if (victim_type.rd_prot)
	return read_protected;
    
    return limited;
}


enum access
access_perm(Conf_no victim,
	    const Connection *viewer_conn,
	    enum access wanted_access)
{
    enum access result;

    if (wanted_access != read_protected
	&& wanted_access != limited
	&& wanted_access != unlimited)
    {
	static int ctr = 0;
	if (ctr < 10)
	{
	    kom_log("WNG: access_perm called with wanted_access=%d\n",
		    (int)wanted_access);
	    ctr++;
	    if (ctr == 10)
		kom_log("WNG: won't log the above warning any more.\n");
	}
    }

    result = access_perm_helper(victim, viewer_conn, wanted_access);

    if (wanted_access < result)
	return wanted_access;
    else
	return result;
}


Bool
has_access(Conf_no victim,
	   const Connection *viewer_conn,
	   enum access wanted_access)
{
    return access_perm(victim, viewer_conn, wanted_access) >= wanted_access;
}


Conf_no
filter_conf_no(Conf_no victim,
	       const Connection *viewer_conn)
{
    if (has_access(victim, viewer_conn, read_protected))
	return victim;
    else
	return 0;
}


/*
 * Locate the Member struct in CONF_C for person PERS_NO
 */

Member *
locate_member(Pers_no      pers_no,
	      Conference * conf_c)
{
    Member * mbr;
    int      i;

    for(mbr = conf_c->members.members, i = conf_c->members.no_of_members;
	i > 0; i--, mbr++)
    {
	if ( mbr->member == pers_no )
	{
	    return mbr;
	}
    }

    return NULL;
}


/*
 * Find the data about PERS_P:s membership in CONF_NO.
 * Return NULL if not found
 */

Membership *
locate_membership(Conf_no       conf_no,
                  const Person *pers_p)
{
    Membership * confp;
    int    i;

    for(confp = pers_p->conferences.confs, i = pers_p->conferences.no_of_confs;
	i > 0; i--, confp++)
    {
	if ( confp->conf_no == conf_no )
	{
            confp->position = pers_p->conferences.no_of_confs - i;
	    return confp;
	}
    }

    return NULL;
}

/*
 * Atomic functions.
 */

/*
 * Unsubscribe from a conference.
 *
 * You must be supervisor of either conf_no or pers_no to be allowed to
 * do this.
 */
extern Success
sub_member(	Conf_no		conf_no,
		Pers_no		pers_no )
{
    Conference  * conf_c;
    Membership	* mship;
    Person	* pers_p;
    
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    GET_C_STAT(conf_c, conf_no, FAILURE);

    if ((pers_p = cached_get_person_stat(pers_no)) == NULL)
    {
	/* If the conference is secret, we have to return an error
	   code for the conference instead of for the person. */

	if (!has_access(conf_no, active_connection, read_protected))
	{
	    kom_errno = KOM_UNDEF_CONF;
	    err_stat = conf_no;
	}
	return FAILURE;
    }

    if ((mship = locate_membership(conf_no, pers_p)) == NULL
	|| membership_visible(active_connection, pers_no, conf_no, 
			      mship->type, FALSE, FALSE) == mv_none)
    {
	/* FIXME (bug 707): If the person is secret, and the viewer
	   has no access to it, we should return KOM_UNDEF_PERS
	   instead of KOM_NOT_MEMBER. */
	set_conf_errno(active_connection, conf_no, KOM_NOT_MEMBER);
	return FAILURE;
    }

    if (!is_supervisor(conf_no, active_connection)
	&& !is_supervisor(pers_no, active_connection)
	&& !ENA(wheel,8)        /* OK -- In an RPC call */
	&& !ENA(admin, 4) )	/* OK -- in an RPC call */
    {
	set_conf_errno(active_connection, conf_no, KOM_PERM);
	return FAILURE;
    }

    return do_sub_member(conf_no, conf_c, NULL, pers_no, pers_p, mship);
}


/*
 * Add a member to a conference (join a conference) or
 * Change the priority of a conference.
 *
 * Anyone may add anyone as a member as long as the new member is not
 * secret and the conference is not rd_prot. This might be a bug.
 *
 * PRIORITY is the assigned priority for the conference. WHERE says
 * where on the list the person wants the conference. 0 is first. WHERE
 * is automatically set to the number of conferences that PERS_NO is member
 * in if WHERE is too big, so it is not an error to give WHERE == ~0 as
 * a parameter.
 *
 * You can only re-prioritize if you are supervisor of pers_no.
 */

static Success
add_member_common(Conf_no              conf_no,
                  Pers_no              pers_no,
                  unsigned char        priority,
                  unsigned short       where,		/* Range of where is [0..] */
                  Membership_type     *type,
                  Bool                 fake_passive
    )
{
    Conference  * conf_c;
    Person	* pers_p;
    Membership	* mship;

    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    GET_C_STAT(conf_c, conf_no, FAILURE);
    GET_P_STAT(pers_p, pers_no, FAILURE);

    /* Force invitation bit if not adding as admin or supervisor */
    if (param.invite_by_default
	&& !is_supervisor(pers_no, active_connection)
	&& !ENA(admin, 2))	/* OK -- Guarded */
    {
        type->invitation = 1;
    }

    /* Check if secret membership bit is set and allowed */
    if (type->secret &&
        (!param.secret_memberships || conf_c->type.forbid_secret))
    {
        err_stat = 0;
        kom_errno = KOM_INVALID_MEMBERSHIP_TYPE;
        return FAILURE;
    }

    /* Check access to the conference. We need limited access or more */
    if (!has_access(conf_no, active_connection, limited)
	&& !ENA(wheel, 8) )     /* OK -- Guarded */
    {
        err_stat = conf_no;
	kom_errno = (conf_c->type).secret ? KOM_UNDEF_CONF : KOM_ACCESS;
	return FAILURE;
    }

    /* Is he already a member? */

    if ( (mship = locate_membership( conf_no, pers_p )) != NULL)
    {
        Bool pers_supervisor;
        Bool conf_supervisor;

        pers_supervisor = is_supervisor(pers_no, active_connection);
        conf_supervisor = is_supervisor(conf_no, active_connection);

        /* Already a member, but a secret member? */
        if (mship->type.secret &&
            !pers_supervisor &&
            !conf_supervisor &&
            !ENA(admin,2) &&    /* OK -- Guarded */
            !ENA(wheel,8))      /* OK -- Guarded */
        {
            /* FIXME (bug 156): This leaks secret information */
            /* The person is already a member, but we are not allowed
               to know this. Pretend that the addition worked, and hope
               that the user does not double-check */

            return OK;
        }

	/* He is already a member. Only change the priority. */
	if( !pers_supervisor &&
            !ENA(admin,2) && 
            !ENA(wheel, 8) )
	{
	    /* Noone else can change one's priorities. */
            err_stat = pers_no;
	    kom_errno = KOM_PERM;
	    return FAILURE;
	}

	do_change_priority( mship, conf_no, conf_c,
                            priority, where, pers_no, pers_p,
                            type, fake_passive );

    }
    else
    {
	if (pers_no != ACTPERS)
        {
	    kom_log("Person %lu added to conference %lu by %lu.\n",
		(unsigned long)pers_no,
		(unsigned long)conf_no,
		(unsigned long)ACTPERS);
        }

	do_add_member(conf_no, conf_c, pers_no, pers_p, ACTPERS,
                      priority, where, type, fake_passive);

        send_async_new_membership(pers_no, conf_no);
    }

    return OK;
}


extern Success
add_member_old(Conf_no	conf_no,
               Pers_no	pers_no,
               unsigned char	priority,
               unsigned short	where)
{
    Membership_type type;

    /* CHK_CONNECTION in add_member_common */
    set_membership_type_bits(&type, 0,0,0,0,0,0,0,0);
    return add_member_common(conf_no, pers_no, priority, where, &type, TRUE);
}


extern Success
add_member(Conf_no	conf_no,
           Pers_no	pers_no,
           unsigned char	priority,
           unsigned short	where,		/* Range of where is [0..] */
           Membership_type     *type
           )
{
    /* CHK_CONNECTION in add_member_common */
    return add_member_common(conf_no, pers_no, priority, where, type, FALSE);
}


#ifdef DEBUG_MARK_AS_READ
static int
check_membership(Pers_no pno,
		 const Conference *conf,
		 Membership *mship)
{
    static int log_no = 0;

    int errors = 0;
    struct read_range *begin;
    struct read_range *end;
    struct read_range *ptr;

    if (mship->no_of_read_ranges == 0 && mship->read_ranges != NULL)
    {
	if (log_no < 80)
	    kom_log("ERROR: check_membership(): (%d): "
		    "no_of_read_ranges == 0 but read_ranges != NULL\n",
		    ++log_no);
	/* We don't know what it points to, so we dare not free it.  */
	mship->read_ranges = NULL;
	errors++;
    }

    if (mship->no_of_read_ranges != 0 && mship->read_ranges == NULL)
    {
	if (log_no < 80)
	    kom_log("ERROR: check_membership(): (%d): "
		    "no_of_read_ranges == %ld but read_ranges == NULL\n",
		    ++log_no, (long)mship->no_of_read_ranges);
	mship->no_of_read_ranges = 0;
	errors++;
    }

    if (mship->read_ranges == NULL)
	return errors;

    begin = &mship->read_ranges[0];
    end = begin + mship->no_of_read_ranges;

    for (ptr = begin; ptr < end; ptr++)
    {
	if (ptr != begin && (ptr-1)->last_read + 1 >= ptr->first_read)
	{
	    if (log_no < 80)
		kom_log("ERROR: check_membership(): (%d): "
			"bad range sequence %lu-%lu, %lu-%lu\n",
			++log_no,
			(unsigned long)(ptr-1)->first_read,
			(unsigned long)(ptr-1)->last_read,
			(unsigned long)ptr->first_read,
			(unsigned long)ptr->last_read);
	    errors++;
	}

	if (ptr->first_read > ptr->last_read)
	{
	    if (log_no < 80)
		kom_log("ERROR: check_membership(): (%d): bad range %lu-%lu\n",
			++log_no,
			(unsigned long)ptr->first_read,
			(unsigned long)ptr->last_read);
	    errors++;
	}
    }

    if ((end-1)->last_read >= l2g_first_appendable_key(&conf->texts))
    {
	if (log_no < 80)
	    kom_log("ERROR: check_membership(): (%d): Person %lu has read text"
		    " %lu in conf %lu, which only has %lu texts.\n",
		    log_no,
		    (unsigned long)pno,
		    (unsigned long)(end-1)->last_read,
		    (unsigned long)mship->conf_no,
		    (unsigned long)(l2g_first_appendable_key(&conf->texts)-1));
	errors++;
    }

    return errors;
}

static void
read_ranges_precondition(Membership *m,
			 Membership *save,
			 const Conference *conf_c,
			 const char *func)
{
    if (check_membership(ACTPERS, conf_c, m) > 0)
	kom_log("%s(): the above error was detected on entry to me.\n", func);

    if (m->read_ranges == NULL && m->no_of_read_ranges != 0)
    {
	kom_log("%s(): m->read_ranges == NULL && "
		"m->no_of_read_ranges == %lu (corrected).\n",
		func,
		(unsigned long)m->no_of_read_ranges);
	m->no_of_read_ranges = 0;
    }
    
    *save = *m;
    if (m->read_ranges != NULL)
    {
	save->read_ranges = smalloc(m->no_of_read_ranges
				    * sizeof(save->read_ranges[0]));
	memcpy(save->read_ranges, m->read_ranges,
	       m->no_of_read_ranges * sizeof(save->read_ranges[0]));
    }
}

static int
read_ranges_postcondition(Membership *m,
			  Membership *save,
			  const Conference *conf_c,
			  const char *func)
{
    static int log_no = 0;
    int ret = 0;

    /* Check that the membership is correct. Otherwise log all info. */
    if (check_membership(ACTPERS, conf_c, m) > 0 && log_no < 40)
    {
	kom_log("%s(): (%d) Person %lu has a corrupt membership:\n",
		func, ++log_no, (unsigned long)ACTPERS);
	kom_log("Dump of data follows: <original membership>"
		"<updated membership>\n");
	foutput_membership(stderr, save);
	putc('\n', stderr);
	foutput_membership(stderr, m);
	putc('\n', stderr);
	ret = -1;
    }

    sfree(save->read_ranges);
    return ret;
}
#endif


/*
 * mark_as_read() is used to tell LysKOM which texts you have read.
 * You can mark several texts in one chunk, but the chunk should not
 * be too big to prevent users from having to re-read texts in case of
 * a server/client/network crash.
 *
 * The texts are marked per conference. If there are several recipients
 * to a text it should be mark_as_read() in all the recipients.
 *
 * If conference is ACTPERS mailbox it will add a rec_time item in the
 * misc_items field.
 *
 * It is only possible to mark texts as read in a conference you are
 * member in.
 *
 * Attempts to mark non-existing texts as read are ignored if the text
 * has existed. If the text has not yet been created KOM_NO_SUCH_LOCAL_TEXT
 * will be returned in kom_errno.
 *
 * If CONFERENCE is the current working conference of ACTPERS, and the
 * text has not previously been marked as read, ACT_P->read_texts will
 * be increased. If the client cooperates this will be correct. If the
 * client change_conference()s to all recipients of a text before
 * marking it as read the read_texts field will be too big. (If anyone
 * cares about that, feel free to rewrite this code as long as it
 * doen't get too CPU-intensive.)
 */
extern Success
mark_as_read (Conf_no		  conference,
	      const Number_list  *texts)
{
    int		 i;
    Membership * m;
    Conference * conf_c;
    Success	 retval = OK;
    Local_text_no lno;
#ifdef DEBUG_MARK_AS_READ
    Membership   original;
#endif

    CHK_CONNECTION(FAILURE);

    if (texts->length > param.mark_as_read_chunk)
    {
        kom_errno = KOM_LONG_ARRAY;
        err_stat = param.mark_as_read_chunk;
        return FAILURE;
    }

    CHK_LOGIN(FAILURE);
    GET_C_STAT(conf_c, conference, FAILURE);

    if ((m = locate_membership(conference, ACT_P)) == NULL)
    {
	set_conf_errno(active_connection, conference, KOM_NOT_MEMBER);
	return FAILURE;
    }

#ifdef DEBUG_MARK_AS_READ
    read_ranges_precondition(m, &original, conf_c, "mark_as_read");
#endif
    
    for (i = 0; i < texts->length; i++)
    {
	lno = texts->data[i];
	if (lno >= l2g_first_appendable_key(&conf_c->texts))
	{
	    kom_errno = KOM_NO_SUCH_LOCAL_TEXT;
	    err_stat = i;
	    retval = FAILURE;
	    break;		/* Exit for-loop */
	}

	if (lno == 0)
	{
	    kom_errno = KOM_LOCAL_TEXT_ZERO;
	    err_stat = i;
	    retval = FAILURE;
	    break;		/* Exit for-loop */
	}
    }

    if (retval == OK)
    {
	for(i = 0; i < texts->length; i++)
	{
	    lno = texts->data[i];

	    /* Is it a letter to ACTPERS? If so, add a rec_time item. */
	    if (conference == ACTPERS)
		add_rec_time(conf_c, lno, ACTPERS);
	
	    /* Update the Membership struct */
	    if (insert_loc_no(lno, m) == OK
		&& active_connection->cwc == conference)
	    {
		++ACT_P->read_texts;
	    }
	}
    
	adjust_read(m, conf_c);
	mark_person_as_changed(ACTPERS);
    }

#ifdef DEBUG_MARK_AS_READ
    if (read_ranges_postcondition(m, &original, conf_c, "mark_as_read") < 0)
    {
	kom_log("this was triggered by an attempt to mark this as read\n");
	fprintf(stderr, "\n%lu { ", (unsigned long)texts->length);
	for (i = 0; i < texts->length; i++)
	    fprintf(stderr, "\n%lu ", (unsigned long)texts->data[i]);
	fprintf(stderr, "}\n");
    }
#endif
    return retval;
}


extern Success
mark_as_unread(Conf_no        conference,
	       Local_text_no  lno)
{
    Membership * m;
    Conference * conf_c;
    Success	 retval = OK;
#ifdef DEBUG_MARK_AS_READ
    Membership   original;
#endif

    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    GET_C_STAT(conf_c, conference, FAILURE);

    if ((m = locate_membership(conference, ACT_P)) == NULL)
    {
	set_conf_errno(active_connection, conference, KOM_NOT_MEMBER);
	return FAILURE;
    }

#ifdef DEBUG_MARK_AS_READ
    read_ranges_precondition(m, &original, conf_c, "unmark_as_read");
#endif

    if (lno == 0)
    {
	kom_errno = KOM_LOCAL_TEXT_ZERO;
	err_stat = 0;
	retval = FAILURE;
    }
    else if (lno >= l2g_first_appendable_key(&conf_c->texts))
    {
	kom_errno = KOM_NO_SUCH_LOCAL_TEXT;
	err_stat = lno;
	retval = FAILURE;
    }
    else
    {
	remove_loc_no(lno, m);
	adjust_read(m, conf_c);
	mark_person_as_changed(ACTPERS);
    }

#ifdef DEBUG_MARK_AS_READ
    if (read_ranges_postcondition(m, &original, conf_c, "unmark_as_read") < 0)
    {
	kom_log("this was triggered by an attempt to mark %lu as not read\n",
		(unsigned long)lno);
    }
#endif
    return retval;
}

static Success
check_range_list(const struct read_range_list *read_ranges)
{
    Local_text_no last = 0;
    unsigned short i;

    if (read_ranges->length > param.max_read_ranges)
    {
	kom_errno = KOM_LONG_ARRAY;
	err_stat = param.max_read_ranges;
	return FAILURE;
    }

    for (i = 0; i < read_ranges->length; i++)
    {
	if (read_ranges->ranges[i].first_read >
	    read_ranges->ranges[i].last_read)
	{
	    kom_errno = KOM_INVALID_RANGE;
	    err_stat = i;
	    return FAILURE;
	}

	if (read_ranges->ranges[i].first_read <= last)
	{
	    if (read_ranges->ranges[i].first_read == 0)
		kom_errno = KOM_LOCAL_TEXT_ZERO;
	    else
		kom_errno = KOM_INVALID_RANGE_LIST;
	    err_stat = i;
	    return FAILURE;
	}

	last = read_ranges->ranges[i].last_read;
    }

    return OK;
}


Success
set_read_ranges(Conf_no conference,
		const struct read_range_list *read_ranges)
{
    Membership * m;
    Conference * conf_c;
    Success	 retval = OK;
#ifdef DEBUG_MARK_AS_READ
    Membership   original;
#endif

    CHK_CONNECTION(FAILURE);

    if (check_range_list(read_ranges) == FAILURE)
	return FAILURE;

    CHK_LOGIN(FAILURE);
    GET_C_STAT(conf_c, conference, FAILURE);

    if ((m = locate_membership(conference, ACT_P)) == NULL)
    {
	set_conf_errno(active_connection, conference, KOM_NOT_MEMBER);
	return FAILURE;
    }

#ifdef DEBUG_MARK_AS_READ
    read_ranges_precondition(m, &original, conf_c, "set_read_ranges");
#endif

    if (read_ranges->length == 0)
    {
	if (m->no_of_read_ranges != 0)
	{
	    sfree(m->read_ranges);
	    m->read_ranges = NULL;
	    m->no_of_read_ranges = 0;
	    mark_person_as_changed(ACTPERS);
	}
    }
    else if (read_ranges->ranges[read_ranges->length-1].last_read
	     >= l2g_first_appendable_key(&conf_c->texts))
    {
	kom_errno = KOM_NO_SUCH_LOCAL_TEXT;
	err_stat = read_ranges->ranges[read_ranges->length-1].last_read;
	retval = FAILURE;
    }
    else
    {
	if (m->no_of_read_ranges != read_ranges->length)
	{
	    m->no_of_read_ranges = read_ranges->length;
	    m->read_ranges = srealloc(
		m->read_ranges,
		m->no_of_read_ranges * sizeof(m->read_ranges[0]));
	}
	memcpy(m->read_ranges, read_ranges->ranges,
	       m->no_of_read_ranges * sizeof(m->read_ranges[0]));
	adjust_read(m, conf_c);
	mark_person_as_changed(ACTPERS);
    }

#ifdef DEBUG_MARK_AS_READ
    read_ranges_postcondition(m, &original, conf_c, "set_read_ranges");
#endif
    return retval;
}



/*
 * Ask what conferences a person is a member of.
 */


static Success
do_get_membership(Pers_no              pers_no,
		  unsigned short       first,
		  unsigned short       no_of_confs,
		  Bool                 keep_ranges,
		  Bool                 want_read_ranges,
		  unsigned long        max_ranges,
		  Membership_list     *memberships)
{
    Person		* p_orig;
    Person		  temp_pers;
    enum access		  acc;
    int			  i;
            
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN (FAILURE);
    
    GET_P_STAT (p_orig, pers_no, FAILURE);
    
    acc = access_perm(pers_no, active_connection, unlimited);
  
    if (acc == error)
	return  FAILURE;

    if (acc == none)
    {
        err_stat = pers_no;
	kom_errno = KOM_UNDEF_PERS;
	return  FAILURE;
    }

    /* Make a copy of the struct. */
    
    temp_pers = *p_orig;
    
    /* Delete all secret and filtered information. */

    if ( acc != unlimited )
    {
	copy_public_confs (active_connection, pers_no,
                           &temp_pers, p_orig, keep_ranges,
                           want_read_ranges, max_ranges);
    }
    else if (!want_read_ranges || max_ranges != 0)
    {
	/* Delete info about read texts. */
	temp_pers.conferences.confs
	    = tmp_alloc(temp_pers.conferences.no_of_confs
			* sizeof(Membership));

	memcpy(temp_pers.conferences.confs,
	       p_orig->conferences.confs,
	       (temp_pers.conferences.no_of_confs
		* sizeof(Membership) ));
	
	if (!want_read_ranges)
	{
	    for (i = 0; i < temp_pers.conferences.no_of_confs; i++)
		if (keep_ranges)
		    temp_pers.conferences.confs[i].skip_read_texts = TRUE;
		else
		    temp_pers.conferences.confs[i].read_ranges = NULL;
	}
	else
	{
	    assert(max_ranges != 0);
	    for (i = 0; i < temp_pers.conferences.no_of_confs; i++)
		if (temp_pers.conferences.confs[i].no_of_read_ranges
		    > max_ranges)
		{
		    temp_pers.conferences.confs[i].no_of_read_ranges =
			max_ranges;
		}
	}
    }

    *memberships = temp_pers.conferences;

    if ( first >= memberships->no_of_confs )
    {
        err_stat = first;
	kom_errno = KOM_INDEX_OUT_OF_RANGE;
	return FAILURE;
    }
    
    memberships->confs += first;
    memberships->no_of_confs = min( memberships->no_of_confs - first,
				   no_of_confs);

    for (i = 0; i < memberships->no_of_confs; i++)
    {
        memberships->confs[i].position = first + i;
    }

    return OK;
}

extern  Success
get_membership_old (Pers_no		  pers_no,
                    unsigned short        first,
                    unsigned short        no_of_confs,
                    Bool                  want_read_texts,
                    Membership_list	* memberships )
{
    Success     result;
    long        i;

    CHK_BOOL(want_read_texts, FAILURE);

    /* CHK_CONNECTION in do_get_membership */
    result = do_get_membership (pers_no,
                                first,
                                no_of_confs,
				TRUE,
                                want_read_texts,
				0,
                                memberships
                                );

    if (result == OK)
    {
        /* Munge passive conferences */
        for (i = 0; i < memberships->no_of_confs; i++)
        {
            if (memberships->confs[i].type.passive)
            {
                memberships->confs[i].priority = 0;
            }
        }
    }

    return result;
}

extern  Success
get_membership_10(Pers_no          pers_no,
		  unsigned short   first,
		  unsigned short   no_of_confs,
		  Bool		   want_read_texts,
		  Membership_list *memberships)
{
    CHK_BOOL(want_read_texts, FAILURE);

    /* CHK_CONNECTION in do_get_membership */
    return do_get_membership (pers_no,
                              first,
                              no_of_confs,
			      TRUE,
                              want_read_texts,
			      0,
                              memberships);
}

extern Success
get_membership(Pers_no           pers_no,
	       unsigned short    first,
	       unsigned short    no_of_confs,
	       Bool              want_read_ranges,
	       unsigned long     max_ranges,
	       Membership_list * memberships)
{
    CHK_BOOL(want_read_ranges, FAILURE);

    /* CHK_CONNECTION in do_get_membership */
    return do_get_membership (pers_no,
                              first,
                              no_of_confs,
			      FALSE,
                              want_read_ranges,
			      max_ranges,
                              memberships);
}

/*
 * first starts at 0.
 */

static Success
do_get_members(Conf_no             conf_no,
	       unsigned short	   first,
	       unsigned short      no_of_members,
	       Member_list        *members)
{
    Conference   *conf_c;
    enum access	  acc;
    unsigned long src;
    unsigned long dst;
    Bool          is_supervisor_of_conf;
        
    CHK_CONNECTION(FAILURE);
    GET_C_STAT(conf_c, conf_no, FAILURE);

    acc = access_perm(conf_no, active_connection, read_protected);

    if (acc == error)
	return FAILURE;

    if (acc == none)
    {
        err_stat = conf_no;
	kom_errno = KOM_UNDEF_CONF;
	return FAILURE;
    }

    if (first >= (conf_c->members).no_of_members)
    {
        err_stat = first;
	kom_errno = KOM_INDEX_OUT_OF_RANGE;
	return FAILURE;
    }

    *members = conf_c->members;
    members->members += first;

    members->no_of_members = min(no_of_members,
				 members->no_of_members - first);
    members->members = tmp_alloc(members->no_of_members * sizeof(Member));
    

    is_supervisor_of_conf = is_supervisor(conf_no, active_connection);
    for (src = first, dst = 0; dst < members->no_of_members; src++, dst++)
    {
	const Member *current = &conf_c->members.members[src];

	if (membership_visible(active_connection, current->member,
			       conf_no, current->type,
			       FALSE, is_supervisor_of_conf) > mv_none)
	{
            members->members[dst] = *current;
        }
        else
        {
	    init_member(&members->members[dst]);
            members->members[dst].type.secret = 1;
        }
    }

    return OK;
}


extern  Success
get_members (Conf_no                 conf_no,
             unsigned short          first,
             unsigned short          no_of_members,
             Member_list            *members
             )
{
    /* CHK_CONNECTION in do_get_members */
    return do_get_members(conf_no,
                          first,
                          no_of_members,
                          members);
}

extern Success
get_members_old (Conf_no conf_no,
                 unsigned short first,
                 unsigned short no_of_members,
                 Member_list * members)
{
    /* CHK_CONNECTION in do_get_members */
    return do_get_members (conf_no,
                           first,
                           no_of_members,
                           members);
}


/*
 * Get a list of all conferences where it is possible that a person has
 * unread articles.
 */

Success
get_unread_confs(Pers_no       pers_no,
		 Conf_no_list *result)
{
    Person *pers_p;
    const Membership *confs;
    const Membership *end;

    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    GET_P_STAT(pers_p, pers_no, FAILURE);

    result->conf_nos = tmp_alloc (pers_p->conferences.no_of_confs
				  * sizeof(Conf_no));
    result->no_of_confs = 0;

    end = &pers_p->conferences.confs[pers_p->conferences.no_of_confs];
    for (confs = pers_p->conferences.confs; confs < end; confs++)
    {
	Conf_no conf_no = confs->conf_no;

	if (confs->type.passive == 0
	    && (last_text_read(confs) < cached_get_highest_local_no(conf_no))
	    && membership_visible(active_connection,
				  pers_no, conf_no, confs->type,
				  FALSE, FALSE) > mv_none)
	{
	    result->conf_nos[result->no_of_confs++] = conf_no;
	}
    }
    return OK;
}


/*
 * Tell the server that I want to mark/unmark texts as read so that I
 * get (approximately) no_of_unread unread texts in conf_no.
 */
extern  Success
set_unread (Conf_no   conf_no,
	    Text_no   no_of_unread)
{
    Membership  *mship;
    Conference  *conf_c;
    Local_text_no highest;
    
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);

    GET_C_STAT(conf_c, conf_no, FAILURE);
    
    if ( (mship = locate_membership(conf_no, ACT_P)) == NULL )
    {
        err_stat = conf_no;
	kom_errno = KOM_NOT_MEMBER;
	return FAILURE;
    }

    highest = l2g_first_appendable_key(&conf_c->texts) - 1;

    if (highest > no_of_unread)
    {
	mship->no_of_read_ranges = 1;
	mship->read_ranges = srealloc(mship->read_ranges,
				      1 * sizeof(mship->read_ranges[0]));
	mship->read_ranges[0].first_read = 1;
	mship->read_ranges[0].last_read = highest - no_of_unread;
    }
    else
    {
	sfree(mship->read_ranges);
	mship->read_ranges = NULL;
	mship->no_of_read_ranges = 0;
    }

    mark_person_as_changed(ACTPERS);
    return OK;
}

/*
 * Tell the server that I want to mark/unmark texts as read so that
 * last_read is the last read text in conf_no.
 */
extern  Success
set_last_read (Conf_no conf_no,
	       Local_text_no last_read)
{
    Membership  *mship;
    Conference  *conf_c;
    Local_text_no last;
    
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);

    GET_C_STAT(conf_c, conf_no, FAILURE);
    
    if ( (mship = locate_membership(conf_no, ACT_P)) == NULL )
    {
        err_stat = conf_no;
	kom_errno = KOM_NOT_MEMBER;
	return FAILURE;
    }

    last = l2g_first_appendable_key(&conf_c->texts) - 1;

    if (last_read > last)
	last_read = last;

    if (last_read > 0)
    {
	mship->no_of_read_ranges = 1;
	mship->read_ranges = srealloc(mship->read_ranges,
				      1 * sizeof(mship->read_ranges[0]));
	mship->read_ranges[0].first_read = 1;
	mship->read_ranges[0].last_read = last_read;
    }
    else
    {
	sfree(mship->read_ranges);
	mship->read_ranges = NULL;
	mship->no_of_read_ranges = 0;
    }

    mark_person_as_changed(ACTPERS);
    return OK;
}

extern Success set_membership_type(Pers_no pers_no,
                                   Conf_no conf_no,
                                   Membership_type *type)
{
    Conference      *conf_c;
    Person          *pers_p;
    Membership      *membership;
    Member          *mbr;
    enum access      acc;

    /* Check for logon */
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);

    /* Find the conference and person in question */
    GET_C_STAT(conf_c, conf_no, FAILURE);
    /* Make sure that ACTPERS may know about conf */
    acc = access_perm(conf_no, active_connection, read_protected);
    if (acc == error)
    {
        return FAILURE;
    }

    if (acc == none)
    {
        err_stat = conf_no;
        kom_errno = KOM_UNDEF_CONF;
        return FAILURE;
    }

    GET_P_STAT(pers_p, pers_no, FAILURE);
    /* Check that ACTPERS may modify memberships of person */
    acc = access_perm(pers_no, active_connection, unlimited);
    if (acc != unlimited &&
        !ENA(wheel, 8) &&       /* OK -- In an RPC call */
        !ENA(admin, 6))         /* OK -- In an RPC call */
    {
        err_stat = pers_no;
        kom_errno = conf_c->type.secret ? KOM_UNDEF_CONF : KOM_PERM;
        return FAILURE;
    }

    /* Find person's membership in the conference */
    membership = locate_membership(conf_no, pers_p);
    if (membership == NULL)
    {
        err_stat = conf_no;
        kom_errno = KOM_NOT_MEMBER;
        return FAILURE;
    }

    /* Find entry in conference's member list */

    mbr = locate_member(pers_no, conf_c);
    if (mbr == NULL)
    {
        /* FIXME (bug 158): If this happens we should do something
           more drastic since it indicates that the database is FUBAR.
           The problem is that the member list and the membership list
           are not in sync. We could add the membership record. */

        kom_log("Membership and member record mismatch for pers %lu"
		" in conf %lu\n",
		(unsigned long)pers_no, (unsigned long)conf_no);
        kom_log("You should run dbck\n");
        err_stat = conf_no;
        kom_errno = KOM_NOT_MEMBER;
        return FAILURE;
    }

    /* Check type restrictions */
    if ((type->secret && !param.secret_memberships) ||
        (type->secret && conf_c->type.forbid_secret) ||
        (!param.allow_reinvite && type->invitation && !membership->type.invitation))
    {
        err_stat = 0;
        kom_errno = KOM_INVALID_MEMBERSHIP_TYPE;
        return FAILURE;
    }


    /* Modify member and membership */
    membership->type = *type;
    mbr->type = *type;
    
    /* Mark the conference and person as changed */
    mark_conference_as_changed(conf_no);
    mark_person_as_changed(pers_no);

    /* Return success */
    return OK;
}
