/*
 * $Id: person.c,v 0.86 2003/08/23 16:38:15 ceder Exp $
 * Copyright (C) 1991-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * person.c
 *
 * All atomic calls that deals with persons.
 */



#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <limits.h>
#include <stdio.h>
#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#endif
#ifdef HAVE_STRING_H
#  include <string.h>
#endif
#include "timewrap.h"
#ifdef HAVE_UNISTD_H
#  include <unistd.h>		/* for crypt() on Linux */
#endif
#include <setjmp.h>
#include <sys/types.h>
#ifdef HAVE_CRYPT_H
#  include <crypt.h>
#endif

#include "misc-types.h"
#include "s-string.h"
#include "kom-types.h"
#include "services.h"
#include "com.h"
#include "async.h"
#include "connections.h"
#include "kom-errno.h"
#include "manipulate.h"
#include "string-malloc.h"
#include "debug.h"
#include "cache.h"
#include "server/smalloc.h"
#include "kom-config.h"
#include "log.h"
#include "minmax.h"
#include "lyskomd.h"
#include "param.h"
#include "aux-items.h"
#include "local-to-global.h"
#include "kom-memory.h"
#include "server-time.h"
#include "send-async.h"
#include "stats.h"

BUGDECL;

/*
 * Static functions.
 */

static Bool
legal_passwd(const String pwd)
{
    int i;

    for (i = 0; i < s_strlen(pwd); i++)
	if (pwd.string[i] == '\0')
	    return FALSE;

    return TRUE; 
}

static Success
do_set_passwd( Password        pwd,
	       const String   new_pwd)
{
#ifdef ENCRYPT_PASSWORDS
    char salt[3];
    static char crypt_seed[] = 
      "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789./";
    char *password;

    if ( !legal_passwd(new_pwd) )
	return FAILURE;

    salt[0] = crypt_seed [rand() % (sizeof (crypt_seed) - 1)];
    salt[1] = crypt_seed [rand() % (sizeof (crypt_seed) - 1)];
    salt[2] = '\0';

    password = s_crea_c_str(new_pwd);
    if (password == NULL)
	return FAILURE;
    else
    {
	strcpy((char *)pwd, (const char *)crypt(password, salt));
	string_free(password);
	return OK;
    }

#else

    /* Compatibility mode... */
    if ( !legal_passwd(new_pwd) )
	return FAILURE;

    *pwd++ = new_pwd.len;		/* Indeed too easy crypt... */
    strncpy(pwd, (const char *)new_pwd.string,
	    min( PASSWD_LEN-1, new_pwd.len ));

    return OK;

#endif
}



/*
 * Mark a text. No check is done if pers_p is really allowed to mark it.
 * This function can mark a text with mark_type == 0.  If a previous mark
 * existed it is changed to the given mark_type.
 */

static Success
do_mark_text(Pers_no    pers_no,
	     Person    *pers_p,		/* May be NULL */
	     Text_no    text_no,
	     Text_stat *text_s,		/* May be NULL */
	     u_char     mark_type)
{
    Mark      *markp;
    Mark      *old_mark = NULL;
    int        i;

    if ( pers_p == NULL )
	GET_P_STAT(pers_p, pers_no, FAILURE);
	
    /* Locate the mark. */
    
    for ( i = pers_p->marks.no_of_marks, markp = pers_p->marks.marks;
	 i > 0 && old_mark == NULL;
	 i--, markp++ )
    {
	if ( markp->text_no == text_no )
	    old_mark = markp;
    }

    if (old_mark != NULL)
    {
	/* Change marktype of an already marked text. (No need to check
	   that the text exists). */
	BUG(("do_mark_text(): Change type of mark.\n"));
	
	old_mark->mark_type = mark_type;
	mark_person_as_changed(pers_no);
    }
    else
    {
	/* A new mark. Check that the text exists. */
	BUG(("do_mark_text(): new mark.\n"));

	if ( text_s == NULL )
	    GET_T_STAT(text_s, text_no, FAILURE);

	if (text_s->no_of_marks >= param.max_marks_text
	    || pers_p->marks.no_of_marks >= param.max_marks_person)
	{
            err_stat = text_no;
	    kom_errno = KOM_MARK_LIMIT;
	    return FAILURE;
	}

	text_s->no_of_marks++;
	mark_text_as_changed(text_no);
	
	pers_p->marks.marks = srealloc(pers_p->marks.marks,
			       ++pers_p->marks.no_of_marks * sizeof(Mark));
	pers_p->marks.marks[pers_p->marks.no_of_marks - 1].text_no = text_no;
	pers_p->marks.marks[pers_p->marks.no_of_marks - 1].mark_type
	    = mark_type;
	mark_person_as_changed(pers_no);
    }
    
    return OK;
}

/* Unmark a text. Returns a failure if the text was not marked. */

static Success
do_unmark_text(Pers_no    pers_no,
	       Person    *pers_p, /* May be NULL */
	       Text_no    text_no,
	       Text_stat *text_s) /* May be NULL */
{
    Mark      *markp;
    Mark      *old_mark = NULL;
    int        i;

    if ( pers_p == NULL )
	GET_P_STAT(pers_p, pers_no, FAILURE);
	
    /* Locate the mark. */
    
    for ( i = pers_p->marks.no_of_marks, markp = pers_p->marks.marks;
	 i > 0 && old_mark == NULL;
	 i--, markp++ )
    {
	if ( markp->text_no == text_no )
	    old_mark = markp;
    }

    if (old_mark != NULL)
    {
	/* Delete this mark. */
	BUG(("do_unmark_text(): Delete mark.\n"));
	
	while ( old_mark < ( pers_p->marks.marks
			    + pers_p->marks.no_of_marks - 1 ))
	{
	    *old_mark = *(old_mark + 1);
	    old_mark++;
	}

	pers_p->marks.no_of_marks--;
	mark_person_as_changed(pers_no);
	
	/* ...and don't forget to decrease the ref_cnt in the text_stat... */

	if ( (text_s = cached_get_text_stat(text_no)) != NULL )
	{
	    if ( text_s->no_of_marks == 0 )
	    {
		kom_log("WNG: do_unmark_text(): Text %lu has no_of_marks==0,"
			"  but person %d had marked the text.\n",
			(unsigned long)text_no, pers_no);
	    }
	    else
	    {
		text_s->no_of_marks--;
		mark_text_as_changed(text_no);
	    }
	}
	/* ...but it is not an error if someone has deleted the text. */
    }
    else
    {
	/* An attempt to delete a non-existent mark. */
	BUG(("do_unmark_text(): delete non-existent mark.\n"));
        err_stat = text_no;
	kom_errno = KOM_NOT_MARKED;
	return FAILURE;
    }
    
    return OK;
}


/*
 * Change user_area of a person. If text_no is 0, there will be
 * no user_area.
 */

static Success
do_set_user_area(Pers_no     pers_no,
		 Person	   * pers_p, /* May be NULL */
		 Text_no     new_area)
{
    Text_no old_area;
    Text_stat *old_stat;
    Text_stat *new_stat = NULL; /* To stop gcc complaining. */

    /* Check that the new user_area exists before deleting the old*/

    if (new_area != 0)
    {
	GET_T_STAT(new_stat, new_area, FAILURE);

        if (!text_read_access(active_connection, new_area, new_stat)
            && !ENA(admin, 2)) /* OK -- In an RPC call */
        {
            kom_errno = KOM_NO_SUCH_TEXT;
            err_stat = new_area;
            return FAILURE;
        }

	if (new_stat->no_of_marks >= param.max_marks_text)
	{
	    kom_log("LIMIT: set_user_area(%d, %lu): "
		    "New user_area's mark count (%d) > %d.\n",
		    pers_no,
		    (unsigned long)new_area,
		    new_stat->no_of_marks,
		    param.max_marks_text);

            err_stat = new_area;
	    kom_errno = KOM_MARK_LIMIT;
	    return FAILURE;
	}
    }

    if (pers_p == NULL)
	GET_P_STAT(pers_p, pers_no, FAILURE);
    
    /* Unmark the previous user_area if it exists. */

    old_area = pers_p->user_area;
    if (old_area != 0 && (old_stat = cached_get_text_stat(old_area)) != NULL)
    {
	if (old_stat->no_of_marks > 0)
	{
	    --old_stat->no_of_marks;
	    mark_text_as_changed(old_area);
	}
	else
	{
	    kom_log("ERROR: set_user_area(%d, %lu): "
		    "Old user_area %lu unmarked.\n",
		    pers_no, (unsigned long)new_area,
		    (unsigned long)old_area);
	}
    }

    /* Mark the new user_area */

    if (new_area != 0)
    {
	++new_stat->no_of_marks;
	mark_text_as_changed(new_area);
    }
    
    pers_p->user_area = new_area;
    mark_person_as_changed(pers_no);

    if (old_area != new_area)
	async_new_user_area(pers_no, old_area, new_area);

    return OK;
}
/*
 * End of static functions.
 */

/*
 * Functions that are exported to the server.
 */

/*
 * Delete a person. (The mailbox is not deleted).
 */

Success
do_delete_pers (Pers_no pers_no)
{
    Person * pers_p;
    int	     i;
    
    GET_P_STAT(pers_p, pers_no, FAILURE);

    /* Cancel membership in all confs. */
    /* Note that because of the way do_sub_member is written the */
    /* loop must be executed this way. */
    for ( i = pers_p->conferences.no_of_confs - 1; i >= 0; i-- )
    {
	if ( do_sub_member( pers_p->conferences.confs[ i ].conf_no, NULL, NULL,
			   pers_no, pers_p,
			   pers_p->conferences.confs + i) != OK)
	{
	    kom_log("ERROR: do_delete_pers(): can't sub_member\n");
	}
    }

    for ( i = pers_p->marks.no_of_marks; i > 0; i-- )
    {
	if ( do_unmark_text(pers_no, pers_p,
			    pers_p->marks.marks[0].text_no, NULL) != OK )
	{
	    kom_log("WNG: do_delete_pers(): can't unmark text %lu (i == %d)\n",
		(unsigned long)pers_p->marks.marks[0].text_no, i);
	}
    }
    
    if ( do_set_user_area(pers_no, pers_p, 0) != OK )
    {
	kom_log("WNG: do_delete_pers(): can't unmark user_area\n");
    }

    s_clear( &pers_p->username );
    l2g_clear(&pers_p->created_texts);

    cached_delete_person(pers_no);
    update_stat(STAT_PERSONS, -1);
    
    /* ASYNC */
    return OK;    
}

/*
 * Check a password
 */
Success
chk_passwd (Password      pwd,
	    const String  try)
{
#ifdef ENCRYPT_PASSWORDS
    char *c_try;

    c_try = s_crea_c_str(try);
    if (c_try == NULL)
	return FAILURE;
    
    if (strcmp ((const char *)pwd,
		(const char *)crypt(c_try, (const char *)pwd)) != 0)
    {
	string_free(c_try);
	return FAILURE;
    }
    else
    {
	string_free(c_try);
	return OK;
    }

#else

    int i;
    
    if ( try.len != *pwd )
	return FAILURE;

    for ( i = 0; i < try.len; i++ )
	if ( pwd[ i + 1 ] != try.string[ i ] )
	    return FAILURE;

    return OK;

#endif
}


/*
 * Mark a text.
 *
 * If the you already have marked the text the mark-type will be changed. 
 * If mark_type == 0 the mark will be deleted. You can only mark texts
 * that you are allowed to read. You are always allowed to read all
 * texts that you have marked even if you are no longer a recipient
 * of the text.
 */

extern Success
mark_text_old(Text_no text_no,
	      u_char mark_type)
{
    /* CHK_CONNECTION in mark_text and unmark_text */
    if (mark_type == 0)
	return unmark_text(text_no);
    else
	return mark_text(text_no, mark_type);
}

extern Success
mark_text(Text_no text_no,   /* Will fail if the user is not */
	  u_char mark_type)	/* allowed to read the text.	*/
{
    Text_stat *text_s = NULL;
        
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);

    /* Check that the user is allowed to read the text. */
    GET_T_STAT(text_s, text_no, FAILURE);
    if ( !text_read_access(active_connection, text_no, text_s) )
    {
        err_stat = text_no;
	kom_errno = KOM_NO_SUCH_TEXT;
	return FAILURE;
    }

    BUG(("Person %d markerar text %lu med typ %d\n",
	 ACTPERS, text_no, mark_type));
    
    return do_mark_text(ACTPERS, ACT_P, text_no, text_s, mark_type);
}

extern Success
unmark_text(Text_no text_no)
{
    Text_stat *text_s = NULL;
        
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    BUG(("Person %d avmarkerar text %lu\n", ACTPERS, text_no));
    
    return do_unmark_text(ACTPERS, ACT_P, text_no, text_s);
}

/*
 * Create a new person.
 *
 * If parameter ``Anyone can create new persons'' is true
 * anyone can create themself. Otherwise you must be logged in to
 * create a new person, and you must have the ``create_pers'' bit
 * set, or ``Anyone can create_new_persons'' must be set.
 *
 * If you are not logged in an auto-login as the created person is
 * performed. (This is so that it is impossible to create a million
 * persons (and so destroy the database) anonymously. This way it will
 * at least be possible to see from which machine the attack came).
 *
 * If you are logged in you will be supervisor and creator of the new
 * person, otherwise the new person will be supervisor and creator.
 *
 * The new person will be rd_prot, but not secret. He will have no
 * privileges, except maybe change_name (if ``Default change name
 * capability'' is true).
 *
 * This function returns the Pers_no of the new person, or 0 if an error
 * occured. (0 is an illegal Pers_no).
 */

static Pers_no
create_person_generic ( const String  name,
                        const String  passwd,
                        Aux_item_list *conf_aux,
                        Personal_flags flags,
                        Bool do_auto_login )
{
    Pers_no 	  new_user;
    Conference	* mailbox;
    Person	* pers_p;
    Membership_type mship_type;

    CHK_CONNECTION(0);
    if (ACTPERS == 0)
    {
	if (!param.create_person_before_login && ACTPERS == 0 )
	{
            err_stat = 0;
	    kom_errno = KOM_LOGIN;
	    return 0;
	}
    }
    else
    {
	if (param.anyone_can_create_new_persons == FALSE
	    && !HAVE_PRIV(ACT_P, create_pers))
	{
            err_stat = 0;
	    kom_errno = KOM_PERM;
	    return 0;
	}
    }

    if ( !legal_name( name ) )
    {
	/* kom_errno will be set by legal_name(). */
	return 0;
    }

    if ( !unique_name( name, 0 ) )
    {
        err_stat = 0;
	kom_errno = KOM_PERS_EXISTS;
	return 0;
    }

    if ( !legal_passwd( passwd ) )
    {
        err_stat = 0;
	kom_errno = KOM_PWD;
	return 0;
    }

    new_user = cached_create_conf( name );
    if (new_user == 0)
    {
        /* kom_errno and err_stat set in cached_create_conf */
        kom_log("ERROR: Couldn't create person. Too many conferences.\n");
        return 0;
    }

    if ( (mailbox = cached_get_conf_stat( new_user ) ) == NULL)
    {
	restart_kom("create_person() - can't get conf_stat");
    }
    
    mailbox->creator		= ACTPERS ? ACTPERS : new_user;
    mailbox->creation_time 	= current_time.tv_sec;
    mailbox->presentation	= 0;		/* No presentation yet */
    mailbox->supervisor		= ACTPERS ? ACTPERS : new_user;
    mailbox->permitted_submitters = 0;
    mailbox->super_conf		= ACTPERS;
    mailbox->type.rd_prot	= 1;
    mailbox->type.original	= 0;
    mailbox->type.secret	= 0;
    mailbox->type.letter_box	= 1;
    mailbox->type.allow_anon    = 1;
    mailbox->type.forbid_secret = 0;
    mailbox->type.reserved2     = 0;
    mailbox->type.reserved3     = 0;
    mailbox->last_written	= mailbox->creation_time;
    mailbox->msg_of_day		= 0;
    mailbox->nice		= param.default_nice;
    mailbox->keep_commented     = param.default_keep_commented;

    mark_conference_as_changed (new_user);

    /* allocate Person */

    if ( cached_create_person( new_user ) == FAILURE )
    {
	cached_delete_conf( new_user );

	/* FIXME (bug 159): The next line will call restart_kom(). */
	mark_conference_as_changed(new_user);

	return 0;
    }

    if ( ( pers_p = cached_get_person_stat( new_user )) == NULL )
    {
	cached_delete_conf( new_user );

	/* FIXME (bug 159): The next line will call restart_kom(). */
	mark_conference_as_changed(new_user);

	restart_kom("create_person() - can't get pers_stat");
    }

	
    /* Fill in Person */
    if ( do_set_passwd( pers_p->pwd, passwd) != OK )
	restart_kom("create_person(): can't set passwd\n");

    pers_p->privileges.change_name = param.default_change_name;
    pers_p->flags = flags;

    mark_person_as_changed( new_user );

    mship_type.invitation = 0;
    mship_type.passive = 0;
    mship_type.secret = 0;
    mship_type.passive_message_invert = 0;
    mship_type.reserved2 = 0;
    mship_type.reserved3 = 0;
    mship_type.reserved4 = 0;
    mship_type.reserved5 = 0;

    do_add_member( new_user, mailbox, new_user, pers_p,
                   ACTPERS ? ACTPERS : new_user,
                   UCHAR_MAX, 0, &mship_type, FALSE );

    prepare_aux_item_list(conf_aux, new_user);

    if (conf_stat_check_add_aux_item_list(mailbox,
                                          new_user,
                                          conf_aux,
                                          active_connection,
                                          TRUE) != OK)
    {
	/* FIXME (bug 160): Conf_no leak: We create a person, check the
	   aux-items, determine that the aux-items are bogus, and
	   immediately delete the person.  This leaks a conference
	   number.  We can live with that, but it isn't pretty.  */

        cached_delete_conf(new_user);
        cached_delete_person(new_user);

        return 0;
    }

    conf_stat_add_aux_item_list(mailbox,
                                new_user,
                                conf_aux,
                                new_user);

    mark_conference_as_changed(new_user);

    if ( ACTPERS )
    {
	ACT_P->created_persons++;
	mark_person_as_changed( ACTPERS );
    }
    else if (do_auto_login)
    {
	/* Auto login, always visible */
	login (new_user, passwd, FALSE);
    }

    update_stat(STAT_CONFS, 1);
    update_stat(STAT_PERSONS, 1);
    return new_user;
}


extern Pers_no
create_person_old(const String name,
                  const String passwd)
{
    Personal_flags flags;

    /* CHK_CONNECTION in create_person_generic */
    init_personal_flags(&flags);
    return create_person_generic(name, passwd, NULL, flags, TRUE);
}

extern Pers_no
create_person(const String name,
              const String passwd,
              Personal_flags flags,
              Aux_item_list *conf_aux)
{
    Pers_no     pers;

    /* CHK_CONNECTION in create_person_generic */
    pers = create_person_generic(name, passwd, conf_aux, flags, FALSE);
    if (pers != 0)
    {
        /* FIXME (bug 909): Send asynch message */
    }

    return pers;
}




/*
 * Get the person status of PERSON.
 */

extern Success
get_person_stat (Pers_no      person,
                 Person	    * result)
{
    Person *p_orig;
    enum access acc;
        
    CHK_CONNECTION(FAILURE);
    GET_P_STAT(p_orig, person, FAILURE);
    
    acc = access_perm(person, active_connection, unlimited);
  
    if ( acc == error )
	return FAILURE;

    if ( acc <= none )
    {
        err_stat = person;
	kom_errno = KOM_UNDEF_PERS;
	return FAILURE;
    }

    *result = *p_orig;	/* Make a copy of the struct. */
    
    /* The user area is normally secret. */

    if( acc != unlimited )
	result->user_area	= 0;
    
    return OK;
}

/* As get_person_stat, but only return the name if bit 0 in mask is set. */

extern  Success
get_person_stat_old (Pers_no	  person,
                     int 	  mask,
                     Person	* result)
{
    Person *p_orig;
    enum access acc;
        
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    
    GET_P_STAT(p_orig, person, FAILURE);
    
    acc = access_perm(person, active_connection, unlimited);
  
    if ( acc == error )
	return FAILURE;

    if ( acc <= none )
    {
        err_stat = person;
	kom_errno = KOM_UNDEF_PERS;
	return FAILURE;
    }

    *result = *p_orig;	/* Make a copy of the struct. */
    
    /* The user area is normally secret. */

    if( acc != unlimited )
	result->user_area	= 0;

    if ( !(mask & 1 ) )
	result->username = EMPTY_STRING;
    
    return OK;
}


extern  Success
get_created_texts(Pers_no       pers_no,
		  Local_text_no first,
		  u_long        len,
		  L2g_iterator * result)
{
    Person *pers_p;
    enum access acc;
    Local_text_no new_first;
    Local_text_no new_len;

    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);

    GET_P_STAT(pers_p, pers_no, FAILURE);

    acc = access_perm(pers_no, active_connection, read_protected);

    if (acc == error)
	return FAILURE;

    if (acc <= none)
    {
        err_stat = pers_no;
	kom_errno = KOM_UNDEF_PERS;
	return FAILURE;
    }

    new_first = l2g_next_key(&pers_p->created_texts, 0);
    if (new_first == 0)
	new_first = l2g_first_appendable_key(&pers_p->created_texts);
    new_first = max(new_first, first);

    if (first >= l2g_first_appendable_key(&pers_p->created_texts))
    {
        err_stat = first;
	kom_errno = KOM_NO_SUCH_LOCAL_TEXT;
	return FAILURE;
    }

    new_len = l2g_first_appendable_key(&pers_p->created_texts) - new_first;
    new_len = min(new_len, len);
    l2gi_searchsome(result, &pers_p->created_texts, new_first,
		    new_first + new_len);

    return OK;
}


extern Success
map_created_texts(Pers_no       pers_no,
		  Local_text_no	first_local_no,
		  unsigned long no_of_texts,
		  Text_mapping *result)
{
    Person *pers_p;
    enum access acc;

    CHK_CONNECTION(FAILURE);
    if (first_local_no == 0)
    {
	err_stat = first_local_no;
	kom_errno = KOM_LOCAL_TEXT_ZERO;
	return FAILURE;
    }

    if (no_of_texts > 255)
    {
	err_stat = 255;
	kom_errno = KOM_LONG_ARRAY;
	return FAILURE;
    }

    GET_P_STAT(pers_p, pers_no, FAILURE);

    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    acc = access_perm(pers_no, active_connection, read_protected);

    if (acc == error)
	return FAILURE;

    if (acc <= none)
    {
        err_stat = pers_no;
	kom_errno = KOM_UNDEF_PERS;
	return FAILURE;
    }

    if (first_local_no >= l2g_first_appendable_key(&pers_p->created_texts))
    {
        err_stat = first_local_no;
	kom_errno = KOM_NO_SUCH_LOCAL_TEXT;
	return FAILURE;
    }

    result->first = first_local_no;
    result->no_of_texts = no_of_texts;
    result->l2g = &pers_p->created_texts;

    return OK;
}


extern Success
map_created_texts_reverse(Pers_no               pers_no,
			  Local_text_no	        local_no_ceiling,
			  unsigned long         no_of_texts,
			  Text_mapping_reverse *result)
{
    Person       *pers_p;
    enum access   acc;
    Local_text_no ceiling;

    CHK_CONNECTION(FAILURE);

    if (no_of_texts > 255)
    {
	err_stat = 255;
	kom_errno = KOM_LONG_ARRAY;
	return FAILURE;
    }

    GET_P_STAT(pers_p, pers_no, FAILURE);

    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    acc = access_perm(pers_no, active_connection, read_protected);

    if (acc == error)
	return FAILURE;

    if (acc <= none)
    {
        err_stat = pers_no;
	kom_errno = KOM_UNDEF_PERS;
	return FAILURE;
    }

    ceiling = l2g_first_appendable_key(&pers_p->created_texts);
    if (local_no_ceiling == 0 || local_no_ceiling > ceiling)
	result->ceiling = ceiling;
    else
	result->ceiling = local_no_ceiling;

    result->no_of_texts = no_of_texts;
    result->l2g = &pers_p->created_texts;

    return OK;
}



/*
 * Set privilege bits of a person. You must have the wheel bit set
 * to be allowed to do this.
 */
extern Success
set_priv_bits(	Pers_no		person,
		Priv_bits	privileges )
{
    Person       *p;
    
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);

    GET_P_STAT(p, person, FAILURE);

    if ( ENA(wheel, 6) )        /* OK -- In an RPC call */
    {
	p->privileges = privileges;
	mark_person_as_changed( person );
	return OK;
    }
    else
    {
        err_stat = 0;
	kom_errno = KOM_PERM;
	return FAILURE;
    }
}

/*
 * Set password. You may set the password of yourself and all persons that
 * you are supervisor of. OLD_PWD is your password. 
 */
extern Success 
set_passwd (Pers_no	    person,
	    const String   old_pwd,	/* of the one who is changing the pwd,
					   not necessarily the person whose pwd
					   is changed. */
	    const String   new_pwd)	/* of person */
{
    Person *p;
    
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);

    GET_P_STAT(p, person, FAILURE);

    if (!is_supervisor(person, active_connection)
	&& !ENA(wheel, 7))	/* OK -- In an RPC call */
    {
        err_stat = person;
	kom_errno = KOM_PERM;
	return FAILURE;	   /* Not allowed to change the other persons pwd */
    }

    if ( chk_passwd( ACT_P->pwd, old_pwd ) == FAILURE )     
    {
        err_stat = person;
	kom_errno = KOM_PWD;
	return FAILURE;
    }

    if ( do_set_passwd( p->pwd, new_pwd ) != OK)
    {
        err_stat = 0;
	kom_errno = KOM_PWD;
	return FAILURE;
    }

    mark_person_as_changed( person );

    return OK;
}

/*
 * Ask which texts a person has read in a certain conference.
 * Can be done before login. Will return an empty membership if VICTIM
 * has his unread_is_secret-flag set. This can not be distinguished
 * from the case when VICTIM has not read any texts in CONF_NO. This
 * is a feature, not a bug.
 *
 * Will fail if VICTIM is not a member of CONF_NO.
 */

static Success
do_query_read_texts(Pers_no	  victim,
                    Conf_no	  conf_no,
		    Bool          want_read_ranges,
		    unsigned long max_ranges,
                    Membership   *result)
{
    Person	* victim_p;
    Membership  * membp;
    enum memb_visibility vis;

    CHK_CONNECTION(FAILURE);
    GET_P_STAT( victim_p, victim, FAILURE);

    if (!has_access(victim, active_connection, read_protected))
    {
        err_stat = victim;
	kom_errno = KOM_UNDEF_PERS;
	return FAILURE;
    }

    membp = locate_membership (conf_no, victim_p);
    if (membp == NULL)
    {
	set_conf_errno(active_connection, conf_no, KOM_NOT_MEMBER);
	return FAILURE;
    }

    vis = membership_visible(active_connection, victim, conf_no, membp->type,
			     FALSE, FALSE);
    if (vis == mv_none)
    {
	set_conf_errno(active_connection, conf_no, KOM_NOT_MEMBER);
        return FAILURE;
    }

    if (want_read_ranges == TRUE)
    {
	const Conference *conf_c;

	if ((conf_c = cached_get_conf_stat(conf_no)) == NULL)
	{
	    kom_log("do_query_read_texts: %lu is a member of %lu, but "
		    "that conference doesn't exist\n",
		    (unsigned long)victim,
		    (unsigned long)conf_no);
	    return FAILURE;
	}

	if (adjust_read(membp, conf_c) == TRUE)
	    mark_person_as_changed(victim);
    }

    *result = *membp;

    if (vis == mv_censor_unread)
    {
	result->last_time_read = NO_TIME;
	result->no_of_read_ranges = 0;
	result->read_ranges = NULL;
    }

    if (want_read_ranges)
    {
	if (max_ranges != 0 && result->no_of_read_ranges > max_ranges)
	    result->no_of_read_ranges = max_ranges;
    }
    else
    {
	result->read_ranges = NULL;
    }

    return OK;
}


extern Success
query_read_texts(Pers_no        pers_no,
		 Conf_no        conf_no,
		 Bool           want_read_ranges,
		 unsigned long  max_ranges,
		 Membership    *result)
{
    CHK_BOOL(want_read_ranges, FAILURE);

    /* CHK_CONNECTION in do_query_read_texts */
    return do_query_read_texts(pers_no,
                               conf_no,
			       want_read_ranges,
			       max_ranges,
                               result);
}

extern Success
query_read_texts_10(Pers_no	 victim,
		    Conf_no	 conf_no,
		    Membership  *result)
{
    /* CHK_CONNECTION in do_query_read_texts */
    return do_query_read_texts(victim,
                               conf_no,
			       TRUE,
			       0,
                               result);
}


extern Success
query_read_texts_old(Pers_no victim,
                     Conf_no conf_no,
                     Membership *result)
{
    /* CHK_CONNECTION in do_query_read_texts */
    return do_query_read_texts(victim, conf_no, TRUE, 0, result);
}



/*
 * Set user_area. The text is a text which has previously been created
 * with create_text(). It typically contains option-settings for the
 * clients, e. g. if you want to be interrupted when new mail arrives.
 * The format of this text is not yet defined.
 *
 * set_user_area(0) to clear the user area.
 */

extern Success
set_user_area(Pers_no	pers_no,
	      Text_no	user_area)
{
    Person *pers_p;
    enum access acc;

    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);

    GET_P_STAT(pers_p, pers_no, FAILURE);
    acc = access_perm(pers_no, active_connection, unlimited);
    if (acc <= none)
    {
        err_stat = pers_no;
	kom_errno = KOM_UNDEF_PERS;
	return FAILURE;
    }
    if (acc != unlimited)
    {
        err_stat = pers_no;
	kom_errno = KOM_PERM;
	return FAILURE;
    }

    return do_set_user_area (pers_no, pers_p, user_area);
}


/*
 * Set the personal flags
 */

extern Success
set_pers_flags(Pers_no pers_no,
               Personal_flags flags)
{
    Person *pers_p;
    enum access acc;

    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);

    GET_P_STAT(pers_p, pers_no, FAILURE);

    acc = access_perm(pers_no, active_connection, unlimited);
    if (acc <= none)
    {
        err_stat = pers_no;
        kom_errno = KOM_UNDEF_PERS;
        return FAILURE;
    }
    if (acc != unlimited)
    {
        err_stat = pers_no;
	kom_errno = KOM_PERM;
	return FAILURE;
    }

    pers_p->flags = flags;
    mark_person_as_changed(pers_no);

    return OK;
}
