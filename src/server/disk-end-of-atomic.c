/*
 * $Id: disk-end-of-atomic.c,v 0.29 2003/08/23 16:38:17 ceder Exp $
 * Copyright (C) 1991-1994, 1998-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * end-of-atomic.c
 *
 * This is the magic function which is called at the end of every atomic
 * call to the server. It is responisble for
 *	Free:ing all tmp_alloc:ated memory
 *	Throw out some cached data if necessary
 *	Forget some old texts if necessary
 *	Save some items to disk if saving
 *
 * idle is TRUE if the server has no pending calls. That might be a good time
 * to forget old texts.
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#include <sys/types.h>
#include "timewrap.h"

#include "misc-types.h"
#include "s-string.h"
#include "kom-types.h"
#include "end-of-atomic.h"
#include "cache.h"
#include "server/smalloc.h"
#include "kom-config.h"
#include "param.h"
#include "timeval-util.h"

struct timeval
end_of_atomic(void)
{
    struct timeval timeout;
    static int limit = 0;
    int parts_left;

    free_tmp();

    /* FIXME (bug 148): make limit_text_stat smarter instead. */
    if (limit++ > 100)
    {
	cache_limit_size();
	limit = 0;
    }

    for (parts_left = param.saved_items_per_call; parts_left > 0; --parts_left)
    {
	timeout = sync_part();
	if (timeval_nonzero(timeout))
	    return timeout;
    }
    return param.synctimeout;
}
