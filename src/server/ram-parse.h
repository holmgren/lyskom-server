/*
 * $Id: ram-parse.h,v 0.14 2003/08/23 16:38:14 ceder Exp $
 * Copyright (C) 1991, 1993-1995, 1997-1999, 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: ram-parse.h,v 0.14 2003/08/23 16:38:14 ceder Exp $
 *
 * ram-parse.h -- parse objects from disk file.
 */

extern unsigned long
fparse_long(FILE *fp);

extern void
fskipwhite(FILE *fp);

extern time_t
fparse_time(FILE *fp);

extern Success
fparse_info(FILE *fp,
            Info *info);

extern Success
fparse_conference(FILE *fp,
		 Conference *result);


Success
fparse_person(FILE *fp,
	     Person *person);

	
extern Success
fparse_membership_list(FILE *fp,
		      Membership_list *result);


extern Success
fparse_conf_list(FILE *fp,
		Conf_list_old *result);


extern Success
fparse_mark_list(FILE *fp,
		Mark_list *result);


extern Success
fparse_text_stat(FILE *fp,
		Text_stat *result);


extern Success
fparse_info(FILE *fp,
	   Info *result);


extern Success
fparse_string(FILE *fp,
	     String *result);

extern Success
fparse_member_list(FILE *fp,
		  Member_list *result);


extern Success
fparse_member(FILE *fp,
	     Member *result);

extern Success
fparse_mark(FILE *fp,
	   Mark *result);


extern Success
fparse_priv_bits(FILE *fp,
		Priv_bits *result);


extern Success
fparse_personal_flags(FILE *fp,
		     Personal_flags *result);

extern Success
fparse_conf_type(FILE *fp,
		Conf_type *result);


extern Success
fparse_who_info(FILE *fp,
	       Who_info *result);

    
extern Success
fparse_who_info_list(FILE *fp,
		     Who_info_list *result);

extern Success
fparse_aux_item_flags(FILE *fp,
                      Aux_item_flags *f);

extern Success
fparse_aux_item_link(FILE *fp,
                     Aux_item_link *link);
extern Success
fparse_aux_item(FILE *fp, 
                Aux_item *result);

extern Success
fparse_aux_item_list(FILE *fp,
		     Aux_item_list *result);

extern Success
fparse_misc_info(FILE *fp, 
		 Misc_info *result);

extern void
set_input_format(int fmt);
