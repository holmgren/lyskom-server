#
# $Id: prot-a-parse-arg-h.awk,v 0.7 2003/08/23 16:38:15 ceder Exp $
# Copyright (C) 1991, 1999, 2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 
#
# $Id: prot-a-parse-arg-h.awk,v 0.7 2003/08/23 16:38:15 ceder Exp $
BEGIN {
    printf("/* Don't edit this file - it is generated automatically");
    printf(" from\n   prot-a-parse-arg-h.awk and fncdef.txt */\n\n");
}
$1 == "#ifdef" {
    printf("#ifdef %s\n", $2);
    next;
}
$1 == "#endif" {
    printf("#endif\n");
    next;
}
$1 != "#" && $1 != "" {
    printf("void prot_a_parse_arg_%s(Connection *client);\n", $3);
}

END {    printf("/* end of this auto generated file. */\n");
}

    
