/*
 * $Id: prot-a-parse.h,v 0.23 2003/08/23 16:38:14 ceder Exp $
 * Copyright (C) 1991-1992, 1994-1999, 2002-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: prot-a-parse.h,v 0.23 2003/08/23 16:38:14 ceder Exp $
 *
 */
extern long
prot_a_parse_long(Connection *client);

extern void
prot_a_parse_num_list(Connection *client,
                      Number_list *res,
		      int maxlen);

extern void
prot_a_parse_priv_bits(Connection *client,
		       Priv_bits  *result);

extern void
prot_a_parse_pers_flags(Connection *client,
                        Personal_flags *res);

extern void
prot_a_parse_membership_type(Connection *client,
                             Membership_type *res);

extern void
prot_a_parse_conf_type(Connection *client,
		       Conf_type      *result);


extern void
prot_a_parse_string(Connection *client,
	     String	    *result,
	     int	     maxlen);

extern void
prot_a_parse_aux_item_flags(Connection *client,
                            Aux_item_flags *res);

extern void
prot_a_parse_aux_item(Connection *client,
                      Aux_item *result);

extern void
prot_a_parse_aux_item_list(Connection *client,
                           Aux_item_list *result,
                           int maxlen);

extern void
prot_a_parse_misc_info_list(Connection *client,
                            Misc_info_list  *result,
                            int maxlen);
extern void
prot_a_parse_misc_info(Connection *client,
		Misc_info      *result);

extern void
prot_a_parse_read_range_list(Connection *client,
			     struct read_range_list *res,
			     int maxlen);

extern void
prot_a_parse_time_date(Connection *client,
		       struct tm  *result);

extern void
prot_a_parse_info(Connection *client,
                  Info *info);

extern void
prot_a_hunt_nl(Connection *client);

extern void
prot_a_parse_skip_whitespace(Connection *client);
