/*
 * $Id: admin.c,v 0.61 2003/08/23 16:38:18 ceder Exp $
 * Copyright (C) 1991, 1993-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * admin.c
 *
 * Administrative calls.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#include <setjmp.h>
#include <sys/types.h>
#include "timewrap.h"
#include <signal.h>

#include "oop.h"

#include "misc-types.h"
#include "s-string.h"
#include "kom-types.h"
#include "com.h"
#include "async.h"
#include "connections.h"
#include "kom-errno.h"
#include "manipulate.h"
#include "cache.h"
#include "kom-config.h"
#include "log.h"
#include "send-async.h"
#include "param.h"
#include "string-malloc.h"
#include "services.h"
#include "version-info.h"
#include "aux-items.h"
#include "unused.h"
#include "sigflags.h"
#include "isc-interface.h"

/* All of the fields in this structure except the version number
   is set from the configuration file at startup (see ramkomd.c).
   The default values are also set in ramkomd.c; the values below are
   never used.  The defaults are as of this writing (1994-01-11
   19:57:22) equal to the values below, but don't trust that that is
   so when you read this. */

/* Actually, these values are (except for the server compatibility
   version number) stored in the database */

Info kom_info = 
{
#include "version.incl"
    ,				/* version */
    1,				/* conf_pres_conf */
    2,				/* pers_pres_conf */
    3,				/* motd_conf */
    4,				/* kom_news_conf */
    0,				/* motd_of_lyskom */
    0,                          /* highest_aux_no */
    { 0, NULL }                 /* aux_item_list */
};

/*
 * Return info about this server. This can (and should) be done
 * before logging in. motd_of_lyskom should be displayed before
 * prompting for username if it isn't 0.
 */
extern Success
get_info_old( Info *result )
{
    *result = kom_info;
    return OK;
}

extern Success
get_info( Info *result )
{
    Aux_item_list   filtered;

    CHK_CONNECTION(FAILURE);
    *result = kom_info;
    filter_aux_item_list(&result->aux_item_list,
                         &filtered,
                         active_connection);
    result->aux_item_list = filtered;
    return OK;
}

extern Success
get_version_info( Version_info *result )
{
    /* Allowed before login. */
    result->protocol_version = kom_version_info.protocol_version;
    result->server_name = s_fcrea_str(kom_version_info.server_name);
    result->server_version = s_fcrea_str(kom_version_info.server_version);
    return OK;
}

extern Success
set_info(Info *info)
{
    Conference     *conf;
    Text_stat      *text;
    Success         tmp;

    CHK_LOGIN(FAILURE);
    if ( !ENA(admin, 1) )       /* OK -- in an RPC call */
    {
        err_stat = 0;
	kom_errno = KOM_PERM;
	return FAILURE;
    }

    /* Check that everything mentioned exists */

    if (info->motd_of_lyskom != 0)
    {
	GET_T_STAT(text, info->motd_of_lyskom, FAILURE);
    }

    GET_C_STAT(conf, info->conf_pres_conf, FAILURE);
    GET_C_STAT(conf, info->pers_pres_conf, FAILURE);
    GET_C_STAT(conf, info->motd_conf, FAILURE);
    GET_C_STAT(conf, info->kom_news_conf, FAILURE);

    if ((tmp = set_motd_of_lyskom(info->motd_of_lyskom)) != OK)
        return tmp;

    kom_info.conf_pres_conf = info->conf_pres_conf;
    kom_info.pers_pres_conf = info->pers_pres_conf;
    kom_info.motd_conf = info->motd_conf;
    kom_info.kom_news_conf = info->kom_news_conf;

    return OK;
}

extern Success
set_motd_of_lyskom (Text_no motd)
{
    Text_stat *old_motd = NULL;
    Text_stat *new_motd = NULL;

    CHK_LOGIN(FAILURE);

    if ( !ENA(admin, 1) )       /* OK -- In an RPC call */
    {
        err_stat = 0;
	kom_errno = KOM_PERM;
	return FAILURE;
    }
    
    /* Check that the new motd exists before deleting the old*/

    if ( motd != 0 )
    {
	GET_T_STAT(new_motd, motd, FAILURE);
	if ( new_motd->no_of_marks >= param.max_marks_text )
	{
	    kom_log("LIMIT: set_motd_of_lyskom(%lu): New motd has %d marks.\n",
		(unsigned long)motd, new_motd->no_of_marks);
            err_stat = motd;
	    kom_errno = KOM_MARK_LIMIT;
	    return FAILURE;
	}
    }
    
    /* Unmark the previous motd if it exists. */

    if ( kom_info.motd_of_lyskom != 0
	&& (old_motd = cached_get_text_stat(kom_info.motd_of_lyskom)) != NULL)
    {
	if ( old_motd->no_of_marks > 0 )
	{
	    --old_motd->no_of_marks;
	    mark_text_as_changed( kom_info.motd_of_lyskom );
	}
	else
	{
	    kom_log("ERROR: set_motd_of_lyskom(): Old motd not marked.\n");
	}
    }

    /* Mark the new motd */

    if ( motd != 0 )
    {
	++new_motd->no_of_marks;
	mark_text_as_changed( motd );
    }
    
    /* FIXME (bug 912): send an async message about a new motd. */
    kom_info.motd_of_lyskom = motd;

    return OK;
}




/*
 * Force all clients to read a message.
 * Sends an asynchronous message. This is obsoleted by send_message.
 */
extern Success
broadcast (const String message)
{
    return send_message(0, message);
}

/*
 * Send a message
 */
extern Success
send_message (Conf_no recipient,
	      const String message)
{
    Conference *conf_c;
    unsigned short end;
    unsigned short ix;
    Success retval;

    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);

    /* Check that the message is not too long */

    if (s_strlen(message) > param.broadcast_len)
    {
        err_stat = param.broadcast_len;
	kom_errno = KOM_LONG_STR;
	return FAILURE;
    }

    /* If the recipient is not everyone, send it to all members */

    if (recipient != 0)
    {
        GET_C_STAT(conf_c, recipient, FAILURE);

        /* Check that the conference is not secret */

        if (!has_access(recipient, active_connection, read_protected))
        {
            err_stat = recipient;
            kom_errno = KOM_UNDEF_CONF;
            return FAILURE;
        }
        
        /* Conference is not secret. Traverse its members */

	end = conf_c->members.no_of_members;
	retval = FAILURE;
        err_stat = 0;
        kom_errno = KOM_MESSAGE_NOT_SENT;
        
	for (ix = 0; ix < end; ix++)
	{
            /* Don't send messages to passive members */

            if (conf_c->members.members[ix].type.passive ^
		conf_c->members.members[ix].type.passive_message_invert)
                continue;

            /* Send message to appropriate sessions */

	    if (async_send_group_message(conf_c->members.members[ix].member,
					 recipient,
                                         ACTPERS,
                                         message,
                                         ENA(admin, 1)) == OK)
	    {
		retval = OK;
	    }
	}
    }
    else
    {
        /* Attempting to broadcast */

        retval =  async_send_message(recipient, ACTPERS,
                                     message, ENA(admin, 1));
    }

    return retval;
}


/*
 * Make LysKOM sync its files.
 */
extern Success
sync_kom (void) 
{
    if (!param.permissive_sync)
    {
        CHK_LOGIN(FAILURE);
        if ( !ENA(admin, 1) )   /* OK -- In an RPC call */
        {
            err_stat = 0;
            kom_errno = KOM_PERM;
            return FAILURE;
        }
    }

    cache_sync_all();
    dump_statistics();
    return OK;
}

/*
 * Close LysKOM. exit_val is (currently) not used. The database is synced.
 */
extern Success
shutdown_kom (int UNUSED(exit_val))
{
    char *name;
    char *user;
    char *host;

    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    if ( !ENA(admin, 1) )       /* OK -- In an RPC call */
    {
        err_stat = 0;
	kom_errno = KOM_PERM;
	return FAILURE;
    }

    name = s_crea_c_str (active_connection->username);
    user = s_crea_c_str (active_connection->ident_user);
    host = s_crea_c_str (active_connection->isc_session->remote);
    kom_log("shutdown initiated by person %d (%s) via %s@%s.\n", 
            ACTPERS, name, user, host);
    string_free(host);
    string_free(user);
    string_free(name);
    
    go_and_die = TRUE;
    return OK;
}

extern Success
modify_system_info(Number_list      *items_to_delete,
                   Aux_item_list    *items_to_add)
{
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);

    if (items_to_delete->length > param.max_delete_aux)
    {
        kom_errno = KOM_LONG_ARRAY;
        err_stat = param.max_delete_aux;
        return FAILURE;
    }

    if (items_to_add->length > param.max_add_aux)
    {
        kom_errno = KOM_LONG_ARRAY;
        err_stat = param.max_add_aux;
        return FAILURE;
    }

    if ( !ENA(admin, 1) )       /* OK -- in an RPC call */
    {
        err_stat = 0;
	kom_errno = KOM_PERM;
	return FAILURE;
    }

    prepare_aux_item_list(items_to_add, ACTPERS);

    if (check_delete_aux_item_list(items_to_delete,
                                   &kom_info.aux_item_list,
                                   0)!=OK)
        return FAILURE;
    delete_aux_item_list(items_to_delete,
                         &kom_info.aux_item_list,
                         INFO_OBJECT_TYPE,
                         0, NULL);

    if (system_check_add_aux_item_list(&kom_info, items_to_add,
				       active_connection) != OK)
    {
        undelete_aux_item_list(items_to_delete, &kom_info.aux_item_list,
                                   INFO_OBJECT_TYPE,
                                   0, NULL);
        return FAILURE;
    }

    system_add_aux_item_list(&kom_info, items_to_add, ACTPERS);
    commit_aux_item_list(&kom_info.aux_item_list);

    return OK;
}



extern Success
get_collate_table (String * result)
{
    CHK_CONNECTION(FAILURE);
    result->string = DEFAULT_COLLAT_TAB;
    result->len = COLLAT_TAB_SIZE;
    return OK;
}
