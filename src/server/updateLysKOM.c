/*
 * $Id: updateLysKOM.c,v 1.25 2003/08/23 16:38:12 ceder Exp $
 * Copyright (C) 1994-1995, 1998-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#endif
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#ifdef HAVE_STRING_H
#  include <string.h>
#else
#  ifdef HAVE_STRINGS_H
#    include <strings.h>
#  endif
#endif
#ifndef HAVE_STRCHR
#  define strchr index
#endif
#include "timewrap.h"
#include <unistd.h>

#include "ldifftime.h"
#include "pidfile.h"
#include "kom-types.h"
#include "s-string.h"
#include "string-malloc.h"
#include "server/smalloc.h"
#include "kom-config.h"
#include "server-config.h"
#include "param.h"
#include "linkansi.h"
#include "eintr.h"

static void
usage(const char *arg0)
{
    fprintf(stderr, "usage: %s [-c config-file] [ -v ] [ -V ]\n",
	    arg0);
    exit(1);
}

static void
checkstatus(FILE *fp,
	    long pid,
	    char *progname)
{
    char lbuf[80];
    struct stat sbuf;
    char *sendmail;
    FILE *sendfp;

    if (fstat(fileno(fp), &sbuf) < 0)
    {
	perror("updateLysKOM: stat failed");
	exit(1);
    }

    if (pid != 0)
    {
	if (kill(pid, SIGTERM) != 0)
	{
	    if (errno != ESRCH)
	    {
		fprintf(stderr, "%s: kill(%ld, SIGTERM) failed",
			progname, pid);
		perror("");
		exit(1);
	    }
	}
	else
	{
	    /* The signal got through... */
	    if (ldifftime(time(NULL), sbuf.st_mtime) >
		60 * param.normal_shutdown_time)
	    {
		/* It takes very long for lyskomd to shut down.  Strange. */
		fprintf(stderr,
			"%s: Warning: it takes lyskomd a long time to die.\n"
			"Consider increasing ``Normal shutdown time'' "
			"in the config file if you get this message often.\n",
			progname);
		exit(1);
	    }
	}
    }

    if (ldifftime(time(NULL), sbuf.st_mtime) < 60 * param.downtime_mail_start)
    {
	/* NOP */
    }
    else if (ldifftime(time(NULL), sbuf.st_mtime)
	     < 60 * param.downtime_mail_end)
    {
	/* The first line of the file should be a mail address to send
	   a reminder to.  */

	if (fgets(lbuf, sizeof(lbuf), fp) == NULL
	    || strcmp(param.sendmail_path, ":") == 0)
	{
	    fprintf(stderr, 
		    "updateLysKOM: LysKOM has been down for a short while\n");
	    exit(2);
	}
	if (strchr(lbuf, '\n'))
	    *strchr(lbuf, '\n') = '\0';

	sendmail = smalloc(strlen(param.sendmail_path) + 4);
	strcpy(sendmail, param.sendmail_path);
	strcat(sendmail, " -t");

	sendfp = popen(sendmail, "w");
	if (sendfp == NULL)
	{
	    fprintf(stderr, "updateLysKOM: failed to open pipe to ");
	    perror(sendmail);
	    exit(2);
	}
	fprintf(sendfp, "From: %s\n", lbuf);
	fprintf(sendfp, "To: %s\n", lbuf);
	fprintf(sendfp, "Subject: lyskomd is down\n");
	fprintf(sendfp, "\n");
	fprintf(sendfp, "Reminder: LysKOM is still not running.\n");
	fprintf(sendfp, ".\n");
	fflush(sendfp);
	if (ferror(sendfp))
	{
	    fprintf(stderr, "writing to sendmail failed.\n");
	    exit(2);
	}

	if (pclose(sendfp) != 0)
	{
	    perror("updateLysKOM: sending mail might have failed");
	    exit(2);
	}
    }
    else
    {
	fprintf(stderr,
		"updateLysKOM: LysKOM has been down for a long time\n");
	exit(2);
    }
}
	
/* Ignore errors in this function. */
static void
savecore(void)
{
    struct stat statbuf;
    char *corefile;

    corefile = smalloc(strlen(param.core_dir) + 1 + 4 + 1);
    strcpy(corefile, param.core_dir);
    strcat(corefile, "/core");

    if (stat(corefile, &statbuf) == 0
	&& stat(param.savecore_path, &statbuf) == 0)
    {
	system(param.savecore_path);
    }
    sfree(corefile);
}

int
main (int    argc,
      char **argv)
{
    int i;
    FILE *fp;
    long pid;
    const char *config_file = NULL;

    if (getuid() == 0 || geteuid() == 0)
    {
	fprintf(stderr, "%s: this program should run as lyskom, not root\n",
		argv[0]);
	exit(1);
    }

    link_ansi();

    /* Initialize the string handling package. */
    s_set_storage_management(string_malloc, string_realloc, string_free);

    /* Parse command line arguments. */
    for (i = 1; i < argc && argv[i][0] == '-'; i++)
    {
	if (argv[i][1] == '\0' || argv[i][2] != '\0')
	    usage(argv[0]);

	switch (argv[i][1])
	{
	case 'c':
	    if (config_file != NULL)
	    {
		fprintf(stderr, "%s: -c may only be used once\n", argv[0]);
		exit(1);
	    }
	    if (++i >= argc)
		usage(argv[0]);
	    config_file = argv[i];
	    break;

	case 'V':
	case 'v':
	    fprintf(stderr, "updateLysKOM from %s-%s\n", PACKAGE, VERSION);
	    exit(0);

	default:
	    usage(argv[0]);
	}
    }
    
    if (i < argc)
	usage(argv[0]);

    /* Read in the configuration file. */

    if (config_file == NULL)
	config_file = get_default_config_file_name();

    read_configuration(config_file);
    free_default_config_file_name();

    pid = read_pid_file(param.pid_name, argv[0]);
    if (pid == 1)
    {
	fprintf(stderr, "%s: got pid %ld.\n", argv[0], pid);
	exit(1);
    }
	
    fp = i_fopen(param.status_file, "r");
    if (fp != NULL)
    {
	checkstatus(fp, pid, argv[0]);
	i_fclose(fp);
    }
    else
    {
	errno = ESRCH;		/* Set sane default if pid==0. */
	if (pid == 0 || kill(pid, SIGUSR1) != 0)
	{
	    if (errno != ESRCH)
	    {
		fprintf(stderr, "%s: kill(%ld, SIGUSR1) failed", argv[0], pid);
		perror("");
		exit(1);
	    }

	    savecore();

	    execl(param.lyskomd_path, "lyskomd", (char *)0);
	    fprintf(stderr, "%s: execl() failed: ", argv[0]);
	    perror("");
	    exit(1);
	}
    }

    exit(0);
}
