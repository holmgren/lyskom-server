/*
 * $Id: prot-a-output.c,v 0.71 2003/08/23 16:38:15 ceder Exp $
 * Copyright (C) 1991-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * prot-a-output.c  -  write objects through an ISC connection.
 *
 * Written by ceder 1990-07-13
 */



#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "timewrap.h"
#include <setjmp.h>
#include <sys/types.h>
#include <sys/socket.h>
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#include <assert.h>

#include "oop.h"

#include "s-string.h"
#include "kom-types.h"
#include "isc-interface.h"
#include "com.h"
#include "async.h"
#include "connections.h"
#include "prot-a-output.h"
#include "lyskomd.h"
#include "param.h"
#include "local-to-global.h"

void
prot_a_output_ul(Connection *fp,
		 unsigned long ul)
{
    isc_putc(' ', fp->isc_session);
    isc_putul(ul, fp->isc_session);
}

static void
prot_a_output_float(Connection *fp,
		    float v)
{
    char buf[80];

#ifdef HAVE_SNPRINTF
    snprintf(buf, sizeof(buf), " %g", v);
#else
    sprintf(buf, " %g", v);
#endif

    isc_puts(buf, fp->isc_session);
}

void
prot_a_output_person (Connection *fp,
                      Person *person)
{
    Local_text_no first_created = l2g_next_key(&person->created_texts, 0);
    Local_text_no uncreated = l2g_first_appendable_key(&person->created_texts);
    Local_text_no num_created;

    if (first_created == 0)
	first_created = uncreated;

    num_created = uncreated - first_created;

    prot_a_output_string (fp, person->username);
    prot_a_output_priv_bits (fp, person->privileges);
    prot_a_output_personal_flags (fp, person->flags);

    prot_a_output_time(fp, person->last_login);

    prot_a_output_ul(fp, person->user_area);
    prot_a_output_ul(fp, person->total_time_present);
    prot_a_output_ul(fp, person->sessions);
    prot_a_output_ul(fp, person->created_lines);
    prot_a_output_ul(fp, person->created_bytes);
    prot_a_output_ul(fp, person->read_texts);
    prot_a_output_ul(fp, person->no_of_text_fetches);
    prot_a_output_ul(fp, person->created_persons);
    prot_a_output_ul(fp, person->created_confs);
    prot_a_output_ul(fp, first_created);
    prot_a_output_ul(fp, num_created);
    prot_a_output_ul(fp, person->marks.no_of_marks);
    prot_a_output_ul(fp, person->conferences.no_of_confs);
}

void prot_a_output_membership_type(Connection *fp,
                                   const Membership_type type)
{
    isc_putc(' ', fp->isc_session);
    isc_putc(type.invitation + '0', fp->isc_session);
    isc_putc(type.passive    + '0', fp->isc_session);
    isc_putc(type.secret     + '0', fp->isc_session);
    isc_putc(type.passive_message_invert + '0', fp->isc_session);
    isc_putc(type.reserved2  + '0', fp->isc_session);
    isc_putc(type.reserved3  + '0', fp->isc_session);
    isc_putc(type.reserved4  + '0', fp->isc_session);
    isc_putc(type.reserved5  + '0', fp->isc_session);
}

void
prot_a_output_membership(Connection *fp,
			 const Membership *mship)
{
    unsigned int i;

    prot_a_output_ul(fp, mship->position);
    prot_a_output_time(fp, mship->last_time_read);
    prot_a_output_ul(fp, mship->conf_no);
    prot_a_output_ul(fp, mship->priority);

    prot_a_output_ul(fp, mship->no_of_read_ranges);
    if (mship->read_ranges != NULL && mship->no_of_read_ranges > 0)
    {
	isc_puts(" {", fp->isc_session);
	for (i = 0; i < mship->no_of_read_ranges; i++)
	{
	    prot_a_output_ul(fp, mship->read_ranges[i].first_read);
	    prot_a_output_ul(fp, mship->read_ranges[i].last_read);
	}
	isc_puts(" }", fp->isc_session);
    }
    else
	isc_puts(" *", fp->isc_session);

    prot_a_output_ul(fp, mship->added_by);
    prot_a_output_time(fp, mship->added_at);
    prot_a_output_membership_type(fp, mship->type);
}


void prot_a_output_membership_10(Connection *fp,
				 const Membership *mship)
{
    prot_a_output_ul(fp, mship->position);
    prot_a_output_membership_old(fp, mship);
    prot_a_output_ul(fp, mship->added_by);
    prot_a_output_time(fp, mship->added_at);
    prot_a_output_membership_type(fp, mship->type);
}

static void
prot_a_output_read_texts(Connection *fp,
			 const Membership *mship)
{
    if (mship->read_ranges == NULL)
	isc_puts(" 0 0 *", fp->isc_session);
    else
    {
	struct read_range *begin;
	struct read_range *end;

	assert(mship->read_ranges != NULL);
	assert(mship->no_of_read_ranges > 0);

	begin = &mship->read_ranges[0];
	end = begin + mship->no_of_read_ranges;

	if (begin->first_read == 1)
	{
	    prot_a_output_ul(fp, begin->last_read);
	    begin++;
	}
	else
	    isc_puts(" 0", fp->isc_session);

	if (begin == end)
	    isc_puts(" 0 *", fp->isc_session);
	else
	{
	    unsigned long no_of_read = 0;
	    const struct read_range *ptr;

	    for (ptr = begin; ptr < end; ++ptr)
		no_of_read += ptr->last_read - ptr->first_read + 1;

	    prot_a_output_ul(fp, no_of_read);
	    if (mship->skip_read_texts)
		isc_puts(" *", fp->isc_session);
	    else
	    {
		isc_puts(" {", fp->isc_session);

		for (ptr = begin; ptr < end; ++ptr)
		{
		    Local_text_no lno;

		    for (lno = ptr->first_read; lno <= ptr->last_read; lno++)
			prot_a_output_ul(fp, lno);
		}
	    
		isc_puts(" }", fp->isc_session);
	    }
	}
    }
}

void
prot_a_output_membership_old(Connection *fp,
			     const Membership *mship)
{
    prot_a_output_time(fp, mship->last_time_read );
    prot_a_output_ul(fp, mship->conf_no);
    prot_a_output_ul(fp, mship->priority);
    prot_a_output_read_texts(fp, mship);
}

	
void
prot_a_output_membership_list(Connection      *fp,
			      Membership_list  mlist)
{
    int i;
    
    prot_a_output_ul(fp, mlist.no_of_confs);

    if ( mlist.confs != NULL && mlist.no_of_confs > 0 )
    {
	isc_puts(" {", fp->isc_session);
	for ( i = 0; i < mlist.no_of_confs; i++)
	    prot_a_output_membership(fp, mlist.confs + i);
	isc_puts(" }", fp->isc_session);
    }
    else
	isc_puts(" *", fp->isc_session);
}

void
prot_a_output_membership_list_old (Connection	* fp,
                                   Membership_list	  mlist)
{
    int i;
    
    prot_a_output_ul(fp, mlist.no_of_confs);

    if ( mlist.confs != NULL && mlist.no_of_confs > 0 )
    {
	isc_puts(" {", fp->isc_session);
	for ( i = 0; i < mlist.no_of_confs; i++)
	    prot_a_output_membership_old(fp, mlist.confs + i);
	isc_puts(" }", fp->isc_session);
    }
    else
	isc_puts(" *", fp->isc_session);
}

void
prot_a_output_membership_list_10(Connection	 *fp,
				 Membership_list  mlist)
{
    int i;
    
    prot_a_output_ul(fp, mlist.no_of_confs);

    if ( mlist.confs != NULL && mlist.no_of_confs > 0 )
    {
	isc_puts(" {", fp->isc_session);
	for ( i = 0; i < mlist.no_of_confs; i++)
	    prot_a_output_membership_10(fp, mlist.confs + i);
	isc_puts(" }", fp->isc_session);
    }
    else
	isc_puts(" *", fp->isc_session);
}

void
prot_a_output_conf_list(Connection *fp,
			Conf_list_old conf_list)
{
    unsigned long i;

    prot_a_output_ul(fp, conf_list.no_of_conf_nos);
    if ( conf_list.conf_nos != NULL && conf_list.no_of_conf_nos > 0 )
    {
	isc_puts(" {", fp->isc_session);
	for ( i = 0; i < conf_list.no_of_conf_nos; i++ )
	    prot_a_output_ul(fp, conf_list.conf_nos[i]);
	isc_puts(" }", fp->isc_session);
    }
    else
	isc_puts(" *", fp->isc_session);

    if ( conf_list.type_of_conf != NULL && conf_list.no_of_conf_nos > 0 )
    {
	isc_puts(" {", fp->isc_session);
	for ( i = 0; i < conf_list.no_of_conf_nos; i++ )
	    prot_a_output_conf_type(fp, conf_list.type_of_conf[ i ]);
	isc_puts(" }", fp->isc_session);
    }
    else
	isc_puts(" *", fp->isc_session);
}

void
prot_a_output_conf_no_list(Connection *fp,
			   Conf_no_list conf_no_list)
{
    int i;

    prot_a_output_ul(fp, conf_no_list.no_of_confs);
    if ( conf_no_list.conf_nos != NULL && conf_no_list.no_of_confs > 0 )
    {
	isc_puts(" {", fp->isc_session);
	for ( i = 0; i < conf_no_list.no_of_confs; i++ )
	    prot_a_output_ul(fp, conf_no_list.conf_nos[i]);
	isc_puts(" }", fp->isc_session);
    }
    else
	isc_puts(" *", fp->isc_session);
}
    

void
prot_a_output_conference (Connection *fp,
                          Conference *conf_c)
{
    Local_text_no first_local_no = l2g_next_key(&conf_c->texts, 0);
    Local_text_no uncreated = l2g_first_appendable_key(&conf_c->texts);
    Local_text_no no_of_texts;

    if (first_local_no == 0)
	first_local_no = uncreated;

    no_of_texts = uncreated - first_local_no;

    prot_a_output_string(fp, conf_c->name);
    prot_a_output_extended_conf_type(fp, conf_c->type);
    prot_a_output_time(fp, conf_c->creation_time );
    prot_a_output_time(fp, conf_c->last_written );
    prot_a_output_ul(fp, conf_c->creator);
    prot_a_output_ul(fp, conf_c->presentation);
    prot_a_output_ul(fp, conf_c->supervisor);
    prot_a_output_ul(fp, conf_c->permitted_submitters);
    prot_a_output_ul(fp, conf_c->super_conf);
    prot_a_output_ul(fp, conf_c->msg_of_day);
    prot_a_output_ul(fp, conf_c->nice);
    prot_a_output_ul(fp, conf_c->keep_commented);
    prot_a_output_ul(fp, conf_c->members.no_of_members);
    prot_a_output_ul(fp, first_local_no);
    prot_a_output_ul(fp, no_of_texts);
    prot_a_output_ul(fp, conf_c->expire);
    prot_a_output_aux_item_list (fp, &conf_c->aux_item_list);
}

void
prot_a_output_conference_old (Connection *fp,
                              Conference *conf_c)
{
    Local_text_no first_local_no = l2g_next_key(&conf_c->texts, 0);
    Local_text_no uncreated = l2g_first_appendable_key(&conf_c->texts);
    Local_text_no no_of_texts;

    if (first_local_no == 0)
	first_local_no = uncreated;

    no_of_texts = uncreated - first_local_no;

    prot_a_output_string(fp, conf_c->name);

    prot_a_output_conf_type(fp, conf_c->type);

    prot_a_output_time(fp, conf_c -> creation_time );
    prot_a_output_time(fp, conf_c -> last_written );

    prot_a_output_ul(fp, conf_c->creator);
    prot_a_output_ul(fp, conf_c->presentation);
    prot_a_output_ul(fp, conf_c->supervisor);
    prot_a_output_ul(fp, conf_c->permitted_submitters);
    prot_a_output_ul(fp, conf_c->super_conf);
    prot_a_output_ul(fp, conf_c->msg_of_day);
    prot_a_output_ul(fp, conf_c->nice);
    prot_a_output_ul(fp, conf_c->members.no_of_members);
    prot_a_output_ul(fp, first_local_no);
    prot_a_output_ul(fp, no_of_texts);
}

void prot_a_output_uconference(Connection *fp,
			       Small_conf *conf_c)
{
    prot_a_output_string(fp, conf_c -> name);
    prot_a_output_extended_conf_type(fp, conf_c -> type);
    prot_a_output_ul(fp, conf_c->highest_local_no);
    prot_a_output_ul(fp, conf_c->nice);
}


void
prot_a_output_mark_list(Connection *fp,
			Mark_list mark_list)
{
    int i;

    prot_a_output_ul(fp, mark_list.no_of_marks);

    if ( mark_list.marks != NULL && mark_list.no_of_marks > 0 )
    {
	isc_puts(" {", fp->isc_session);
	for ( i = 0; i < mark_list.no_of_marks; i++ )
	    prot_a_output_mark(fp, mark_list.marks[ i ]);
	isc_puts(" }", fp->isc_session);
    }
    else
	isc_puts(" *", fp->isc_session);
    
}

void
prot_a_output_aux_item_flags(Connection *fp,
                             Aux_item_flags flags)
{
    isc_putc(' ', fp->isc_session);
    isc_putc(flags.deleted + '0', fp->isc_session);
    isc_putc(flags.inherit + '0', fp->isc_session);
    isc_putc(flags.secret + '0', fp->isc_session);
    isc_putc(flags.hide_creator + '0', fp->isc_session);
    isc_putc(flags.dont_garb + '0', fp->isc_session);
    isc_putc(flags.reserved3 + '0', fp->isc_session);
    isc_putc(flags.reserved4 + '0', fp->isc_session);
    isc_putc(flags.reserved5 + '0', fp->isc_session);
}

void
prot_a_output_aux_item(Connection *fp,
                       Aux_item *item)
{
    prot_a_output_ul(fp, item->aux_no);
    prot_a_output_ul(fp, item->tag);
    prot_a_output_ul(fp, item->creator);
    prot_a_output_time(fp, item->sent_at);
    prot_a_output_aux_item_flags(fp, item->flags);
    prot_a_output_ul(fp, item->inherit_limit);
    prot_a_output_string(fp, item->data);
}

void
prot_a_output_text_stat_old(Connection *fp,
                            Text_stat *t_stat)
{
    int i;

    prot_a_output_time(fp, t_stat->creation_time);
    
    prot_a_output_ul(fp, t_stat->author);
    prot_a_output_ul(fp, t_stat->no_of_lines);
    prot_a_output_ul(fp, t_stat->no_of_chars);
    prot_a_output_ul(fp, t_stat->no_of_marks);
    prot_a_output_ul(fp, t_stat->no_of_misc);

    if ( t_stat->misc_items != NULL && t_stat->no_of_misc > 0 )
    {
	isc_puts(" {", fp->isc_session);
	for ( i = 0; i < t_stat->no_of_misc; i++ )
	    prot_a_output_misc_info(fp, t_stat->misc_items[ i ]);
	isc_puts(" }", fp->isc_session);
    }
    else
	isc_puts(" *", fp->isc_session);
}	

void
prot_a_output_aux_item_list(Connection *fp,
			    Aux_item_list *aux)
{
  int i;

    prot_a_output_ul(fp, aux->length);

    if ( aux->items != NULL && aux->length > 0 )
    {
        isc_puts(" {", fp->isc_session);
        for (i = 0; i < aux->length; i++)
            prot_a_output_aux_item(fp, &aux->items[i]);
        isc_puts(" }", fp->isc_session);
    }
    else
	isc_puts(" *", fp->isc_session);
}

void
prot_a_output_text_stat(Connection *fp,
                        Text_stat *t_stat)
{
    prot_a_output_text_stat_old(fp, t_stat);
    prot_a_output_aux_item_list(fp, &t_stat->aux_item_list);
}	


static void
prot_a_output_who_info_ident(Connection *fp,
			     Who_info_ident *info)
{
    prot_a_output_ul(fp, info->person);
    prot_a_output_ul(fp, info->working_conference);
    prot_a_output_ul(fp, info->session_no);

    prot_a_output_string(fp, info->what_am_i_doing);
    prot_a_output_string(fp, info->username);
    prot_a_output_string(fp, info->hostname);
    prot_a_output_string(fp, info->ident_user);
}


void
prot_a_output_who_info(Connection *fp,
		       Who_info *info)
{
    prot_a_output_ul(fp, info->person);
    prot_a_output_ul(fp, info->working_conference);
    prot_a_output_ul(fp, info->session_no);

    prot_a_output_string(fp, info->what_am_i_doing); 
    prot_a_output_string(fp, info->username);
}


void
prot_a_output_who_info_list(Connection *fp,
			    Who_info_list info)
{
    int i;
    
    prot_a_output_ul(fp, info.no_of_persons);

    if ( info.info != NULL && info.no_of_persons > 0 )
    {
	isc_puts(" {", fp->isc_session);
	for ( i = 0; i < info.no_of_persons; i++ )
	{
	    prot_a_output_who_info(fp, &info.info[ i ]);
	    /* The username is specially alloced in who_is_on() in
	       session.c. */
	    s_clear(&info.info[i].username);
	}
	isc_puts(" }", fp->isc_session);
    }
    else
	isc_puts(" *", fp->isc_session);
}

void
prot_a_output_who_info_ident_list(Connection *fp,
				  Who_info_ident_list info)
{
    int i;
    
    prot_a_output_ul(fp, info.no_of_persons);

    if ( info.info != NULL && info.no_of_persons > 0 )
    {
	isc_puts(" {", fp->isc_session);
	for ( i = 0; i < info.no_of_persons; i++ )
	{
	    prot_a_output_who_info_ident(fp, &info.info[ i ]);
	}
	isc_puts(" }", fp->isc_session);
    }
    else
	isc_puts(" *", fp->isc_session);
}

void
prot_a_output_who_info_list_old(Connection *fp,
				Who_info_list_old info)
{
    int i;
    
    prot_a_output_ul(fp, info.no_of_persons);

    if ( info.info != NULL && info.no_of_persons > 0 )
    {
	isc_puts(" {", fp->isc_session);
	for ( i = 0; i < info.no_of_persons; i++ )
	{
	    prot_a_output_ul(fp, info.info[i].person);
	    prot_a_output_ul(fp, info.info[i].working_conference);
	    
	    prot_a_output_string(fp, info.info[ i ].what_am_i_doing);
	}
	isc_puts(" }", fp->isc_session);
    }
    else
	isc_puts(" *", fp->isc_session);
}

void
prot_a_output_session_info(Connection *fp,
			   Session_info *info)
{
    prot_a_output_ul(fp, info->person);
    prot_a_output_ul(fp, info->working_conference);
    prot_a_output_ul(fp, info->session);

    prot_a_output_string(fp, info->what_am_i_doing);
    prot_a_output_string(fp, info->username);

    prot_a_output_ul(fp, info->idle_time);
    prot_a_output_time(fp, info->connection_time);
}

void
prot_a_output_session_info_ident(Connection *fp,
				 Session_info_ident *info)
{
    prot_a_output_ul(fp, info->person);
    prot_a_output_ul(fp, info->working_conference);
    prot_a_output_ul(fp, info->session);

    prot_a_output_string(fp, info->what_am_i_doing);
    prot_a_output_string(fp, info->username);
    prot_a_output_string(fp, info->hostname);
    prot_a_output_string(fp, info->ident_user);

    prot_a_output_ul(fp, info->idle_time);
    prot_a_output_time(fp, info->connection_time);
}
    
void
prot_a_output_info(Connection *fp,
                   Info *info)
{
    prot_a_output_info_old(fp, info);
    prot_a_output_aux_item_list(fp, &info->aux_item_list);
}

void
prot_a_output_info_old(Connection *fp,
                       Info *info)
{
    prot_a_output_ul(fp, info->version);
    prot_a_output_ul(fp, info->conf_pres_conf);
    prot_a_output_ul(fp, info->pers_pres_conf);
    prot_a_output_ul(fp, info->motd_conf);
    prot_a_output_ul(fp, info->kom_news_conf);
    prot_a_output_ul(fp, info->motd_of_lyskom);
}


extern void
prot_a_output_string(Connection *fp,
		     String str)
{
    prot_a_output_ul(fp, str.len);
    isc_putc('H', fp->isc_session);
    isc_write(fp->isc_session, str.string, str.len);
}


extern void
prot_a_output_priv_bits(Connection *fp,
			Priv_bits bits)
{
    isc_putc(' ', fp->isc_session);
    isc_putc(bits.wheel + '0', fp->isc_session);
    isc_putc(bits.admin + '0', fp->isc_session);
    isc_putc(bits.statistic + '0', fp->isc_session);
    isc_putc(bits.create_pers + '0', fp->isc_session);
    isc_putc(bits.create_conf + '0', fp->isc_session);
    isc_putc(bits.change_name + '0', fp->isc_session);
    isc_putc(bits.flg7 + '0', fp->isc_session);
    isc_putc(bits.flg8 + '0', fp->isc_session);
    isc_putc(bits.flg9 + '0', fp->isc_session);
    isc_putc(bits.flg10 + '0', fp->isc_session);
    isc_putc(bits.flg11 + '0', fp->isc_session);
    isc_putc(bits.flg12 + '0', fp->isc_session);
    isc_putc(bits.flg13 + '0', fp->isc_session);
    isc_putc(bits.flg14 + '0', fp->isc_session);
    isc_putc(bits.flg15 + '0', fp->isc_session);
    isc_putc(bits.flg16 + '0', fp->isc_session);
}


extern void
prot_a_output_personal_flags(Connection *fp,
			     Personal_flags flags)
{
    isc_putc(' ', fp->isc_session);
    isc_putc(flags.unread_is_secret + '0', fp->isc_session);
    isc_putc(flags.flg2 + '0', fp->isc_session);
    isc_putc(flags.flg3 + '0', fp->isc_session);
    isc_putc(flags.flg4 + '0', fp->isc_session);
    isc_putc(flags.flg5 + '0', fp->isc_session);
    isc_putc(flags.flg6 + '0', fp->isc_session);
    isc_putc(flags.flg7 + '0', fp->isc_session);
    isc_putc(flags.flg8 + '0', fp->isc_session);
}	

extern void
prot_a_output_conf_type(Connection *fp,
			Conf_type type)
{
    isc_putc(' ', fp->isc_session);
    isc_putc(type.rd_prot + '0', fp->isc_session);
    isc_putc(type.original + '0', fp->isc_session);
    isc_putc(type.secret + '0', fp->isc_session);
    isc_putc(type.letter_box + '0', fp->isc_session);
}

extern void
prot_a_output_extended_conf_type(Connection *fp,
				 Conf_type type)
{
    isc_putc(' ', fp->isc_session);
    isc_putc(type.rd_prot + '0', fp->isc_session);
    isc_putc(type.original + '0', fp->isc_session);
    isc_putc(type.secret + '0', fp->isc_session);
    isc_putc(type.letter_box + '0', fp->isc_session);
    isc_putc(type.allow_anon + '0', fp->isc_session);
    isc_putc(type.forbid_secret + '0', fp->isc_session);
    isc_putc(type.reserved2 + '0', fp->isc_session);
    isc_putc(type.reserved3 + '0', fp->isc_session);
}

extern void
prot_a_output_member_list(Connection *fp,
                          Member_list m_list)
{
    int i;

    prot_a_output_ul(fp, m_list.no_of_members);
    if ( m_list.members != NULL && m_list.no_of_members > 0 )
    {
	isc_puts(" {", fp->isc_session);
	for ( i = 0; i < m_list.no_of_members; i++ )
	    prot_a_output_member(fp, m_list.members[ i ]);
	isc_puts(" }", fp->isc_session);
    }
    else
	isc_puts(" *", fp->isc_session);
}

extern void
prot_a_output_member_list_old(Connection *fp,
                              Member_list m_list)
{
    int i;

    prot_a_output_ul(fp, m_list.no_of_members);
    if ( m_list.members != NULL && m_list.no_of_members > 0 )
    {
	isc_puts(" {", fp->isc_session);
	for ( i = 0; i < m_list.no_of_members; i++ )
	    prot_a_output_member_old(fp, m_list.members[ i ]);
	isc_puts(" }", fp->isc_session);
    }
    else
	isc_puts(" *", fp->isc_session);
}


void
prot_a_output_member(Connection *fp,
		     Member member)
{
    prot_a_output_ul(fp, member.member);
    prot_a_output_ul(fp, member.added_by);
    prot_a_output_time(fp, member.added_at);
    prot_a_output_membership_type(fp, member.type);
}

void
prot_a_output_member_old(Connection *fp,
		     Member member)
{
    prot_a_output_ul(fp, member.member);
}

    
extern void
prot_a_output_mark(Connection *fp,
		   Mark mark)
{
    prot_a_output_ul(fp, mark.text_no);
    prot_a_output_ul(fp, mark.mark_type);
}


extern void
prot_a_output_misc_info(Connection *fp, 
			Misc_info misc)
{
    prot_a_output_ul(fp, misc.type);
    
    switch(misc.type)
    {
    case recpt:
    case cc_recpt:
    case bcc_recpt:
	prot_a_output_ul(fp, misc.datum.recipient);
	break;
	
    case loc_no:
	prot_a_output_ul(fp, misc.datum.local_no);
	break;
	
    case rec_time:
	prot_a_output_time(fp, misc.datum.received_at);
	break;
	
    case comm_to:
    case comm_in:
    case footn_to:
    case footn_in:
	prot_a_output_ul(fp, misc.datum.text_link);
	break;
	
    case sent_by:
	prot_a_output_ul(fp, misc.datum.sender);
	break;
	
    case sent_at:
	prot_a_output_time(fp, misc.datum.sent_at);
	break;

#ifndef COMPILE_CHECKS
    default:
#endif
    case unknown_info:
	restart_kom("prot_a_output_misc_info: Illegal misc\n");
    }
}


void
prot_a_output_time(Connection *fp,
		   time_t clk)
{
    struct tm *t;

    if (fp->use_utc)
	t = gmtime(&clk);
    else
	t = localtime(&clk);

    prot_a_output_ul(fp, t->tm_sec);
    prot_a_output_ul(fp, t->tm_min);
    prot_a_output_ul(fp, t->tm_hour);
    prot_a_output_ul(fp, t->tm_mday);
    prot_a_output_ul(fp, t->tm_mon);
    prot_a_output_ul(fp, t->tm_year);
    prot_a_output_ul(fp, t->tm_wday);
    prot_a_output_ul(fp, t->tm_yday);
    prot_a_output_ul(fp, t->tm_isdst);
}


void
prot_a_output_session_no(Connection *fp,
			 Session_no session_no)
{
    prot_a_output_ul(fp, session_no);
}

void
prot_a_output_text_no(Connection *fp,
		      Text_no text)
{
    prot_a_output_ul(fp, text);
}

void
prot_a_output_conf_no(Connection *fp,
		      Conf_no conf)
{
    prot_a_output_ul(fp, conf);
}

static void
prot_a_output_conf_z_info (Connection  *fp,
			   Conf_z_info *conf_c)
{
    prot_a_output_string(fp, conf_c->name);
    prot_a_output_conf_type(fp, conf_c->type);
    prot_a_output_ul(fp,  conf_c->conf_no);
}

extern void
prot_a_output_conf_z_info_list(Connection *fp,
			       Conf_z_info_list c_list)
{
    int i;

    prot_a_output_ul(fp, c_list.no_of_confs);
    if ( c_list.confs != NULL && c_list.no_of_confs > 0 )
    {
	isc_puts(" {", fp->isc_session);
	for ( i = 0; i < c_list.no_of_confs; i++ )
	    prot_a_output_conf_z_info(fp, &c_list.confs[ i ]);
	isc_puts(" }", fp->isc_session);
    }
    else
	isc_puts(" *", fp->isc_session);
}

void
prot_a_output_version_info (Connection  *fp,
			    Version_info *v_info)
{
    prot_a_output_ul(fp, v_info->protocol_version);
    prot_a_output_string(fp, v_info->server_name);
    prot_a_output_string(fp, v_info->server_version);
}

void
prot_a_output_num_list (Connection *fp,
                        Number_list *num_list)
{
    int i;
    
    prot_a_output_ul(fp, num_list->length);
    if (num_list->length == 0)
        isc_puts(" *", fp->isc_session);
    else
    {
        isc_puts(" {", fp->isc_session);
        for (i = 0; i < num_list->length; i++)
            prot_a_output_ul(fp, num_list->data[i]);
        isc_puts(" }", fp->isc_session);
    }
}

static void
prot_a_output_session_flags(Connection *fp,
			    Session_flags flags)
{
    isc_putc(' ', fp->isc_session);
    isc_putc(flags.invisible + '0', fp->isc_session);
    isc_putc(flags.user_active_used + '0', fp->isc_session);
    isc_putc(flags.user_absent + '0', fp->isc_session);
    isc_putc(flags.reserved3 + '0', fp->isc_session);
    isc_putc(flags.reserved4 + '0', fp->isc_session);
    isc_putc(flags.reserved5 + '0', fp->isc_session);
    isc_putc(flags.reserved6 + '0', fp->isc_session);
    isc_putc(flags.reserved7 + '0', fp->isc_session);
}	
static void
prot_a_output_dynamic_session_info (Connection *fp,
				    Dynamic_session_info *info)
{
    prot_a_output_ul(fp, info->session);
    prot_a_output_ul(fp, info->person);
    prot_a_output_ul(fp, info->working_conference);
    prot_a_output_ul(fp, info->idle_time);
    prot_a_output_session_flags(fp, info->flags);
    prot_a_output_string(fp, info->what_am_i_doing);
}

void
prot_a_output_static_session_info (Connection *fp,
				   Static_session_info *info)
{
    prot_a_output_string(fp, info->username);
    prot_a_output_string(fp, info->hostname);
    prot_a_output_string(fp, info->ident_user);
    prot_a_output_time(fp, info->connection_time);
}

void
prot_a_output_dynamic_session_info_list (Connection *fp,
					 Dynamic_session_info_list *list)
{
    long i;

    prot_a_output_ul(fp, list->no_of_sessions);
    if (list->no_of_sessions == 0)
	isc_puts(" *", fp->isc_session);
    else
    {
	isc_puts(" {", fp->isc_session);
	for (i = 0; i < list->no_of_sessions; i++)
	    prot_a_output_dynamic_session_info(fp, &list->sessions[i]);
	isc_puts(" }", fp->isc_session);
    }
}

void
prot_a_output_l2g_iterator_as_text_list(Connection *fp,
					L2g_iterator *iter)
{
    Local_text_no lno = l2gi_begin(iter);
    Local_text_no limit = l2gi_end(iter);

    prot_a_output_ul(fp, lno);
    prot_a_output_ul(fp, limit - lno);

    if (limit - lno > 0)
    {
	isc_puts(" {", fp->isc_session);
	for (; !iter->search_ended; l2gi_next(iter))
	{
	    /* Write the correct number of zeroes. */
	    for (; lno < iter->lno; ++lno)
		isc_puts(" 0", fp->isc_session);
	    /* Write this piece of data. */
	    prot_a_output_ul(fp, iter->tno);
	    ++lno;
	}
	/* Write any trailing zeroes */
	for (; lno < limit; ++lno)
	    isc_puts(" 0", fp->isc_session);
	isc_puts(" }", fp->isc_session);
    }
    else
	isc_puts(" *", fp->isc_session);
}

void
prot_a_output_text_mapping(Connection *fp,
			   Text_mapping *map)
{
    Local_text_no lowest;
    Local_text_no highest;
    Local_text_no limit;
    Local_text_no zeroes = 0;
    Local_text_no nonzeroes = 0;
    L2g_iterator iter;

    /* Initialize iter to something. Otherwise some tools will flag
       a read uninitialized memory in the for initializer. */

    iter.l2g = NULL;
    iter.binfo = NULL;
    iter.arrindex = 0;
    iter.beginval = 0;
    iter.endval = 0;
    iter.search_ended = 0;
    iter.lno = 0;
    iter.tno = 0;
    
    /* Count the number of internal zeroes to determine if we should
       use a dense or sparse representation. */

    for (l2gi_searchsome(&iter, map->l2g, map->first, 0),
	     highest = (lowest = iter.search_ended?map->first:iter.lno) - 1;
	 !iter.search_ended && nonzeroes < map->no_of_texts;
	 l2gi_next(&iter))
    {
	zeroes += (iter.lno - highest) - 1;
	nonzeroes++;
	highest = iter.lno;
    }

    limit = highest + 1;

    prot_a_output_ul(fp, map->first);
    if (iter.search_ended)
	prot_a_output_ul(fp, l2g_first_appendable_key(map->l2g));
    else
	prot_a_output_ul(fp, limit);

    /* Emit the "more-texts-exists" flag. */

    isc_puts(iter.search_ended ? " 0" : " 1", fp->isc_session);


    /* Emit the block, either a sparse or a dense one. */

    if (nonzeroes == 0)
    {
	/* Special case: an empty array.  Use the sparse format so
	   that we don't have to philosophize about what the lower
	   limit in the text-list should be. */
	isc_puts(" 0 0 *", fp->isc_session);
    }
    else if (zeroes >= nonzeroes)
    {
	/* There are at least as many zeroes as real mappings, so use
	   a sparse representation. */

	isc_puts(" 0", fp->isc_session);
	prot_a_output_ul(fp, nonzeroes);
	isc_puts(" {", fp->isc_session);

	for (l2gi_searchsome(&iter, map->l2g, map->first, limit);
	     !iter.search_ended;
	     l2gi_next(&iter))
	{
	    prot_a_output_ul(fp, iter.lno);
	    prot_a_output_ul(fp, iter.tno);
	}
	isc_puts(" }", fp->isc_session);
    }
    else
    {
	/* Emit a dense block. */
	isc_puts(" 1", fp->isc_session);
	prot_a_output_ul(fp, lowest);
	prot_a_output_ul(fp, zeroes + nonzeroes);
	isc_puts(" {", fp->isc_session);
	highest = lowest;
	for (l2gi_searchsome(&iter, map->l2g, map->first, limit);
	     !iter.search_ended;
	     l2gi_next(&iter))
	{
	    while (highest++ < iter.lno)
		isc_puts(" 0", fp->isc_session);
	    prot_a_output_ul(fp, iter.tno);
	}
	isc_puts(" }", fp->isc_session);
    }
}

void
prot_a_output_text_mapping_reverse(Connection *fp,
				   Text_mapping_reverse *map)
{
    Local_text_no lowest;
    Local_text_no zeroes = 0;
    Local_text_no nonzeroes = 0;
    L2g_iterator iter;
    L2g_reverse_iterator riter;

    /* Initialize iterators to something. Otherwise some tools will flag
       a read uninitialized memory in the for initializer. */

    iter.l2g = NULL;
    iter.binfo = NULL;
    iter.arrindex = 0;
    iter.beginval = 0;
    iter.endval = 0;
    iter.search_ended = 0;
    iter.lno = 0;
    iter.tno = 0;

    riter.l2g = NULL;
    riter.binfo = NULL;
    riter.arrindex = 0;
    riter.beginval = 0;
    riter.endval = 0;
    riter.search_ended = 0;
    riter.lno = 0;
    riter.tno = 0;
    
    /* Count the number of internal zeroes to determine if we should
       use a dense or sparse representation. */

    for (l2gi_searchsome_reverse(&riter, map->l2g, 0, map->ceiling),
	     lowest = riter.search_ended ? map->ceiling : riter.lno + 1;
	 !riter.search_ended && nonzeroes < map->no_of_texts;
	 l2gi_prev(&riter))
    {
	zeroes += (lowest - riter.lno) - 1;
	nonzeroes++;
	lowest = riter.lno;
    }

    prot_a_output_ul(fp, lowest);
    prot_a_output_ul(fp, map->ceiling);

    /* Emit the "more-texts-exists" flag. */
    isc_puts(riter.search_ended ? " 0" : " 1", fp->isc_session);

    /* Emit the block, either a sparse or a dense one. */
    if (nonzeroes == 0)
    {
	/* Special case: an empty array.  Use the sparse format so
	   that we don't have to philosophize about what the lower
	   limit in the text-list should be. */
	isc_puts(" 0 0 *", fp->isc_session);
    }
    else if (zeroes >= nonzeroes)
    {
	/* There are at least as many zeroes as real mappings, so use
	   a sparse representation. */

	isc_puts(" 0", fp->isc_session);
	prot_a_output_ul(fp, nonzeroes);
	isc_puts(" {", fp->isc_session);

	for (l2gi_searchsome(&iter, map->l2g, lowest, map->ceiling);
	     !iter.search_ended;
	     l2gi_next(&iter))
	{
	    prot_a_output_ul(fp, iter.lno);
	    prot_a_output_ul(fp, iter.tno);
	}
	isc_puts(" }", fp->isc_session);
    }
    else
    {
	/* Emit a dense block. */
	isc_puts(" 1", fp->isc_session);
	prot_a_output_ul(fp, lowest);
	prot_a_output_ul(fp, zeroes + nonzeroes);
	isc_puts(" {", fp->isc_session);
	for (l2gi_searchsome(&iter, map->l2g, lowest, map->ceiling);
	     !iter.search_ended;
	     l2gi_next(&iter))
	{
	    while (lowest++ < iter.lno)
		isc_puts(" 0", fp->isc_session);
	    prot_a_output_ul(fp, iter.tno);
	}
	isc_puts(" }", fp->isc_session);
    }
}

void
prot_a_output_stats_description(Connection *fp,
				Stats_description *result)
{
    int i;

    prot_a_output_ul(fp, result->no_of_stats);
    if (result->no_of_stats == 0)
        isc_puts(" *", fp->isc_session);
    else
    {
        isc_puts(" {", fp->isc_session);
        for (i = 0; i < result->no_of_stats; i++)
            prot_a_output_string(fp, result->stat_names[i]);
        isc_puts(" }", fp->isc_session);
    }

    prot_a_output_num_list(fp, &result->intervals);
}

static void
prot_a_output_stats(Connection *fp,
		    const Stats *result)
{
    prot_a_output_float(fp, result->average);
    prot_a_output_float(fp, result->ascent_rate);
    prot_a_output_float(fp, result->descent_rate);
}

void
prot_a_output_stats_list(Connection *fp,
			 const Stats_list *result)
{
    int i;

    prot_a_output_ul(fp, result->no_of_stats);
    if (result->no_of_stats == 0)
        isc_puts(" *", fp->isc_session);
    else
    {
        isc_puts(" {", fp->isc_session);
        for (i = 0; i < result->no_of_stats; i++)
            prot_a_output_stats(fp, &result->stats[i]);
        isc_puts(" }", fp->isc_session);
    }
}

void
prot_a_output_static_server_info(Connection *fp,
				 const Static_server_info *result)
{
    prot_a_output_time(fp, result->boot_time);
    prot_a_output_time(fp, result->save_time);
    prot_a_output_string(fp, result->db_status);
    prot_a_output_ul(fp, result->existing_texts);
    prot_a_output_ul(fp, result->highest_text_no);
    prot_a_output_ul(fp, result->existing_confs);
    prot_a_output_ul(fp, result->existing_persons);
    prot_a_output_ul(fp, result->highest_conf_no);
}

void
prot_a_output_scheduling_info(Connection *fp,
			      const Scheduling_info *result)
{
    prot_a_output_ul(fp, result->priority);
    prot_a_output_ul(fp, result->weight);
}


#ifdef DEBUG_CALLS
void
prot_a_output_memory_info(Connection *fp,
                          Memory_info *result)
{
    prot_a_output_ul(fp, result->arena);
    prot_a_output_ul(fp, result->ordblks);
    prot_a_output_ul(fp, result->smblks);
    prot_a_output_ul(fp, result->hblks);
    prot_a_output_ul(fp, result->hblkhd);
    prot_a_output_ul(fp, result->usmblks);
    prot_a_output_ul(fp, result->fsmblks);
    prot_a_output_ul(fp, result->uordblks);
    prot_a_output_ul(fp, result->fordblks);
    prot_a_output_ul(fp, result->keepcost);
}
#endif
