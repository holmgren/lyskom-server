/*
 * $Id: send-async.h,v 0.24 2003/08/23 16:38:13 ceder Exp $
 * Copyright (C) 1991, 1994-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: send-async.h,v 0.24 2003/08/23 16:38:13 ceder Exp $
 *
 */
extern void
async_new_text(Connection *cptr,
	       Text_no text_no, 
	       Text_stat *text_s);

extern void
async_new_text_old(Connection *cptr,
                   Text_no text_no, 
                   Text_stat *text_s);

extern void
async_i_am_on(Who_info info);

extern void
async_logout(Pers_no pers_no, 
	     Session_no session_no);

extern void
async_new_name(Conf_no 	     conf_no,
	       const String  old_name,
	       const String  new_name);

extern void
async_sync_db(void);


extern void
async_forced_leave_conf (Connection *cptr,
			 Conf_no 	   conf_no);


extern void
async_login(Pers_no	pers_no,
	    int		client_no);

extern void
async_rejected_connection(void);

extern Success
async_send_message(Pers_no recipient,
		   Pers_no sender,
		   String  message,
                   Bool    force_message);

extern Success
async_send_group_message(Pers_no recipient,
			 Conf_no group_recipient,
			 Pers_no sender,
			 String  message,
                         Bool    force_message);

extern void
async_deleted_text(Connection *cptr,
                   Text_no text_no, 
                   Text_stat *text_s);

extern void
async_new_recipient(struct connection   *cptr,
                    Text_no              text_no,
                    Conf_no              conf_no,
                    enum info_type       type);

extern void
async_sub_recipient(struct connection   *cptr,
                    Text_no              text_no,
                    Conf_no              conf_no,
                    enum info_type       type);

void
async_new_membership(struct connection   *cptr,
                     Pers_no pers_no,
                     Conf_no conf_no);

void
async_new_user_area(Pers_no person,
		    Text_no old_user_area,
		    Text_no new_user_area);

void
async_new_presentation(Connection *cptr,
		       Conf_no conf_no,
		       Text_no old_presentation,
		       Text_no new_presentation);

void
async_new_motd(Connection *cptr,
	       Conf_no conf_no,
	       Text_no old_motd,
	       Text_no new_motd);

void
async_text_aux_changed(Connection    *cptr,
		       Text_no        text_no,
		       Aux_item_list *aux_list,
		       unsigned long  highest_old_aux);


#ifdef DEBUG_CALLS
void
async_garb_ended(int no_deleted);
#endif
