/*
 * $Id: dbck.c,v 0.76 2003/08/23 16:38:17 ceder Exp $
 * Copyright (C) 1991-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * dbck.c - A simple database checker and corrector.
 *
 * Author: Per Cederqvist.
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#endif
#include <stdio.h>
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#include "timewrap.h"
#include <sys/types.h>
#ifdef HAVE_CRYPT_H
#  include <crypt.h>
#endif
#ifdef HAVE_UNISTD_H
#  include <unistd.h>
#endif
#ifdef HAVE_STRING_H
#  include <string.h>
#endif
#include <assert.h>
#include <setjmp.h>
#include <errno.h>

#include "getopt.h"
#include "misc-types.h"
#include "s-string.h"
#include "kom-types.h"
#include "lyskomd.h"
#include "log.h"
#include "server/smalloc.h"
#include "misc-parser.h"
#include "cache.h"
#include "kom-config.h"
#include "debug.h"
#include "dbck-cache.h"
#include "param.h"
#include "server-config.h"
#include "async.h"
#include "com.h"
#include "connections.h"
#include "kom-errno.h"
#include "manipulate.h"
#include "version-info.h"
#include "ram-output.h"
#include "unused.h"
#include "local-to-global.h"
#include "lockdb.h"
#include "linkansi.h"
#include "eintr.h"


#define OPT_PERS_PRESENTATION_CONF      1
#define OPT_CONF_PRESENTATION_CONF      2
#define OPT_MOTD_CONF                   3
#define OPT_MOTD_OF_KOM                 4
#define OPT_KOM_NEWS_CONF               5


static struct option longopts[] = {
    {"compact-text-mass", 0, 0, 'g' },
    {"interactive", 0, 0, 'i' },
    {"auto-repair", 0, 0, 'r' },
    {"verbose", 0, 0, 'v' },
    {"print-statistics", 0, 0, 's' },
    {"list-text-no", 0, 0, 't' },
    {"set-change-name", 0, 0, 'c' },
    {"clear-password", required_argument, 0, 'P' },
    {"grant-all", required_argument, 0, 'G' },
    {"output-version", required_argument, 0, 'o' },
    {"force-output", 0, 0, 'F' },
    {"pers-pres-conf", required_argument, 0, 
     OPT_PERS_PRESENTATION_CONF },
    {"conf-pres-conf", required_argument, 0, 
     OPT_CONF_PRESENTATION_CONF },
    {"motd-conf", required_argument, 0, OPT_MOTD_CONF },
    {"motd-of-kom", required_argument, 0, OPT_MOTD_OF_KOM },
    {"kom-news-conf", required_argument, 0, OPT_KOM_NEWS_CONF },
    {"help", 0, 0, 'h' },
    { 0, 0, 0, 0 }
};

    

extern Info kom_info;

/* This is set to TRUE if init_cache finds out that the last part of the
   database is missing. */
Bool truncated_texts = FALSE;

int vflag=0;			/* Verbose - list statistics also. */
static int iflag=0;		/* Interactive - prompt user and repair. */
static int rflag=0;		/* Repair simple error without confirmation. */
static int gflag=0;		/* Garbage collect text-file. */
static int sflag=0;		/* Statistic flag. */
static Pers_no reset_pwd=0;	/* Person whose password should be cleared. */
static Pers_no grant_all=0;	/* Person which should receive all bits. */

/* The following variable holds the output format */

long oformat=-1;		/* Output format */
static long force_output=0;	/* Force sync (for conversions) */

/* The following variables hold kom_info modifications */

int pers_pres_conf = -1;
int conf_pres_conf = -1;
int motd_conf = -1;
Text_no motd_of_lyskom = (Text_no)-1;
int kom_news_conf = -1;


/* The following variable corresponds to the -c flag, and is
   present here due to a bug in lyskomd 1.6.1. */
static int unset_change_name_is_error=0;

/* A list of all available text numbers was useful when developing
   new data structures in lyskomd in 1995.  Option: -t. */
static int dump_text_numbers=0;

int modifications = 0;

typedef struct {
    int	created_confs;
} Person_scratchpad;

static const Person_scratchpad   EMPTY_PERSON_SCRATCHPAD = { 0 };

static Person_scratchpad **person_scratchpad = NULL;

int buglevel = 0;
BUGDECL;


struct delete_list {
    struct delete_list *next;
    Local_text_no lno;
};

static void
delete_list_append(struct delete_list **head,
		   Local_text_no lno)
{
    struct delete_list *item = smalloc(sizeof(struct delete_list));
    item->next = *head;
    item->lno =  lno;
    *head = item;
}

static void
execute_deletions(Local_to_global *l2g,
		  struct delete_list **head)
{
    struct delete_list *item;

    while (*head != NULL)
    {
	item = *head;
	*head = item->next;
	l2g_delete(l2g, item->lno);
	sfree(item);
    }
}


#if defined(HAVE_VFPRINTF) && defined(HAVE_STDARG_H)
extern void
kom_log (const char * format, ...)
{
    va_list AP;

    va_start(AP, format);

    vfprintf(stdout, format, AP);

    va_end(AP);
}
#else
extern void
kom_log (format, a, b, c, d, e, f, g)
     const char * format;
     int a, b, c, d, e, f, g;
{
    fprintf(stdout, format, a, b, c, d, e, f, g);
}
#endif

#if defined(HAVE_VFPRINTF) && defined(HAVE_STDARG_H)
extern void
restart_kom (const char * format, ...)
{
    va_list AP;

    va_start(AP, format);

    vfprintf(stdout, format, AP);

    va_end(AP);
    exit(1);
}
#else
extern void
restart_kom (format, a, b, c, d, e, f, g)
     const char * format;
     int a, b, c, d, e, f, g;
{
    fprintf(stdout, format, a, b, c, d, e, f, g);
    exit(1);
}
#endif


static Person_scratchpad *
alloc_person_scratchpad(void)
{
    Person_scratchpad *p;

    p = smalloc(sizeof(Person_scratchpad));
    *p = EMPTY_PERSON_SCRATCHPAD;
    return p;
}


static Bool
is_comment_to(Text_no    comment,
	      Text_stat *parent)
{
    int i;

    for ( i = 0; i < parent->no_of_misc; i++ )
    {
	switch( parent->misc_items[ i ].type )
	{
	case comm_in:
	    if ( parent->misc_items[ i ].datum.text_link == comment )
		return TRUE;
	    break;
	default:
	    break;
	}
    }

    return FALSE;
}

static Bool
is_commented_in(Text_no    parent,
		Text_stat *child)
{
    int i;

    for ( i = 0; i < child->no_of_misc; i++ )
    {
	switch( child->misc_items[ i ].type )
	{
	case comm_to:
	    if ( child->misc_items[ i ].datum.text_link == parent )
		return TRUE;
	    break;
	default:
	    break;
	}
    }

    return FALSE;
}
	
static Bool
is_footnote_to(Text_no    footnote,
	       Text_stat *parent)
{
    int i;

    for ( i = 0; i < parent->no_of_misc; i++ )
    {
	switch( parent->misc_items[ i ].type )
	{
	case footn_in:
	    if ( parent->misc_items[ i ].datum.text_link == footnote )
		return TRUE;
	    break;
	default:
	    break;
	}
    }

    return FALSE;
}

static Bool
is_footnoted_in(Text_no    parent,
		Text_stat *child)
{
    int i;

    for ( i = 0; i < child->no_of_misc; i++ )
    {
	switch( child->misc_items[ i ].type )
	{
	case footn_to:
	    if ( child->misc_items[ i ].datum.text_link == parent )
		return TRUE;
	    break;
	default:
	    break;
	}
    }

    return FALSE;
}

Member *
locate_member(Pers_no      pers_no,
	      Conference * conf_c)
{
    Member * mbr;
    int      i;

    for(mbr = conf_c->members.members, i = conf_c->members.no_of_members;
	i > 0; i--, mbr++)
    {
	if ( mbr->member == pers_no )
	{
	    return mbr;
	}
    }

    return NULL;
}

/*
 * Delete a misc_info.
 * If it is a recpt, cc_recpt, comm_to or footn_to delete any
 * loc_no, rec_time, sent_by or sent_at that might follow it.
 *
 * Note that the Misc_info is not reallocated.
 */

static void
delete_misc (Text_stat *tstat,
	     Misc_info *misc)	/* Pointer to first misc_item to delete. */
{
    int del = 1;		/* Number of items to delete. */
    				/* Always delete at least one item. */
    Bool ready = FALSE;
    
    /* Check range of misc */

    if (misc < tstat->misc_items
	|| misc >= tstat->misc_items + tstat->no_of_misc )
    {
	restart_kom("delete_misc() - misc out of range\n");
    }

    while (ready == FALSE
	   && misc + del < tstat->misc_items + tstat->no_of_misc )
    {
	switch ( misc[ del ].type )
	{
	case loc_no:
	case rec_time:
	case sent_by:
	case sent_at:
	    del++;
	    break;

	case recpt:
	case cc_recpt:
        case bcc_recpt:
	case footn_to:
	case footn_in:
	case comm_to:
	case comm_in:
	    ready = TRUE;
	    break;

#ifndef COMPILE_CHECKS
	default:
#endif
	case unknown_info:
	    restart_kom("delete_misc() - illegal misc found.\n");
	}
    }

    tstat->no_of_misc -= del;

    /* Move items beyond the deleted ones. */

    while ( misc < tstat->misc_items + tstat->no_of_misc )
    {
	misc[ 0 ] = misc[ del ];
	misc++;
    }
}

static int
confirm(const char *question)
{
    if ( iflag )
    {
	fputs(question, stdout);
	fputs(" (y/n) ", stdout);
	while(1)
	    switch(getchar())
	    {
	    case 'y':
	    case 'Y':
		return 1;
	    case 'n':
	    case 'N':
	    case EOF:
		return 0;
	    default:
		break;
	    }
    }
    else
	return 0;
}


static long
check_misc_any_recipient(Text_no tno,
			 Text_stat *tstat,
			 const char *pretty_type,
			 Conf_no rcpt,
			 Local_text_no lno,
			 Misc_info *previous,
			 const Misc_info **misc)
{
    Conference *c;
    long errors = 0;
    Local_text_no conf_min;
    Local_text_no conf_max;

    c = cached_get_conf_stat(rcpt);
    if (c == NULL && rcpt == 0)
    {
	kom_log("Conference 0 is a %s of text %lu.\n",
		pretty_type, (unsigned long)tno);
	if (rflag || confirm("Repair by deleting misc_item? "))
	{
	    delete_misc(tstat, previous);
	    kom_log("Repaired: Conference 0 is no longer a %s.\n",
		    pretty_type);
	    mark_text_as_changed(tno);
	    modifications++;
	    *misc = previous;
	}
	else
	    errors++;

	return errors;
    }

    if (c == NULL)
	return errors;

    /* Check loc_no */
    conf_min = l2g_lookup(&c->texts, 0);
    conf_max = l2g_first_appendable_key(&c->texts);

    if (lno < conf_min)
    {
	kom_log("Text %lu: %s %lu<%lu>: loc_no is less than %lu\n",
		(unsigned long)tno,
		pretty_type,
		(unsigned long)rcpt,
		(unsigned long)lno,
		(unsigned long)conf_min);
	errors++;
    }
    else if (lno >= conf_max)
    {
	kom_log("Text %lu: %s %lu<%lu>: loc_no is greater than %lu\n",
		(unsigned long)tno,
		pretty_type,
		(unsigned long)rcpt,
		(unsigned long)lno,
		(unsigned long)conf_max);
	errors++;
    }
    else if (l2g_lookup(&c->texts, lno) != tno)
    {
	kom_log("Text %lu: %s %lu<%lu>: that local number is mapped to %lu.\n",
		(unsigned long)tno,
		pretty_type,
		(unsigned long)rcpt,
		(unsigned long)lno,
		(unsigned long)l2g_lookup(&c->texts, lno));
	errors++;
    }

    return errors;
}

    
static long
check_misc_infos(Text_no    tno,
		 Text_stat *tstat)
{
    const Misc_info   * misc = tstat->misc_items;
    Misc_info   * previous;
    Misc_info_group group;
    Text_stat *t;
    
    long errors=0;

    while (previous = (Misc_info *)misc,
	   group = parse_next_misc(&misc,
				   tstat->misc_items + tstat->no_of_misc),
	   group.type != m_end_of_list && group.type != m_error )
    {
	switch ( group.type )
	{
	case m_recpt:
	    check_misc_any_recipient(tno, tstat, "recipient",
				     group.recipient, group.local_no,
				     previous, &misc);
	    break;

	case m_cc_recpt:
	    check_misc_any_recipient(tno, tstat, "cc_recipient",
				     group.cc_recipient, group.local_no,
				     previous, &misc);
	    break;

	case m_bcc_recpt:
	    check_misc_any_recipient(tno, tstat, "bcc_recipient",
				     group.bcc_recipient, group.local_no,
				     previous, &misc);
	    break;

	case m_comm_to:
	    t = cached_get_text_stat(group.comment_to);

	    if ( t == NULL )
	    {
		kom_log("Text %lu is a comment to %lu, which doesn't exist.\n",
		    (unsigned long)tno, (unsigned long)group.comment_to);

		if (rflag || confirm("Repair by deleting misc_item? "))
		{
		    delete_misc(tstat, previous);
		    mark_text_as_changed(tno);
		    modifications++;
		    kom_log("Repaired: Comment-link deleted.\n");
		    misc = previous;
		}
		else
		    errors++;
		    
		errors++;
	    }
	    else if (!is_comment_to(tno, t))
	    {
		kom_log("Text %lu is a comment to %lu, but not the reverse.\n",
		    (unsigned long)tno, (unsigned long)group.comment_to);
		errors++;
	    }

	    break;

	case m_comm_in:
	    t = cached_get_text_stat(group.commented_in);

	    if ( t == NULL )
	    {
		kom_log("Text %lu is commented in %lu, which doesn't exist.\n",
		    (unsigned long)tno, (unsigned long)group.commented_in);

		if (rflag || confirm("Repair by deleting misc_item? "))
		{
		    delete_misc(tstat, previous);
		    mark_text_as_changed(tno);
		    modifications++;
		    kom_log("Repaired: Comment-link deleted.\n");
		    misc = previous;
		}
		else
		    errors++;
	    }
	    else if (!is_commented_in(tno, t))
	    {
		kom_log("Text %lu is commented in %lu, but not the reverse.\n",
		    (unsigned long)tno, (unsigned long)group.commented_in);
		errors++;
	    }

	    break;

	case m_footn_to:
	    t = cached_get_text_stat(group.footnote_to);

	    if ( t == NULL )
	    {
		kom_log("Text %lu is a footnote to %lu, which doesn't exist.\n",
		    (unsigned long)tno, (unsigned long)group.footnote_to);

		if (rflag || confirm("Repair by deleting misc_item? "))
		{
		    delete_misc(tstat, previous);
		    mark_text_as_changed(tno);
		    modifications++;
		    kom_log("Repaired: Footnote-link deleted.\n");
		    misc = previous;
		}
		else
		    errors++;
	    }
	    else if (!is_footnote_to(tno, t))
	    {
		kom_log("Text %lu is a footnote to %lu, but not the reverse.\n",
		    (unsigned long)tno, (unsigned long)group.footnote_to);
		errors++;
	    }

	    break;
	    
	case m_footn_in:
	    t = cached_get_text_stat(group.footnoted_in);

	    if ( t == NULL )
	    {
		kom_log("Text %lu is footnoted in %lu, which doesn't exist.\n",
		    (unsigned long)tno, (unsigned long)group.footnoted_in);

		if (rflag || confirm("Repair by deleting misc_item? "))
		{
		    delete_misc(tstat, previous);
		    mark_text_as_changed(tno);
		    modifications++;
		    kom_log("Repaired: Footnote-link deleted.\n");
		    misc = previous;
		}
		else
		    errors++;
	    }
	    else if (!is_footnoted_in(tno, t))
	    {
		kom_log("Text %lu is footnoted in %lu, but not the reverse.\n",
		    (unsigned long)tno, (unsigned long)group.footnoted_in);
		errors++;
	    }

	    break;

	default:
	    kom_log("check_misc_infos(): parse_next_misc returned type %lu\n",
		(unsigned long)group.type);
	    break;
	}
    }

    if ( group.type == m_error )
    {
	kom_log("Text %lu has a bad misc_info_list.\n", (unsigned long)tno);
	errors++;
    }

    return errors;
}

		    
		     
		 
static long
check_texts(void)
{
    Text_no    ct = 0;
    Text_stat *ctp=NULL;
    long	errors = 0;
    Text_no	number_of_texts = 0;
    unsigned long	bytes=0;
    unsigned long	max_bytes=0;
    Text_no	max_text=0;

    while ( (ct = traverse_text(ct)) != 0 )
    {
	number_of_texts++;
	
	ctp = cached_get_text_stat( ct );
	if ( ctp == NULL )
	{
	    kom_log("Text %lu nonexistent.\n", ct);
	    errors++;
	}
	else
	{
	    if (dump_text_numbers)
		kom_log("Checking text_no %ld\n", (unsigned long)ct);

	    bytes += ctp->no_of_chars;
	    if ( (unsigned long)ctp->no_of_chars > max_bytes )
	    {
		max_bytes = ctp->no_of_chars;
		max_text = ct;
	    }
	    
	    /* FIXME (bug 147): no_of_marks is not yet checked. */
	    errors += check_misc_infos(ct, ctp);
	}
    }

    if (vflag)
    {
	if ( number_of_texts == 0 )
	    kom_log("WARNING: No texts found.\n");
	else
	{
	    kom_log("Total of %lu texts (total %lu bytes, avg. %lu bytes/text).\n",
		(unsigned long)number_of_texts,
		(unsigned long)bytes,
		(unsigned long)(bytes/number_of_texts));
	    kom_log("Longest text is %lu (%lu bytes).\n",
		(unsigned long)max_text, (unsigned long)max_bytes);
	}
    }
    
    return errors;
}


static int
check_created_texts(Pers_no pno,
		    Local_to_global *created)
{
    Text_stat *t;
    int errors=0;
    L2g_iterator iter;
    struct delete_list *del_list = NULL;

    for (l2gi_searchall(&iter, created); !iter.search_ended; l2gi_next(&iter))
    {
	assert(iter.tno != 0);
	assert(iter.lno != 0);

	t = cached_get_text_stat(iter.tno);
	if ( t != NULL && t->author != pno)
	{
	    kom_log("Person %lu is author of text %lu whose author is %lu.\n",
		(unsigned long)pno, (unsigned long)iter.tno,
		(unsigned long)t->author);
	    errors++;
	}

	if ( t == NULL )
	{
	    kom_log("Person %lu is author of text %lu, which doesn't exist.\n",
		(unsigned long)pno, (unsigned long)iter.tno);
	    if ( rflag ||
		 confirm("Repair by setting to text_no to 0 in local map"))
	    {
		delete_list_append(&del_list, iter.lno);
		mark_person_as_changed(pno);
		modifications++;
		kom_log("Repaired: created_texts corrected.\n");
	    }
	    else
		errors++;
	}
	
    }
    execute_deletions(created, &del_list);
    
    return errors;
}

static int
check_membership(Pers_no pno,
		 Membership *mship)
{
    int errors=0;
    Conference *conf;
    Local_text_no last=0;
    Member *mem;
    struct read_range *begin;
    struct read_range *end;
    struct read_range *ptr;
    
    
    conf = cached_get_conf_stat(mship->conf_no);
    if ( conf == NULL )
    {
	kom_log("Person %lu is a member in the non-existing conference %lu.\n",
	    (unsigned long)pno, (unsigned long)mship->conf_no);
	errors++;
    }
    else
    {
	/* Check read texts */

	last = 0;
	if (mship->no_of_read_ranges > 0)
	{
	    begin = &mship->read_ranges[0];
	    end = begin + mship->no_of_read_ranges;

	    for (ptr = begin; ptr < end; ptr++)
	    {
		if (ptr->first_read > ptr->last_read)
		{
		    kom_log("Person %lu's membership in %lu is corrupt: "
			    "bad range: %lu-%lu.\n",
			    (unsigned long)pno, (unsigned long)mship->conf_no,
			    (unsigned long)ptr->first_read,
			    (unsigned long)ptr->last_read);
		    errors++;
		}
		if (ptr != begin && last + 1 == ptr->first_read)
		{
		    kom_log("Person %lu's membership in %lu is corrupt: "
			    "adjoining ranges not joined at around %lu.\n",
			    (unsigned long)pno, (unsigned long)mship->conf_no,
			    (unsigned long)ptr->first_read);
		    errors++;
		}
		if (ptr != begin && last >= ptr->first_read)
		{
		    kom_log("Person %lu's membership in %lu is corrupt: "
			    "overlapping ranges: %lu-%lu, %lu-%lu.\n",
			    (unsigned long)pno, (unsigned long)mship->conf_no,
			    (unsigned long)(ptr-1)->first_read,
			    (unsigned long)(ptr-1)->last_read,
			    (unsigned long)ptr->first_read,
			    (unsigned long)ptr->last_read);
		    errors++;
		}
		last = ptr->last_read;
	    }
	}
	
	if (last >= l2g_first_appendable_key(&conf->texts))
	{
	    kom_log("Person %lu has read text %lu in conf %lu, "
		    "which only has %lu texts.\n",
		    (unsigned long)pno,
		    (unsigned long)last,
		    (unsigned long)mship->conf_no,
		    (unsigned long)(l2g_first_appendable_key(&conf->texts)-1));
	    errors++;
	}

	/* Check that he is a member */
	if ( (mem = locate_member(pno, conf)) == NULL )
	{
	    kom_log("Person %lu is a member in %lu in which he isn't a member.\n",
		(unsigned long)pno, (unsigned long)mship->conf_no);
	    errors++;
	}
        else
        {
            /* Check duplicated information
               Short circuit and in each check sets foo_e
               if an error has been detected
            */

            if (mem->type.invitation != mship->type.invitation ||
                mem->type.passive != mship->type.passive ||
                mem->type.secret != mship->type.secret ||
                mem->type.passive_message_invert !=
		mship->type.passive_message_invert ||
                mem->type.reserved2 != mship->type.reserved2 ||
                mem->type.reserved3 != mship->type.reserved3 ||
                mem->type.reserved4 != mship->type.reserved4 ||
                mem->type.reserved5 != mship->type.reserved5 ||
                mem->added_at != mship->added_at ||
                mem->added_by != mship->added_by)
            {
                kom_log("Person %lu membership in %lu does not match member record.\n",
                    (unsigned long)pno,
                    (unsigned long)mship->conf_no
                    );

                errors++;

                fputs("Membership:", stdout);
                printf("  type:      %s%s%s%s%s%s%s%s\n",
                       mship->type.invitation ? "invitation " : "",
                       mship->type.passive ? "passive " : "",
                       mship->type.secret ? "secret " : "",
                       mship->type.passive_message_invert ?
		       "passive_message_invert "  : "",
                       mship->type.reserved2 ? "rsv2 "  : "",
                       mship->type.reserved3 ? "rsv3 "  : "",
                       mship->type.reserved4 ? "rsv4 "  : "",
                       mship->type.reserved5 ? "rsv5"  : "");
                printf("  added_by: %lu\n", (unsigned long)mship->added_by);
                printf("  added_at: %lu\n", (unsigned long)mship->added_at);
                fputs("Member:", stdout);
                printf("  type:      %s%s%s%s%s%s%s%s\n",
                       mem->type.invitation ? "invitation " : "",
                       mem->type.passive ? "passive " : "",
                       mem->type.secret ? "secret " : "",
                       mem->type.passive_message_invert ?
		       "passive_message_invert "  : "",
                       mem->type.reserved2 ? "rsv2 "  : "",
                       mem->type.reserved3 ? "rsv3 "  : "",
                       mem->type.reserved4 ? "rsv4 "  : "",
                       mem->type.reserved5 ? "rsv5"  : "");
                printf("  added_by: %lu\n", (unsigned long)mem->added_by);
                printf("  added_at: %lu\n", (unsigned long)mem->added_at);

                if (confirm("Copy membership to member"))
                {
                    mem->added_at = mship->added_at;
                    mem->added_by = mship->added_by;
                    mem->type = mship->type;
                    modifications++;
                }
                else if (confirm("Copy member to membership"))
                {
                    mship->added_at = mem->added_at;
                    mship->added_by = mem->added_by;
                    mship->type = mem->type;
                    modifications++;
                }
            }
        }
    }

    return errors;
}

	    

static int
check_membership_list(Pers_no pno,
		      const Membership_list *mlist)
{
    int errors=0;
    int i;
    
    for (i = 0; i < mlist->no_of_confs; i++)
	errors += check_membership(pno, &mlist->confs[i]);

    return errors;
}


static int
check_persons(void)
{
    static char crypt_seed[] = 
	"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789./";
    Pers_no     cp = 0;
    Person     *pstat=NULL;
    Conference *cstat=NULL;
    long	errors = 0;
    Pers_no	number_of_persons=0;

    while ( (cp = traverse_person(cp)) != 0 )
    {
	number_of_persons++;
	pstat = cached_get_person_stat (cp);
	cstat = cached_get_conf_stat (cp);
	
	if ( pstat == NULL )
	{
	    kom_log("Person %lu nonexistent.\n", (unsigned long)cp);
	    errors++;
	}
	else if (cstat == NULL)
	{
	    kom_log("Person %lu has no conference.\n", (unsigned long)cp);
	    errors++;
	}
	else if (!cstat->type.letter_box)
	{
	    kom_log("Person %lu's conference is not a letter_box.\n",
		(unsigned long)cp);
	    errors++;
	}
	else
	{
	    errors += (check_created_texts(cp, &pstat->created_texts)
		       + check_membership_list(cp, &pstat->conferences));
	}

	if (unset_change_name_is_error == 1
	    && pstat->privileges.change_name == 0)
	{
	    kom_log("Person %lu has no change_name capability.\n",
		(unsigned long)cp);

	    if (rflag || confirm("Grant him the capability"))
	    {
		pstat->privileges.change_name = 1;
		mark_person_as_changed(cp);
		modifications++;
	    }
	    else
		errors++;
	}
	if (cp == reset_pwd)
	{
	    char salt[3];
	  
	    salt[0] = crypt_seed [rand() % (sizeof (crypt_seed) - 1)];
	    salt[1] = crypt_seed [rand() % (sizeof (crypt_seed) - 1)];
	    salt[2] = '\0';
	  
	    strcpy((char *)pstat->pwd, (const char *)crypt("", salt));
	    mark_person_as_changed(cp);
	    modifications++;
	}

	if (cp == grant_all)
	{
	    pstat->privileges.wheel = 1;
	    pstat->privileges.admin = 1;
	    pstat->privileges.statistic = 1;
	    pstat->privileges.create_pers = 1;
	    pstat->privileges.create_conf = 1;
	    pstat->privileges.change_name = 1;
	    pstat->privileges.flg7 = 1;
	    pstat->privileges.flg8 = 1;
	    pstat->privileges.flg9 = 1;
	    pstat->privileges.flg10 = 1;
	    pstat->privileges.flg11 = 1;
	    pstat->privileges.flg12 = 1;
	    pstat->privileges.flg13 = 1;
	    pstat->privileges.flg14 = 1;
	    pstat->privileges.flg15 = 1;
	    pstat->privileges.flg16 = 1;
	    mark_person_as_changed(cp);
	    modifications++;
	}
    }

    if (vflag)
	kom_log("Total of %lu persons.\n", (unsigned long)number_of_persons);
    
    return errors;
}

static Bool
is_recipient(Conf_no	 conf_no,
	     Text_stat * t_stat)
{
    int i;
    
    for ( i = 0; i < t_stat->no_of_misc; i++ )
    {
	switch( t_stat->misc_items[ i ].type )
	{
	case recpt:
	case cc_recpt:
	case bcc_recpt:
	    if ( t_stat->misc_items[ i ].datum.recipient == conf_no )
	    {
		return TRUE;
	    }
	    break;

	case rec_time:
	case comm_to:
	case comm_in:
	case footn_to:
	case footn_in:
	case sent_by:
	case sent_at:
	case loc_no:
	    break;
	    
#ifndef COMPILE_CHECKS
	default:
#endif
	case unknown_info:
	    restart_kom("is_recipient(): illegal misc_item\n");
	}
    }

    return FALSE;
}

static int
check_texts_in_conf(Conf_no cc,
		    Local_to_global *tlist)
{
    Text_stat *t;
    int errors=0;
    L2g_iterator iter;
    struct delete_list *del_list = NULL;
    
    for (l2gi_searchall(&iter, tlist); !iter.search_ended; l2gi_next(&iter))
    {
	assert(iter.lno != 0);
	assert(iter.tno != 0);
	t = cached_get_text_stat(iter.tno);
	if (t == NULL)
	{
	    kom_log("Text %lu<%lu> in conference %lu is non-existent.\n",
		(unsigned long)iter.tno,
		(unsigned long)iter.lno,
		(unsigned long)cc);

	    if (rflag || confirm("Repair by deleting the Text_no in the map?"))
	    {
		delete_list_append(&del_list, iter.lno);
		mark_conference_as_changed(cc);
		modifications++;
		kom_log("Repaired: %lu is no longer a recipient.\n",
		    (unsigned long)cc);
	    }
	    else
		errors++;
	}
	else
	{
	    if (!is_recipient(cc, t))
	    {
		kom_log("Text %lu<%lu> in conference %lu %s.\n",
		    (unsigned long)iter.tno,
		    (unsigned long)iter.lno,
		    (unsigned long)cc,
		    "doesn't have the conference as recipient");

		if (confirm("Repair by deleting Text_no from the map?") )
		{
		    delete_list_append(&del_list, iter.lno);
		    mark_conference_as_changed(cc);
		    modifications++;
		    kom_log("Repaired: %lu is no longer a recipient.\n",
			(unsigned long)cc);
		}
		else
		    errors++;
	    }
	}
    }
    execute_deletions(tlist, &del_list);

    return errors;
}

Membership *
locate_membership(Conf_no       conf_no,
		  const Person *pers_p)
{
    Membership * confp;
    int    i;

    for(confp = pers_p->conferences.confs, i = pers_p->conferences.no_of_confs;
	i > 0; i--, confp++)
    {
	if ( confp->conf_no == conf_no )
	{
	    return confp;
	}
    }

    return NULL;
}

static int
check_member(Conf_no cc,
	     Member *memb)
{
    Person *pp;
    int errors=0;

    pp = cached_get_person_stat(memb->member);
    if ( pp == NULL )
    {
	kom_log("Person %lu, supposedly a member in conf %lu, is nonexistent.\n",
	    (unsigned long)memb->member, (unsigned long)cc);
	errors++;
    }
    else
    {
	if ( locate_membership(cc, pp) == NULL )
	{
	    kom_log("Person %lu is not a member in conf %lu.\n",
		(unsigned long)memb->member,
		(unsigned long)cc);
	    errors++;
	}
    }

    return errors;
}

static int
check_member_list(Conf_no cc,
		  const Member_list *mlist)
{
    int errors=0;
    int i;
    
    for (i = 0; i < mlist->no_of_members; i++)
	errors += check_member(cc, &mlist->members[i]);

    return errors;
}


static int
check_confs(void)
{
    Conf_no     cc = 0;
    Person     *pstat=NULL;
    Conference *cstat=NULL;
    long	errors = 0;
    Conf_no	number_of_confs = 0;

    while ( (cc = traverse_conference(cc)) != 0 )
    {
	number_of_confs++;
	cstat = cached_get_conf_stat (cc);
	
	if ( cstat == NULL )
	{
	    kom_log("Conference %lu nonexistent.\n", (unsigned long)cc);
	    errors++;
	}
	else
	{
	    if (cstat->type.letter_box)
	    {
		pstat = cached_get_person_stat(cc);
		if (pstat == NULL)
		{
		    kom_log("Mailbox %lu has no person.\n", (unsigned long)cc);
		    errors++;
		}
	    }
	    else		/* not letter_box */
	    {
		/* Remember that the creator might no longer exist. */
		if ( person_scratchpad[ cstat->creator ] != NULL )
		    ++person_scratchpad[ cstat->creator ]->created_confs;
	    }

	    errors += (check_texts_in_conf(cc, &cstat->texts)
		       + check_member_list(cc, &cstat->members));
	}
    }

    if ( vflag )
	kom_log("Total of %lu conferences.\n", (unsigned long)number_of_confs);

    return errors;
}

static void
init_person_scratch(void)
{
    Pers_no pno = 0;
    Pers_no i = 0;
    
    if (person_scratchpad == NULL)
    {
        person_scratchpad = smalloc(sizeof(*person_scratchpad) * param.max_conf);
        for (i = 0; i < param.max_conf; i++)
            person_scratchpad[i] = NULL;
    }

    while( (pno = traverse_person(pno)) != 0 )
    {
	person_scratchpad[pno] = alloc_person_scratchpad();
    }
}

static long
post_check_persons(void)
{
    long errors = 0;
    
    Pers_no pers_no = 0;
    Person *pstat;
  
    while ((pers_no = traverse_person(pers_no)) != 0)
    {
	if ((pstat = cached_get_person_stat(pers_no)) == NULL)
	{
	    kom_log("%s(): can't cached_get_person_stat(%d).\n", 
		"INTERNAL DBCK ERROR: post_check_persons", pers_no);
	}
    }

    return errors;
}

		
/*
 * Returns 0 if the database seems to be correct.
 */
static long
check_data_base(void)
{
    long errors;

    init_person_scratch();
    errors = check_texts() + check_persons() + check_confs();
    return errors + post_check_persons();
}

static void
init_data_base(void)
{
    if ( vflag )
    {
	kom_log("Database = %s\n", param.datafile_name);
	kom_log("Backup   = %s\n", param.backupfile_name);
	kom_log("Text     = %s\n", param.textfile_name);
	kom_log("Textback = %s\n", param.textbackupfile_name);
    }
    
    if ( init_cache() == FAILURE )
	restart_kom("Can't find database.\n");
}

static void
garb_text_file(void)
{
    Text_no tno = 0;
    String text;

    kom_log("Renaming %s to %s\n", param.textfile_name, param.textbackupfile_name);
    if (i_rename(param.textfile_name, param.textbackupfile_name) < 0)
    {
	restart_kom("rename failed: %s\n", strerror(errno));
    }
    kom_log("Writing texts to (new) %s\n", param.textfile_name);
    fflush(stdout);
    fflush(stderr);
    cache_open_new_text_file();

    while ( (tno = traverse_text(tno)) != 0 )
    {
	text = cached_get_text(tno);
	cached_flush_text(tno, text);
	free_tmp();
    }
    kom_log("Writing datafile with new indexes.\n");
    fflush(stdout);
    fflush(stderr);
    cache_sync_all();
}

	
	
static void
print_statistics(void)
{
    Text_stat *ts;
    Text_no    t;
    int  *hist;
    int i;

    hist = calloc(param.text_len, sizeof(int));

    if (hist == NULL)
    {
	perror("dbck: print_statistics(): can't calloc()");
	return;
    }
    
    for (t=0; (t=traverse_text(t)) != 0;)
    {
	ts = cached_get_text_stat(t);
	if (ts == NULL)
	{
	    kom_log("print_statistics(): Can't get text_stat.\n");
	    return;
	}
		
	hist[ts->no_of_chars]++;
    }

    kom_log("Length  Frequency\n");
    for(i=0; i<param.text_len; i++)
	if(hist[i] != 0)
	    kom_log("%8d %d\n", i, hist[i]);
}

static void
give_help(const char *default_config)
{
    printf("dbck for %s version %s\n",
           kom_version_info.server_name,
           kom_version_info.server_version);
    puts("Usage: dbck [options] [config_file]");
    puts("Available options:");
    puts("Short   Long option         Explanation");
    puts("  -g  --compact-text-mass   Reclaim unused space among texts.");
    puts("  -i  --interactive         Offer to repair some error types.");
    puts("  -r  --auto-repair         "
         "Repair simple errors without prompting.");
    puts("  -v  --verbose             Talk more.");
    puts("  -o  --output-version=N    File format 0, 1 or 2.");
    puts("  -F  --force-output        Save db even if no changes.");
    puts("  -s  --print-statistics    Output a lot of statistics.");
    puts("  -t  --list-text-no        "
         "List the text number of all existing texts.");
    puts("  -c  --set-change-name     "
         "Consider unset change_name capabilities an error.");
    puts("  -P  --clear-password=pno  "
         "Set password to empty string for a user.");
    puts("  -G  --grant-all=pno       Give a user all available privileges.");
    puts("      --pers-pres-conf=N    Set conference for user presentations.");
    puts("      --conf-pres-conf=N    Set conference for conference "
         "presentations.");
    puts("      --kom-news-conf=N     Set conference for news about KOM.");
    puts("      --motd-conf=N         Set conference for lapps on the doors.");
    puts("      --motd-of-kom=N       Set KOM login message text.");
    puts("  -h  --help                Display this help.");
    puts("Note: the -P and -G options "
         "also require a person number as argument");
    printf("The config_file argument defaults to %s\n", default_config);
    puts("WARNING: Don't allow lyskomd "
         "and dbck to modify the database simultaneously!");
    exit(0);
}

/* Stop "no previous prototype" warning from gcc 2.0 */
int main(int, char**);

int
main (int    argc,
      char **argv)
{
    int hflag = 0;
    int errors;
    const char *config_file;
    int optc;
    int need_rw = 0;
    int have_lock = 0;

    link_ansi();

#ifdef TRACED_ALLOCATIONS
    /* We must do this before we allocate any memory... */
    {
      char buf[1024];
      char *nl;

      fputs("Where does the trace want to go today? [stderr]\n", stdout);
      fflush(stdout);
      if (fgets(buf, sizeof(buf), stdin) != buf)
      {
	  fprintf(stderr, "unable to read trace location\n");
	  exit(1);
      }

      if ((nl = strchr(buf, '\n')) != NULL)
	  *nl = '\0';
      trace_alloc_file(buf);
    }
#endif


    while ((optc = getopt_long(argc, argv, "girvstdcP:G:Fo:",
			       longopts, (int *) 0)) != EOF)
    {
	switch (optc)
	{
        case OPT_PERS_PRESENTATION_CONF:
            pers_pres_conf = atoi(optarg);
            if (pers_pres_conf == 0)
                restart_kom("%s: bad conference number %s\n", argv[0], optarg);
	    need_rw = 1;
            break;

        case OPT_CONF_PRESENTATION_CONF:
            conf_pres_conf = atoi(optarg);
            if (conf_pres_conf == 0)
                restart_kom("%s: bad conference number %s\n", argv[0], optarg);
	    need_rw = 1;
            break;

        case OPT_MOTD_CONF:
            motd_conf = atoi(optarg);
            if (motd_conf == 0)
                restart_kom("%s: bad conference number %s\n", argv[0], optarg);
	    need_rw = 1;
            break;

        case OPT_MOTD_OF_KOM:
            motd_of_lyskom = atol(optarg);
            if (motd_of_lyskom == 0 && *optarg != '0')
                restart_kom("%s: bad text number %s\n", argv[0], optarg);
	    need_rw = 1;
            break;

        case OPT_KOM_NEWS_CONF:
            kom_news_conf = atoi(optarg);
            if (kom_news_conf == 0)
                restart_kom("%s: bad conference number %s\n", argv[0], optarg);
	    need_rw = 1;
            break;

#ifndef NDEBUG
	case 'd':
	    buglevel++;
	    break;
#endif

	case 'i':		/* Running interactively. */
	    iflag++;		/* Will ask user and try to repair. */
	    need_rw = 1;
	    break;

	case 'r':		/* Repair simple errors wihtout asking. */
	    rflag++;
	    need_rw = 1;
	    break;

	case 'v':		/* Verbose: report more than errors. */
	    vflag++;
	    break;

	case 'g':		/* Garbage collect: compress text-file. */
	    gflag++;
	    need_rw = 1;
	    break;

	case 'h':		/* Help: give usage message. */
	    hflag++;
	    break;

	case 's':		/* Statistics: text length et c. */
	    sflag++;
	    break;

	case 'c':		/* Consider an unset change_name an error. */
	    unset_change_name_is_error = 1;
	    /* No need for rw, since this only flags the condition as
	       an error.  You still need -r or -i to fix the error.  */
	    break;

	case 't':
	    dump_text_numbers++;
	    break;

	case 'P':
	    reset_pwd = atol(optarg);
	    kom_log("Will reset password of person %ld\n", (long)reset_pwd);
#ifdef ENCRYPT_PASSWORDS
            /* Seed the random number generator. */
            srand(time(NULL) + getpid());
#endif
	    need_rw = 1;
	    break;

	case 'G':
	    grant_all = atol(optarg);
	    kom_log("Will grant all bits to person %ld\n", (long)grant_all);
	    need_rw = 1;
	    break;

	case 'F':
	    force_output = 1;
	    need_rw = 1;
	    break;

	case 'o':		/* Select output format */
	    oformat = atoi(optarg);
	    if (oformat == 0 && *optarg != '0')
		restart_kom("%s: bad output format %s\n", argv[0], optarg);
	    /* Enumerate all supported output formats here. */
            set_output_format(oformat);
	    break;
	    
	default:
	    restart_kom("Try --help for usage message.\n");
	}
    }

    /* Check if help was requested. */
    if (hflag > 0)
	give_help(get_default_config_file_name());

    /* Continue reading in the configuration file. */

    if (optind < argc)
	config_file = argv[optind++];
    else
	config_file = get_default_config_file_name();

    read_configuration(config_file);
    free_default_config_file_name();

    /*
     *      Override configuration with command-line options
     */

    if (pers_pres_conf != -1)
    {
        kom_info.pers_pres_conf = pers_pres_conf;
	modifications++;
    }
    if (conf_pres_conf != -1)
    {
        kom_info.conf_pres_conf = conf_pres_conf;
	modifications++;
    }
    if (motd_conf != -1)
    {
        kom_info.motd_conf = motd_conf;
	modifications++;
    }
    if (kom_news_conf != -1)
    {
        kom_info.kom_news_conf = kom_news_conf;
	modifications++;
    }
    if (motd_of_lyskom != (Text_no)-1)
    {
	kom_info.motd_of_lyskom = motd_of_lyskom;
	modifications++;
    }

    if (optind != argc)
	restart_kom("%s: too many arguments.  %s --help for usage.\n",
		    argv[0], argv[0]);

    s_set_storage_management(smalloc, srealloc, sfree);

    if (need_rw)
    {
	if (lock_db() < 0)
	{
	    /* Don't actually die until something is entered on stdin in debug
	       mode.  This is mainly here for the benefit of the test suite,
	       but is could also be useful to be able to attach a debugger and
	       do pre-mortem debugging of the process at this point.  */
	    if (buglevel > 0)
	    {
		kom_log("Press enter to terminate dbck\n");
		getchar();
	    }

	    exit(1);
	}
	
	have_lock = 1;
    }

    init_data_base();
    errors = check_data_base();

    if (truncated_texts == TRUE)
	modifications++;

    if ( iflag )
	kom_log("Total of %d error%s remains.\n", errors, errors == 1 ? "" : "s");
    else if ( vflag && errors > 0 )
	kom_log("%d error%s found.\n", errors, errors == 1 ? "" : "s");

    if ( modifications > 0 || force_output)
    {
	kom_log("%d modification%s made. Syncing...\n",
	    modifications, modifications == 1 ? "" : "s");
	fflush(stdout);
	fflush(stderr);
	if (have_lock)
	    cache_sync_all();
	else
	    kom_log("Database not saved since the lock was not obtained.\n");
	kom_log("ready.\n");
    }

    if ( sflag )
	print_statistics();
	
    if (gflag && have_lock)
    {
	if ( modifications == 0 && errors == 0 )
	{
	    kom_log("No errors found. Compressing textfile.\n");
	    fflush(stdout);
	    fflush(stderr);
	    garb_text_file();
	    kom_log("ready.\n");
	}
	else
	{
	    kom_log("Found %d errors; performed %d modifications.\n",
		errors, modifications);
	    fflush(stdout);
	    fflush(stderr);
	    if (confirm ("garb texts anyhow?"))
	    {
		garb_text_file();
		kom_log("ready.\n");
	    }
	    else
	    {
		kom_log("Compression not done since errors was found.\n");
	    }
	}
    }

    if (have_lock)
	unlock_db();

    /* Don't actually die until something is entered on stdin in debug
       mode.  This is mainly here for the benefit of the test suite,
       but is could also be useful to be able to attach a debugger and
       do pre-mortem debugging of the process at this point.  */
    if (buglevel > 0)
    {
	kom_log("Press enter to terminate dbck\n");
	getchar();
    }

    return errors != 0;
}
