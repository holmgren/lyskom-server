/*
 * File: local_to_global.h
 *
 * Copyright 1996, 1998-1999, 2002-2003 Inge Wallin, Per Cederqvist
 */


#ifndef LOCAL2GLOBAL_H_INCLUDED
#define LOCAL2GLOBAL_H_INCLUDED

/* Set the block size.  This function may be called at most once,
   before any other function declared in this file is called.  Once
   the block size has been set it cannot be changed.  */
void l2g_set_block_size(int sz);

/* Contstructor.  Take raw memory and create an empty
   Local_to_global mapping. */
void l2g_init(Local_to_global *l2g);

/* Destructor.  Frees all memory associated with ``l2g'' and destruct
   the object, turning it into raw memory once again.  */
void l2g_destruct(Local_to_global *l2g);

/* Re-initialize ``l2g''.  "lg2_clear(&foo)" is equivalent to
   "l2g_destruct(&foo); l2g_init(&foo)" but slightly more efficient. */
void l2g_clear(Local_to_global *l2g);

/* Copy ``src'' to ``dest''.  Both ``src'' and ``dest'' must be
   constructed Local_to_global objects.  This operation will ensure
   that they contain the same data.  */
void l2g_copy(Local_to_global *dest, const Local_to_global *src);

/* Append a new mapping from LNO to TNO.  LNO must be higher than any
   LNO previously stored in the structure.
   Invalidates all iterators.  */
void l2g_append(Local_to_global *l2g, Local_text_no lno, Text_no tno);

/* Set a new mapping from LNO to TNO.  This can be used for any LNO,
   even on that is already defined, but this may be an expensive
   operation.  It should only be used by dbck to repair a damaged
   database.  Invalidates all iterators.  */
void l2g_expensive_set(Local_to_global *l2g, Local_text_no lno, Text_no tno);

/* Delete LNO from the structure.  Invalidates all iterators.  */
void l2g_delete(Local_to_global *l2g, Local_text_no lno);

/* Return the TNO previously stored under LNO.  Returns 0 if no such
   LNO exists.  */
Text_no l2g_lookup(const Local_to_global *l2g, Local_text_no lno);

/* Returns the next local text number, or 0 if lno is larger than the
   largest local text number.  */
Local_text_no l2g_next_key(const Local_to_global *l2g, Local_text_no lno);

/* Returns the lowest local text number that has never been present in
   the structure.  This is the same as one plus the highest local text
   number that is present in the structure (unless it has been deleted
   -- deletions do not cause this to decrease).  Returns 1 if the
   structure has always been empty. */
Local_text_no l2g_first_appendable_key(const Local_to_global *l2g);

/* Set the first the number returned by l2g_first_appendable_key.  You
   can only set it to a higher value than it already has.  This
   function is probably only useful when reading in a Local_to_global
   structure that was saved with something other than l2g_write.  */
void l2g_set_first_appendable_key(Local_to_global *l2g,
				  Local_text_no    key);

/* Delete global text number TNO.  This function can only be called if
   both the local and global text numbers are monotonous series, as
   they are in the Person::created_text_map field. */
void l2g_delete_global_in_sorted(Local_to_global *l2g, Text_no tno);

/* Dump all internal state.  This is used by the test suite to peek at
   the internal representation.  It should not be used except for such
   debugging purposes. */
void l2g_dump(FILE *file, const Local_to_global *l2g);

/* Write an external representation of the mapping to ``file''. */
void l2g_write(FILE *file, const Local_to_global *l2g);

/* Initialize the mapping from the representation found in ``file''.
   ``l2g'' must already be constructed.  */
Success l2g_read(FILE *file, Local_to_global *l2g);

/* Dump global usage statistics to ``file''. */
void dump_l2g_stats(FILE *file);



/* ================================================================ */


/*
 * Iterator for a Local_to_global.
 *
 * Usage:
 *
 * Local_to_global  l2g;
 * L2g_iterator     l2gi;
 *
 * for (l2gi_searchall(&l2gi, &l2g); !l2gi.search_ended; l2gi_next(&l2gi)) {
 *     use(l2gi.lno, l2gi.tno);
 * }
 *
 * or:
 *
 * for (l2gi_searchsome(&l2gi, &l2g, startval, endval);
 *      !l2gi.search_ended;
 *      l2gi_next(&l2gi)) 
 * {
 *     use(l2gi.lno, l2gi.tno);
 * }
 */

/* Create an iterator that will loop through all texts. */
void l2gi_searchall (L2g_iterator *l2gi, const Local_to_global *l2g);

/* Create an iterator that will iterate through all existing texts
   with a local text number lno such that
   	begin <= lno < end
   The search will be unbound if end is set to 0. */
void l2gi_searchsome(L2g_iterator *l2gi, const Local_to_global *l2g, 
		     Local_text_no begin, Local_text_no end);

/* Step the iterator forward.  */
void l2gi_next(L2g_iterator *l2gi);

/* This can only be used on iterators created with l2gi_searchsome.
   Returns the value supplied to the end argument when the iterator
   was created.  */
Local_text_no l2gi_end(const L2g_iterator *l2gi);

/* This can only be used on iterators created with l2gi_searchsome.
   Returns the value supplied to the start argument when the iterator
   was created.  */
Local_text_no l2gi_begin(const L2g_iterator *l2gi);

/* ================================================================ */


/*
 * Reverse iterator for a Local_to_global.
 *
 * Usage:
 *
 * Local_to_global  l2g;
 * L2g_reverse_iterator l2gi;
 *
 * for (l2gi_searchall_reverse(&l2gi, &l2g);   // Not implemented yet.
 *      !l2gi.search_ended;
 *      l2gi_prev(&l2gi))
 * {
 *     use(l2gi.lno, l2gi.tno);
 * }
 *
 * or:
 *
 * for (l2gi_searchsome_reverse(&l2gi, &l2g, lower_limit, upper_limit);
 *      !l2gi.search_ended;
 *      l2gi_prev(&l2gi)) 
 * {
 *     use(l2gi.lno, l2gi.tno);
 * }
 */

/* Create an iterator that will iterate through all existing texts
   with a local text number lno such that
   	begin <= lno < end
   starting at end-1 and ending at begin. */
void l2gi_searchsome_reverse(L2g_reverse_iterator *l2gi,
			     const Local_to_global *l2g, 
			     Local_text_no begin,
			     Local_text_no end);

/* Step the iterator forward, eh, backwards, eh, towards begin.  */
void l2gi_prev(L2g_reverse_iterator *l2gi);

#endif /* LOCAL2GLOBAL_H_INCLUDED */
