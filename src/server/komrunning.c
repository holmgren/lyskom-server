/*
 * $Id: komrunning.c,v 1.11 2003/08/23 16:38:16 ceder Exp $
 * Copyright (C) 1999, 2002-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include "timewrap.h"

#include "kom-types.h"
#include "s-string.h"
#include "string-malloc.h"
#include "server/smalloc.h"
#include "kom-config.h"
#include "server-config.h"
#include "param.h"
#include "pidfile.h"
#include "linkansi.h"
#include "eintr.h"

static void
usage(const char *arg0)
{
    fprintf(stderr,
	    "usage: %s [-c config-file] [ -v ] [ -V ] [start | stop]\n",
	    arg0);
    exit(1);
}

static void
create_status(const char *arg0)
{
    FILE *fp;
    int saved_errno;
    struct passwd *pwent;
    const char *user;

    user = getenv("USER");

    if (user == NULL)
    {
	pwent = getpwuid(getuid());
	if (pwent != NULL)
	    user = pwent->pw_name;
    }

    if (user == NULL)
	user = "root";

    fp = i_fopen(param.status_file, "w");
    if (fp == NULL)
    {
	saved_errno = errno;
	fputs(arg0, stderr);
	errno = saved_errno;
	perror(param.status_file);
	exit(1);
    }

    if (fputs(user, fp) == EOF || putc('\n', fp) == EOF)
    {
	saved_errno = errno;
	fprintf(stderr, "%s: writing to ", arg0);
	errno = saved_errno;
	perror(param.status_file);
	exit(1);
    }

    if (i_fclose(fp) != 0)
    {
	saved_errno = errno;
	fprintf(stderr, "%s: closing ", arg0);
	errno = saved_errno;
	perror(param.status_file);
	exit(1);
    }
}


static void
shutdown_lyskom(pid_t pid, const char *arg0)
{
    int saved_errno;

    if (pid < 2)
    {
	fprintf(stderr, "%s: insane pid %ld found in %s\n",
		arg0, (long)pid, param.pid_name);
	exit(1);
    }

    if (kill(pid, SIGTERM) != 0)
    {
	saved_errno = errno;
	fprintf(stderr, "%s: sending SIGTERM to pid %ld", arg0, (long)pid);
	errno = saved_errno;
	perror("");
	exit(1);
    }

    while (kill(pid, 0) == 0)
	sleep(1);

    if (errno != ESRCH)
    {
	saved_errno = errno;
	fprintf(stderr, "%s: sending signal 0 to pid %ld", arg0, (long)pid);
	errno = saved_errno;
	perror("");
	exit(1);
    }
}
	

int
main (int    argc,
      char **argv)
{
    const char *config_file = NULL;
    int bring_up = 0;
    int bring_down = 0;
    int i;
    pid_t pid;
    int saved_errno;
    struct stat statbuf;

    link_ansi();

    /* Initialize the string handling package. */
    s_set_storage_management(string_malloc, string_realloc, string_free);

    /* Parse command line arguments. */
    for (i = 1; i < argc && argv[i][0] == '-'; i++)
    {
	if (argv[i][1] == '\0' || argv[i][2] != '\0')
	    usage(argv[0]);

	switch (argv[i][1])
	{
	case 'c':
	    if (config_file != NULL)
	    {
		fprintf(stderr, "%s: -c may only be used once\n", argv[0]);
		exit(1);
	    }
	    if (++i >= argc)
		usage(argv[0]);
	    config_file = argv[i];
	    break;

	case 'V':
	case 'v':
	    fprintf(stderr, "komrunning from %s-%s\n", PACKAGE, VERSION);
	    exit(0);

	default:
	    usage(argv[0]);
	}
    }

    if (i < argc && !strcmp(argv[i], "start"))
    {
	i++;
	bring_up = 1;
    }

    if (i < argc && !strcmp(argv[i], "stop"))
    {
	i++;
	bring_down = 1;
    }

    if (i < argc || (bring_up && bring_down))
	usage(argv[0]);

    /* Read in the configuration file. */

    if (config_file == NULL)
	config_file = get_default_config_file_name();

    read_configuration(config_file);

    if (bring_up)
    {
	if (remove(param.status_file) < 0 && errno != ENOENT)
	{
	    saved_errno = errno;
	    fprintf(stderr, "%s: ", argv[0]);
	    errno = saved_errno;
	    perror(param.status_file);
	    exit(2);
	}
    }
    else
    {
	pid = read_pid_file(param.pid_name, argv[0]);
	if (bring_down)
	{
	    create_status(argv[0]);
	    shutdown_lyskom(pid, argv[0]);
	}
	else
	{
	    if (stat(param.status_file, &statbuf) == 0)
	    {
		if (pid < 1)
		    printf("LysKOM is probably DOWN (pid file not found)\n");
		else if (kill(pid, 0) == 0 || errno == EPERM)
		    printf("LysKOM is going DOWN (pid %ld)\n", (long)pid);
		else if (errno == ESRCH)
		    printf("LysKOM is DOWN\n");
		else
		    perror("kill");
	    }
	    else
	    {
		if (pid < 1)
		    printf("LysKOM is probably going up "
			   "(pid file not found)\n");
		else if (kill(pid, 0) == 0 || errno == EPERM)
		    printf("LysKOM is UP\n");
		else if (errno == ESRCH)
		    printf("LysKOM is going up\n");
		else
		    perror("kill");
	    }
	}
    }
    free_default_config_file_name();

    return 0;
}
