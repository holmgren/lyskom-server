/*
 * $Id: text.c,v 0.118 2003/08/25 17:24:23 ceder Exp $
 * Copyright (C) 1991-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * text.c
 *
 * All atomic calls that deals with texts.
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include "timewrap.h"
#include <setjmp.h>
#include <sys/types.h>
#include <sys/types.h>

#include "aux-no.h"
#include "server/smalloc.h"
#include "s-string.h"
#include "misc-types.h"
#include "kom-types.h"
#include "services.h"
#include "com.h"
#include "async.h"
#include "connections.h"
#include "kom-errno.h"
#include "manipulate.h"
#include "lyskomd.h"
#include "kom-config.h"
#include "internal-connections.h"
#include "cache.h"
#include "log.h"
#include "minmax.h"
#include "admin.h"
#include "send-async.h"
#include "param.h"
#include "kom-memory.h"
#include "aux-items.h"
#include "local-to-global.h"
#include "server-time.h"
#include "text.h"
#include "string-malloc.h"
#include "stats.h"


/*
 * Forward declarations
 */

static Text_no
do_create_text(const String   message,
               u_short	      no_of_misc,
               Misc_info     *misc,
               Aux_item_list *aux,
               Bool           anonymous,
               Text_stat    **ret_stat);

static void
filter_secret_info(Text_stat *result,
		   const Text_stat *original,
                   const Connection *viewer_conn,
                   Bool output_bcc);

static Bool
interested_party(const Connection *cptr,
		 Text_no tno,
		 const Text_stat *text_s);
/*
 * Static functions
 */


/*
 * Add text_no to the list of texts in a conference. Return the local number
 * the text gets in this conference.
 */

static Local_text_no
add_text_in_conf(Conference * conf_c,
		 Text_no      text_no)
{
    Local_text_no res;

    conf_c->last_written = current_time.tv_sec;
    
    /* Add number last on the text_list */
    res = l2g_first_appendable_key(&conf_c->texts);
    l2g_append(&conf_c->texts, res, text_no);
    return res;
}


/*
 * Count how many recipients and cc_recipients a text has.
 */

static int
count_recipients(const Text_stat  *t_stat)
{
    int 	 n = 0;
    Misc_info  * end = t_stat->misc_items + t_stat->no_of_misc;
    Misc_info  * misc;

    for (misc = t_stat->misc_items; misc < end; misc++)
	if (misc->type == recpt
	    || misc->type == cc_recpt
	    || misc->type == bcc_recpt)
	{
	    n++;
	}

    return n;
}

/*
 * Count how many footnotes a text has.
 */

static int
count_footn(const Text_stat *t_stat)
{
    int 	 n = 0;
    Misc_info  * end = t_stat->misc_items + t_stat->no_of_misc;
    Misc_info  * misc;

    for (misc = t_stat->misc_items; misc < end; misc++)
	if ( misc->type == footn_in )
	    n++;

    return n;
}

/*
 * Count how many commments a text has.
 */

static int
count_comment(const Text_stat *t_stat)
{
    int 	 n = 0;
    Misc_info  * end = t_stat->misc_items + t_stat->no_of_misc;
    Misc_info  * misc;

    for (misc = t_stat->misc_items; misc < end; misc++)
	if ( misc->type == comm_in )
	    n++;

    return n;
}


/*
 * Check if CONF_NO is a recipient of the text whose text_stat is given.
 */

static int
find_recipient(Conf_no	        conf_no,
	       const Text_stat *t_stat)
{
    int i;
    
    for (i = 0; i < t_stat->no_of_misc; i++)
    {
	switch (t_stat->misc_items[i].type)
	{
	case recpt:
	case cc_recpt:
	case bcc_recpt:
	    if (t_stat->misc_items[i].datum.recipient == conf_no)
		return i;
	    break;
	    
	case rec_time:
	case comm_to:
	case comm_in:
	case footn_to:
	case footn_in:
	case sent_by:
	case sent_at:
	case loc_no:
	    break;
	    
#ifndef COMPILE_CHECKS
	default:
#endif
        case unknown_info:
	    restart_kom("find_recipient(): illegal misc_item\n");
	}
    }

    return -1;
}


/*
 * Find the misc-index of the comment or footnote link between CHILD
 * and PARENT, or return -1 if no such comment or footnote link
 * exists.
 *
 * Note: the caller must check if the link is a comment or footnote
 * link.
 */
static int
find_textlink(const Text_stat *child,
	      Text_no          parent)
{
    int i;

    for (i = 0; i < child->no_of_misc; i++)
    {
	switch (child->misc_items[i].type)
	{
	case comm_to:
	case footn_to:
	    if (child->misc_items[i].datum.text_link == parent)
		return i;
	    break;
	default:
	    break;
	}
    }

    return -1;
}

/*
 * Return the conference which the text goes to. This is normally conf_no, but
 * it may be a super_conf. If ACTPERS is not allowed to submit a text to
 * conf_no or a super_conf, return 0.
 */
static Conf_no
submit_to(Conf_no	conf_no, /* The conference the user is trying to */
	  			 /* submit a text to. */
	  const Conference  *conf_c)	 /* May be NULL */
{
    int i;
    enum access acc;
    
    CHK_CONNECTION(0);
    CHK_LOGIN(0);
    
    if (conf_c == NULL)
	GET_C_STAT(conf_c, conf_no, 0);

    for (i=0; i < param.max_super_conf_loop; i++)
    {
	acc = access_perm(conf_no, active_connection, unlimited);
	
	if (acc <= none)
        {
            err_stat = conf_no;
            kom_errno = KOM_UNDEF_CONF;
	    return 0;
        }
	
	if (conf_c->permitted_submitters == 0
	    || acc == unlimited
	    || locate_membership(conf_c->permitted_submitters, ACT_P) != NULL)
	{
	    return conf_no;
	}


	if (conf_c->super_conf == 0)
        {
            err_stat = conf_no;
            kom_errno = KOM_ACCESS;
	    return 0;
        }
        conf_no = conf_c->super_conf;
	
	GET_C_STAT(conf_c, conf_no, 0);
    }

    err_stat = conf_no;
    kom_errno = KOM_ACCESS;
    return 0;
}


/*
 * Say that CHILD is a footnote to PARENT.
 */

static Success
do_add_footnote(Text_no    child,
		Text_stat *child_s,
		Text_no    parent,
		Text_stat *parent_s) /* May be NULL. */
{
    assert(child_s != NULL);

    if (parent_s == NULL)
	GET_T_STAT(parent_s, parent, FAILURE);

    ADD_MISC(child_s,  footn_to, text_link, parent);
    ADD_MISC(parent_s, footn_in, text_link, child);

    mark_text_as_changed(child);
    mark_text_as_changed(parent);
    
    return OK;
}


/*
 * Say that CHILD is a comment to PARENT.
 */

static Success
do_add_comment(Text_no    child,
	       Text_stat *child_s,
	       Text_no    parent,
	       Text_stat *parent_s) /* May be NULL. */
{
    assert(child_s != NULL);

    if (parent_s == NULL)
	GET_T_STAT(parent_s, parent, FAILURE);

    ADD_MISC(child_s,  comm_to, text_link, parent);
    ADD_MISC(parent_s, comm_in, text_link, child);

    mark_text_as_changed(child);
    mark_text_as_changed(parent);
    
    return OK;
}

    
/*
 * Say that RECEIVER is a recipient of TEXT.
 */
static Success
do_add_recipient(Text_no        text,
		 Text_stat    * text_s,
		 Conf_no        receiver,
		 enum info_type recv_type)
{
    Conference *rece_c;

    assert(recv_type == recpt
	   || recv_type == cc_recpt
	   || recv_type == bcc_recpt);

    assert(text_s != NULL);
    
    GET_C_STAT(rece_c, receiver, FAILURE);

    ADD_MISC(text_s, recv_type, recipient, receiver);
    ADD_MISC(text_s, loc_no, local_no, add_text_in_conf(rece_c, text));

    mark_text_as_changed(text);
    mark_conference_as_changed(receiver);
    
    return OK;
}

static Bool
is_member_in(const Person *person,
	     Conf_no conf_no,
	     Bool skip_passive)
{
    Membership *mship;

    if ((mship = locate_membership(conf_no, person)) != NULL)
    {
	if (!(mship->type.passive && skip_passive))
	    return TRUE;
    }
    return FALSE;
}



/*
 * Check if person is a member in any of the recipients,
 * cc_recipients or bcc_recipients of text_s.  If skip_passive is
 * true, ignore passive memberships.
 */
static  Bool
is_member_in_recpt(const Person    *person,
		   const Text_stat *text_s,
                   Bool             skip_passive)
{
    int i;

    for (i = 0; i < text_s->no_of_misc; i++)
    {
	switch(text_s->misc_items[i].type)
	{
	case recpt:
	case cc_recpt:
	case bcc_recpt:
	    if (is_member_in(person, text_s->misc_items[i].datum.recipient,
			     skip_passive) == TRUE)
		return TRUE;
	    break;

	case comm_to:
	case comm_in:
	case footn_to:
	case footn_in:
	case loc_no:
	case rec_time:
	case sent_by:
	case sent_at:
	    break;

#ifndef COMPILE_CHECKS
	default:
#endif
        case unknown_info:
	    kom_log("is_member_in_recpt(): bad misc_item.\n");
	    break;
	}
    }

    return FALSE;
}

static  Bool
is_member_in_recpt_of(const Person  *person,
		      const Text_no  text_no,
		      Bool           skip_passive)
{
    Text_stat *text_s;

    GET_T_STAT(text_s, text_no, FALSE);
    return is_member_in_recpt(person, text_s, skip_passive);
}


static void
report_bad_aux(Text_no tno,
	       String *data,
	       int tag,
	       const char *reason)
{
    static Text_no last = 0;
    char *d;

    if (tno == last)
	return;

    last = tno;
    d= s_crea_c_str(*data);
    kom_log("Bad aux-item %d found in text %ld: \"%s\": %s.\n",
	    tag, (unsigned long)tno, d, reason);
    string_free(d);
}


/*
 * Check if person is a member in any of the recipients of a text that
 * is linked to this text.  If skip_passive is true, ignore passive
 * memberships.
 */
static  Bool
is_member_in_linked_recpt(const Person    *person,
			  Text_no          tno,
			  const Text_stat *text_s,
			  Bool             skip_passive)
{
    int i;
    Text_no   linked_nr;
    String *data;
#if 0
    String copy;
#endif
    String_size end;

    for (i = 0; i < text_s->no_of_misc; i++)
    {
	switch(text_s->misc_items[i].type)
	{
	case comm_to:
	case comm_in:
	case footn_to:
	case footn_in:
	    linked_nr = text_s->misc_items[i].datum.text_link;
	    if (is_member_in_recpt_of(person, linked_nr, skip_passive) == TRUE)
		return TRUE;
	    break;

	case recpt:
	case cc_recpt:
	case bcc_recpt:
	case loc_no:
	case rec_time:
	case sent_by:
	case sent_at:
	    break;

#ifndef COMPILE_CHECKS
	default:
#endif
        case unknown_info:
	    kom_log("is_member_in_linked_recpt(): bad misc_item.\n");
	    break;
	}
    }

    for (i = 0; i < text_s->aux_item_list.length; i++)
    {
	data = &text_s->aux_item_list.items[i].data;
	switch (text_s->aux_item_list.items[i].tag)
	{
	case aux_cross_reference:
	    /* FIXME (bug 23): Until there is a mirroring aux-item,
	        there is no point doing this. */
#if 0
	    /* Data is "T", optional space, a text number, a space, and
	       some junk we don't care about. */
	    if (s_strlen(*data) < 2)
	    {
		report_bad_aux(tno, data, "cross-reference", "too short");
		continue;
	    }
	    if (data->string[0] != 'T')
		continue;
	    copy = s_fsubstr(*data, 1, END_OF_STRING);
	    linked_nr = s_strtol(copy, &end);
	    if (end == -1)
	    {
		report_bad_aux(tno, data, "cross-reference",
			       "no number found");
		continue;
	    }
	    if (end < s_strlen(copy) && copy.string[end] != ' ')
	    {
		report_bad_aux(tno, data, "cross-reference",
			       "trailing garbage");
		continue;
	    }

	    if (is_member_in_recpt_of(person, linked_nr, skip_passive) == TRUE)
		return TRUE;

#endif
	    break;

	case aux_mx_mime_belongs_to:
	case aux_mx_mime_part_in:
	    /* Data is a single text number. */

	    linked_nr = s_strtol(*data, &end);
	    if (end == -1)
	    {
		report_bad_aux(tno, data, text_s->aux_item_list.items[i].tag,
			       "bad number");
		continue;
	    }
	    if (end != s_strlen(*data))
	    {
		report_bad_aux(tno, data, text_s->aux_item_list.items[i].tag,
			       "trailing garbage");
		continue;
	    }

	    if (is_member_in_recpt_of(person, linked_nr, skip_passive) == TRUE)
		return TRUE;
	    break;

	case aux_faq_for_conf:
	    /* Data is a conference number.  0 means the entire system.  */
	    linked_nr = s_strtol(*data, &end);
	    if (end == -1)
		continue;
	    if (end != s_strlen(*data))
		continue;

	    if (linked_nr == 0
		|| is_member_in(person, linked_nr, skip_passive) == TRUE)
		return TRUE;
	    break;

	default:
	    break;
	}
    }
    
    return FALSE;
}


/*
 * Return number of lines in a text
 */

static u_short
count_lines(String str)
{
    u_short l = 0;

    while (str.len-- > 0)
	if(*str.string++ == '\n')
	    l++;

    return l;
}


/*
 * Delete misc_info at location loc.
 * If it is a recpt, cc_recpt, comm_to or footn_to delete any
 * loc_no, rec_time, sent_by or sent_at that might follow it.
 *
 * Note that the Misc_info is not reallocated.
 */

static void
do_delete_misc(u_short	 * no_of_misc,
	       Misc_info * misc,
	       int	   loc)
{
    int del = 1;		/* Number of items to delete. */
    				/* Always delete at least one item. */
    Bool ready = FALSE;
    
    /* Check range of loc */

    if (loc < 0 || loc >= *no_of_misc)
	restart_kom("do_delete_misc() - loc out of range");
    
    while (ready == FALSE && loc + del < *no_of_misc)
    {
	switch (misc[loc+del].type)
	{
	case loc_no:
	case rec_time:
	case sent_by:
	case sent_at:
	    del++;
	    break;

	case recpt:
	case cc_recpt:
        case bcc_recpt:
	case footn_to:
	case footn_in:
	case comm_to:
	case comm_in:
	    ready = TRUE;
	    break;
	    
#ifndef COMPILE_CHECKS
	default:
#endif
        case unknown_info:
	    restart_kom("do_delete_misc() - illegal misc");
	}
    }

    *no_of_misc -= del;

    /* Move items beyond the deleted ones. */

    while (loc < *no_of_misc)
    {
	misc[loc] = misc[loc+del];
	loc++;
    }
}


static void
send_async_sub_recipient(Text_no          text_no,
			 const Text_stat *text_s,
			 Conf_no          conf_no,
			 enum info_type   type)
{
    Connection *cptr;
    Session_no i = 0;

    while ((i = traverse_connections(i)) != 0)
    {
	cptr = get_conn_by_number(i);
	if (interested_party(cptr, text_no, text_s)
	    && text_read_access(cptr, text_no, text_s))
	{
	    Text_stat copy;

	    filter_secret_info(&copy, text_s, cptr, TRUE);
	    if (find_recipient(conf_no, &copy) != -1)
		async_sub_recipient(cptr, text_no, conf_no, type);
	}
    }
}

/*
 * Delete the recipient ``conf_no'' from text ``text_no''.
 * The misc-item is locate at position ``loc'' in the miscinfolist.
 * ``text_s'' and ``conf_s'' must be supplied.  An async-sub-recipient
 * message will be sent if ``want_async'' is true.
 */
static Success
perform_subtraction(Text_no     text_no,
		    Text_stat  *text_s,
		    Conf_no     conf_no,
		    Conference *conf_s, /* NULL if the conf_no doesn't exist */
		    int         loc,
		    Bool        want_async)
{
    if (conf_s != NULL)
    {
	/* Only if the conference exists: */
	if (text_s->misc_items[loc+1].type == loc_no)
	{
	    l2g_delete(&conf_s->texts,
		       text_s->misc_items[loc+1].datum.local_no);
	    mark_conference_as_changed(conf_no);
	}
	else
	{
	    kom_log("Bad misc-info-list detected in perform_subtraction()"
		    " for text number %lu.\n", (unsigned long)text_no);
	}
    }

    if (want_async)
	send_async_sub_recipient(text_no, text_s, conf_no,
				 text_s->misc_items[loc].type);

    do_delete_misc(&text_s->no_of_misc, text_s->misc_items, loc);
    mark_text_as_changed(text_no);

    return OK;
}

/*
 * Delete a recipient from a text. Does not fail if the recipient doesn't
 * exist - that is not an error.  (The recipients of a text are not
 * removed when a conference is deleted.)
 */
static Success
do_sub_recpt(Text_no	    text_no,
	     Text_stat    * text_s,  /* May be NULL */
	     Conf_no        conf_no,
	     Conference   * conf_s,  /* May be NULL */
	     Bool           want_async)
{
    int i;

    if (text_s == NULL)
	GET_T_STAT(text_s, text_no, FAILURE);
    
    if (conf_s == NULL)
	conf_s = cached_get_conf_stat(conf_no); /* Might still be NULL. */
    	
    for (i = 0; i < text_s->no_of_misc; i++)
    {
	switch (text_s->misc_items[i].type)
	{
	case recpt:
	case cc_recpt:
	case bcc_recpt:
	    if (text_s->misc_items[i].datum.recipient == conf_no)
		return perform_subtraction(text_no, text_s, conf_no, conf_s,
					   i, want_async);
	    break;

	case comm_to:
	case comm_in:
	case footn_to:
	case footn_in:
	case loc_no:
	case rec_time:
	case sent_by:
	case sent_at:
	    break;
	    
#ifndef COMPILE_CHECKS
	default:
#endif
        case unknown_info:
	    kom_log("do_sub_recpt(): bad misc_item.\n");
	    break;
	}
    }

    err_stat = conf_no;
    kom_errno = KOM_NOT_RECIPIENT;
    return FAILURE;
}


/* OTHER is a INFO_TYPE to text number TEXT (whose text status is
   TEXT_S).  This function removes that misc-info from TEXT_S.  */
static void
remove_misc_item(Text_no        text,
		 Text_stat     *text_s,
		 enum info_type info_type,
		 Text_no        other)
{
    int i;

    assert(info_type == comm_to || info_type == comm_in
	   || info_type == footn_to || info_type == footn_in);

    for (i = 0; i < text_s->no_of_misc; i++)
    {
	if (text_s->misc_items[i].type == info_type
	    && text_s->misc_items[i].datum.text_link == other)
	{
	    do_delete_misc(&text_s->no_of_misc, text_s->misc_items, i);
	    mark_text_as_changed(text);
	    return;
	}
    }

    restart_kom("remove_misc_item() failed.\n");
}

/*
 * Delete the link between comment and comment_to.
 */
static void
do_sub_comment(Text_no	      comment,    /* The comment. */
	       Text_stat    * text_s,
	       Text_no	      comment_to, /* The commented. */
	       Text_stat    * parent_s )
{
    if (text_s == NULL)
	VOID_GET_T_STAT(text_s, comment);

    if (parent_s == NULL)
	VOID_GET_T_STAT(parent_s, comment_to);
    
    remove_misc_item(comment, text_s, comm_to, comment_to);
    remove_misc_item(comment_to, parent_s, comm_in, comment);
}


/*
 * Delete the link between footnote and footnote_to.
 */
static void
do_sub_footnote(Text_no        footnote,
		Text_stat    * text_s,
		Text_no	       footnote_to,
		Text_stat    * parent_s)
{
    if (text_s == NULL)
	VOID_GET_T_STAT(text_s, footnote);

    if (parent_s == NULL)
	VOID_GET_T_STAT(parent_s, footnote_to);

    remove_misc_item(footnote, text_s, footn_to, footnote_to);
    remove_misc_item(footnote_to, parent_s, footn_in, footnote);
}


/*
 * Return true if the sender of T_STAT misc-item number I is a person
 * that the one logged in on CONN is a supervisor of.
 */
static Bool
is_supervisor_of_sender(const Text_stat *t_stat,
			int i,
			const Connection *conn)
{
    for (i++ ; i < t_stat->no_of_misc; i++)
    {
	switch (t_stat->misc_items[i].type)
	{
	case sent_by:
	    return is_supervisor(t_stat->misc_items[i].datum.sender, conn);

	case recpt:
	case cc_recpt:
        case bcc_recpt:
	case comm_to:
	case comm_in:
	case footn_to:
	case footn_in:
	case sent_at:
	    return FALSE;       /* No sender. */

	case loc_no:
	case rec_time:
	    break;		/* These may come before a sent_by. */

#ifndef COMPILE_CHECKS
	default:
#endif
        case unknown_info:
	    restart_kom("ERROR: is_supervisor_of_sender(): "
			"Illegal misc_item found.\n");
	}
    }
    return FALSE;		/* No sender. */
}


/*
 * Check if ACTPERS is allowed to add a footnote to a text. Sets errno if
 * there is an error.
 *
 * Note: Caller must set err_stat
 */

static Success
check_footn(const Text_stat * t_stat)
{
    if (active_connection == NULL
	|| t_stat->author != ACTPERS)
    {
	kom_errno = KOM_NOT_AUTHOR;
	return FAILURE;
    }

    if (count_footn(t_stat) >= param.max_foot)
    {
        err_stat = 0;
	kom_errno = KOM_FOOT_LIMIT;
	return FAILURE;
    }

    return OK;
}

/*
 * Check if ACTPERS is allowed to add a comment to a text. Sets errno if
 * there is an error. Note that it is allowed to comment a text even if
 * you are not allowed to read it.
 */

static Success
check_comm(const Text_stat * t_stat)
{
    if (count_comment(t_stat) >= param.max_comm)
    {
	kom_errno = KOM_COMM_LIMIT;
	return FAILURE;
    }

    return OK;
}


/*
 * Return a pointer to a Mark if pers_no has marked the text text_no.
 * Otherwise, return NULL.
 */
static Mark *
locate_mark(Pers_no pers_no,
	    const Person *pers_p,	/* May be NULL. */
	    Text_no text_no)
{
    Mark *mp;
    Mark *result = NULL;
    int i;
    Mark_list mlist;

    if (pers_p == NULL)
	GET_P_STAT(pers_p, pers_no, NULL);

    mlist = pers_p->marks;
    
    for (i = mlist.no_of_marks, mp = mlist.marks;
	 i > 0 && result == NULL;
	 i--, mp++)
    {
	if (mp->text_no == text_no)
	    result = mp;
    }

    return result;
}

/*
 * Skip one misc-item with all additional data.
 * 
 * This is only used from get_text_stat.
 */
static unsigned short
skip_recp(unsigned short    misc_index,
	  const Text_stat  *text_stat)
{
    ++misc_index;
    while (misc_index < text_stat->no_of_misc)
    {
	switch (text_stat->misc_items[misc_index].type)
	{
	case loc_no:
	case rec_time:
	case sent_by:
	case sent_at:
	    ++misc_index;
	    break;

	case recpt:
	case cc_recpt:
        case bcc_recpt:
	case comm_to:
	case comm_in:
	case footn_to:
	case footn_in:
	    return misc_index;
	    break;
#ifndef COMPILE_CHECKS
	default:
#endif
        case unknown_info:
	    restart_kom("skip_recp() - illegal misc\n");
	}
    }
    return misc_index;
}


/*
 * Copy the text_stat original into result, but only those part
 * that viewer is allowed to see.  (Censor away information about
 * conferences that viewer is not allowed to know about).
 *
 * All memory is allocated via tmp_alloc().
 */
static void
filter_secret_info(Text_stat *result,
		   const Text_stat *original,
                   const Connection *viewer_conn,
                   Bool output_bcc)
{
    unsigned short   orig;
    unsigned short   copy;
    Pers_no          bcc;

    /* FIXME (bug 177): Possible optimisation:
       No need to copy unless there is a secret conf among the recipients. */

    *result = *original;

    filter_aux_item_list(&original->aux_item_list,
                         &result->aux_item_list,
                         viewer_conn);

    result->misc_items = tmp_alloc(result->no_of_misc * sizeof(Misc_info));
    result->no_of_misc = 0;

    copy = 0;
    for (orig = 0; orig < original->no_of_misc; )
    {
	switch (original->misc_items[orig].type)
	{
	case recpt:
	case cc_recpt:
	    if (!has_access(original->misc_items[orig].datum.recipient,
			    viewer_conn, read_protected)
		&& !ENA_C(viewer_conn, admin, 4))
	    {
		orig = skip_recp(orig, original);
	    }
	    else
	    {
		result->misc_items[copy++] = original->misc_items[orig++];
		++result->no_of_misc;
	    }
	    break;

        case bcc_recpt:
            /*
             * We will send this if any of the following is true:
             * - The viewer is the recipient.
             * - The recipient is an open conference.
             * - The viewer sent the BCC.
             * - The viewer is supervisor of the author, and allowed
	     *   to know that the recipient exists.
             */

	    bcc = original->misc_items[orig].datum.recipient;
            if (viewer_conn->person != NULL
		&& (has_access(bcc, viewer_conn, limited)
		    || locate_membership(bcc, viewer_conn->person)
		    || ENA_C(viewer_conn, admin, 4)
		    || is_supervisor_of_sender(original, orig, viewer_conn)
		    || (is_supervisor(original->author, viewer_conn)
			&& (has_access(bcc, viewer_conn, read_protected)))))
            {
		result->misc_items[copy++] = original->misc_items[orig++];
		++result->no_of_misc;
                if (!output_bcc)
                    result->misc_items[copy-1].type = cc_recpt;
            }
            else
            {
		orig = skip_recp(orig, original);
            }
            break;

	case loc_no:
	case rec_time:
	case comm_to:
	case comm_in:
	case footn_to:
	case footn_in:
	case sent_by:
	case sent_at:
	    result->misc_items[copy++] = original->misc_items[orig++];
	    ++result->no_of_misc;
	    break;

#ifndef COMPILE_CHECKS
	default:
#endif
        case unknown_info:
	    restart_kom("filter_secret_info() - illegal misc_item!\n");    
	}
    }
}

static Bool
interested_party(const Connection *cptr,
		 Text_no           tno,
		 const Text_stat  *text_s)
{
    if (cptr->person == NULL)
	return FALSE;

    if (is_member_in_recpt(cptr->person, text_s, TRUE) == TRUE)
	return TRUE;

    if (is_member_in_linked_recpt(cptr->person, tno, text_s, TRUE) == TRUE)
	return TRUE;

    return FALSE;
}


/*
 * Send message to all interested parties that a text has been deleted.
 */

static void
send_async_deleted_text(Text_no text_no,
			const Text_stat *text_s)
{
    Connection *cptr;
    Text_stat filtered;
    Session_no i = 0;

    init_text_stat(&filtered);

    while ((i = traverse_connections(i)) != 0)
    {
        cptr = get_conn_by_number(i);
	if (interested_party(cptr, text_no, text_s)
	    && text_read_access(cptr, text_no, text_s))
	{
	    filter_secret_info(&filtered, text_s, cptr, TRUE);
            async_deleted_text(cptr, text_no, &filtered);
        }
    }

#if 0
    /*
     * This is not strictly necessary since the text-stat is sent
     * in the previous message, and the clients should be able to
     * update their caches from that information.
     */

    for (misc = text_s->misc_items;
         misc < text_stat->misc_items + text_stat->no_of_misc;
         misc++)
    {
        if (misc->type == footn_in
	    || misc->type == comm_in
	    || misc->type == comm_to
	    || misc->type == footn_to)
        {
            i = 0;
            c_text_no = 0;
            if (misc->type == comm_to)
                c_text_no = misc->datum.comment_to;
            else if (misc->type == comm_in)
                c_text_no = misc->datum.comment_in;
            else if (misc->type == footn_in)
                c_text_no = misc->datum.footnote_in;
            else if (misc->type == footn_to)
                c_text_no = misc->datum.footnote_to;

            c_text = get_text_stat(c_text_no);
            if (c_text)
            {
                while ((i = traverse_connections(i)) != 0)
                {
                    cptr = get_conn_by_number(i);
                    if (cptr->person != NULL
                        && is_member_in_recpt(cptr->person, c_text) == TRUE)
                    {
                        async_invalidate_text(cptr,
                                              c_text_no);
                    }
                }
            }
        }
    }
#endif
}


/*
 * End of static functions.
 */

/*
 * Functions that are exported to the rest of the server.
 */

Bool
text_read_access(const Connection *conn,
                 Text_no           text_no,
		 const Text_stat  *text_stat)
{
    int             i           = 0;
    Misc_info      *misc        = NULL;
    Conference     *recipient   = NULL;

    if (conn == NULL)
    {
	kom_log("ERROR: text_read_access() called with conn==NULL for t=%ld\n",
		(unsigned long)text_no);
	return FALSE;
    }

    /* Everyone may read the MOTD of KOM */

    if (text_no == kom_info.motd_of_lyskom)
	return TRUE;

    if (text_stat == NULL
	&& (text_stat = cached_get_text_stat(text_no)) == NULL)
    {
	if (!conn->pers_no)
	{
	    /* If you are not logged in, you are not allowed to know
	       if the text exists or not. */
	    err_stat = 0;
	    kom_errno = KOM_LOGIN;
	}
	return FALSE;
    }

    /* Check for world-readable aux item. */

    for (i = 0; i < text_stat->aux_item_list.length; i++)
	if (text_stat->aux_item_list.items[i].tag == aux_world_readable)
	    return TRUE;

    /* Users who are not logged in may not read anything else. */

    if (!conn->pers_no)
    {
        err_stat  = 0;
	kom_errno = KOM_LOGIN;
	return FALSE;
    }

    /* Some bits let you read everything. */

    if (ENA_C(conn, wheel, 10))
        return TRUE;

    if (text_stat->author == conn->pers_no)
        return TRUE;
    
    /* Check if conn->pers_no or current working conference is a recipient */
    
    for (i = text_stat->no_of_misc, misc = text_stat->misc_items;
         i; i--, misc++)
    {
        if ((misc->type == recpt
	     || misc->type == cc_recpt
	     || misc->type == bcc_recpt)
	    && (misc->datum.recipient == conn->cwc
		|| misc->datum.recipient == conn->pers_no))
        {
            return TRUE;
        }
    }

    /* Check if conn->pers_no is member in any of the recipients */
    
    for (i = text_stat->no_of_misc, misc = text_stat->misc_items;
         i > 0;
	 i--, misc++)
    {
        if ((misc->type == recpt
	     || misc->type == cc_recpt
	     || misc->type == bcc_recpt)
	    && locate_membership(misc->datum.recipient, conn->person) != NULL)
        {
            return TRUE;
        }
    }
    
    if (locate_mark(conn->pers_no, conn->person, text_no) != NULL)
        return TRUE;


    /* Check if any of the recipients is an open conference, or if the
       user of conn is a supervisor.  (Note: He is not supervisor of
       anything if he isn't logged in.)  */

    for (i = text_stat->no_of_misc, misc = text_stat->misc_items;
	 i > 0;
	 i--, misc++)
    {
 	if ((misc->type == recpt
	     || misc->type == cc_recpt
	     || misc->type == bcc_recpt)
	    && (recipient=cached_get_conf_stat(misc->datum.recipient)) != NULL)
	{
	    if (!recipient->type.rd_prot
		|| (ENA_C(conn, wheel, 8))
		|| is_supervisor(misc->datum.recipient, conn) == TRUE)
	    {
		return TRUE;
	    }
	}
    }

    return FALSE;
}


Success
do_delete_text(Text_no    text_no,
	       Text_stat *text_s)
{
    Person *author;
    
    if (text_s == NULL)
	GET_T_STAT(text_s, text_no, FAILURE);

    send_async_deleted_text(text_no, text_s);

    if ((author = cached_get_person_stat(text_s->author)) != NULL)
    {
	l2g_delete_global_in_sorted(&author->created_texts, text_no);
	mark_person_as_changed(text_s->author);
    }

    while (text_s->no_of_misc > 0)
    {
	switch (text_s->misc_items[0].type)
	{
	case recpt:
	case cc_recpt:
        case bcc_recpt:
	    if (do_sub_recpt(text_no, text_s,
			     text_s->misc_items[0].datum.recipient,
			     NULL, FALSE) != OK)
		restart_kom("do_delete_text(): error pos 1.\n");
	    break;

	case comm_to:
	    do_sub_comment(text_no, text_s,
			   text_s->misc_items[0].datum.text_link, NULL);
	    break;
	    
	case comm_in:
	    do_sub_comment(text_s->misc_items[0].datum.text_link, NULL,
			   text_no, text_s);
	    break;
	    
	case footn_to:
	    do_sub_footnote(text_no, text_s,
			    text_s->misc_items[0].datum.text_link, NULL);
	    break;
	    
	case footn_in:
	    do_sub_footnote(text_s->misc_items[0].datum.text_link, NULL,
			    text_no, text_s);
	    break;
	    

	case loc_no:
	case rec_time:
	case sent_by:
	case sent_at:
	    restart_kom("do_delete_text(): Illegal misc-item syntax.\n");
#ifndef COMPILE_CHECKS
	default:
#endif
        case unknown_info:
	    restart_kom("do_delete_text(): Illegal misc-item.\n");
	}
    }

    cached_delete_text(text_no);
    update_stat(STAT_TEXTS, -1);
    return OK;
}

/*
 * Atomic calls.
 */

/*
 * Calls to handle marks.
 *
 * Marks are secret. No else can know what you have marked.
 */

/*
 * Get text_nos of all marked texts.
 */
extern Success
get_marks(Mark_list *result)
{
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    *result = ACT_P->marks;
    return OK;
}




/*
 *  Get the text. The text will not be marked as read until you
 *  explicitly mark_as_read() it. start_char = 0 && end_char = END_OF_STRING
 *  gives the entire text.
 */
extern Success
get_text(Text_no       text_no,
	 String_size   start_char,
	 String_size   end_char,
	 String      * result)
{
    Text_stat  * text_s;

    CHK_CONNECTION(FAILURE);
    GET_T_STAT(text_s, text_no, FAILURE);
    
    /* Check if ACTPERS has read acess to the text */
    
    if (text_read_access(active_connection, text_no, text_s) != TRUE)
    {
	kom_errno = KOM_NO_SUCH_TEXT;
	err_stat = text_no;
	
    	return FAILURE;
    }

    *result = cached_get_text(text_no);

    if (start_char > result->len)
    {
        err_stat = start_char;
	kom_errno = KOM_INDEX_OUT_OF_RANGE;
	return FAILURE;
    }

    /* FIXME (bug 179): Should use something in s-string.c to do this.  */
    result->string += start_char;
    result->len = min(result->len - 1, end_char) + 1 - start_char;

    if (ACTPERS)
    {
	++ACT_P->no_of_text_fetches;
	mark_person_as_changed(ACTPERS);
    }
	
    return OK;
}


/*
 * Get text status.
 *
 * If there are recipients of the text that are secret confs
 * those misc-items will be censored.
 */

extern Success
get_text_stat_old(Text_no text_no,
		  Text_stat *result)
{
    Text_stat	* text_stat;

    CHK_CONNECTION(FAILURE);
    GET_T_STAT(text_stat, text_no, FAILURE);
    
    if (!text_read_access(active_connection, text_no, text_stat)
	&& !ENA(admin, 2)) /* OK -- In an RPC call */
    {
	kom_errno = KOM_NO_SUCH_TEXT;
        err_stat = text_no;
	return FAILURE;
    }

    filter_secret_info(result, text_stat, active_connection, FALSE);
    return OK;
}

extern Success
get_text_stat(Text_no	  text_no,
	      Text_stat *result)
{
    Text_stat	* text_stat;

    CHK_CONNECTION(FAILURE);
    GET_T_STAT(text_stat, text_no, FAILURE);
    
    if (!text_read_access(active_connection,
			  text_no,
			  text_stat)
	&& !ENA(admin, 2)) /* OK -- In an RPC call */
    {
	kom_errno = KOM_NO_SUCH_TEXT;
        err_stat = text_no;
	return FAILURE;
    }

    filter_secret_info(result, text_stat, active_connection, TRUE);
    return OK;
}

/*
 * Functions local to create_text:
 */


/*
 * Check that the text can be submitted anonymously
 */

static Success
check_anonymous_subm(Conf_no addressee)
{
    Conf_type conf_type;

    conf_type = cached_get_conf_type(addressee);
    if (conf_type.allow_anon)
	return OK;
    else
	return FAILURE;
}

/*
 * Check that the recipient or cc_recipient at LOC is not already a
 * recipient or cc_recipient of this text.
 */
static Success
check_double_subm(const Misc_info *misc,
		  int		   loc,
		  Conf_no	   addressee)
{
    int j;
    
    for (j = 0; j < loc; j++)
    {
	switch (misc[j].type)
	{
	case recpt:
	case cc_recpt:
	case bcc_recpt:
	    if (misc[j].datum.recipient == addressee)
		return FAILURE;

	    break;

	case comm_to:
	case comm_in:
	case footn_to:
	case footn_in:
	case loc_no:
	case rec_time:
	case sent_by:
	case sent_at:
	    break;
	    
#ifndef COMPILE_CHECKS
	default:
#endif
        case unknown_info:
	    kom_log("check_double_subm(): bad misc_item.\n");
	    break;
	}
    }
    return OK;
}

/*
 * Check that none of the first 'pos' misc_items pointed to by misc
 * is a comment or footnote to text forbidden.
 */
static Success
check_double_comm(const Misc_info *misc,
		  int              pos,
		  Text_no          forbidden)
{
    for (;pos > 0; pos--, misc++)
    {
	switch (misc->type)
	{
	case comm_to:
	case footn_to:
	    if (misc->datum.text_link == forbidden)
		return FAILURE;
	    break;

	case recpt:
	case cc_recpt:
        case bcc_recpt:
	case comm_in:
	case footn_in:
	case loc_no:
	case rec_time:
	case sent_by:
	case sent_at:
	    break;
	    
#ifndef COMPILE_CHECKS
	default:
#endif
        case unknown_info:
	    kom_log("check_double_comm(): bad misc_item.\n");
	    break;
	}
    }

    return OK;
}

	
    
/*
 * Check that all misc_items are legal. Return OK / FAILURE.
 * Update recpt & cc_recpt fields if the text goes to a super_conf.
 * Signal an error if a conference is recipient more than once, or if
 * a text is a comment to the same text more than once.
 */
static Success
create_text_check_misc(u_short   *no_of_misc,
		       Misc_info *misc,
		       Bool       anonymous,
		       Bool       need_public_jubel)
{
    int		i;
    Text_stat  *parent;
    Conf_no	addressee;

    for (i = 0; i < *no_of_misc; i++)
    {
	err_stat = i;	/* In case of an error, err_stat indicates which
			   misc_item caused the error. */

	switch (misc[i].type)
	{
	case footn_to:
	case comm_to:
	    GET_T_STAT(parent, misc[i].datum.text_link, FAILURE);

	    if (!text_read_access(active_connection,
                                  misc[i].datum.text_link,
                                  parent))
	    {
                err_stat = misc[i].datum.text_link;
		kom_errno = KOM_NO_SUCH_TEXT;
		return FAILURE;
	    }
	    
	    if ((misc[i].type == footn_to ? check_footn : check_comm)(parent)
		== FAILURE)
            {
                err_stat = misc[i].datum.text_link;
		return FAILURE;
            }

	    if (check_double_comm(misc, i, misc[i].datum.text_link) != OK)
	    {
                err_stat = i;
		kom_errno = KOM_ILL_MISC;
		return FAILURE;
	    }
	    break;
	    
	case recpt:
	case cc_recpt:
	case bcc_recpt:
	    /* Check that ACTPERS has write access to the conference or a
	       superconference. */
	    /* Superconfs are recursive */
 
 	    addressee = submit_to(misc[i].datum.recipient, NULL);

	    /* Update in case of super_conf */
	    
            misc[i].datum.recipient = addressee;
	    if (addressee == 0)
	    {
                /* Error data set in submit_to */
		return FAILURE;
	    }

	    /* Check that this recipient is not already a recipient. */

	    if (check_double_subm(misc, i, addressee) != OK)
	    {
                err_stat = i;
		kom_errno = KOM_ILL_MISC;
		return FAILURE;
	    }

	    /* Check that an anonymous text can be created */

	    if (anonymous && check_anonymous_subm(addressee) != OK)
	    {
                err_stat = addressee;
		kom_errno = KOM_ANON_REJECTED;
		return FAILURE;
	    }

	    /* Check if this is a public conference. */

	    if (need_public_jubel)
	    {
		Conf_type conf_type;

		conf_type = cached_get_conf_type(addressee);
		if (!conf_type.rd_prot)
		    need_public_jubel = FALSE;
	    }
	    break;

	case loc_no:		/* Ignore loc_no */
	    break;

        case unknown_info:      /* Unrecognized info type */
            err_stat = misc[i].datum.unknown_type;
            kom_errno = KOM_ILLEGAL_INFO_TYPE;
            return FAILURE;
            
	case comm_in:
	case footn_in:
	case rec_time:
	case sent_by:
	case sent_at:
	    /* Fall through */
#ifndef COMPILE_CHECKS
 	default:
#endif
            err_stat = i;
	    kom_errno = KOM_ILL_MISC;
	    return FAILURE;
	}
    }

    if (need_public_jubel)
    {
	/* We still have not found an open conference among the recipients. */
	err_stat = 0;
	kom_errno = KOM_TEMPFAIL;
	kom_log("Jubel stopped because no public recipient was found.\n");
	return FAILURE;
    }

    return OK;
}


/*
 * Add the aux_infos to the new text stat
 */
static Success
create_text_add_aux(Text_stat *t_stat,
                    Text_no text_no,
		    Aux_item_list *aux,
                    Pers_no creator)
{
    u_short i;
    Text_stat *parent;

    text_stat_add_aux_item_list(t_stat, text_no, aux, creator);

    for (i = 0; i < t_stat->no_of_misc; i++)
    {
        switch (t_stat->misc_items[i].type)
        {
        case footn_to:
        case comm_to:
            parent =
                cached_get_text_stat(t_stat->misc_items[i].datum.text_link);
            if (parent != NULL
		&& text_read_access(active_connection,
				    t_stat->misc_items[i].datum.text_link,
				    parent))
            {
                aux_inherit_items(&t_stat->aux_item_list,
                                  &parent->aux_item_list,
                                  &t_stat->highest_aux,
                                  t_stat->author,
                                  text_no, t_stat);
            }
            break;

        default:
            break;
        }
    }
    return OK;
}


/*
 * Fix all double references. Eg if misc contains comm_to, add a comm_in field.
 * No access-permission checking is done.
 */
static Success
create_text_add_miscs(Text_no     new_text,
		      Text_stat  *new_stat,
		      int         no_of_misc,
		      Misc_info * misc)
{
    int		 i;
    Conference  *conf_c;

    for (i = 0; i < no_of_misc; i++)
    {
	err_stat = i;	/* In case of an error, err_stat indicates which
			   misc_item caused the error. */

	switch (misc[i].type)
	{
	case footn_to:
	    if (do_add_footnote(new_text, new_stat,
				misc[i].datum.text_link, NULL) != OK)
		return FAILURE;

	    break;

	case comm_to:
	    if (do_add_comment(new_text, new_stat,
			       misc[i].datum.text_link, NULL) != OK)
		return FAILURE;

	    break;

	case recpt:
	case cc_recpt:
	case bcc_recpt:
	    if (do_add_recipient(new_text, new_stat,
				 misc[i].datum.recipient, misc[i].type) != OK)
		return FAILURE;
            GET_C_STAT(conf_c, misc[i].datum.recipient, FAILURE);
            conf_c->last_written = current_time.tv_sec;
	    break;

	case loc_no:		/* Ignore loc_no. */
	    break;

	case comm_in:
	case footn_in:
	case rec_time:
	case sent_by:
	case sent_at:
	    /* Fall through */
#ifndef COMPILE_CHECKS
	default:
#endif
        case unknown_info:
	    restart_kom("create_text_add_misc() - illegal enum info_type");
	}
    }
    return OK;
}

/*
 * Send an asynchronous message, but filter any secret information.
 */

static void
send_async_new_text_old(Text_no text_no,
                        const Text_stat *text_s)
{
    Connection *cptr;
    Session_no i = 0;
    Text_stat filtered;

    init_text_stat(&filtered);

    while ((i = traverse_connections(i)) != 0)
    {
	cptr = get_conn_by_number(i);

	/*
	 * filter_secret_info copies the text-stat to filtered, but
	 * since it is allocated with tmp_alloc we do not need to free
	 * the memory now.
	 */
	if (interested_party(cptr, text_no, text_s)
	    && text_read_access(cptr, text_no, text_s))
	{
	    filter_secret_info(&filtered, text_s, cptr, FALSE);
	    async_new_text_old(cptr, text_no, &filtered);
	}
    }
}

static void
send_async_new_text(Text_no text_no,
		    const Text_stat *text_s)
{
    Connection *cptr;
    Session_no i = 0;
    Text_stat filtered;

    init_text_stat(&filtered);

    while ((i = traverse_connections(i)) != 0)
    {
	cptr = get_conn_by_number(i);

	/*
	 * filter_secret_info copies the text-stat to filtered, but
	 * since it is allocated with tmp_alloc we do not need to free
	 * the memory now.
	 */
	if (interested_party(cptr, text_no, text_s)
	    && text_read_access(cptr, text_no, text_s))
	{
	    filter_secret_info(&filtered, text_s, cptr, TRUE);
	    async_new_text(cptr, text_no, &filtered);
	}
    }
}

                        

/* 
 * Special case handling of special text numbers.
 *
 * In the KOMmunity, there has arisen an odd phenomenon known as
 * "jubel" to the local citizens of LysKOM.  There are several kinds
 * of "jubel"; a text number divisible by 1000 is one famous such.
 *
 * There is a feeling that jubels, at least the more prominent jubels,
 * should not be created by automatic postings.  This code is
 * primarily aimed at stopping such things, but it can have other, eh,
 * interesting uses as well...
 */
struct jubel {
    struct jubel *next;
    Pers_no bad_guy;
    Text_no divisor;		/* 0 is used to represent infinity */
    Text_no remainder;
    Bool    public;		/* Text must be sent to a public conference */
};
static struct jubel *jubel_root = NULL;

/*
 * register_jubel(char*)  -  tell lyskomd about a jubel, and a
 * person which is not allowed to create that jubel.  The argument is
 * a string consisting of two or three numbers:
 *       persno textno
 *       persno divisor remainder
 * meaning that PERSNO is not allowed to create text TEXTNO, or any
 * text number which fulfills the relation textno%DIVISOR==REMAINDER.
 */
void
register_jubel(Pers_no pno, 
	       Text_no divis,	/* 0 if no division should be made. */
	       Text_no tno,
	       Bool    public)
{
    struct jubel *j = smalloc(sizeof(struct jubel));
    j->next = jubel_root;
    j->bad_guy = pno;
    j->divisor = divis;
    j->remainder = tno;
    j->public = public;
    jubel_root = j;
}

/* Free the jubel list */
void
free_all_jubel(void)
{
    struct jubel *a;
    struct jubel *b;
    
    a = jubel_root;
    while (a != NULL)
    {
	b = a;
	a = a->next;
	sfree(b);
    }
    jubel_root = NULL;
}
    
/* Check if it is ok for ACTPERS to create the next text. */
static Bool
ok_to_create_next_text(const Connection *conn,
		       Bool *must_be_public) /* [out] */
{
    struct jubel *j;
    Text_no next_tno;
    Bool is_jubel = FALSE;

    *must_be_public = FALSE;

    if (conn == NULL)
        return FALSE;

    next_tno = query_next_text_num();
    for (j = jubel_root; j != NULL; j = j->next)
    {
	if ((j->divisor == 0 && next_tno == j->remainder)
	    ||(j->divisor != 0 && next_tno%j->divisor == j->remainder))
	{
	    if (conn->pers_no == j->bad_guy)
	    {
		kom_log("Stopped person %d from creating jubel %lu.\n",
			conn->pers_no, next_tno);
		return FALSE;
	    }
	    else
		is_jubel = TRUE;

	    if (j->public)
		*must_be_public = TRUE;
	}
    }

    if (is_jubel)
    {
	if (*must_be_public)
	    kom_log("Granted jubel %lu to person %d, pending public check.\n",
		    next_tno, conn->pers_no);
	else
	    kom_log("Granted jubel %lu to person %d.\n",
		    next_tno, conn->pers_no);
    }

    return TRUE;
}


/*
 * Create a text.
 *
 * The recipients may change. See doc for set_permitted_submitters.
 *
 * The only allowed Misc_items are recpt, cc_recpt, comm_to and footn_to.
 * loc_no are allowed, but ignored.
 *
 * Returns text_no of the created text, or 0 if there was an error.
 */

extern Text_no
create_text_old(const String message,
                Misc_info_list * misc_l)
{
    Text_stat *t_stat;
    Text_no    text;

    /* CHK_CONNECTION in do_create_text */
    text = do_create_text(message, misc_l->no_of_misc,
                          misc_l->misc, NULL, FALSE, &t_stat);

    if (text != 0)
    {
        send_async_new_text_old(text, t_stat);
        send_async_new_text(text, t_stat);
    }

    return text;
}

extern Text_no
create_text(const String    message,
            Misc_info_list *misc_l,
            Aux_item_list  *aux)
{
    Text_stat *t_stat;
    Text_no    text;

    /* CHK_CONNECTION in do_create_text */
    text = do_create_text(message, misc_l->no_of_misc,
                          misc_l->misc, aux, FALSE, &t_stat);

    if (text != 0)
    {
        send_async_new_text_old(text, t_stat);
        send_async_new_text(text, t_stat);
    }

    return text;
}


/*
 * Create an anonymous text.
 *
 * This is just like create_text, but the author of the text is set to
 * Pers_no 0, to guarantee that the author is anonymous.
 *
 * Returns text_no of the created text, or 0 if there was an error.
 */

extern Text_no
create_anonymous_text(const String    message,
                      Misc_info_list *misc_l,
                      Aux_item_list  *aux)
{
    Text_no text;
    Text_stat * t_stat;

    /* CHK_CONNECTION in do_create_text */
    text = do_create_text(message, misc_l->no_of_misc,
                          misc_l->misc, aux, TRUE, &t_stat);

    if (text != 0)
    {
        send_async_new_text_old(text, t_stat);
        send_async_new_text(text, t_stat);
    }

    return text;
}

extern Text_no
create_anonymous_text_old(const String    message,
                          Misc_info_list *misc_l)
{
    Text_no text;
    Text_stat * t_stat;

    /* CHK_CONNECTION in do_create_text */
    text = do_create_text(message, misc_l->no_of_misc,
                          misc_l->misc, NULL, TRUE, &t_stat);

    if (text != 0)
    {
        send_async_new_text_old(text, t_stat);
        send_async_new_text(text, t_stat);
    }

    return text;
}

/*
 * Generic create_text call. This is used by all the other create_text
 * calls, old and new, anonymous and not
 */

static Text_no
do_create_text(const String   message,
               u_short	      no_of_misc,
               Misc_info     *misc,
               Aux_item_list *aux,
               Bool           anonymous,
               Text_stat    **ret_stat)
{
    Text_no text;
    Text_stat *t_stat;
    Bool need_public_jubel = FALSE;

    CHK_CONNECTION(0);
    CHK_LOGIN(0);

    if (no_of_misc > param.max_crea_misc)
    {
        err_stat = param.max_crea_misc;
        kom_errno = KOM_LONG_ARRAY;
        return 0;
    }

    /* Check the length of the text. */

    if (s_strlen(message) > param.text_len)
    {
        err_stat = param.text_len;
	kom_errno = KOM_LONG_STR;
	return 0;
    }

    /* Check that the author is allowed to write this text number */

    if (ok_to_create_next_text(active_connection, &need_public_jubel) == FALSE)
    {
        err_stat = 0;
	kom_errno = KOM_TEMPFAIL;
	return 0;
    }

    /* Check all misc-items and aux-items */
    
    prepare_aux_item_list(aux, anonymous?0:ACTPERS);
    if (create_text_check_misc(&no_of_misc, misc, anonymous,
			       need_public_jubel) != OK
	|| text_stat_check_add_aux_item_list(NULL, aux,
					     active_connection) != OK
	|| (text = cached_create_text(message)) == 0)
    {
	return 0;
    }

    if ((t_stat = cached_get_text_stat(text)) == NULL)
    {
	restart_kom("%s.\nText == %lu, kom_errno == %lu, errno == %lu\n",
		    "create_text: can't get text-stat of newly created text",
		   (unsigned long)text,
		   (unsigned long)kom_errno,
		   (unsigned long)errno);
    }

    *ret_stat = t_stat;
    t_stat->author = anonymous?0:ACTPERS;
    t_stat->creation_time = current_time.tv_sec;
    t_stat->no_of_lines = count_lines(message);
    t_stat->no_of_chars = s_strlen(message);
    
    if (create_text_add_miscs(text, t_stat, no_of_misc, misc) != OK)
    {
	kom_log("ERROR: create_text(): can't add miscs.\n");
	return 0;
    }

    if (create_text_add_aux(t_stat, text, aux, anonymous?0:ACTPERS) != OK)
    {
        kom_log("ERROR: create_text(): can't add aux.\n");
        return 0;
    }
    
    if (!anonymous)
    {
	l2g_append(&ACT_P->created_texts,
		   l2g_first_appendable_key(&ACT_P->created_texts),
		   text);
        ACT_P->created_lines += t_stat->no_of_lines;
        ACT_P->created_bytes += t_stat->no_of_chars;

        mark_person_as_changed(ACTPERS);
    }

    mark_text_as_changed(text);

    update_stat(STAT_TEXTS, 1);
    return text;
}




/*
 * Delete a text.
 *
 * Only a supervisor of the author may delete a text.
 */
extern Success
delete_text(Text_no text_no)
{
    Text_stat *text_s;
    
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    GET_T_STAT(text_s, text_no, FAILURE);

    if (!is_supervisor(text_s->author, active_connection)
	&& !ENA(wheel, 8)        /* OK -- In an RPC call */
	&& !ENA(admin, 5))       /* OK -- In an RPC call */
    {
        err_stat = text_no;
	if (text_read_access(active_connection, text_no, text_s))
	    kom_errno = KOM_NOT_AUTHOR;
	else
	    kom_errno = KOM_NO_SUCH_TEXT;
	return FAILURE;
    }

    return do_delete_text(text_no, text_s);
}

/* Return TRUE if ``a'' is greater than ``b'. */
static Bool
greater(const struct tm *a,
	const struct tm *b)
{
    if (a->tm_year < b->tm_year)
	return FALSE;
    if (a->tm_year > b->tm_year)
	return TRUE;

    if (a->tm_mon < b->tm_mon)
	return FALSE;
    if (a->tm_mon > b->tm_mon)
	return TRUE;

    if (a->tm_mday < b->tm_mday)
	return FALSE;
    if (a->tm_mday > b->tm_mday)
	return TRUE;

    if (a->tm_hour < b->tm_hour)
	return FALSE;
    if (a->tm_hour > b->tm_hour)
	return TRUE;

    if (a->tm_min  < b->tm_min)
	return FALSE;
    if (a->tm_min  > b->tm_min)
	return TRUE;

    if (a->tm_sec < b->tm_sec)
	return FALSE;
    if (a->tm_sec > b->tm_sec)
	return TRUE;

    /* ``a'' is not greater if it is equal to ``b''. */
    return FALSE;
}

/*
 * Lookup a text according to creation-time.
 * The text-no of the text created closest before WANTED_TIME is returned.
 * The returned text is not necessarily readable.
 */
extern Success
get_last_text(struct tm *wanted_time,
	      Text_no *result)
{
    struct tm *texttime;
    Text_no lower  = 0;
    Text_no higher = query_next_text_num() - 1;
    Text_stat *text_stat = NULL;
    Text_no try;
    Text_no middle;

    /*
     * We search for the text in the interval [lower, higher]
     *(inclusive). Middle is set to the middle(rounded towards
     * infinity), and we search from there and upward until we find a
     * text.
     */

    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    while (lower < higher)
    {
	middle = (lower + higher)/2 + 1; /* Binary search */
	try = middle;
	text_stat = NULL;

	/* FIXME (bug 180): Once there is a more efficient structure
	   that maps Text_nos to the internal cache_node this loop
	   could probably be rewritten in a more efficient way.  */

        while (text_stat == NULL && try <= higher)
	    text_stat = cached_get_text_stat(try++);

	if (text_stat == NULL)
	    higher = middle - 1;
	else
	{
	    if (active_connection->use_utc)
		texttime = gmtime(&text_stat->creation_time);
	    else
		texttime = localtime(&text_stat->creation_time);
	    if (greater(wanted_time, texttime))
		lower = try - 1;
	    else
		higher = middle - 1;
	}
    }

    *result = lower;

    return OK;
}


/*
 * Return next existing text-no, which ACTPERS is allowed to read.
 */

extern  Success
find_next_text_no(Text_no start,
		  Text_no *result)
{
    Text_no    highest = query_next_text_num();
    Text_stat *text_s;

    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    err_stat = start;
    while (++start < highest)
    {
	text_s = cached_get_text_stat(start);
	if (text_s != NULL
	    && text_read_access(active_connection, start, text_s) == TRUE)
	{
	    *result = start;
	    return OK;
	}
    }

    kom_errno = KOM_NO_SUCH_TEXT;
    return FAILURE;
}


/*
 * Return previous existing text-no, which ACTPERS is allowed to read.
 */

extern  Success
find_previous_text_no(Text_no start,
		      Text_no *result)
{
    Text_stat *text_s;
    Text_no next_tno;
    const Text_no saved_start = start;
    
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    
    if (start >(next_tno = query_next_text_num()))
	start = next_tno;

    while (start-- > 0)
    {
	text_s = cached_get_text_stat(start);
	if (text_s != NULL
	    && text_read_access(active_connection, start, text_s) == TRUE)
	{
	    *result = start;
	    return OK;
	}
    }

    kom_errno = KOM_NO_SUCH_TEXT;
    err_stat = saved_start;
    return FAILURE;
}

static void
send_async_add_recipient(Text_no text_no,
			 const Text_stat *text_s,
			 Conf_no conf_no,
			 enum info_type type)
{
    Connection *cptr;
    Session_no i = 0;

    while ((i = traverse_connections(i)) != 0)
    {
	cptr = get_conn_by_number(i);
	if (interested_party(cptr, text_no, text_s)
	    && text_read_access(cptr, text_no, text_s))
	{
	    Text_stat copy;

	    filter_secret_info(&copy, text_s, cptr, TRUE);
	    if (find_recipient(conf_no, &copy) != -1)
		async_new_recipient(cptr, text_no, conf_no, type);
	}
    }
}

/*
 * Add a recipient to a text.
 */
extern Success
add_recipient(Text_no         text_no,
	      Conf_no         conf_no,
	      enum info_type  type)	/* recpt, cc_recpt or bcc_recpt */
{
    Text_stat           *t_stat;
    Conference          *conf_c;
    int                  rcpt_index;
    Conf_no              submit_conf;
    
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);

    /* Get the conference */
    GET_C_STAT(conf_c, conf_no, FAILURE);
    if (!has_access(conf_no, active_connection, read_protected))
    {
        err_stat = conf_no;
        kom_errno = KOM_UNDEF_CONF;
        return FAILURE;
    }

    /* Get the text */
    GET_T_STAT(t_stat, text_no, FAILURE);
    if (!text_read_access(active_connection, text_no, t_stat)
	&& !ENA(admin, 4)) /* OK -- In an RPC call */
    {
        err_stat = text_no;
	kom_errno = KOM_NO_SUCH_TEXT;
	return FAILURE;
    }

    rcpt_index = find_recipient(conf_no, t_stat); 
    if (rcpt_index != -1)
    {
	/* You cannot add the same conference twice. */
	if (t_stat->misc_items[rcpt_index].type == type)
	{
            err_stat = conf_no;
	    kom_errno = KOM_ALREADY_RECIPIENT;
	    return FAILURE;
	}

	if (type != recpt && type != cc_recpt && type != bcc_recpt)
	{
            err_stat = type;
	    kom_errno = KOM_ILLEGAL_INFO_TYPE;
	    return FAILURE;
	}

	if (!is_supervisor(t_stat->author, active_connection)
	    && !is_supervisor(conf_no, active_connection)
	    && !is_supervisor_of_sender(t_stat, rcpt_index, active_connection)
	    && !ENA(wheel, 8))     /* OK -- In an RPC call */
	{
            err_stat = conf_no;
	    kom_errno = KOM_PERM;
	    return FAILURE;
	}

	/* Change a recpt to a cc_recpt or a cc_recpt to a recpt.
	   The ability to do this was introduced after a request
	   from Lisa Hallingstrom, who wanted to do this repeatedly
	   when managing the conference "Fragor och svar". */

	t_stat->misc_items[rcpt_index].type = type;
#if 0
	/* FIXME (bug 182): This should possibly be done, but
	   differently.  ADD_MISC adds things lasts in the misc-info,
	   and that is not the correct thing to do.  The change is now
	   performed silently instead.  Also note that these fields
	   may already be present...  */
	if (t_stat->author != ACTPERS)
	    ADD_MISC(t_stat, sent_by, sender, ACTPERS);

	ADD_MISC(t_stat, sent_at, sent_at, current_time);
#endif

	mark_text_as_changed(text_no);
        send_async_add_recipient(text_no, t_stat, conf_no, type);

	return OK;
    }

    if (count_recipients(t_stat) >= param.max_recipients)
    {
        err_stat = text_no;
	kom_errno = KOM_RECIPIENT_LIMIT;
	return FAILURE;
    }

    submit_conf = submit_to(conf_no, conf_c);

    if (submit_conf == 0)
    {
        /* Error data set in submit_to */
	return FAILURE;
    }
    else if (submit_conf != conf_no)
    {
        /* Error data not set in submit_to */
        err_stat = conf_no;
        kom_errno = KOM_ACCESS;
        return FAILURE;
    }


    if (!conf_c->type.allow_anon && t_stat->author == 0)
    {
        kom_errno = KOM_ANON_REJECTED;
        err_stat = 0;
        return FAILURE;
    }

    switch (type)
    {
    case recpt:
    case cc_recpt:
    case bcc_recpt:
	if (do_add_recipient(text_no, t_stat, conf_no, type) != OK)
	    return FAILURE;
	break;

    case comm_to:
    case comm_in:
    case footn_to:
    case footn_in:
    case loc_no:
    case rec_time:
    case sent_by:
    case sent_at:
    case unknown_info:
	/* Fall through */
#ifndef COMPILE_CHECKS
    default:
#endif
        err_stat = type;
	kom_errno = KOM_ILLEGAL_INFO_TYPE;
	return FAILURE;
    }
    
    if (t_stat->author != ACTPERS)
	ADD_MISC(t_stat, sent_by, sender, ACTPERS);

    ADD_MISC(t_stat, sent_at, sent_at, current_time.tv_sec);

    mark_text_as_changed(text_no);
    send_async_add_recipient(text_no, t_stat, conf_no, type);
    
    return OK;
}



/*
 * Subtract a recipient from a text.
 *
 * This may be done by
 *	a) a supervisor of the author.
 *	b) a supervisor of the recipient.
 *	c) the sender of the text to the recipient
 */
extern Success
sub_recipient(Text_no		text_no,
	      Conf_no		conf_no)
{
    Text_stat  *text_s;
    Conference *conf_c;
    int         misc_index;
    
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    GET_T_STAT(text_s, text_no, FAILURE);

    if (!text_read_access(active_connection, text_no, text_s)
	&& !ENA(admin, 4))        /* OK -- In an RPC call */
    {
        err_stat = text_no;
	kom_errno = KOM_NO_SUCH_TEXT;
	return FAILURE;
    }
    
    if (!has_access(conf_no, active_connection, read_protected))
    {
        err_stat = conf_no;
        kom_errno = KOM_UNDEF_CONF;
        return FAILURE;
    }

    GET_C_STAT(conf_c, conf_no, FAILURE);

    if ((misc_index = find_recipient(conf_no, text_s)) == -1)
    {
        err_stat = conf_no;
	kom_errno = KOM_NOT_RECIPIENT;
	return FAILURE;
    }

    
    if (!is_supervisor(text_s->author, active_connection)
	&& !is_supervisor(conf_no, active_connection)
	&& !ENA(wheel, 8)      /* OK -- In an RPC call */
	&& !is_supervisor_of_sender(text_s, misc_index, active_connection))
    {
        err_stat = text_no;
	kom_errno = KOM_PERM;
	return FAILURE;
    }

    return do_sub_recpt(text_no, text_s, conf_no, conf_c, TRUE);
}


/* Make all checks that are common to add-comment and add-footnote.
   child is the text that will be a comment or footnote of the parent
   if the operation is allowed.  parent_checker should be either
   check_comm() or check_footn().  
*/
static Success
check_add_textlink(Text_no child,
		   Text_no parent,
		   Success (*parent_checker)(const Text_stat *),
		   Text_stat **child_p,
		   Text_stat **parent_p)
{
    Text_stat *child_s;
    Text_stat *parent_s;
    int        misc_index;

    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);

    /* FIXME (bug 183, bug 184): The following code can not cope with
       a text that is a comment/footnote to itself. That is considered
       to be a bug. Work around it for now, until a proper
       misc-info-list handling package is written.  */

    if (child == parent)
    {
	kom_errno = KOM_INDEX_OUT_OF_RANGE;
        err_stat = child;
	return FAILURE;
    }

    GET_T_STAT(child_s, child, FAILURE);
    if (!text_read_access(active_connection, child, child_s))
    {
        kom_errno = KOM_NO_SUCH_TEXT;
        err_stat = child;
        return FAILURE;
    }

    GET_T_STAT(parent_s, parent, FAILURE);
    if (!text_read_access(active_connection, parent, parent_s))
    {
	kom_errno = KOM_NO_SUCH_TEXT;
        err_stat = parent;
	return FAILURE;
    }
    
    err_stat = parent;     
    if (parent_checker(parent_s) != OK)
	return FAILURE;

    /* Check if already comment/footnote. */
    if ((misc_index = find_textlink(child_s, parent)) != -1)
    {
        err_stat = child;
	if (child_s->misc_items[misc_index].type == comm_to)
	    kom_errno = KOM_ALREADY_COMMENT;
	else
	    kom_errno = KOM_ALREADY_FOOTNOTE;
	return FAILURE;
    }

    *child_p = child_s;
    *parent_p = parent_s;
    return OK;
}



/*
 * Add a comment-link between two existing texts.
 */
extern Success
add_comment(Text_no	child,
	    Text_no 	parent)
{
    Text_stat *child_s;
    Text_stat *parent_s;

    if (check_add_textlink(child, parent, check_comm,
			   &child_s, &parent_s) != OK)
	return FAILURE;

    if (do_add_comment(child, child_s, parent, parent_s) != OK)
	return FAILURE;

    if (child_s->author != ACTPERS)
	ADD_MISC(child_s, sent_by, sender, ACTPERS);

    ADD_MISC(child_s, sent_at, sent_at, current_time.tv_sec);

    mark_text_as_changed(child);
    return OK;
}

/*
 * Delete a comment-link between two texts.
 *
 * This can be done by:
 *	a) a supervisor of the author of the comment.
 *	b) a supervisor of the creator of the comment link.
 */
extern Success
sub_comment(Text_no child,	/* 'child' is no longer a comment */
	    Text_no parent)	/* to 'parent' */
{
    Text_stat  *child_s;
    Text_stat  *parent_s;
    int         misc_index;


    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);

    GET_T_STAT(child_s, child, FAILURE);
    GET_T_STAT(parent_s, parent, FAILURE);

    if ((misc_index = find_textlink(child_s, parent)) == -1
	|| child_s->misc_items[misc_index].type != comm_to)
    {
        err_stat = child;
	kom_errno = KOM_NOT_COMMENT;
	return FAILURE;
    }
    
    if (!is_supervisor(child_s->author, active_connection)
	&& !ENA(wheel, 8)      /* OK -- In an RPC call */
	&& !is_supervisor_of_sender(child_s, misc_index, active_connection))
    {
        err_stat = child;
	kom_errno = KOM_PERM;
	return FAILURE;
    }

    do_sub_comment(child, child_s, parent, parent_s);
    return OK;
}

/*
 * Add a footnote-link between two existing texts. Only the author
 * may do this. The texts must have the same author.
 */
extern Success
add_footnote(Text_no  child,
	     Text_no  parent)
{
    Text_stat *child_s;
    Text_stat *parent_s;

    if (check_add_textlink(child, parent, check_footn,
			   &child_s, &parent_s) != OK)
	return FAILURE;

    if (child_s->author != parent_s->author)
    {
	kom_errno = KOM_NOT_AUTHOR;
	/* check_footn() has already checked that the active person is
	   author of the parent, so it must be the child that is in
	   error. */
        err_stat = child;
	return FAILURE;
    }
    
    if (do_add_footnote(child, child_s, parent, parent_s) != OK)
	return FAILURE;

    ADD_MISC(child_s, sent_at, sent_at, current_time.tv_sec);

    mark_text_as_changed(child);
    return OK;
}

/*
 * Delete a footnote-link between two texts.
 * Only the author may do this.
 */
extern Success
sub_footnote(Text_no child,	/* 'child' is no longer a  */
	     Text_no parent)	/* footnote to 'parent' */
{
    Text_stat  *child_s;
    Text_stat  *parent_s;
    int         misc_index;

    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);

    GET_T_STAT(child_s, child, FAILURE);
    GET_T_STAT(parent_s, parent, FAILURE);

    if ((misc_index = find_textlink(child_s, parent)) == -1
	|| child_s->misc_items[misc_index].type != footn_to)
    {
        err_stat = child;
	kom_errno = KOM_NOT_FOOTNOTE;
	return FAILURE;
    }
    
    if (!is_supervisor(child_s->author, active_connection)
	&& !ENA(wheel,8))          /* OK -- In an RPC call */
    {
        err_stat = child;
	kom_errno = KOM_PERM;
	return FAILURE;
    }

    do_sub_footnote(child, child_s, parent, parent_s);
    return OK;
}


/*
 * Get mapping from Local_text_no to(global) Text_no for part of
 * a conference.
 */
extern Success
get_map(Conf_no	      conf_no,
	Local_text_no first_local_no,
	unsigned long no_of_texts,
	L2g_iterator *result)
{
    Conference  * conf_c;
    Local_text_no highest;
    Local_text_no first_unwanted;
    Local_text_no res_first;
    int res_nr;
    enum access acc;
    
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    GET_C_STAT(conf_c, conf_no, FAILURE);

    acc = access_perm(conf_no, active_connection, limited);

    if (acc <= none)
    {
        err_stat = conf_no;
	kom_errno = KOM_UNDEF_CONF;
	return FAILURE;
    }

    if (acc == read_protected)
    {
        err_stat = conf_no;
	kom_errno = KOM_ACCESS;
	return FAILURE;
    }
    
    highest = l2g_first_appendable_key(&conf_c->texts) - 1;
    
    if (first_local_no > highest)
    {
        err_stat = first_local_no;
	kom_errno = KOM_NO_SUCH_LOCAL_TEXT;
	return FAILURE;
    }

    /* Get the lowest existing text, or, if no text exists, the next
       text number that will be used.  */
    res_first = l2g_next_key(&conf_c->texts, 0);
    if (res_first == 0)
	res_first = l2g_first_appendable_key(&conf_c->texts);

    /* Start where the user wants to start, unless that is too small.  */
    res_first = max(res_first, first_local_no);

    /* Find the endpoint. */
    first_unwanted = min(highest+1, first_local_no + no_of_texts);

    if (first_unwanted >= res_first)
	res_nr = first_unwanted - res_first;
    else
	res_nr = 0;

    l2gi_searchsome(result, &conf_c->texts, res_first, res_first + res_nr);

    return OK;
}

extern Success
local_to_global(Conf_no	        conf_no,
		Local_text_no	first_local_no,
		unsigned long   no_of_texts,
		Text_mapping   *result)
{
    Conference  * conf_c;
    enum access acc;

    CHK_CONNECTION(FAILURE);
    if (first_local_no == 0)
    {
	err_stat = first_local_no;
	kom_errno = KOM_LOCAL_TEXT_ZERO;
	return FAILURE;
    }

    if (no_of_texts > 255)
    {
	err_stat = 255;
	kom_errno = KOM_LONG_ARRAY;
	return FAILURE;
    }

    CHK_LOGIN(FAILURE);
    GET_C_STAT(conf_c, conf_no, FAILURE);

    acc = access_perm(conf_no, active_connection, limited);

    if (acc <= none)
    {
        err_stat = conf_no;
	kom_errno = KOM_UNDEF_CONF;
	return FAILURE;
    }

    if (acc == read_protected)
    {
        err_stat = conf_no;
	kom_errno = KOM_ACCESS;
	return FAILURE;
    }

    if (first_local_no >= l2g_first_appendable_key(&conf_c->texts))
    {
        err_stat = first_local_no;
	kom_errno = KOM_NO_SUCH_LOCAL_TEXT;
	return FAILURE;
    }

    result->first = first_local_no;
    result->no_of_texts = no_of_texts;
    result->l2g = &conf_c->texts;

    return OK;
}

extern Success
local_to_global_reverse(Conf_no	              conf_no,
			Local_text_no	      local_no_ceiling,
			unsigned long         no_of_texts,
			Text_mapping_reverse *result)
{
    Conference  * conf_c;
    enum access   acc;
    Local_text_no ceiling;

    CHK_CONNECTION(FAILURE);

    if (no_of_texts > 255)
    {
	err_stat = 255;
	kom_errno = KOM_LONG_ARRAY;
	return FAILURE;
    }

    CHK_LOGIN(FAILURE);
    GET_C_STAT(conf_c, conf_no, FAILURE);

    acc = access_perm(conf_no, active_connection, limited);

    if (acc <= none)
    {
        err_stat = conf_no;
	kom_errno = KOM_UNDEF_CONF;
	return FAILURE;
    }

    if (acc == read_protected)
    {
        err_stat = conf_no;
	kom_errno = KOM_ACCESS;
	return FAILURE;
    }

    ceiling = l2g_first_appendable_key(&conf_c->texts);

    if (local_no_ceiling == 0 || local_no_ceiling > ceiling)
	result->ceiling = ceiling;
    else
	result->ceiling = local_no_ceiling;

    result->no_of_texts = no_of_texts;
    result->l2g = &conf_c->texts;

    return OK;
}


static void
send_async_text_aux_changed(Text_no        text_no,
			    Text_stat     *text_s,
			    unsigned long  highest_aux)
{
    Connection *cptr;
    Session_no i = 0;
    Aux_item_list copy;

    while ((i = traverse_connections(i)) != 0)
    {
	cptr = get_conn_by_number(i);

	/* Check the want_async and handshake_ok here to avoid the
	   expensive calls to access_perm and filter_aux_item_list. */

	if (cptr->want_async[ay_text_aux_changed] == FALSE)
	    continue;
	
	if (!handshake_ok(cptr, 0))
	    continue;

	if (!interested_party(cptr, text_no, text_s))
	    continue;

	if (!text_read_access(cptr, text_no, text_s))
	    continue;
	
	filter_aux_item_list(&text_s->aux_item_list, &copy, cptr);

	async_text_aux_changed(cptr, text_no, &copy, highest_aux);
    }
}

extern Success
modify_text_info(Text_no        text,
                 Number_list   *items_to_delete, 
                 Aux_item_list *aux)
{
    Text_stat       *text_s;
    unsigned long    highest_aux;
    
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);

    if (items_to_delete->length > param.max_delete_aux)
    {
        kom_errno = KOM_LONG_ARRAY;
        err_stat = param.max_delete_aux;
        return FAILURE;
    }

    if (aux->length > param.max_add_aux)
    {
        kom_errno = KOM_LONG_ARRAY;
        err_stat = param.max_add_aux;
        return FAILURE;
    }

    GET_T_STAT(text_s, text, FAILURE);
    if (!text_read_access(active_connection, text, text_s))
    {
        err_stat = text;
        kom_errno = KOM_NO_SUCH_TEXT;
        return FAILURE;
    }

    /* Store the number of the oldest existing aux-item, so that we
       can send an async message about the new items. */
    highest_aux = text_s->highest_aux;

    /* Check if we may delete and add the items */

    prepare_aux_item_list(aux, ACTPERS);

    if (check_delete_aux_item_list(items_to_delete,
                                   &text_s->aux_item_list,
                                   text_s->author)!=OK)
        return FAILURE;
    delete_aux_item_list(items_to_delete,
                         &text_s->aux_item_list,
                         TEXT_OBJECT_TYPE,
                         text, text_s);

    if (text_stat_check_add_aux_item_list(text_s, aux,
					  active_connection) != OK)
    {
        undelete_aux_item_list(items_to_delete,
                               &text_s->aux_item_list,
                               TEXT_OBJECT_TYPE,
                               text,
                               text_s);
        return FAILURE;
    }

    text_stat_add_aux_item_list(text_s, text, aux, ACTPERS);
    send_async_text_aux_changed(text, text_s, highest_aux);
    commit_aux_item_list(&text_s->aux_item_list);

    mark_text_as_changed(text);

    return OK;
}


Success
first_unused_text_no(Text_no *result)
{
    CHK_CONNECTION(FAILURE);

    *result = query_next_text_num();
    return OK;
}
