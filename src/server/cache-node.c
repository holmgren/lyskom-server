/*
 * $Id: cache-node.c,v 0.26 2003/08/23 16:38:18 ceder Exp $
 * Copyright (C) 1991, 1993-1994, 1996, 1998-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * cache-node.c
 *
 * Used in diskomd.
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <stdio.h>
#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif

#include "cache-node.h"
#include "exp.h"
#include "server/smalloc.h"
#include "lyskomd.h"

EXPORT const Cache_node EMPTY_CACHE_NODE =
    {{ 0, 0 }, NULL, NULL, 0, 0, 0, 0, NULL, NULL, 0};

EXPORT const Cache_node_block EMPTY_CACHE_NODE_BLOCK =
    { 0, NULL, NULL};

EXPORT const Cache_node_mcb EMPTY_CACHE_NODE_MCB =
    { 0, NULL, 0, 0, NULL, NULL, 0, NULL };


EXPORT  void
unlink_lru(Cache_node *node,
	   Cache_node **lru,
	   Cache_node **mru)
{
    Cache_node *link;
    
    link = node->next;

    if ( node->next != NULL )
	node->next->prev = node->prev;
    else if (*lru == node)
	*lru = node->prev;

    if ( node->prev != NULL )
	node->prev->next = link;
    else if (*mru == node)
	*mru = link;

    node->next = NULL;
    node->prev = NULL;
}

static void
insert_mru(Cache_node *node,
	   Cache_node **lru,
	   Cache_node **mru)
{
    node->prev = NULL;
    node->next = *mru;
    *mru = node;
    if ( *lru == NULL )
	*lru = node;

    if ( node->next != NULL )
	node->next->prev = node;
}

static Cache_node_mcb *
alloc_cache_node_mcb(void)
{
    Cache_node_mcb *t;

    t = smalloc(sizeof(Cache_node_mcb));
    *t = EMPTY_CACHE_NODE_MCB;
    return t;
}

static Cache_node_block *
alloc_cache_node_block(int table_size)
{
    Cache_node_block *t;

    t = smalloc(sizeof(Cache_node_block));
    *t = EMPTY_CACHE_NODE_BLOCK;
    t->nodes = smalloc (table_size * sizeof (Cache_node));
    t->next_free = 0;
    t->link = NULL;

    return t;
}


EXPORT Cache_node_mcb *
create_cache_node_mcb(int mcb_size, 
		      int table_size)
{
    Cache_node_mcb *tmp;

    tmp = alloc_cache_node_mcb();
    *tmp = EMPTY_CACHE_NODE_MCB;
    tmp->lookup_table = smalloc(sizeof(Cache_node *) * table_size);
    tmp->lookup_table_size = table_size;
    tmp->mcb_size = mcb_size;
    tmp->last_block = NULL;

    return tmp;
}

/* Note: No check is done that it is initialized. */
EXPORT Cache_node *
get_cache_node (Cache_node_mcb *control,
		unsigned long   key)
{
    if ( key >= control->lookup_table_size )
	return NULL;

    return control->lookup_table[ key ];
}

static Cache_node *
alloc_cache_node (Cache_node_mcb *control)
{
    Cache_node *c;
    Cache_node_block *new_block;

    if ( control->last_block == NULL
	|| control->last_block->next_free >= control->mcb_size )
    {
	new_block = alloc_cache_node_block (control->mcb_size);
	new_block->link = control->last_block;
	control->last_block = new_block;
    }

    c = &control->last_block->nodes[ control->last_block->next_free++ ];

    *c = EMPTY_CACHE_NODE;
    return c;
}

EXPORT void
destruct_cache_node(Cache_node_mcb  *control,
		    unsigned long    key)
{
    if ( key >= control->lookup_table_size )
	return;

    control->lookup_table[ key ] = NULL;
}

EXPORT  void
create_cache_node (Cache_node_mcb *control,
		   unsigned long   key)
{
    if ( key >= control->lookup_table_size )
	restart_kom("%s(%lu, %lu): lookup_table_size = %lu\n",
		    "ERROR: create_cache_node",
		    (unsigned long) control, key,
		    control->lookup_table_size);

    control->lookup_table[ key ] = alloc_cache_node(control);
}

EXPORT  void
zero_init_cache_node (Cache_node_mcb *control,
		      unsigned long   key)
{
    if ( key >= control->lookup_table_size )
	restart_kom("%s(%lu, %lu): lookup_table_size = %lu\n",
		    "ERROR: zero_init_cache_node",
		    (unsigned long)control, key,
		    control->lookup_table_size);

    control->lookup_table[ key ] = NULL;
}



EXPORT void
set_mru(Cache_node_mcb *mcb,
	unsigned long   key)
{
    Cache_node *node;

    node = get_cache_node(mcb, key);

    if (node == NULL)
	restart_kom("set_mru(%lu): nonexistent.\n", key);

    unlink_lru(node, &mcb->lru, &mcb->mru);
    insert_mru(node, &mcb->lru, &mcb->mru);
}

static void
free_cache_node_block (Cache_node_block *block)
{
    sfree(block->nodes);
    sfree(block);
}

extern void
free_cache_node_mcb(Cache_node_mcb *control)
{
    Cache_node_block *block;

    while ( control->last_block != NULL )
    {
	block = control->last_block;
	control->last_block = block->link;
	free_cache_node_block (block);
    }

    sfree(control->lookup_table);
    sfree(control);
}
