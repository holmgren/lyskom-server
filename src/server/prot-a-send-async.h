/*
 * $Id: prot-a-send-async.h,v 0.23 2003/08/23 16:38:14 ceder Exp $
 * Copyright (C) 1991, 1994-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: prot-a-send-async.h,v 0.23 2003/08/23 16:38:14 ceder Exp $
 *
 * Asynchronous messages in protocol A.
 */

#define ASYNC_CHECK_ACCEPT(_conn, _msg)             \
    {                                               \
        if (_conn->want_async[_msg] == FALSE) return;   \
    }


void
prot_a_async_new_text(Connection *cptr,
		      Text_no	      text_no,
		      Text_stat      *text_s);

void
prot_a_async_new_text_old(Connection *cptr,
                          Text_no	      text_no,
                          Text_stat      *text_s);

void
prot_a_async_i_am_on(Connection *cptr,
		     Who_info info);
void
prot_a_async_logout(Connection *cptr,
		    Pers_no pers_no,
		    Session_no session_no);

void
prot_a_async_new_name(Connection *cptr,
		      Conf_no conf_no,
		      String old_name,
		      String new_name);

void
prot_a_async_sync_db(Connection *cptr);

void
prot_a_async_forced_leave_conf(Connection *cptr,
			       Conf_no	       conf_no);

void
prot_a_async_login(Connection *cptr,
		   Pers_no	   pers_no,
		   int		   session_no);

void
prot_a_async_rejected_connection(Connection *cptr);

void
prot_a_async_send_message(Connection *cptr,
			  Conf_no     recipient,
			  Pers_no     sender,
			  String      message);

void
prot_a_async_delete_text(Connection *cptr,
                         Text_no	      text_no,
                         Text_stat      *text_s);

void
prot_a_async_deleted_text(Connection *cptr,
                         Text_no	      text_no,
                         Text_stat      *text_s);

void
prot_a_async_new_recipient(Connection     *cptr,
                           Text_no        text_no,
                           Conf_no        conf_no,
                           enum info_type type);

void
prot_a_async_sub_recipient(Connection     *cptr,
                           Text_no        text_no,
                           Conf_no        conf_no,
                           enum info_type type);



void
prot_a_async_new_membership(Connection   *cptr,
                            Pers_no pers_no,
                            Conf_no conf_no);

void
prot_a_async_new_user_area(Connection   *cptr,
			   Pers_no       pers_no,
			   Text_no       old_user_area,
			   Text_no       new_user_area);

void
prot_a_async_new_presentation(Connection   *cptr,
			      Conf_no       conf_no,
			      Text_no       old_presentation,
			      Text_no       new_presentation);

void
prot_a_async_new_motd(Connection   *cptr,
		      Conf_no       conf_no,
		      Text_no       old_motd,
		      Text_no       new_motd);

void
prot_a_async_text_aux_changed(Connection    *cptr,
			      Text_no        text_no,
			      Aux_item_list *aux_list,
			      unsigned long  highest_old_aux);


#ifdef DEBUG_CALLS
void
prot_a_async_garb_ended(Connection *cptr,
			int deleted_texts);
#endif
