/* A Bison parser, made from aux-item-def-parse.y
   by GNU bison 1.35.  */

#define YYBISON 1  /* Identify Bison output.  */

#define YYLSP_NEEDED 1

# define	NUMBER	257
# define	BOOLEAN	258
# define	ID	259
# define	STRING	260
# define	DISABLED	261
# define	TEXT	262
# define	CONFERENCE	263
# define	LETTERBOX	264
# define	TOK_SERVER	265
# define	TOK_ANY	266
# define	VOID	267
# define	CREATE	268
# define	MODIFY	269

#line 1 "aux-item-def-parse.y"

/*
 * $Id: aux-item-def-parse.y,v 1.22 2003/08/23 16:38:18 ceder Exp $
 * Copyright (C) 1994-1996, 1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

/* Rename hack from the automake 1.4 manual. */

#define	yymaxdepth aid_maxdepth
#define	yyparse	aid_parse
#define	yylex	aid_lex
#define	yyerrorf aid_error
#define	yylval	aid_lval
#define	yychar	aid_char
#define	yydebug	aid_debug
#define	yypact	aid_pact
#define	yyr1	aid_r1
#define	yyr2	aid_r2
#define	yydef	aid_def
#define	yychk	aid_chk
#define	yypgo	aid_pgo
#define	yyact	aid_act
#define	yyexca	aid_exca
#define yyerrflag aid_errflag
#define yynerrs	aid_nerrs
#define	yyps	aid_ps
#define	yypv	aid_pv
#define	yys	aid_s
#define	yy_yys	aid_yys
#define	yystate	aid_state
#define	yytmp	aid_tmp
#define	yyv	aid_v
#define	yy_yyv	aid_yyv
#define	yyval	aid_val
#define	yylloc	aid_lloc
#define yyreds	aid_reds
#define yytoks	aid_toks
#define yylhs	aid_yylhs
#define yylen	aid_yylen
#define yydefred aid_yydefred
#define yydgoto	aid_yydgoto
#define yysindex aid_yysindex
#define yyrindex aid_yyrindex
#define yygindex aid_yygindex
#define yytable	 aid_yytable
#define yycheck	 aid_yycheck
#define yyname   aid_yyname
#define yyrule   aid_yyrule

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#if STDC_HEADERS || HAVE_STRING_H
#  include <string.h>
#else
#  include <strings.h>
#endif
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#include <stdlib.h>
#include <setjmp.h>
#include "timewrap.h"

#include "kom-types.h"
#include "com.h"
#include "async.h"
#include "connections.h"
#include "aux-items.h"
#include "s-string.h"
#include "server/smalloc.h"
#include "lyskomd.h"
#include "debug.h"
#include "log.h"
#include "string-malloc.h"
#include "eintr.h"

BUGDECL;    

#define YYDEBUG 1
    
static Aux_item_definition      def;
static Bool                     errorFlag = FALSE;

#define CHK_ASSIGN(_i_, _f_, _t_, _d_, _e_, _l_) \
        found = found || aux_item_def_check_assign(_i_,_d_,&def._f_,_t_,&(_e_),_l_)

#define CHK_FLAG_A(_i_, _f_, _d_, _e_, _l_) \
        if (!s_strcmp(s_fcrea_str(_i_),_d_)) \
        { \
            found = 1; \
            if (_e_.type != BOOLEAN ) \
            { \
                  aux_item_def_error_line = _l_; \
                yyerrorf("invalid type: expected %s, got %s", \
                        aux_item_def_typename(BOOLEAN), \
                        aux_item_def_typename(_e_.type)); \
            } \
            if (_e_.val.num) { def.set_flags._f_ = 1; } \
            else { def.clear_flags._f_ = 1; } \
        } 


extern int yylex(void);

static char *inputFile;
int aux_item_def_error_line;
extern int yylineno;
#if defined(HAVE_VFPRINTF) && defined(HAVE_STDARG_H)
void yyerrorf(const char * format, ...)
#  if HAVE_ATTRIBUTE_FORMAT_PRINTF
    __attribute__ ((format (printf, 1, 2)))
#  endif
    ;
#else
void yyerrorf();
#endif
#define yyerror(msg) yyerrorf("%s", msg)

struct aux_item_def_value_type;
static int  aux_item_def_check_assign(const char  *,
                                      String,
                                      void  *,
                                      int,
                                      struct aux_item_def_value_type *,
                                      int);
static const char *aux_item_def_typename(int);
static
short aux_item_def_check_trigger(const char *check_name,
                                 int type,
                                 String trigger_name,
                                 String function_name,
                                 unsigned long *counter,
                                 Aux_item_trigger **triggers);

static
short aux_item_def_check_validate(const char *check_name,
                                  String field_name,
                                  int type,
                                  String data,
                                  Aux_item_definition *def);


#define YYERROR_VERBOSE

#line 168 "aux-item-def-parse.y"
#ifndef YYSTYPE
typedef union
{
    String          str;
    unsigned long   num;
    struct aux_item_def_value_type
    {
        int         type;
        union
        {
            String          str;
            unsigned long   num;
        } val;
    } value;
} yystype;
# define YYSTYPE yystype
# define YYSTYPE_IS_TRIVIAL 1
#endif

#ifndef YYLTYPE
typedef struct yyltype
{
  int first_line;
  int first_column;

  int last_line;
  int last_column;
} yyltype;

# define YYLTYPE yyltype
# define YYLTYPE_IS_TRIVIAL 1
#endif

#ifndef YYDEBUG
# define YYDEBUG 0
#endif



#define	YYFINAL		39
#define	YYFLAG		-32768
#define	YYNTBASE	24

/* YYTRANSLATE(YYLEX) -- Bison token number corresponding to YYLEX. */
#define YYTRANSLATE(x) ((unsigned)(x) <= 269 ? yytranslate[x] : 33)

/* YYTRANSLATE[YYLEX] -- Bison token number corresponding to YYLEX. */
static const char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      19,    20,     2,     2,    21,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    18,    23,
       2,    22,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    16,     2,    17,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    15
};

#if YYDEBUG
static const short yyprhs[] =
{
       0,     0,     3,     4,     9,    16,    24,    28,    30,    33,
      36,    39,    41,    44,    47,    50,    51,    54,    57,    58,
      63,    65,    67,    69,    73
};
static const short yyrhs[] =
{
      24,    25,     0,     0,    26,    16,    30,    17,     0,     3,
      18,     5,    19,    27,    20,     0,     3,    18,     5,    19,
      27,    20,     7,     0,    27,    21,    28,     0,    28,     0,
      29,     8,     0,    29,     9,     0,    29,    10,     0,    11,
       0,    29,    12,     0,    29,    14,     0,    29,    15,     0,
       0,    30,    31,     0,    30,     1,     0,     0,     5,    22,
      32,    23,     0,     4,     0,     6,     0,     3,     0,     5,
      19,    20,     0,    13,     0
};

#endif

#if YYDEBUG
/* YYRLINE[YYN] -- source line where rule number YYN was defined. */
static const short yyrline[] =
{
       0,   193,   194,   197,   207,   220,   231,   232,   235,   236,
     237,   239,   240,   249,   250,   251,   254,   255,   256,   259,
     345,   346,   347,   348,   349
};
#endif


#if (YYDEBUG) || defined YYERROR_VERBOSE

/* YYTNAME[TOKEN_NUM] -- String name of the token TOKEN_NUM. */
static const char *const yytname[] =
{
  "$", "error", "$undefined.", "NUMBER", "BOOLEAN", "ID", "STRING", 
  "DISABLED", "TEXT", "CONFERENCE", "LETTERBOX", "TOK_SERVER", "TOK_ANY", 
  "VOID", "CREATE", "MODIFY", "'{'", "'}'", "':'", "'('", "')'", "','", 
  "'='", "';'", "items", "item", "head", "targets", "target", "action", 
  "body", "assign", "value", 0
};
#endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives. */
static const short yyr1[] =
{
       0,    24,    24,    25,    26,    26,    27,    27,    28,    28,
      28,    28,    28,    29,    29,    29,    30,    30,    30,    31,
      32,    32,    32,    32,    32
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN. */
static const short yyr2[] =
{
       0,     2,     0,     4,     6,     7,     3,     1,     2,     2,
       2,     1,     2,     2,     2,     0,     2,     2,     0,     4,
       1,     1,     1,     3,     1
};

/* YYDEFACT[S] -- default rule to reduce with in state S when YYTABLE
   doesn't specify something else to do.  Zero means the default is an
   error. */
static const short yydefact[] =
{
       2,     0,     0,     1,     0,     0,    18,     0,     0,    15,
      17,     0,     3,    16,    11,     0,     7,     0,     0,     4,
      15,     8,     9,    10,    12,    13,    14,    22,    20,     0,
      21,    24,     0,     5,     6,     0,    19,    23,     0,     0
};

static const short yydefgoto[] =
{
       1,     3,     4,    15,    16,    17,     8,    13,    32
};

static const short yypact[] =
{
  -32768,    10,   -15,-32768,    -7,     6,-32768,    -5,    -1,     1,
  -32768,    -2,-32768,-32768,-32768,   -19,-32768,     9,     2,    15,
       1,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,     7,
  -32768,-32768,     4,-32768,-32768,     5,-32768,-32768,    28,-32768
};

static const short yypgoto[] =
{
  -32768,-32768,-32768,-32768,    11,-32768,-32768,-32768,-32768
};


#define	YYLAST		31


static const short yytable[] =
{
      10,    19,    20,     5,    11,    27,    28,    29,    30,     6,
      38,     7,    14,     2,     9,    31,    12,    21,    22,    23,
      18,    24,    33,    25,    26,    37,    35,    36,    39,     0,
       0,    34
};

static const short yycheck[] =
{
       1,    20,    21,    18,     5,     3,     4,     5,     6,    16,
       0,     5,    11,     3,    19,    13,    17,     8,     9,    10,
      22,    12,     7,    14,    15,    20,    19,    23,     0,    -1,
      -1,    20
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "/i/bison/1.35/share/bison/bison.simple"

/* Skeleton output parser for bison,

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002 Free Software
   Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* This is the parser code that is written into each bison parser when
   the %semantic_parser declaration is not specified in the grammar.
   It was written by Richard Stallman by simplifying the hairy parser
   used when %semantic_parser is specified.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

#if ! defined (yyoverflow) || defined (YYERROR_VERBOSE)

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# if YYSTACK_USE_ALLOCA
#  define YYSTACK_ALLOC alloca
# else
#  ifndef YYSTACK_USE_ALLOCA
#   if defined (alloca) || defined (_ALLOCA_H)
#    define YYSTACK_ALLOC alloca
#   else
#    ifdef __GNUC__
#     define YYSTACK_ALLOC __builtin_alloca
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
# else
#  if defined (__STDC__) || defined (__cplusplus)
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   define YYSIZE_T size_t
#  endif
#  define YYSTACK_ALLOC malloc
#  define YYSTACK_FREE free
# endif
#endif /* ! defined (yyoverflow) || defined (YYERROR_VERBOSE) */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (YYLTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short yyss;
  YYSTYPE yyvs;
# if YYLSP_NEEDED
  YYLTYPE yyls;
# endif
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAX (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# if YYLSP_NEEDED
#  define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE) + sizeof (YYLTYPE))	\
      + 2 * YYSTACK_GAP_MAX)
# else
#  define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE))				\
      + YYSTACK_GAP_MAX)
# endif

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  register YYSIZE_T yyi;		\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAX;	\
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif


#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# if defined (__STDC__) || defined (__cplusplus)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# endif
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	goto yyacceptlab
#define YYABORT 	goto yyabortlab
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { 								\
      yyerror ("syntax error: cannot back up");			\
      YYERROR;							\
    }								\
while (0)

#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Compute the default location (before the actions
   are run).

   When YYLLOC_DEFAULT is run, CURRENT is set the location of the
   first token.  By default, to implement support for ranges, extend
   its range to the last symbol.  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)       	\
   Current.last_line   = Rhs[N].last_line;	\
   Current.last_column = Rhs[N].last_column;
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#if YYPURE
# if YYLSP_NEEDED
#  ifdef YYLEX_PARAM
#   define YYLEX		yylex (&yylval, &yylloc, YYLEX_PARAM)
#  else
#   define YYLEX		yylex (&yylval, &yylloc)
#  endif
# else /* !YYLSP_NEEDED */
#  ifdef YYLEX_PARAM
#   define YYLEX		yylex (&yylval, YYLEX_PARAM)
#  else
#   define YYLEX		yylex (&yylval)
#  endif
# endif /* !YYLSP_NEEDED */
#else /* !YYPURE */
# define YYLEX			yylex ()
#endif /* !YYPURE */


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)
/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
#endif /* !YYDEBUG */

/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   SIZE_MAX < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#if YYMAXDEPTH == 0
# undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif

#ifdef YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  register const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  register char *yyd = yydest;
  register const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif
#endif

#line 315 "/i/bison/1.35/share/bison/bison.simple"


/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
#  define YYPARSE_PARAM_ARG void *YYPARSE_PARAM
#  define YYPARSE_PARAM_DECL
# else
#  define YYPARSE_PARAM_ARG YYPARSE_PARAM
#  define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
# endif
#else /* !YYPARSE_PARAM */
# define YYPARSE_PARAM_ARG
# define YYPARSE_PARAM_DECL
#endif /* !YYPARSE_PARAM */

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
# ifdef YYPARSE_PARAM
int yyparse (void *);
# else
int yyparse (void);
# endif
#endif

/* YY_DECL_VARIABLES -- depending whether we use a pure parser,
   variables are global, or local to YYPARSE.  */

#define YY_DECL_NON_LSP_VARIABLES			\
/* The lookahead symbol.  */				\
int yychar;						\
							\
/* The semantic value of the lookahead symbol. */	\
YYSTYPE yylval;						\
							\
/* Number of parse errors so far.  */			\
int yynerrs;

#if YYLSP_NEEDED
# define YY_DECL_VARIABLES			\
YY_DECL_NON_LSP_VARIABLES			\
						\
/* Location data for the lookahead symbol.  */	\
YYLTYPE yylloc;
#else
# define YY_DECL_VARIABLES			\
YY_DECL_NON_LSP_VARIABLES
#endif


/* If nonreentrant, generate the variables here. */

#if !YYPURE
YY_DECL_VARIABLES
#endif  /* !YYPURE */

int
yyparse (YYPARSE_PARAM_ARG)
     YYPARSE_PARAM_DECL
{
  /* If reentrant, generate the variables here. */
#if YYPURE
  YY_DECL_VARIABLES
#endif  /* !YYPURE */

  register int yystate;
  register int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Lookahead token as an internal (translated) token number.  */
  int yychar1 = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack. */
  short	yyssa[YYINITDEPTH];
  short *yyss = yyssa;
  register short *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  register YYSTYPE *yyvsp;

#if YYLSP_NEEDED
  /* The location stack.  */
  YYLTYPE yylsa[YYINITDEPTH];
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;
#endif

#if YYLSP_NEEDED
# define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
# define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  YYSIZE_T yystacksize = YYINITDEPTH;


  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
#if YYLSP_NEEDED
  YYLTYPE yyloc;
#endif

  /* When reducing, the number of symbols on the RHS of the reduced
     rule. */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;
#if YYLSP_NEEDED
  yylsp = yyls;
#endif
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  */
# if YYLSP_NEEDED
	YYLTYPE *yyls1 = yyls;
	/* This used to be a conditional around just the two extra args,
	   but that might be undefined if yyoverflow is a macro.  */
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yyls1, yysize * sizeof (*yylsp),
		    &yystacksize);
	yyls = yyls1;
# else
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);
# endif
	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyoverflowlab;
# else
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	goto yyoverflowlab;
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;

      {
	short *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyoverflowlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);
# if YYLSP_NEEDED
	YYSTACK_RELOCATE (yyls);
# endif
# undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
#if YYLSP_NEEDED
      yylsp = yyls + yysize - 1;
#endif

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yychar1 = YYTRANSLATE (yychar);

#if YYDEBUG
     /* We have to keep this `#if YYDEBUG', since we use variables
	which are defined only if `YYDEBUG' is set.  */
      if (yydebug)
	{
	  YYFPRINTF (stderr, "Next token is %d (%s",
		     yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise
	     meaning of a token, for further debugging info.  */
# ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
# endif
	  YYFPRINTF (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */
  YYDPRINTF ((stderr, "Shifting token %d (%s), ",
	      yychar, yytname[yychar1]));

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#if YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to the semantic value of
     the lookahead token.  This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

#if YYLSP_NEEDED
  /* Similarly for the default location.  Let the user run additional
     commands if for instance locations are ranges.  */
  yyloc = yylsp[1-yylen];
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
#endif

#if YYDEBUG
  /* We have to keep this `#if YYDEBUG', since we use variables which
     are defined only if `YYDEBUG' is set.  */
  if (yydebug)
    {
      int yyi;

      YYFPRINTF (stderr, "Reducing via rule %d (line %d), ",
		 yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (yyi = yyprhs[yyn]; yyrhs[yyi] > 0; yyi++)
	YYFPRINTF (stderr, "%s ", yytname[yyrhs[yyi]]);
      YYFPRINTF (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif

  switch (yyn) {

case 3:
#line 198 "aux-item-def-parse.y"
{
                if (def.tag != 0)
                {
                    aux_item_definition_add(&def);
                }
                def = empty_aux_item_definition;
            }
    break;
case 4:
#line 208 "aux-item-def-parse.y"
{
                def.tag = yyvsp[-5].num;
                def.name = s_crea_c_str(yyvsp[-3].str);
                if (buglevel)
                {
                    kom_log("Parsing definition of aux-item %ld (%s)\n",
                            def.tag, def.name);
                }
                s_clear(&(yyvsp[-3].str));
                yyvsp[-3].str = EMPTY_STRING;
                yylval.str = EMPTY_STRING;
            }
    break;
case 5:
#line 221 "aux-item-def-parse.y"
{
                def.tag = yyvsp[-6].num;
                def.name = s_crea_c_str(yyvsp[-4].str);
                def.disabled = TRUE;
                s_clear(&(yyvsp[-4].str));
                yyvsp[-4].str = EMPTY_STRING;
                yylval.str = EMPTY_STRING;
            }
    break;
case 8:
#line 235 "aux-item-def-parse.y"
{ def.texts = TRUE; def.text_a = yyvsp[-1].num; }
    break;
case 9:
#line 236 "aux-item-def-parse.y"
{ def.confs = TRUE; def.conf_a = yyvsp[-1].num; }
    break;
case 10:
#line 237 "aux-item-def-parse.y"
{ def.letterboxes = TRUE;
                                        def.conf_a = yyvsp[-1].num; }
    break;
case 11:
#line 239 "aux-item-def-parse.y"
{ def.system = TRUE; }
    break;
case 12:
#line 241 "aux-item-def-parse.y"
{
                def.texts       = TRUE; def.text_a = yyvsp[-1].num;
                def.confs       = TRUE; def.conf_a = yyvsp[-1].num;
                def.letterboxes = TRUE; 
                def.system      = TRUE;
            }
    break;
case 13:
#line 249 "aux-item-def-parse.y"
{ yyval.num = yyvsp[-1].num | AUX_ITEM_ADD_ON_CREATE; }
    break;
case 14:
#line 250 "aux-item-def-parse.y"
{ yyval.num = yyvsp[-1].num | AUX_ITEM_ADD_ON_MODIFY; }
    break;
case 15:
#line 251 "aux-item-def-parse.y"
{ yyval.num = 0; }
    break;
case 19:
#line 260 "aux-item-def-parse.y"
{
                int found = 0;

                CHK_ASSIGN("author-only", author_only, BOOLEAN,
                           yyvsp[-3].str, yyvsp[-1].value, yylsp[-1].first_line);
                CHK_ASSIGN("supervisor-only", supervisor_only,
                           BOOLEAN, yyvsp[-3].str, yyvsp[-1].value, yylsp[-1].first_line);
                CHK_ASSIGN("system-only", system_only,
                           BOOLEAN, yyvsp[-3].str, yyvsp[-1].value, yylsp[-1].first_line);
                CHK_ASSIGN("inherit-limit", inherit_limit, NUMBER,
                           yyvsp[-3].str, yyvsp[-1].value, yylsp[-1].first_line);
                CHK_ASSIGN("unique", one_per_person, BOOLEAN,
                           yyvsp[-3].str, yyvsp[-1].value, yylsp[-1].first_line);
                CHK_ASSIGN("unique-data", unique_data, BOOLEAN,
                           yyvsp[-3].str, yyvsp[-1].value, yylsp[-1].first_line);
                CHK_ASSIGN("permanent", may_not_delete, BOOLEAN,
                           yyvsp[-3].str, yyvsp[-1].value, yylsp[-1].first_line);
                CHK_ASSIGN("owner-delete", owner_delete, BOOLEAN,
                           yyvsp[-3].str, yyvsp[-1].value, yylsp[-1].first_line);
                CHK_FLAG_A("inherit", inherit, yyvsp[-3].str, yyvsp[-1].value, yylsp[-1].first_line);
                CHK_FLAG_A("secret", secret, yyvsp[-3].str, yyvsp[-1].value, yylsp[-1].first_line);
                CHK_FLAG_A("hide-creator", hide_creator,
                           yyvsp[-3].str, yyvsp[-1].value, yylsp[-1].first_line);
                CHK_FLAG_A("dont-garb", dont_garb, yyvsp[-3].str, yyvsp[-1].value, yylsp[-1].first_line);
                CHK_FLAG_A("reserved-2", reserved3, yyvsp[-3].str, yyvsp[-1].value, yylsp[-1].first_line); 
                CHK_FLAG_A("reserved-3", reserved4, yyvsp[-3].str, yyvsp[-1].value, yylsp[-1].first_line); 
                CHK_FLAG_A("reserved-4", reserved5, yyvsp[-3].str, yyvsp[-1].value, yylsp[-1].first_line); 


                found = found ? 1 :
                    aux_item_def_check_validate("validate",
                                                yyvsp[-3].str,
                                                yyvsp[-1].value.type,
                                                yyvsp[-1].value.val.str,
                                                &def);

                found = found ? 1 :
                    aux_item_def_check_trigger("delete-trigger",
                                               yyvsp[-1].value.type,
                                               yyvsp[-3].str,
                                               yyvsp[-1].value.val.str,
                                               &def.num_delete_triggers,
                                               &def.delete_triggers);


                found = found ? 1 :
                    aux_item_def_check_trigger("undelete-trigger",
                                               yyvsp[-1].value.type,
                                               yyvsp[-3].str,
                                               yyvsp[-1].value.val.str,
                                               &def.num_undelete_triggers,
                                               &def.undelete_triggers);


                found = found ? 1 :
                    aux_item_def_check_trigger("add-trigger",
                                               yyvsp[-1].value.type,
                                               yyvsp[-3].str,
                                               yyvsp[-1].value.val.str,
                                               &def.num_add_triggers,
                                               &def.add_triggers);


                if (found == 0)
                {
                    char *tmp;

                    tmp = s_crea_c_str(yyvsp[-3].str);
                    aux_item_def_error_line = yylsp[-3].first_line;
                    yyerrorf("invalid field name: %s", tmp);
                    string_free(tmp);
                }

                if (yyvsp[-1].value.type == STRING || yyvsp[-1].value.type == ID)
                {
                    s_clear(&(yyvsp[-1].value).val.str);
                    yyvsp[-1].value.val.str = EMPTY_STRING;
                    yylval.value.val.str = EMPTY_STRING;
                }
                s_clear(&(yyvsp[-3].str));
                yyvsp[-3].str = EMPTY_STRING;
                yylval.str = EMPTY_STRING;
            }
    break;
case 20:
#line 345 "aux-item-def-parse.y"
{ yyval.value.val.num = yyvsp[0].num; yyval.value.type = BOOLEAN; }
    break;
case 21:
#line 346 "aux-item-def-parse.y"
{ yyval.value.val.str = yyvsp[0].str; yyval.value.type = STRING;  }
    break;
case 22:
#line 347 "aux-item-def-parse.y"
{ yyval.value.val.num = yyvsp[0].num; yyval.value.type = NUMBER;  }
    break;
case 23:
#line 348 "aux-item-def-parse.y"
{ yyval.value.val.str = yyvsp[-2].str; yyval.value.type = ID;}
    break;
case 24:
#line 349 "aux-item-def-parse.y"
{ YYERROR; }
    break;
}

#line 705 "/i/bison/1.35/share/bison/bison.simple"


  yyvsp -= yylen;
  yyssp -= yylen;
#if YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG
  if (yydebug)
    {
      short *yyssp1 = yyss - 1;
      YYFPRINTF (stderr, "state stack now");
      while (yyssp1 != yyssp)
	YYFPRINTF (stderr, " %d", *++yyssp1);
      YYFPRINTF (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;
#if YYLSP_NEEDED
  *++yylsp = yyloc;
#endif

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  YYSIZE_T yysize = 0;
	  char *yymsg;
	  int yyx, yycount;

	  yycount = 0;
	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  for (yyx = yyn < 0 ? -yyn : 0;
	       yyx < (int) (sizeof (yytname) / sizeof (char *)); yyx++)
	    if (yycheck[yyx + yyn] == yyx)
	      yysize += yystrlen (yytname[yyx]) + 15, yycount++;
	  yysize += yystrlen ("parse error, unexpected ") + 1;
	  yysize += yystrlen (yytname[YYTRANSLATE (yychar)]);
	  yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg != 0)
	    {
	      char *yyp = yystpcpy (yymsg, "parse error, unexpected ");
	      yyp = yystpcpy (yyp, yytname[YYTRANSLATE (yychar)]);

	      if (yycount < 5)
		{
		  yycount = 0;
		  for (yyx = yyn < 0 ? -yyn : 0;
		       yyx < (int) (sizeof (yytname) / sizeof (char *));
		       yyx++)
		    if (yycheck[yyx + yyn] == yyx)
		      {
			const char *yyq = ! yycount ? ", expecting " : " or ";
			yyp = yystpcpy (yyp, yyq);
			yyp = yystpcpy (yyp, yytname[yyx]);
			yycount++;
		      }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exhausted");
	}
      else
#endif /* defined (YYERROR_VERBOSE) */
	yyerror ("parse error");
    }
  goto yyerrlab1;


/*--------------------------------------------------.
| yyerrlab1 -- error raised explicitly by an action |
`--------------------------------------------------*/
yyerrlab1:
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;
      YYDPRINTF ((stderr, "Discarding token %d (%s).\n",
		  yychar, yytname[yychar1]));
      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;


/*-------------------------------------------------------------------.
| yyerrdefault -- current state does not do anything special for the |
| error token.                                                       |
`-------------------------------------------------------------------*/
yyerrdefault:
#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */

  /* If its default is to accept any token, ok.  Otherwise pop it.  */
  yyn = yydefact[yystate];
  if (yyn)
    goto yydefault;
#endif


/*---------------------------------------------------------------.
| yyerrpop -- pop the current state because it cannot handle the |
| error token                                                    |
`---------------------------------------------------------------*/
yyerrpop:
  if (yyssp == yyss)
    YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#if YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG
  if (yydebug)
    {
      short *yyssp1 = yyss - 1;
      YYFPRINTF (stderr, "Error: state stack now");
      while (yyssp1 != yyssp)
	YYFPRINTF (stderr, " %d", *++yyssp1);
      YYFPRINTF (stderr, "\n");
    }
#endif

/*--------------.
| yyerrhandle.  |
`--------------*/
yyerrhandle:
  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

  YYDPRINTF ((stderr, "Shifting error token, "));

  *++yyvsp = yylval;
#if YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

/*---------------------------------------------.
| yyoverflowab -- parser overflow comes here.  |
`---------------------------------------------*/
yyoverflowlab:
  yyerror ("parser stack overflow");
  yyresult = 2;
  /* Fall through.  */

yyreturn:
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}
#line 352 "aux-item-def-parse.y"


extern FILE *yyin;

#if defined(HAVE_VFPRINTF) && defined(HAVE_STDARG_H)
void yyerrorf(const char *format, ...)
{
    va_list     AP;

    va_start(AP, format);
    fprintf(stderr, "%s: %d: ", inputFile, aux_item_def_error_line);
    vfprintf(stderr, format, AP);
    fprintf(stderr, "\n");
    fflush(stderr);

    errorFlag = TRUE;
    va_end(AP);
}
#else
void yyerrorf(const char *s,
             int a, int b, int c, int d, int e, int f, int g)
{
    fprintf(stderr, "%s: %d: ",  inputFile, aux_item_def_error_line);
    fprintf(stderr, format, a, b, c, d, e, f, g);
    fprintf(stderr, "\n");
    fflush(stderr);

    errorFlag = TRUE;
}
#endif

static const char *aux_item_def_typename(int type)
{
    switch (type)
    {
    case STRING:  return "string";
    case NUMBER:  return "number";
    case BOOLEAN: return "boolean";
    case ID:      return "identifier";
    default:
        return "unknown";
    }
}

static int  aux_item_def_check_assign(const char  *id,
                                      String field,
                                      void  *data,
                                      int    type,
                                      struct aux_item_def_value_type *val,
                                      int    lineno)
{
    if (!s_strcmp(s_fcrea_str(id), field))
    {
        if (type != val->type)
        {
            aux_item_def_error_line = lineno;
            yyerrorf("invalid type: expected %s, got %s",
                     aux_item_def_typename(type),
                     aux_item_def_typename(val->type));
            return 0;
        }
        else if (type == STRING)
        {
            *((char **)data) = s_crea_c_str(val->val.str);
        }
        else if (type == NUMBER)
        {
            *((unsigned long *)data) = val->val.num;
        }
        else if (type == BOOLEAN)
        {
            *((Bool *)data) = val->val.num ? TRUE : FALSE;
        }
        else
        {
            restart_kom("Internal error: bad type in aux-item definition "
                        "assignment (can't happen.)\n");
        }
        return 1;
    }
    else
    {
        return 0;
    }
}

static short aux_item_def_check_trigger(const char *check_name,
					int type,
					String trigger_name,
					String function_name,
					unsigned long *counter,
					Aux_item_trigger **triggers)
{
    Aux_item_trigger trigger;
    char *tmp_string;

    if (s_strcmp(s_fcrea_str(check_name), trigger_name) == 0)
    {
        if (type != ID)
        {
            yyerrorf("invalid type: expected %s, got %s",
                    aux_item_def_typename(ID), 
                    aux_item_def_typename(type)); 
            return 0;
        }

        tmp_string = s_crea_c_str(function_name);
        trigger = aux_item_find_trigger(tmp_string);
        string_free(tmp_string);

        if (trigger == NULL)
        {
            yyerrorf("undefined function: %s",
                    tmp_string);
            return 1;
        }

        *counter += 1;
        *triggers =
            srealloc(*triggers,
                     *counter *
                     sizeof(Aux_item_trigger));
        *triggers[*counter-1] = trigger;

        return 1;
    }
    return 0;
}

void parse_aux_item_definitions(char *file)
{
    inputFile = file;
    yyin = i_fopen(file, "r");
    if (yyin == NULL)
    {
        perror(file);
        restart_kom("Unable to open aux-item definition file\n");
    }

    def = empty_aux_item_definition;
    yyparse();
    i_fclose(yyin);

    if (errorFlag == TRUE)
    {
        restart_kom("Errors reading aux-item definition file\n");
    }

    /*
      {
        extern Aux_item_definition *aux_item_definition_list;
        extern unsigned long num_aux_item_definitions;
        Aux_item_definition *def;

        fprintf(stderr, "Number of defs: %ld\n", num_aux_item_definitions);
        def = aux_item_definition_list;

        while (def != NULL)
        {
            fprintf(stderr, "Name:            '%s'\n", def->name);
            fprintf(stderr, "Tag:             %ld\n", def->tag);
            fprintf(stderr, "Clear flags:     ");
            if (def->clear_flags.deleted) fprintf(stderr, "deleted ");
            if (def->clear_flags.inherit) fprintf(stderr, "inherit ");
            if (def->clear_flags.secret) fprintf(stderr, "secret ");
            if (def->clear_flags.hide_creator) fprintf(stderr,"hide_creator ");
            if (def->clear_flags.reserved2) fprintf(stderr, "reserved2 ");
            if (def->clear_flags.reserved3) fprintf(stderr, "reserved3 ");
            if (def->clear_flags.reserved4) fprintf(stderr, "reserved4 ");
            if (def->clear_flags.reserved5) fprintf(stderr, "reserved5 ");
            fprintf(stderr, "\n");
            fprintf(stderr, "Set flags:       ");
            if (def->set_flags.deleted) fprintf(stderr, "deleted ");
            if (def->set_flags.inherit) fprintf(stderr, "inherit ");
            if (def->set_flags.secret) fprintf(stderr, "secret ");
            if (def->set_flags.hide_creator) fprintf(stderr,"hide_creator ");
            if (def->set_flags.reserved2) fprintf(stderr, "reserved2 ");
            if (def->set_flags.reserved3) fprintf(stderr, "reserved3 ");
            if (def->set_flags.reserved4) fprintf(stderr, "reserved4 ");
            if (def->set_flags.reserved5) fprintf(stderr, "reserved5 ");
            fprintf(stderr, "\n");
            fprintf(stderr, "Author only:     %d\n", def->author_only);
            fprintf(stderr, "Supervisor only: %d\n", def->supervisor_only);
            fprintf(stderr, "Unique:          %d\n", def->one_per_person);
            fprintf(stderr, "Unique-data:     %d\n", def->unique_data);
            fprintf(stderr, "Permanent:       %d\n", def->may_not_delete);
            fprintf(stderr, "Inherit limit:   %ld\n", def->inherit_limit);
            fprintf(stderr, "Texts:           %d\n", def->texts);
            fprintf(stderr, "Conferences:     %d\n", def->confs);
            fprintf(stderr, "Letterboxes:     %d\n", def->letterboxes);
            fprintf(stderr, "Validate regexp: '%s'\n",
                    def->validate_regexp?def->validate_regexp:"0x0");
            def = def->next;
        }
    }
    */
}


static short aux_item_def_check_validate(const char *check_name,
					 String field_name,
					 int type,
					 String data,
					 Aux_item_definition *def)
{
    Aux_item_validation_function        validator;
    char                               *tmp_string;

    if (s_strcmp(s_fcrea_str(check_name), field_name) == 0)
    {
        /*
         * Validator is a function
         */

        if (type == ID)
        {
            tmp_string = s_crea_c_str(data);
            validator = aux_item_find_validator(tmp_string);
            string_free(tmp_string);

            if (validator == NULL)
            {
                yyerrorf("undefined function: %s",
                        tmp_string);
                return 1;
            }

            def->num_validators += 1;
            def->validators = srealloc(def->validators,
                                       def->num_validators *
                                       sizeof(*def->validators));

            def->validators[def->num_validators-1].type = AUX_VALIDATE_FUNCTION;
            def->validators[def->num_validators-1].v.fn.function = validator;

            return 1;
            
        }
        else if (type == STRING)
        {
            /*
             * Validator is a string (regexp)
             */

            def->num_validators += 1;
            def->validators = srealloc(def->validators,
                                       def->num_validators *
                                       sizeof(*def->validators));


            def->validators[def->num_validators-1].type = AUX_VALIDATE_REGEXP;
            def->validators[def->num_validators-1].v.re.regexp = s_crea_c_str(data);
            def->validators[def->num_validators-1].v.re.cached_re_buf = NULL;
        }
        else
        {
            yyerrorf("invalid type: expected %s or %s, got %s",
                    aux_item_def_typename(ID), 
                    aux_item_def_typename(STRING), 
                    aux_item_def_typename(type)); 
            return 0;
        }

        return 1;
    }
    return 0;
}
