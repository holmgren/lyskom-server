/*
 * $Id: conference.c,v 0.92 2003/08/23 16:38:17 ceder Exp $
 * Copyright (C) 1991-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * conference.c
 *
 * All atomic calls that deals with conferences.
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <ctype.h>
#include <stdio.h>
#include "timewrap.h"
#include <setjmp.h>
#include <sys/types.h>

#include "s-string.h"
#include "kom-types.h"
#include "log.h"
#include "services.h"
#include "s-string.h"
#include "cache.h"
#include "misc-types.h"
#include "s-collat-tabs.h"
#include "com.h"
#include "async.h"
#include "connections.h"
#include "kom-errno.h"
#include "manipulate.h"
#include "server/smalloc.h"
#include "kom-config.h"
#include "parser.h"
#include "internal-connections.h"
#include "lyskomd.h"
#include "debug.h"
#include "send-async.h"
#include "param.h"
#include "aux-items.h"
#include "local-to-global.h"
#include "server-time.h"
#include "stats.h"

BUGDECL;

/*
 * Static functions
 */

/*
 * Delete a conference. Delete all references to this conf.
 */

static void
do_delete_conf (Conf_no      conf_no,
		Conference * conf_c) /* Not NULL */
{
    int i;
    
    if ( do_set_presentation(conf_no, conf_c, 0) != OK )
    {
	kom_log("ERROR: do_delete_conf() - couldn't unmark presentation.\n");
    }
    
    if ( do_set_etc_motd(conf_no, conf_c, 0) != OK )
    {
	kom_log("ERROR: do_delete_conf() - couldn't unmark motd.\n");
    }

    /* Delete all members */
    /* Note that because of the way do_sub_member is written it is important */
    /* that the loop is executed this way. */
    for ( i = conf_c->members.no_of_members - 1; i >= 0; i-- )
    {
	do_sub_member( conf_no, conf_c, conf_c->members.members + i,
		      conf_c->members.members[ i ].member, NULL, NULL);
    }

    sfree( conf_c->members.members );
    conf_c->members.members = NULL;

    /* texts */

    /*
     * The texts are not deleted at once, but since they now have no recipient
     * they will not live long.
     */

    /*
     * Note that there will still be a recipient in the texts, but it
     * will not exist in reality.  This is maybe unfortunate, but
     * there might be thousands of texts in the conference, and we
     * cannot modify all those text statuses in a single atomic call.
     */

    /* FIXME (bug 905): There should be an async message about the deletion. */

    cached_delete_conf( conf_no );
    update_stat(STAT_CONFS, -1);
    
    return;
}

/*
 * Functions that are exported to the server.
 */

void
set_conf_errno(const Connection *viewer_conn,
	       Conf_no conf_no,
	       enum kom_err errcode)
{
    err_stat = conf_no;
    if (!has_access(conf_no, viewer_conn, read_protected))
	kom_errno = KOM_UNDEF_CONF;
    else
	kom_errno = errcode;
}


/*
 * Return TRUE if NAME is not already used. Set kom_errno to
 * KOM_LONG_STR or KOM_BAD_NAME if it fails.
 */

Bool
legal_name( String name )
{
    if (name.len == 0 || name.len > param.conf_name_len )
    {
        err_stat = name.len?  param.conf_name_len : 0;
	kom_errno = name.len? KOM_LONG_STR        : KOM_BAD_NAME;
	return FALSE;
    }

    while( name.len-- )
    {
#ifdef HAVE_LOCALE_H
	if (param.force_iso_8859_1
	    ? (*name.string < ' ' ||
	       (*name.string > 126 && *name.string < ' ' + 128))
	    : !isprint(*name.string))
	{
            err_stat = 0;
	    kom_errno = KOM_BAD_NAME;
	    return FALSE;
	}
#else
	if (*name.string < ' ' ||
	    (*name.string > 126 && *name.string < ' ' + 128))
	{
            err_stat = 0;
	    kom_errno = KOM_BAD_NAME;
	    return FALSE;
	}
#endif
	name.string++;
    }

    return TRUE;
}

/*
 * Return TRUE if name is unique, or if the only match is conf_no. Use 0
 * as conf_no if it should not be allowed to be changed.
 */
Bool
unique_name( const String name, Conf_no conf_no )
{
    Parse_info   parse_info;
    Parse_token *name_token;
    Parse_token *existing_token;
    Bool	 exact_match_found;
    Bool	 diff_found;
    int i;
    int n;
        
    parse_info = parse(name, match_table, FALSE, TRUE,
		       s_fcrea_str(WHITESPACE),
		       DEFAULT_COLLAT_TAB);

    if ( parse_info.no_of_matches == 0 ) /* Doesn't match any name. */
    {
	sfree(parse_info.indexes);
	return TRUE;
    }

    if ( parse_info.no_of_matches == -1 ) /* Error. */
    {
	kom_log("unique_name(): parse returned error.\n");
	sfree(parse_info.indexes);
	return FALSE;
    }
    
    if ( parse_info.no_of_matches == 1 && parse_info.indexes[ 0 ] == -1 )
    {
	/* Empty name is not allowed. */
	sfree(parse_info.indexes);
	return FALSE;
    }
    
    /* The name matches some conference. Check if they are equal. */

    name_token = tokenize(name, s_fcrea_str(WHITESPACE));

    exact_match_found = FALSE;
    
    for ( i = 0; !exact_match_found && i < parse_info.no_of_matches; i++ )
    {
	existing_token = match_table[ parse_info.indexes[ i ] ].tokens;
	diff_found = FALSE;
	
	for ( n = 0;
	     (!diff_found && !s_empty(existing_token[ n ].word)
	      && !s_empty(name_token[ n ].word));
	     ++n)
	{
	    if ( !s_usr_streq(existing_token[ n ].word,
			      name_token[ n ].word,
			      DEFAULT_COLLAT_TAB) )
	    {
		diff_found = TRUE;
	    }
	}

	if (! s_empty(existing_token[ n ].word)
	    || ! s_empty(name_token[ n ].word) )
	{
	    /* The length (number of words) differed. */
	    diff_found = TRUE;
	}

	if ( !diff_found && match_table[ parse_info.indexes[ i ] ].conf_no != conf_no )
	    exact_match_found = TRUE;
    }
	    
    sfree(parse_info.indexes);
    free_tokens(name_token);
    return exact_match_found ? FALSE : TRUE;		
}

/*
 * Create a conference.
 */
static Conf_no
do_create_conf(String	 name,
	       Pers_no	 creator,
	       Conf_no	 supervisor,
	       Conf_no	 super_conf,
	       Conf_type type,
	       Connection *creating_connection,
               Aux_item_list *aux)
{
    Conf_no 	 conf_no;
    Conference * conf_c;
    
    /* Prepare, then check the aux items */

    /* Allocate memory for conf_c */

    conf_no = cached_create_conf( name );
    if (conf_no == 0)
    {
        /* kom_errno and err_stat set in cached_create_conf */
        kom_log("ERROR: Couldn't create conference. Too many conferences.\n");
        return 0;
    }

    if ( (conf_c = cached_get_conf_stat( conf_no ) ) == NULL)
    {
	restart_kom("create_conf() - can't get conf_stat\n");
    }

    conf_c->creator	= creator;
    conf_c->creation_time = current_time.tv_sec;
    conf_c->presentation= 0;		/* No presentation yet */
    conf_c->supervisor	= supervisor;
    conf_c->permitted_submitters = 0;
    conf_c->super_conf	= super_conf;
    conf_c->type	= type;
    conf_c->last_written= conf_c->creation_time;
    conf_c->msg_of_day	= 0;
    conf_c->nice	= param.default_nice;
    conf_c->keep_commented = param.default_keep_commented;
    conf_c->highest_aux = 0;
    conf_c->expire      = 0;

    /* Update small_conf_arr before conf_stat_check_add_aux_item_list
       uses it to check that the user is allowed to add the aux-items. */
    mark_conference_as_changed (conf_no);

    prepare_aux_item_list(aux, creator);
    if (conf_stat_check_add_aux_item_list(conf_c,
                                          conf_no,
                                          aux,
                                          creating_connection,
                                          TRUE) != OK)
    {
	/* FIXME (bug 146): Conf_no leak: We create a conference, check the
	   aux-items, determine that the aux-items are bogus, and
	   immediately delete the conference.  This leaks a conference
	   number.  We can live with that, but it isn't pretty.  */

        cached_delete_conf(conf_no);
        return 0;
    }

    conf_stat_add_aux_item_list(conf_c, conf_no, aux, creator);

    mark_conference_as_changed (conf_no);
    update_stat(STAT_CONFS, 1);

    return conf_no;
}


/*
 * Return TRUE if viewer is a supervisor to CONF.
 */

Bool
is_supervisor(Conf_no conf,
	      const Connection *viewer)
{
    if (viewer->pers_no == 0)
	return FALSE;

    /* A person is ALWAYS supervisor to his/her own mailbox!  */
    if (viewer->pers_no == conf)
        return TRUE;

    return is_strictly_supervisor(conf, viewer->pers_no, viewer->person);
}


Bool
is_strictly_supervisor(Conf_no       conf,
                       Pers_no       viewer,
                       const Person *viewer_p) /* May be NULL */
{
    Conf_no supervisor;

    if (viewer == 0)		/* Not yet logged in. */
	return FALSE;

    if (!cached_conf_exists(conf))
	return FALSE;

    if ((supervisor = cached_get_conf_supervisor(conf)) == 0)
        return FALSE;
	
    if (viewer == supervisor)
        return TRUE;

    if (viewer_p == NULL)
	GET_P_STAT(viewer_p, viewer, FALSE);

    if (locate_membership(supervisor, viewer_p) != NULL)
	return TRUE;
    
    return FALSE;
}


/*
 * Atomic functions
 */

/*
 * Change name of a person or conference. You must be supervisor
 * of the conference to use this call.
 */
extern Success
change_name (Conf_no	   conf_no,
	     const String  new_name)
{
    Conference * conf_c;
    enum access	 acc;

    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    GET_C_STAT(conf_c, conf_no, FAILURE);

    acc = access_perm(conf_no, active_connection, unlimited);

    if ( acc <= none )
    {
        err_stat = conf_no;
	kom_errno = KOM_UNDEF_CONF;
	return FAILURE;
    }

    if ( !ACT_P->privileges.change_name
	|| (acc != unlimited && !ENA(admin, 3))) /* OK -- In an RPC call */
    {
        err_stat = conf_no;
	kom_errno = KOM_PERM;
	return FAILURE;
    }

    if ( !legal_name( new_name ) )
    {
	/* kom_errno will be set by legal_name(). */
	return FAILURE;
    }

    if ( !unique_name( new_name, conf_no ) )
    {
        err_stat = 0;
	kom_errno = KOM_CONF_EXISTS;
	return FAILURE;
    }

    async_new_name(conf_no, conf_c->name, new_name);

    s_strcpy(&conf_c->name, new_name);
    mark_conference_as_changed(conf_no);

    cached_change_name(conf_no, new_name);

    return OK;
}

/*
 * Create a conference: Default modes:
 *	ACTPERS becomes supervisor and super_conf.
 *	Anyone can submitt texts.
 *	Noone (not even the creator) is a member in the conf.
 *
 * If ANYONE_CAN_CREATE_NEW_CONFS (#defined in config.h) is not true
 * you must have the 'create_conf' capability.
 *
 * It is currently not allowed to have a conference that is secret
 * and not rd_prot. This restriction might be lifted in the future
 * (but I don't understand the use of such a conference...)
 */

static Conf_no
create_conf_generic(const String  name,
                    Conf_type	  type,
                    Aux_item_list *aux)
{
    Conf_no	 conf_no;
    
    CHK_CONNECTION(0);
    CHK_LOGIN(0);

    if (param.anyone_can_create_new_confs == FALSE
	&& !HAVE_PRIV(ACT_P, create_conf))
    {
        err_stat = 0;
	kom_errno = KOM_PERM;
	return 0;
    }

    if ( !legal_name( name ) )
    {
	/* kom_errno will be set by legal_name(). */
	return 0;
    }

    if ( !unique_name( name, 0 ) )
    {
        err_stat = 0;
	kom_errno = KOM_CONF_EXISTS;
	return 0;
    }

    if ( type.letter_box )	/* A letter_box can only be created via */
    {				/* create_person.			*/
	kom_errno = KOM_PERM;
	return 0;
    }

    if ( type.secret && !type.rd_prot )
    {
        err_stat = 0;
	kom_errno = KOM_SECRET_PUBLIC;
	return 0;
    }
    
    conf_no = do_create_conf(name, ACTPERS, ACTPERS, ACTPERS, type,
			     active_connection, aux);

    if ( conf_no != 0)
    {
	ACT_P->created_confs++;
	mark_person_as_changed( ACTPERS );
    }
    
    return conf_no;
}

Conf_no
create_conf_old(const String name,
                Conf_type    type)
{
    /* CHK_CONNECTION in create_conf_generic */
    return create_conf_generic(name, type, NULL);
}

Conf_no
create_conf(const String name,
            Conf_type    type,
            Aux_item_list *aux)
{
    Conf_no     conf;

    /* CHK_CONNECTION in create_conf_generic */
    conf = create_conf_generic(name, type, aux);
    if (conf != 0)
    {
        /* Send message no. 2 */
    }

    return conf;
}



/*
 * Log out a person from any connection he might be logged on to.
 */
static void
logout_person(Pers_no pers_no)
{
    Session_no i = 0;
    Connection *real_active_connection;

    real_active_connection = active_connection;

    while ( (i = traverse_connections(i)) != 0)
    {
	active_connection = get_conn_by_number(i);

	if ( active_connection->pers_no == pers_no )
	    logout();
    }

    active_connection = real_active_connection;
}

/*
 * Delete a conference or person. You must be supervisor of the
 * conference to be allowed to delete it.
 */
extern Success
delete_conf (Conf_no	conf_no )
{
    Conference	   * conf_c;
    enum access	     acc;
    
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    GET_C_STAT(conf_c, conf_no, FAILURE);

    acc = access_perm(conf_no, active_connection, unlimited);
    
    if ( acc != unlimited )
    {
        err_stat = conf_no;
	kom_errno = (acc <= none) ? KOM_UNDEF_CONF : KOM_PERM ;
	return FAILURE;
    }

    if ( conf_c->type.letter_box )
    {
	/* Make sure the person that is deleted is not logged in. */

	logout_person (conf_no);

	if ( do_delete_pers (conf_no) != OK )
	{
	    kom_log("ERROR: delete_conf(): can't delete person.\n");
	}
    }

    do_delete_conf (conf_no, conf_c);

    return OK;
}

/*
 * Map conference name to number. Can be done without logging in.
 * Secret conferences will not be returned unless ACTPERS is supervisor
 * of, or member in, that conference.
 */
extern Success
lookup_name (const String    name,
	     Conf_list_old * result)
{
    Conf_no	* no, * no_copy;
    Conf_type   * type, *type_copy;
    int		i;
    
    CHK_CONNECTION(FAILURE);
    if ( cached_lookup_name( name, result ) != OK )
	return FAILURE;

    no = no_copy = result->conf_nos;
    type = type_copy = result->type_of_conf;

    for ( i = result->no_of_conf_nos; i > 0; i-- )
    {
	if (!has_access(*no, active_connection, read_protected))
	    --result->no_of_conf_nos;
	else
	{
	    *no_copy++ = *no;
	    *type_copy++ = *type;
	}

	++no;
	++type;
    }
    return OK;
}

extern Success
lookup_z_name (const String       name,
	       int                want_persons,
	       int                want_confs,
	       Conf_z_info_list * result)
{
    Conf_no	 *no;
    Conf_type    *type;
    Conf_z_info  *res;
    int		  i;
    int           n_filtered;
    Conf_list_old raw_matches;
    
    CHK_CONNECTION(FAILURE);
    CHK_BOOL(want_persons, FAILURE);
    CHK_BOOL(want_confs, FAILURE);
    if (cached_lookup_name(name, &raw_matches) != OK)
	return FAILURE;

    no = raw_matches.conf_nos;
    type = raw_matches.type_of_conf;
    n_filtered = raw_matches.no_of_conf_nos;
    for (i = raw_matches.no_of_conf_nos; i > 0; i--)
    {
	if (*no == 0)
	    restart_kom("Internal error detected in lookup_z_name\n");

	if ((type->letter_box ? want_persons : want_confs) == 0
	    || !has_access(*no, active_connection, read_protected))
	{
	    *no = 0;
	    n_filtered--;
	}

	no++;
	type++;
    }

    result->no_of_confs = n_filtered;
    result->confs = tmp_alloc(n_filtered * sizeof(Conf_z_info));
    no = raw_matches.conf_nos;
    type = raw_matches.type_of_conf;
    res = result->confs;
    for (i = raw_matches.no_of_conf_nos; i > 0; i--)
    {
	if (*no != 0)
	{
	    res->conf_no = *no;
	    res->type = *type;
	    res->name = cached_get_name(*no);
	    res++;
	}
	no++;
	type++;
    }

    if (res != result->confs + n_filtered) 
	restart_kom("Internal error in lookup_z_name\n");

    return OK;
}

static Success
do_lookup (Connection *conn,
           const String    name,
	   Conf_no_list * result,
	   Bool want_persons)
{
    Conf_list_old raw_match;
    unsigned long i;
    unsigned short retsize;
    unsigned int letterflag = want_persons;

    if ( cached_lookup_name( name, &raw_match ) != OK )
	return FAILURE;

    /* Find out how much space we need to allocate */
    retsize = 0;
    for (i = 0; i < raw_match.no_of_conf_nos; i++)
    {
	/* Don't call check access permissions here. It doesn't matter
	   that much if we allocate slightly too much memory. */
	if (raw_match.type_of_conf[i].letter_box == letterflag)
	{
	    retsize++;
	    /* i is "unsigned long", and retsize is "unsigned short".
	       They should probably both be "Conf_no" or
	       "Conf_no_iterator".  This should never be a problem in
	       practice as long as Conf_no is a short. */
	    if (retsize == 0)
	    {
		kom_log("WNG: do_lookup: far too many matches\n");
		--retsize;
	    }
	}
    }

    result->conf_nos = tmp_alloc(sizeof(Conf_no) * retsize);
    result->no_of_confs = 0;

    for (i = 0; i < raw_match.no_of_conf_nos; i++)
    {
	if (raw_match.type_of_conf[i].letter_box == letterflag
	    && has_access(raw_match.conf_nos[i], conn, read_protected))
	{
	    result->conf_nos[result->no_of_confs++] = raw_match.conf_nos[i];
	    if (result->no_of_confs > retsize)
		restart_kom("ERROR: conference.c: do_lookup: error.\n");
	}
    }

    return OK;
}

extern  Success
lookup_person (const String  pattern,
	       Conf_no_list  *result)
{
    CHK_CONNECTION(FAILURE);
    return do_lookup(active_connection, pattern, result, TRUE);
}

extern  Success
lookup_conf (const String  pattern,
	     Conf_no_list  *result)
{
    CHK_CONNECTION(FAILURE);
    return do_lookup(active_connection, pattern, result, FALSE);
}



/*
 * Get status for a conference.
 */

extern Success
get_conf_stat(Conf_no         conf_no,
              Conference    * result)
{
    Aux_item_list    filtered;

    CHK_CONNECTION(FAILURE);
    if (get_conf_stat_old(conf_no, result) == OK)
    {
        filter_aux_item_list(&result->aux_item_list,
                             &filtered,
                             active_connection);
        result->aux_item_list = filtered;
        return OK;
    }

    return FAILURE;
}

extern  Success
get_conf_stat_old (Conf_no		  conf_no,
                   Conference           * result )
{
    Conference * conf_c;
    enum access	 acc;
        
    CHK_CONNECTION(FAILURE);
    GET_C_STAT(conf_c, conf_no, FAILURE);

    acc = access_perm(conf_no, active_connection, read_protected);

    if ( acc == error )
	return FAILURE;

    if ( acc <= none )
    {
        err_stat = conf_no;
	kom_errno = KOM_UNDEF_CONF;
	return FAILURE;
    }

    *result = *conf_c;

    return OK;
}

/*
 * Get small status for a conference
 */

extern Success
get_uconf_stat (Conf_no		conf_no,
	       Small_conf     * result)
{
    enum access		acc;
    Small_conf	      * conf_c;

    CHK_CONNECTION(FAILURE);
    conf_c = cached_get_small_conf_stat(conf_no);
    if (conf_c != NULL)
	acc = access_perm(conf_no, active_connection, read_protected);
    else
	acc = error;

    switch (acc)
    {
    case error:
	return FAILURE;
    case none:
        err_stat = conf_no;
	kom_errno = KOM_UNDEF_CONF;
	return FAILURE;
    default:
	*result = *conf_c;
	return OK;
    }
}

extern  Success
get_conf_stat_older (Conf_no	  conf_no,
                     int	  mask,
                     Conference	* result )
{
    Conference * conf_c;
    enum access	 acc;
        
    CHK_CONNECTION(FAILURE);
    GET_C_STAT(conf_c, conf_no, FAILURE);

    acc = access_perm(conf_no, active_connection, read_protected);

    if ( acc == error )
	return FAILURE;

    if ( acc <= none )
    {
        err_stat = conf_no;
	kom_errno = KOM_UNDEF_CONF;
	return FAILURE;
    }

    *result = *conf_c;

    if ( !(mask & 1) )
	result->name = EMPTY_STRING;	

    return OK;
}


		
/*
 * Set or delete the presentation of a conference. If text_no == 0 there
 * will be no presentation.
 *
 * The text's mark-count will be increased so that it will not be deleted
 * when it gets old.
 */

extern Success
set_presentation (Conf_no	conf_no,
		  Text_no	text_no )
{
    Conference * conf_c;
    enum access	 acc;
    
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    GET_C_STAT(conf_c, conf_no, FAILURE);

    acc = access_perm(conf_no, active_connection, unlimited);
    
    if ( acc < unlimited )
    {
        err_stat = conf_no;
	kom_errno = (acc <= none) ? KOM_UNDEF_CONF : KOM_PERM;
	return FAILURE;
    }

    return do_set_presentation(conf_no, conf_c, text_no);
}

/*
 * Set a message-of-the-day for a conference. There should normally
 * be no motd unless something extraordinary happens. If there is
 * a motd the client should show it as soon as possible.
 *
 * Only the supervisor can change the motd.
 *
 * FIXME (bug 906): There is no asynchronous message that reports new motds.
 */
extern Success
set_etc_motd(	Conf_no		conf_no,
		Text_no		text_no )
{
    Conference * conf_c;
    enum access	 acc;
    
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    GET_C_STAT(conf_c, conf_no, FAILURE);

    if ((acc = access_perm(conf_no, active_connection, unlimited)) < unlimited)
    {
        err_stat = conf_no;
	kom_errno = (acc <= none ) ? KOM_UNDEF_CONF : KOM_PERM;
	BUG(("set_etc_motd failed. Conf %ld Text %ld Acc %ld < %ld (%d).\n",
	     (u_long)conf_no, (u_long)text_no, (u_long)acc,
	     (u_long)unlimited, acc < unlimited));
	return FAILURE;
    }

    return do_set_etc_motd(conf_no, conf_c, text_no);
}

/*
 * Set a new supervisor for a conference. May only be used by
 * the old supervisor.
 *
 * NEW_SUPER is either a person or a conference number. If it is a
 * conference number it means that all the members in NEW_SUPER become
 * supervisors to CONF_NO. (NEW_SUPER should normally be rd_prot to
 * prevent anyone from makeing themselves supervisors).
 */
extern Success
set_supervisor(	Conf_no	 conf_no,
		Conf_no	 new_super )
{
    Conference * conf_c;
    
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    GET_C_STAT(conf_c, conf_no, FAILURE);
    if (new_super != 0)
	CHK_EXIST(new_super, FAILURE);

    if ( !is_strictly_supervisor(conf_no, ACTPERS, ACT_P) &&
         !ENA(wheel, 8) &&      /* OK -- in an RPC call */
         !ENA(admin, 6) )       /* OK -- in an RPC call */
    {
        err_stat = conf_no;
	kom_errno = conf_c->type.secret ? KOM_UNDEF_CONF : KOM_PERM;
	return FAILURE;
    }
    
    conf_c->supervisor = new_super;
    mark_conference_as_changed( conf_no );
	
    return OK;
}

/*
 * Allow certain users to submit texts to CONF_NO.
 *
 * If NEW_PERM_SUB == 0 anyone may submit texts.
 * If it is a person only that person can submit texts to CONF_NO.
 * If it is a conference only the members in that conference can
 * submit texts.
 *
 * If anyone tries to submit a text to a conference he is not allowed
 * to submit texts to the text will silently be redirected to the
 * superconf. If he is not allowed to submit to that conference either
 * it will be redirected again and so forth, but there is a limit
 * (MAX_SUPER_CONF_LOOP) on how many redirections will be done.
 *
 * It is possible to have a secret conference as super_conf. Users
 * will then be able to send texts to it, but they will not see
 * where the text went...
 */
extern Success
set_permitted_submitters (Conf_no  conf_no,
			  Conf_no  new_perm_sub ) 
{
    Conference * conf_c;
    
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    if (new_perm_sub != 0)
	CHK_EXIST(new_perm_sub, FAILURE);
    GET_C_STAT(conf_c, conf_no, FAILURE);

    if (!is_supervisor(conf_no, active_connection)
	&& !ENA(wheel, 8)	/* OK -- In an RPC call */
	&& !ENA(admin, 6))	/* OK -- In an RPC call */
    {
        err_stat = conf_no;
	kom_errno = conf_c->type.secret ? KOM_UNDEF_CONF : KOM_PERM;
	return FAILURE;
    }
	
    conf_c->permitted_submitters = new_perm_sub;
    mark_conference_as_changed( conf_no );
	
    return OK;
}

/*
 * Set the super_conf of CONF_NO. This call may only be used of a
 * supervisor of CONF_NO.
 *
 * See documentation for set_permitted_submitters().
 */
extern Success
set_super_conf (Conf_no  conf_no,
		Conf_no  new_super_conf )
{
    Conference * conf_c;
    
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    GET_C_STAT(conf_c, conf_no, FAILURE);
    if (new_super_conf != 0)
	CHK_EXIST(new_super_conf, FAILURE);

    if (!is_supervisor(conf_no, active_connection)
	&& !ENA(wheel, 8)	/* OK -- In an RPC call */
	&& !ENA(admin, 5))	/* OK -- In an RPC call */
    {
        err_stat = conf_no;
	kom_errno = conf_c->type.secret ? KOM_UNDEF_CONF : KOM_PERM;
	return FAILURE;
    }

    conf_c->super_conf = new_super_conf;
    mark_conference_as_changed( conf_no );
	
    return OK;
}

/*
 * Set the type of a conference. Only the supervisor of a conference can
 * set the conf_type. The letter_box flag can not be changed.
 *
 * FIXME (bug 70): It is allowed to set the type to 'secret' for
 * persons. I don't think we want it to be that way. /ceder
 */
extern Success
set_conf_type (Conf_no    conf_no,
	       Conf_type  type )
{
    Conference      *conf_c;
    Member          *memb;
    enum access      acc;
    unsigned long    i;
        
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    GET_C_STAT(conf_c, conf_no, FAILURE);

    /*
     * Can't be secret and public at the same time?
     */

    if ( type.secret && !type.rd_prot )
    {
        err_stat = 0;
	kom_errno = KOM_SECRET_PUBLIC;
	return FAILURE;
    }

    acc = access_perm(conf_no, active_connection, unlimited);
    
    /*
     * Can't change type without privs
     */

    if ( acc < unlimited )
    {
        err_stat = conf_no;
	kom_errno = ( acc <= none ) ? KOM_UNDEF_CONF : KOM_PERM ;
	return FAILURE;
    }
    
    /*
     * Can't change the letterbox flag.
     */

    if ( type.letter_box != conf_c->type.letter_box )
    {
        err_stat = conf_no;
	kom_errno = KOM_LETTER_BOX;
	return FAILURE;
    }

    /*
     * Can't change to secret if there are secret memberships
     * Only check this if we are changing the forbid_secret
     * flag to on from off
     */

    if (type.forbid_secret && !conf_c->type.forbid_secret)
    {
        for (i = 0; i < conf_c->members.no_of_members; i++)
        {
            memb = &conf_c->members.members[i];
            if (memb->type.secret)
            {
                err_stat = memb->member;
                kom_errno = KOM_INVALID_MEMBERSHIP_TYPE;
                return FAILURE;
            }
        }
    }

    conf_c->type = type;
    mark_conference_as_changed (conf_no);

    return OK;
}


/*
 * Set garb_nice for a conference. This controls how long messages
 * to a conference will live. The argument is probably the number of
 * days a message will live. Only the supervisor of the conference
 * may change the garb_nice.
 */
extern Success
set_garb_nice(	Conf_no		conf_no,
		Garb_nice	nice	)	/* number of days */
{
    Conference * conf_c;
    enum access	 acc;
        
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    GET_C_STAT(conf_c, conf_no, FAILURE);

    acc = access_perm(conf_no, active_connection, unlimited);
    
    if ( acc < unlimited )
    {
        err_stat = conf_no;
	kom_errno = (acc <= none) ? KOM_UNDEF_CONF : KOM_PERM;
	return FAILURE;
    }

    conf_c->nice = nice;
    mark_conference_as_changed( conf_no );

    return OK;
}

/*
 * Set expire for a conference. This controls nothing at the moment, but
 * will probably control when a conference is killed automatically by
 * the server in cases of extreme inactivity.
 */
extern Success
set_expire(	Conf_no		conf_no,
		Garb_nice	expire	)	/* number of days */
{
    Conference * conf_c;
    enum access	 acc;
        
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    GET_C_STAT(conf_c, conf_no, FAILURE);

    acc = access_perm(conf_no, active_connection, unlimited);
    
    if ( acc < unlimited )
    {
        err_stat = conf_no;
	kom_errno = (acc <= none) ? KOM_UNDEF_CONF : KOM_PERM;
	return FAILURE;
    }

    conf_c->expire = expire;
    mark_conference_as_changed( conf_no );

    return OK;
}

/*
 * Set keep_commented for a conference.
 */
extern Success
set_keep_commented(Conf_no	conf_no,
                   Garb_nice	keep_commented)	/* number of days */
{
    Conference * conf_c;
    enum access	 acc;
        
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    GET_C_STAT(conf_c, conf_no, FAILURE);

    acc = access_perm(conf_no, active_connection, unlimited);
    
    if ( acc < unlimited )
    {
        err_stat = conf_no;
	kom_errno = (acc <= none) ? KOM_UNDEF_CONF : KOM_PERM;
	return FAILURE;
    }

    conf_c->keep_commented = keep_commented;
    mark_conference_as_changed( conf_no );

    return OK;
}

static void
send_async_new_presentation(Conf_no     conf_no,
			    Text_no     old_tno,
			    Text_stat  *old_stat,
			    Text_no     new_tno,
			    Text_stat  *new_stat)
{
    Session_no i = 0;
    Connection *cptr;
    Text_no used_old;
    Text_no used_new;

    if (!param.send_async_messages)
	return;
     
    while ((i = traverse_connections(i)) != 0)
    {
	cptr = get_conn_by_number(i);
     
	/* Check the want_async here to avoid the potentially
	    expensive calls to access_perm. */
	if (cptr->want_async[ay_new_presentation] == FALSE)
	    continue;

	if (!has_access(conf_no, cptr, read_protected))
	    continue;

	used_old = text_read_access(cptr, old_tno, old_stat) ? old_tno : 0;
	used_new = text_read_access(cptr, new_tno, new_stat) ? new_tno : 0;

	if (used_old == used_new)
	    continue;

	async_new_presentation(cptr, conf_no, used_old, used_new);
    }
}


/*
 * Change presentation of a conference. If text_no is 0, there will be
 * no presentation.
 */

Success
do_set_presentation(Conf_no 	 conf_no,
		    Conference * conf_c,
		    Text_no	 new_text_no)
{
    Text_stat * old_stat = NULL;
    Text_stat * new_stat = NULL;
    Text_no     old_text_no = 0;

    /* Check that the new presentation exists before deleting the old*/

    if (new_text_no != 0)
    {
	GET_T_STAT(new_stat, new_text_no, FAILURE);
	if (new_stat->no_of_marks >= param.max_marks_text)
	{
	    kom_log("LIMIT: do_set_presentation(%d, ptr, %lu): "
		    "New presentation has %d marks.\n",
		    conf_no,
		    (unsigned long)new_text_no, new_stat->no_of_marks);
            err_stat = new_text_no;
	    kom_errno = KOM_MARK_LIMIT;
	    return FAILURE;
	}
    }
    
    /* Unmark the previous presentation if it exists. */

    old_text_no = conf_c->presentation;
    if (old_text_no != 0)
    {
	if ((old_stat = cached_get_text_stat(old_text_no)) == NULL)
	{
	    /* FIXME (bug 85): This is not currently an error, but it
	       will be once bug 85 is fixed.  */
	    kom_log("do_set_presentation(): Old presentation %lu of conf %lu"
		    " does not exist.\n",
		    (unsigned long)old_text_no, (unsigned long)conf_no);
	    old_text_no = 0;
	}
	else if (old_stat->no_of_marks > 0)
	{
	    --old_stat->no_of_marks;
	    mark_text_as_changed(old_text_no);
	}
	else
	{
	    kom_log("ERROR: do_set_presentation(%d, ptr, %lu): "
		    "Old presentation %lu not marked.\n",
		    conf_no, 
		    (unsigned long)new_text_no,
		    (unsigned long)old_text_no);
	}
    }

    /* Mark the new presentation */

    if (new_text_no != 0)
    {
	++new_stat->no_of_marks;
	mark_text_as_changed(new_text_no);
    }
    
    conf_c->presentation = new_text_no;
    mark_conference_as_changed(conf_no);

    send_async_new_presentation(conf_no,
				old_text_no, old_stat,
				new_text_no, new_stat);
    return OK;
}


static void
send_async_new_motd(Conf_no     conf_no,
		    Text_no     old_tno,
		    Text_stat  *old_stat,
		    Text_no     new_tno,
		    Text_stat  *new_stat)
{
    Session_no i = 0;
    Connection *cptr;
    Text_no used_old;
    Text_no used_new;

    if (!param.send_async_messages)
	return;
     
    while ((i = traverse_connections(i)) != 0)
    {
	cptr = get_conn_by_number(i);
     
	/* Check the want_async here to avoid the potentially
	    expensive calls to access_perm. */
	if (cptr->want_async[ay_new_motd] == FALSE)
	    continue;

	if (!has_access(conf_no, cptr, read_protected))
	    continue;

	used_old = text_read_access(cptr, old_tno, old_stat) ? old_tno : 0;
	used_new = text_read_access(cptr, new_tno, new_stat) ? new_tno : 0;

	if (used_old == used_new)
	    continue;

	async_new_motd(cptr, conf_no, used_old, used_new);
    }
}

/*
 * Change motd of a conference. If text_no is 0, there will be
 * no motd.
 */

Success
do_set_etc_motd(Conf_no      conf_no,
		Conference * conf_c,
		Text_no      new_text_no)
{
    Text_stat * old_stat = NULL;
    Text_stat * new_stat = NULL;
    Text_no     old_text_no = 0;

    /* Check that the new motd exists before deleting the old*/

    if (new_text_no != 0)
    {
	GET_T_STAT(new_stat, new_text_no, FAILURE);
	if (new_stat->no_of_marks >= param.max_marks_text)
	{
	    kom_log("LIMIT: do_set_etc_motd(%d, ptr, %lu): "
		    "New motd has %d marks.\n",
		    conf_no,
		    (unsigned long)new_text_no, new_stat->no_of_marks);
            err_stat = new_text_no;
	    kom_errno = KOM_MARK_LIMIT;
	    return FAILURE;
	}
    }
    
    /* Unmark the previous motd if it exists. */

    old_text_no = conf_c->msg_of_day;
    if (old_text_no != 0)
    {
	if ((old_stat = cached_get_text_stat(old_text_no)) == NULL)
	{
	    /* FIXME (bug 86): This is not currently an error, but it
	       will be once bug 86 is fixed.  */
	    kom_log("do_set_etc_motd(): Old motd %lu of conf %lu"
		    " does not exist.\n",
		    (unsigned long)old_text_no, (unsigned long)conf_no);
	    old_text_no = 0;
	}
	else if (old_stat->no_of_marks > 0)
	{
	    --old_stat->no_of_marks;
	    mark_text_as_changed(old_text_no);
	}
	else
	{
	    kom_log("ERROR: do_set_etc_motd(%d, ptr, %lu): "
		    "Old motd %lu not marked.\n",
		    conf_no, 
		    (unsigned long)new_text_no,
		    (unsigned long)old_text_no);
	}
    }

    /* Mark the new motd */

    if (new_text_no != 0)
    {
	++new_stat->no_of_marks;
	mark_text_as_changed(new_text_no);
    }
    
    conf_c->msg_of_day = new_text_no;
    mark_conference_as_changed(conf_no);

    send_async_new_motd(conf_no,
			old_text_no, old_stat,
			new_text_no, new_stat);
    return OK;
}


extern Success
modify_conf_info(Conf_no        conf_no,
                 Number_list   *items_to_delete, 
                 Aux_item_list *aux)
{
    Conference     *conf;

    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    
    if (items_to_delete->length > param.max_delete_aux)
    {
        kom_errno = KOM_LONG_ARRAY;
        err_stat = param.max_delete_aux;
        return FAILURE;
    }

    if (aux->length > param.max_add_aux)
    {
        kom_errno = KOM_LONG_ARRAY;
        err_stat = param.max_add_aux;
        return FAILURE;
    }

    GET_C_STAT(conf, conf_no, FAILURE);
    if (!has_access(conf_no, active_connection, read_protected))
    {
        err_stat  = conf_no;
        kom_errno = KOM_UNDEF_CONF;
        return FAILURE;
    }

    /* Check if we may delete and add the items */

    prepare_aux_item_list(aux, ACTPERS);

    if (check_delete_aux_item_list(items_to_delete,
                                   &conf->aux_item_list,
                                   conf_no)!=OK)
        return FAILURE;
    delete_aux_item_list(items_to_delete, &conf->aux_item_list,
                         CONF_OBJECT_TYPE,
                         conf_no, conf);

    if (conf_stat_check_add_aux_item_list(conf, conf_no, aux,
					  active_connection, FALSE) != OK)
    {
        undelete_aux_item_list(items_to_delete, &conf->aux_item_list,
                               CONF_OBJECT_TYPE,
                               conf_no, conf);
        return FAILURE;
    }

    /* Then add the items */
    
    conf_stat_add_aux_item_list(conf, conf_no, aux, ACTPERS);
    commit_aux_item_list(&conf->aux_item_list);

    /* FIXME (bug 907): async_conf_changed(conf_no, conf);  */
    mark_conference_as_changed(conf_no);

    return OK;
}


Success
first_unused_conf_no(Conf_no *result)
{
    CHK_CONNECTION(FAILURE);

    *result = query_next_conf_no();
    return OK;
}

Success
find_next_conf_no(Conf_no start,
		  Conf_no *result)
{
    Conf_no    highest = query_next_conf_no();

    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    err_stat = start;
    while (++start < highest)
	if (has_access(start, active_connection, read_protected))
	{
	    *result = start;
	    return OK;
	}

    kom_errno = KOM_UNDEF_CONF;
    return FAILURE;
}


extern  Success
find_previous_conf_no(Conf_no start,
		      Conf_no *result)
{
    Conf_no next_cno;
    const Conf_no saved_start = start;
    
    CHK_CONNECTION(FAILURE);
    CHK_LOGIN(FAILURE);
    
    if (start > (next_cno = query_next_conf_no()))
	start = next_cno;

    while (start-- > 0)
	if (has_access(start, active_connection, read_protected))
	{
	    *result = start;
	    return OK;
	}

    kom_errno = KOM_UNDEF_CONF;
    err_stat = saved_start;
    return FAILURE;
}

