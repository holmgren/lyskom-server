/*
 * $Id: prot-a-parse.c,v 0.63 2003/08/23 16:38:15 ceder Exp $
 * Copyright (C) 1991-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * prot-a-parse.c - parse protocol-A messages.
 *
 * FIXME (bug 342): Not all functions are used, I think. /ceder
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <assert.h>
#include <stdio.h>
#include <setjmp.h>
#include "timewrap.h"
#include <sys/types.h>
#include <sys/socket.h>
#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#endif
#include <string.h>

#include "oop.h"

#include "debug.h"
#include "s-string.h"
#include "kom-types.h"
#include "server/smalloc.h"
#include "com.h"
#include "async.h"
#include "connections.h"
#include "prot-a-parse.h"
#include "isc-parse.h"
#include "kom-config.h"
#include "isc-interface.h"
#include "log.h"
#include "minmax.h"
#include "param.h"
#include "kom-memory.h"

BUGDECL;

/* Forward declarations */

static void prot_a_hunt_array_end(Connection *client);

/*
 * Return next token from the input stream. Note that the String returned
 * by this call points into data that might be freed by the next call to
 * get_token or any function which reads from the stream.
 */

static String
prot_a_get_token(Connection *client)
{
    String result;
    String_size old_first;

    old_first = client->first_to_parse;
    
    result = s_strtok(client->unparsed, &client->first_to_parse,
		      s_fcrea_str(WHITESPACE));

    /* Check that there was at least one trailing blank. */
    
    if ( client->first_to_parse >= s_strlen(client->unparsed) )
    {
        if (client->first_to_parse - old_first > 1000)
	{
	    isc_puts("%%Insane token length.\n", client->isc_session);
	    isc_flush(client->isc_session);
	    longjmp(parse_env, KOM_LOGOUT);
	}
	client->first_to_parse = old_first;
	longjmp(parse_env, KOM_MSG_INCOMPLETE);
    }

    return result;
}


long
prot_a_parse_long(Connection *client)
{
    String	token;
    String_size end;
    long	res;

    token = prot_a_get_token(client);
    res = s_strtol(token, &end);
    if (end != s_strlen(token))
	longjmp(parse_env, KOM_PROTOCOL_ERR);
    
    return res;
}


void
prot_a_parse_num_list(Connection *client,
                      Number_list *res,
		      int maxlen)
{
    static unsigned long err_cnt = 0;

    switch (client->array_parse_pos)
    {
    case 0:			/* The array size. */

	/* This could just as well have been an assertion. */
	if ((res->length != 0 || res->data != NULL) && err_cnt++ < 20)
	{
	    kom_log("WNG: prot_a_parse_num_list(): len = %lu data = %lu\n",
		(unsigned long)res->length, (unsigned long)res->data);
	    res->length = 0;
	    res->data = NULL;
	    if (err_cnt == 20)
		kom_log("The above warning is now turned off.\n");
	}

        client->array_parse_parsed_length = prot_a_parse_long(client);
	if (client->array_parse_parsed_length < 0)
	{
	    isc_puts("%%Insane array size.\n", client->isc_session);
	    isc_flush(client->isc_session);
	    longjmp(parse_env, KOM_LOGOUT);
	}

	client->array_parse_pos = 1;
	/* Fall through */
    case 1:			/* The opening curly brace. */
	if ( parse_nonwhite_char(client) != '{' )
	    longjmp(parse_env, KOM_PROTOCOL_ERR);

	res->length = min(maxlen+1, client->array_parse_parsed_length);
        res->data   = smalloc(sizeof(*res->data) * res->length);

	client->array_parse_index = 0;
        client->array_parse_pos = 2;
	/* Fall through */
    case 2:			/* The elements in the array. */
	while (client->array_parse_index < client->array_parse_parsed_length)
	{
	    long tmp = prot_a_parse_long(client);

            /* Enter the value into the array only if the
               parse index is inside the allocated array */

            if (client->array_parse_index < res->length)
            {
                res->data[client->array_parse_index] = tmp;
            }
            client->array_parse_index += 1;
	}
	client->array_parse_pos = 3;
	/* Fall through */
    case 3:			/* Closing brace. */
        /* Adjust the result length if we were parsing a long array */

        /* Read the closing brace */
	if ( parse_nonwhite_char(client) != '}' )
	    longjmp(parse_env, KOM_PROTOCOL_ERR);
    default:
	client->array_parse_pos = 0;
    }
}

void
prot_a_parse_priv_bits(Connection *client,
		       Priv_bits      *res)
{
    String token;
    String_size len;

    token = prot_a_get_token(client);
    len = s_strlen(token);
    if (len <= 0 )
	longjmp(parse_env, KOM_PROTOCOL_ERR);
    
    init_priv_bits(res);
    switch (len)
    {
    default:
    case 16: res->flg16 =       token.string[ 15 ] != '0';
    case 15: res->flg15 =       token.string[ 14 ] != '0';
    case 14: res->flg14 =       token.string[ 13 ] != '0';
    case 13: res->flg13 =       token.string[ 12 ] != '0';
    case 12: res->flg12 =       token.string[ 11 ] != '0';
    case 11: res->flg11 =       token.string[ 10 ] != '0';
    case 10: res->flg10 =       token.string[ 9 ]  != '0';
    case 9:  res->flg9 =        token.string[ 8 ]  != '0';
    case 8:  res->flg8 =        token.string[ 7 ]  != '0';
    case 7:  res->flg7 =        token.string[ 6 ]  != '0';
    case 6:  res->change_name = token.string[ 5 ]  != '0';
    case 5:  res->create_conf = token.string[ 4 ]  != '0';
    case 4:  res->create_pers = token.string[ 3 ]  != '0';
    case 3:  res->statistic =   token.string[ 2 ]  != '0';
    case 2:  res->admin =       token.string[ 1 ]  != '0';
    case 1:  res->wheel =       token.string[ 0 ]  != '0';
    }
}

void
prot_a_parse_pers_flags(Connection *client,
                        Personal_flags *res)
{
    String          token;
    String_size     len;

    token = prot_a_get_token(client);
    len = s_strlen(token);
    if (len <= 0)
        longjmp(parse_env, KOM_PROTOCOL_ERR);

    init_personal_flags(res);
    switch (len)
    {
    default:
    case 8: res->flg8               = token.string[ 7 ] != '0';
    case 7: res->flg7               = token.string[ 6 ] != '0';
    case 6: res->flg6               = token.string[ 5 ] != '0';
    case 5: res->flg5               = token.string[ 4 ] != '0';
    case 4: res->flg4               = token.string[ 3 ] != '0';
    case 3: res->flg3               = token.string[ 2 ] != '0';
    case 2: res->flg2               = token.string[ 1 ] != '0';
    case 1: res->unread_is_secret   = token.string[ 0 ] != '0';
    }
}

void
prot_a_parse_membership_type(Connection *client,
                             Membership_type *res)
{
    String token;
    String_size len;

    token = prot_a_get_token(client);
    len = s_strlen(token);
    if (len <= 0)
	longjmp(parse_env, KOM_PROTOCOL_ERR);
  
    init_membership_type(res);
    switch (len)
    {
    default:
    case 8: res->reserved5  = token.string[ 7 ] != '0';
    case 7: res->reserved4  = token.string[ 6 ] != '0';
    case 6: res->reserved3  = token.string[ 5 ] != '0';
    case 5: res->reserved2  = token.string[ 4 ] != '0';
    case 4: res->passive_message_invert = token.string[ 3 ] != '0';
    case 3: res->secret     = token.string[ 2 ] != '0';
    case 2: res->passive    = token.string[ 1 ] != '0';
    case 1: res->invitation = token.string[ 0 ] != '0';
    }
}

void
prot_a_parse_conf_type(Connection *client,
		Conf_type      *res)
{
    String token;
    String_size len;

    token = prot_a_get_token(client);
    len = s_strlen(token);
    if (len <= 0)
	longjmp(parse_env, KOM_PROTOCOL_ERR);
  
    init_conf_type(res);
    switch (len)
    {
    default:
    case 8: res->reserved3      = token.string[ 7 ] != '0';
    case 7: res->reserved2      = token.string[ 6 ] != '0';
    case 6: res->forbid_secret  = token.string[ 5 ] != '0';
    case 5: res->allow_anon     = token.string[ 4 ] != '0';
    case 4: res->letter_box     = token.string[ 3 ] != '0';
    case 3: res->secret         = token.string[ 2 ] != '0';
    case 2: res->original       = token.string[ 1 ] != '0';
    case 1: res->rd_prot        = token.string[ 0 ] != '0';
    }
}

/*
 * Parse a string. At most 'maxlen' characters are allowed. If the
 * client sends a longer string only the first 'maxlen+1' characters
 * are read. Any remaining characters are discarded.
 */
void
prot_a_parse_string(Connection  *client,
		    String	*result,
		    int		 maxlen)
{
    String_size hptr;		/* Pointer to 'H' */
    String_size client_len;	/* The len the client is sending. */
    String_size truncated_len;	/* How much the server will receive. */
    String_size to_skip;
    static unsigned long err_cnt = 0;

    switch ( client->string_parse_pos )
    {
    case 0:
	if ( (result->len != 0 || result->string != NULL) && err_cnt++ < 20 )
	{
	    kom_log ("%s == %lu, result->string == %lu. %s\n",
		 "prot_a_parse_string(): result->len",
		 (unsigned long)result->len, (unsigned long)result->string,
		 "This memory will not be free()'d.");
	    *result = EMPTY_STRING;
	    if ( err_cnt == 20 )
		kom_log("Won't log the above warning no more.\n");
	}

	/* Get number and discard trailing 'H' */
	client_len = s_strtol(s_fsubstr(client->unparsed,
					client->first_to_parse,
					END_OF_STRING),
			      &hptr);

	if ( hptr == -1
	    || client->first_to_parse + hptr
	    >= s_strlen(client->unparsed) )
	{
	    longjmp(parse_env, KOM_MSG_INCOMPLETE);
	}
	
	if (client_len < 0)
	{
	    isc_puts("%%Insane string length.\n", client->isc_session);
	    isc_flush(client->isc_session);
	    BUG(("%%%%Insane string length.\n"));
	    longjmp(parse_env, KOM_LOGOUT);
	}
	
	/* Check that
	      a) there is a trailing H
	      b) there was at least one digit before the H */

	if ( client->unparsed.string[ client->first_to_parse
					  + hptr ] != 'H'
	    || hptr <= 0 )
	{
	    longjmp(parse_env, KOM_PROTOCOL_ERR);
	}

	client->first_to_parse += 1 + hptr;
	client->string_parse_pos = 1;
	/* Transfer the total length across the restart point, by
	   storing it in result->len.  */
	/* FIXME (bug 162): It would be better to add a client_len
	   field to the Connection struct.  Adding four bytes to that
	   struct won't measurably change the size of the lyskomd
	   process, and it will make this code easier to maintain.
	   This is also dangerous, since it means that result->len > 0
	   while result->string == NULL, and that might confuse some
	   code somewhere. */
	result->len = client_len;
	/* Fall through */
    case 1:
	client_len = result->len;
	/* Check that the entire string is transmitted. */
	/* (Don't care about the trailing part that will be skipped if the
	 *  string is longer than maxlen) */
	truncated_len = min(maxlen + 1, client_len);
	
	if ( client->first_to_parse + truncated_len
	    > s_strlen(client->unparsed) )
	{
	    longjmp(parse_env, KOM_MSG_INCOMPLETE);
	}

	*result = EMPTY_STRING;

	s_mem_crea_str(result,
		       client->unparsed.string + client->first_to_parse,
		       truncated_len);

	/* Transfer the length across the restart point.  This is
	   slightly ugly: if the string is truncated, we are not
	   allowed to access all of the string!  */
	/* FIXME (bug 162): It would be better to add a client_len
	   field to the Connection struct.  Adding four bytes to that
	   struct won't measurably change the size of the lyskomd
	   process, and it will make this code easier to maintain.  */
	result->len = client_len;

	client->first_to_parse += truncated_len;
	client->string_parse_pos = 2;
	/* Fall through */
    case 2:
	/* Was the string too long? If so, skip the truncated data. */
        /* It looks like we're leaving one extra character in the
           string here, which is necessary if we want clients of
           this function to be able to detect long strings. */

	client_len = result->len;
	truncated_len = min(maxlen+1, client_len);
	
	if ( client_len > truncated_len )
	{
	    to_skip = min(client_len - truncated_len,
			   client->unparsed.len - client->first_to_parse);
	    client_len -= to_skip;
	    client->first_to_parse += to_skip;
	}

	result->len = client_len;
	
	if ( client_len > truncated_len )
	    longjmp(parse_env, KOM_MSG_INCOMPLETE);
	/* Fall through */
    default:
	client->string_parse_pos = 0;
    }
}

extern void
prot_a_parse_aux_item_flags(Connection *client,
                            Aux_item_flags *res)
{
    String token;
    String_size len;

    token = prot_a_get_token(client);
    len = s_strlen(token);
    if (len <= 0)
	longjmp(parse_env, KOM_PROTOCOL_ERR);
    
    init_aux_item_flags(res);
    switch (len)
    {
    default:
    case 8: res->reserved5      = token.string[ 7 ] != '0';
    case 7: res->reserved4      = token.string[ 6 ] != '0';
    case 6: res->reserved3      = token.string[ 5 ] != '0';
    case 5: res->dont_garb      = token.string[ 4 ] != '0';
    case 4: res->hide_creator   = token.string[ 3 ] != '0';
    case 3: res->secret         = token.string[ 2 ] != '0';
    case 2: res->inherit        = token.string[ 1 ] != '0';
    case 1: res->deleted        = token.string[ 0 ] != '0';
    }
}


extern void
prot_a_parse_aux_item(Connection *client,
                      Aux_item *result)
{
    switch ( client->struct_parse_pos )
    {
    case 0:
        init_aux_item(result);
        result->tag = prot_a_parse_long(client);
        client->struct_parse_pos = 1;
    case 1:
        prot_a_parse_aux_item_flags(client, &result->flags);
        client->struct_parse_pos = 2;
    case 2:
        result->inherit_limit = prot_a_parse_long(client);
        client->struct_parse_pos = 3;
    case 3:
        prot_a_parse_string(client,
                            &(result->data),
                            param.aux_len);
    default:
        client->struct_parse_pos = 0;
    }
}

extern void
prot_a_parse_aux_item_list(Connection *client,
                           Aux_item_list *result,
                           int maxlen)
{
    static unsigned long err_cnt = 0;
    int i;

    switch (client->array_parse_pos)
    {
    case 0:
	if ((result->length != 0 || result->items != NULL) && err_cnt++ < 20)
	{
	    kom_log("WNG: prot_a_parse_aux_item_list(): len = %lu data = %lu\n",
		(unsigned long)result->length, (unsigned long)result->items);
	    result->length = 0;
	    result->items = NULL;
	    if (err_cnt == 20)
		kom_log("The above warning is now turned off.\n");
	}

	client->array_parse_parsed_length = prot_a_parse_long(client);
        if (client->array_parse_parsed_length < 0)
        {
	    isc_puts("%%Insane array size.\n", client->isc_session);
	    isc_flush(client->isc_session);
	    longjmp(parse_env, KOM_LOGOUT);
        }

	client->array_parse_pos = 1;
        /* Fall through */

    case 1:
	if ( parse_nonwhite_char(client) != '{' )
	    longjmp(parse_env, KOM_PROTOCOL_ERR);

        result->length = min(maxlen+1, client->array_parse_parsed_length);
        result->items = smalloc(result->length * sizeof(Aux_item));
        
        for (i = 0; i < result->length; i++)
        {
            result->items[i].data = EMPTY_STRING;
        }
	client->array_parse_index = 0;
	client->array_parse_pos = 2;
    case 2:
	while( client->array_parse_index < client->array_parse_parsed_length)
	{
            /* Enter a parsed aux item into the list only if we
               have parsed less than the length of the list */
            if (client->array_parse_index < result->length)
            {
                prot_a_parse_aux_item(client,
                                      &result->items[
                                          client->array_parse_index]);
            }
            else
            {
                prot_a_parse_aux_item(client, &client->dummy_aux_item);
                clear_aux_item(&client->dummy_aux_item);
            }
	    client->array_parse_index += 1;
	}
	client->array_parse_pos = 3;
    case 3:
        /* Adjust the length of the result to the length allocated */

        /* Read the closing brace */
	if ( parse_nonwhite_char(client) != '}' )
	    longjmp(parse_env, KOM_PROTOCOL_ERR);
    default:
	client->array_parse_pos = 0;
    }
}
                               
extern void
prot_a_parse_misc_info_list(Connection *client,
                            Misc_info_list  *result,
                            int maxlen)
{
    static unsigned long err_cnt = 0;
    int i;
    Misc_info dummy_misc_info;

    switch (client->array_parse_pos)
    {
    case 0:
	if ((result->no_of_misc != 0 || result->misc != NULL) && err_cnt++ < 20)
	{
	    kom_log("WNG: prot_a_parse_misc_info_list():"
		    " len = %lu data = %lu\n",
		    (unsigned long)result->no_of_misc,
		    (unsigned long)result->misc);
	    result->no_of_misc = 0;
	    result->misc = NULL;
	    if (err_cnt == 20)
		kom_log("The above warning is now turned off.\n");
	}

	client->array_parse_parsed_length = prot_a_parse_long(client);
        if (client->array_parse_parsed_length < 0)
        {
	    isc_puts("%%Insane array size.\n", client->isc_session);
	    isc_flush(client->isc_session);
	    longjmp(parse_env, KOM_LOGOUT);
        }

	client->array_parse_pos = 1;
        /* Fall through */

    case 1:
	if ( parse_nonwhite_char(client) != '{' )
	    longjmp(parse_env, KOM_PROTOCOL_ERR);

        result->no_of_misc = min(maxlen+1, client->array_parse_parsed_length);
        result->misc = smalloc(result->no_of_misc * sizeof(Misc_info));
        
        for (i = 0; i < result->no_of_misc; i++)
        {
            result->misc[i].type = unknown_info;
        }
	client->array_parse_index = 0;
	client->array_parse_pos = 2;
    case 2:
	while(client->array_parse_index < client->array_parse_parsed_length)
	{
            /* Enter a parsed aux item into the list only if we
               have parsed less than the no_of_misc of the list */
            if (client->array_parse_index < result->no_of_misc)
            {
                prot_a_parse_misc_info(client,
                                      &result->misc[
                                          client->array_parse_index]);
                
                /* If the most recently parsed item is an unknown, the
                   misc-info parser will have skipped to the end of the
                   array. Make allowances for this. */
                if (result->misc[client->array_parse_index].type == unknown_info)
                {
                    result->no_of_misc = client->array_parse_index + 1;
                }
            }
            else
            {
                prot_a_parse_misc_info(client, &dummy_misc_info);
            }
	    client->array_parse_index += 1;
	}
	client->array_parse_pos = 3;
    case 3:
        /* Adjust the no_of_misc of the result to the length allocated */

        /* Read the closing brace */
	if ( parse_nonwhite_char(client) != '}' )
	    longjmp(parse_env, KOM_PROTOCOL_ERR);
    default:
	client->array_parse_pos = 0;
    }
}


extern void
prot_a_parse_misc_info(Connection *client,
		       Misc_info      *result)
{
    struct tm tmp_time;

    switch ( client->struct_parse_pos )
    {
    case 0:
	result->type = prot_a_parse_long(client);
	client->struct_parse_pos = 1;
	/* Fall through */
    case 1:
	switch( result->type )
	{
	case recpt:
	case cc_recpt:
	case bcc_recpt:
	    result->datum.recipient = prot_a_parse_long(client);
	    break;

	case loc_no:
	    result->datum.local_no = prot_a_parse_long(client);
	    break;
	    
	case comm_to:
	case footn_to:
	case comm_in:
	case footn_in:
	    result->datum.text_link = prot_a_parse_long(client);
	    break;
	    
	case rec_time:
            prot_a_parse_time_date(client, &tmp_time);

	    /* This value is never used.  It is illegal to send a
	       rec_time to the server.  To avoid portability problems
	       with timegm() we simply store a 0, instead of switching
	       between using timegm or mktime depending on the use_utc
	       setting.  We used to store "mktime(&tmp_time)". */

            result->datum.received_at = 0;
            break;
            
	case sent_by:
            result->datum.sender = prot_a_parse_long(client);
            break;
            
	case sent_at:
            prot_a_parse_time_date(client, &tmp_time);

	    /* This value is never used.  It is illegal to send a
	       sent_at to the server.  To avoid portability problems
	       with timegm() we simply store a 0, instead of switching
	       between using timegm or mktime depending on the use_utc
	       setting.  We used to store "mktime(&tmp_time)". */

            result->datum.sent_at = 0;
            break;
            
	default:
            result->datum.unknown_type = result->type;
            result->type = unknown_info;
            client->array_hunt_depth = 1;
            prot_a_hunt_array_end(client);
	}
    default:
	client->struct_parse_pos = 0;
    }
}

static void
prot_a_parse_read_range(Connection *client,
			struct read_range *result)
{
    switch (client->struct_parse_pos)
    {
    case 0:
	result->first_read = prot_a_parse_long(client);
	client->struct_parse_pos = 1;
	/* Fall through */
    case 1:
	result->last_read = prot_a_parse_long(client);
	/* Fall through */
    default:
	client->struct_parse_pos = 0;
    }
}

void
prot_a_parse_read_range_list(Connection *client,
			     struct read_range_list *res,
			     int maxlen)
{
    static unsigned long err_cnt = 0;
    struct read_range dummy_read_range;

    switch (client->array_parse_pos)
    {
    case 0:
	if ((res->length != 0 || res->ranges != NULL) && err_cnt < 20)
	{
	    kom_log("WNG: prot_a_parse_read_range_list():"
		    " len = %lu data = %lu\n",
		    (unsigned long)res->length,
		    (unsigned long)res->ranges);
	    res->length = 0;
	    res->ranges = NULL;
	    if (++err_cnt == 20)
		kom_log("The above warning is now turned off.\n");
	}

	client->array_parse_parsed_length = prot_a_parse_long(client);
        if (client->array_parse_parsed_length < 0)
        {
	    isc_puts("%%Insane array size.\n", client->isc_session);
	    isc_flush(client->isc_session);
	    longjmp(parse_env, KOM_LOGOUT);
        }

	client->array_parse_pos = 1;
        /* Fall through */

    case 1:
	if (parse_nonwhite_char(client) != '{')
	    longjmp(parse_env, KOM_PROTOCOL_ERR);

        res->length = min(maxlen+1, client->array_parse_parsed_length);
        res->ranges = smalloc(res->length * sizeof(struct read_range));

	client->array_parse_index = 0;
	client->array_parse_pos = 2;
    case 2:
	while(client->array_parse_index < client->array_parse_parsed_length)
	{
            /* Enter a parsed range into the list only if we
               have parsed less than the no_of_misc of the list. */
            if (client->array_parse_index < res->length)
                prot_a_parse_read_range(
		    client, &res->ranges[client->array_parse_index]);
            else
                prot_a_parse_read_range(client, &dummy_read_range);
	    client->array_parse_index += 1;
	}
	client->array_parse_pos = 3;
    case 3:
        /* Read the closing brace */
	if ( parse_nonwhite_char(client) != '}' )
	    longjmp(parse_env, KOM_PROTOCOL_ERR);
    default:
	client->array_parse_pos = 0;
    }
}


void
prot_a_parse_time_date(Connection *client,
		       struct tm  *result)
{
    /* A time never occur inside a string, and a string never occur
       inside a time. Thus I use string_parse_pos here instead of
       inventing a new variable.  */

    switch( client->string_parse_pos )
    {
    case 0:
	result->tm_sec = prot_a_parse_long(client);
	client->string_parse_pos = 1;
	/* Fall through */
    case 1:
	result->tm_min = prot_a_parse_long(client);
	client->string_parse_pos = 2;
	/* Fall through */
    case 2:
	result->tm_hour = prot_a_parse_long(client);
	client->string_parse_pos = 3;
	/* Fall through */
    case 3:
	result->tm_mday = prot_a_parse_long(client);
	client->string_parse_pos = 4;
	/* Fall through */
    case 4:
	result->tm_mon = prot_a_parse_long(client);
	client->string_parse_pos = 5;
	/* Fall through */
    case 5:
	result->tm_year = prot_a_parse_long(client);
	client->string_parse_pos = 6;
	/* Fall through */
    case 6:
	result->tm_wday = prot_a_parse_long(client);
	client->string_parse_pos = 7;
	/* Fall through */
    case 7:
	result->tm_yday = prot_a_parse_long(client);
	client->string_parse_pos = 8;
	/* Fall through */
    case 8:
	result->tm_isdst = prot_a_parse_long(client);
	/* Fall through */
    default:
	client->string_parse_pos = 0;
    }
}



void prot_a_parse_info(Connection *client,
                       Info *result)
{
    switch( client->struct_parse_pos )
    {
    case 0:
        result->version = prot_a_parse_long(client);
        client->struct_parse_pos = 1;
        /* Fall through */
    case 1:
        result->conf_pres_conf = prot_a_parse_long(client);
        client->struct_parse_pos = 2;
        /* Fall through */
    case 2:
        result->pers_pres_conf = prot_a_parse_long(client);
        client->struct_parse_pos = 3;
        /* Fall through */
    case 3:
        result->motd_conf = prot_a_parse_long(client);
        client->struct_parse_pos = 4;
        /* Fall through */
    case 4:
        result->kom_news_conf = prot_a_parse_long(client);
        client->struct_parse_pos = 5;
        /* Fall through */
    case 5:
        result->motd_of_lyskom = prot_a_parse_long(client);
        /* Fall through */
    default:
        client->struct_parse_pos = 0;
    }
}

void
prot_a_hunt_nl(Connection *client)
{
    String_size number_end;
    String_size len;

    while (1) 
    {
	switch (client->fnc_parse_pos)
	{
	case 0:			/* whitspace/simple tokens */
	    if ( client->first_to_parse >= s_strlen(client->unparsed) )
		longjmp(parse_env, KOM_MSG_INCOMPLETE);

	    switch(client->unparsed.string[client->first_to_parse])
	    {
	    case ' ':
	    case '\r':
	    case '\t':
	    case '{':
	    case '}':
	    case '*':
		client->first_to_parse++;
		break;
	    case '\n':
		/* We found a newline -- end of message.  Return now. */
		client->first_to_parse++;
		return;
	    case '0': case '1': case '2': case '3': case '4':
	    case '5': case '6': case '7': case '8': case '9':
		/* Entering a number -- possibly a string. */
		/* Don't increase first_to_parse. */
		client->fnc_parse_pos = 1;
		break;
	    default:
		longjmp(parse_env, KOM_PROTOCOL_ERR);
	    }
	    break;
	case 1:			/* number/string */
	    /* Entering a number.  The first char is known to be a digit.
	       Parse the entire number at once. */
	    len = client->unparsed.string[client->first_to_parse] - '0';
	    for (number_end = client->first_to_parse + 1;
		 number_end < s_strlen(client->unparsed);
		 number_end++)
	    {
		if (client->unparsed.string[number_end] >= '0'
		    && client->unparsed.string[number_end] <= '9')
		{
		    len = 10 * len + client->unparsed.string[number_end] - '0';
		}
		else
		{
		    /* The end of the number was reached. */
		    break;
		}
	    }

	    if (number_end == s_strlen(client->unparsed))
		longjmp(parse_env, KOM_MSG_INCOMPLETE);

	    if (client->unparsed.string[number_end] == 'H')
	    {
		/* We are entering a string.   Use num0 to store the
		   lenght of the string to skip. */
		client->num0 = len;
		client->first_to_parse = number_end + 1;
		client->fnc_parse_pos = 2;
	    }
	    else
	    {
		/* We have just skipped past a number that was not the
		   start of a string. */
		client->first_to_parse = number_end;
		client->fnc_parse_pos = 0;
	    }
	    break;
	case 2:			/* Skipping a string. */
	    if (client->num0 == 0)
	    {
		/* The entire string has been skipped. */
		client->fnc_parse_pos = 0;
		break;
	    }
	    len = s_strlen(client->unparsed) - client->first_to_parse;
	    if (client->num0 <= len)
	    {
		/* The entire string is present.  Skip it. */
		client->first_to_parse += client->num0;
		client->fnc_parse_pos = 0;
		break;
	    }
	    else
	    {
		/* Skip as much of the string as possible. */
		client->num0 -= len;
		client->first_to_parse = s_strlen(client->unparsed);
		longjmp(parse_env, KOM_MSG_INCOMPLETE);
	    }
	    abort();
	default:
	    abort();
	}
    }
}

/*
 * Hunt for the end of an array. For this to work you need to set
 * array_hunt_depth to the number of nested arrays you want to
 * break out of (normally 1.) After this call the next token on
 * the input stream from the client will be the closing brace of
 * the array.
 */

static void
prot_a_hunt_array_end(Connection *client)
{
    String_size number_end;
    String_size len;

    while (1) 
    {
	switch (client->hunt_parse_pos)
	{
	case 0:			/* whitspace/simple tokens */
	    if ( client->first_to_parse >= s_strlen(client->unparsed) )
		longjmp(parse_env, KOM_MSG_INCOMPLETE);

	    switch(client->unparsed.string[client->first_to_parse])
	    {
	    case ' ':
	    case '\r':
	    case '\t':
	    case '*':
	    case '\n':
		client->first_to_parse++;
		break;
	    case '{':
                client->array_hunt_depth += 1;
                client->first_to_parse++;
                break;
	    case '}':
                client->array_hunt_depth -= 1;
                if (client->array_hunt_depth <= 0)
                {
                    client->hunt_parse_pos = 0;
                    return;
                }
                else
                {
                    client->first_to_parse++;
                }
                break;

	    case '0': case '1': case '2': case '3': case '4':
	    case '5': case '6': case '7': case '8': case '9':
		/* Entering a number -- possibly a string. */
		/* Don't increase first_to_parse. */
		client->hunt_parse_pos = 1;
		break;
	    default:
		longjmp(parse_env, KOM_PROTOCOL_ERR);
	    }
	    break;
	case 1:			/* number/string */
	    /* Entering a number.  The first char is known to be a digit.
	       Parse the entire number at once. */
	    len = client->unparsed.string[client->first_to_parse] - '0';
	    for (number_end = client->first_to_parse + 1;
		 number_end < s_strlen(client->unparsed);
		 number_end++)
	    {
		if (client->unparsed.string[number_end] >= '0'
		    && client->unparsed.string[number_end] <= '9')
		{
		    len = 10 * len + client->unparsed.string[number_end] - '0';
		}
		else
		{
		    /* The end of the number was reached. */
		    break;
		}
	    }

	    if (number_end == s_strlen(client->unparsed))
		longjmp(parse_env, KOM_MSG_INCOMPLETE);

	    if (client->unparsed.string[number_end] == 'H')
	    {
		/* We are entering a string.   Use num0 to store the
		   lenght of the string to skip. */
		client->array_hunt_num = len;
		client->first_to_parse = number_end + 1;
		client->hunt_parse_pos = 2;
	    }
	    else
	    {
		/* We have just skipped past a number that was not the
		   start of a string. */
		client->first_to_parse = number_end;
		client->hunt_parse_pos = 0;
	    }
	    break;
	case 2:			/* Skipping a string. */
	    if (client->array_hunt_num == 0)
	    {
		/* The entire string has been skipped. */
		client->hunt_parse_pos = 0;
		break;
	    }
	    len = s_strlen(client->unparsed) - client->first_to_parse;
	    if (client->array_hunt_num <= len)
	    {
		/* The entire string is present.  Skip it. */
		client->first_to_parse += client->array_hunt_num;
		client->hunt_parse_pos = 0;
		break;
	    }
	    else
	    {
		/* Skip as much of the string as possible. */
		client->array_hunt_num -= len;
		client->first_to_parse = s_strlen(client->unparsed);
		longjmp(parse_env, KOM_MSG_INCOMPLETE);
	    }
	    abort();            /* NOTREACHED */
	default:
            abort();
	}
    }
}

void
prot_a_parse_skip_whitespace(Connection *client)
{
    while (client->first_to_parse < client->unparsed.len
	   && strchr(WHITESPACE,
		     client->unparsed.string[client->first_to_parse]) != NULL)
	client->first_to_parse++;
}
