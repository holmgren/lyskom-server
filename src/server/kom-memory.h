/*
 * $Id: kom-memory.h,v 1.15 2003/08/23 16:38:16 ceder Exp $
 * Copyright (C) 1991, 1993-1994, 1996-1999, 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: kom-memory.h,v 1.15 2003/08/23 16:38:16 ceder Exp $
 *
 * The time has come to get some order into this mess.
 *
 * From now on the following terminology is used:
 *
 *    alloc_		smalloc() the object. Increase _cnt.
 *    free_		sfree() the object. Decrease _cnt.
 *    clear_		free_() any objects contained in the object,
 *			but not the object itself.
 *    copy_		smalloc() a new object and also smalloc()
 *			any objects contained in it.
 *    init_		initialize all fields in a allocated object.
 */


/* 
 * Memory handling functions, grouped by data type, sorted alphabetically.
 */

/* Conf_type */
extern void init_conf_type(Conf_type *ct);

/* Conference */
extern Conference *alloc_conference (void);
extern void        free_conference  (Conference *c);
extern void        clear_conference (Conference *c);
extern Conference *copy_conference  (const Conference *c);
extern void        init_conference  (Conference *c);

/* Dynamic_session_info */

extern void init_dynamic_session_info (Dynamic_session_info *d);

/* Mark_list */
extern void  init_mark_list (Mark_list *ml);

/* Member_list */
extern void init_member_list (Member_list *ml);

/* Membership type */
extern void init_membership_type(Membership_type *m);

/* Member */
extern void init_member(Member *mem);

/* Membership */
extern void init_membership(Membership *m);

/* Membership_list */
extern void init_membership_list (Membership_list *m);

/* Person */
extern Person *alloc_person (void);
extern void    free_person  (Person *p);
extern void    clear_person (Person *p);
extern Person *copy_person  (const Person *p);
extern void    init_person  (Person *p);

/* Personal_flags */
extern void  init_personal_flags (Personal_flags *p);

/* Priv_bits */
extern void init_priv_bits (Priv_bits *pb);

/* Session_info */
extern void init_session_info (Session_info *s);

/* Session_info_ident */
extern void init_session_info_ident (Session_info_ident *s);

/* Static_session_info */
extern void init_static_session_info (Static_session_info *d);

/* Text_stat */
extern Text_stat *alloc_text_stat (void);
extern void       free_text_stat  (Text_stat *t);
extern void       clear_text_stat (Text_stat *t);
extern Text_stat *copy_text_stat  (const Text_stat *t);
extern void       init_text_stat  (Text_stat *t);

/* struct tm */
extern void       init_struct_tm (struct tm *t);

/* Who_info */
extern void init_who_info (Who_info *w);

/* Who_info_ident */
extern void init_who_info_ident (Who_info_ident *w);

/* Who_info_old */
extern void init_who_info_old (Who_info_old *w);

/* Aux_item_list */

extern void free_aux_item_list (Aux_item_list *list);
extern void init_aux_item_list (Aux_item_list *list);

extern void init_aux_item_link(Aux_item_link *dest);
extern void init_aux_item_flags(Aux_item_flags *dest);
extern void clear_aux_item(Aux_item *item);
extern void init_aux_item(Aux_item *dest);
extern void copy_aux_item (Aux_item *dest, const Aux_item *src);

/*
 * Other kind of functions 
 */
extern void
dump_alloc_counts(FILE *stat_file);
