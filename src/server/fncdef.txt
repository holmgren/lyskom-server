#
# $Id: fncdef.txt,v 0.67 2005/12/18 21:54:17 ceder Exp $
# Copyright (C) 1991-1999, 2001-2003, 2005
# Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 
#
# $Id: fncdef.txt,v 0.67 2005/12/18 21:54:17 ceder Exp $
# 
# This file is used to describe the functions in services.c. All
# functions that are reachable from the clients are listed here, together
# with their argument and return types.
#
# NEVER alter any functions that have been in use for a while! Add 
# new/modified functions last on this list!
#
# Functions that are reachable when using protocal A:
#
# (Functions whose name end in _old, _older, _10, _2 and so forth
# are obsolete; better versions exists.)
#
#
# Protocol version 1
#
0    success login_old           num c_string (param.pwd_len)
1    success logout	    
2    success change_conference   num
3    success change_name         num c_string (param.conf_name_len)
4    success change_what_i_am_doing string (param.what_do_len)
5    number  create_person_old   c_string (param.conf_name_len) c_string (param.pwd_len)
6    success get_person_stat_old num num : person
7    success set_priv_bits       num priv_bits
8    success set_passwd          num c_string (param.pwd_len) c_string (param.pwd_len)
9    success query_read_texts_old num num : membership_old
10   number  create_conf_old     c_string (param.conf_name_len) conf_type
11   success delete_conf         num
12   success lookup_name         c_string (param.conf_name_len) : conf_list
13   success get_conf_stat_older num num : conference_old
14   success add_member_old      num num num num
15   success sub_member          num num
16   success set_presentation    num num
17   success set_etc_motd        num num
18   success set_supervisor      num num
19   success set_permitted_submitters num num
20   success set_super_conf      num num
21   success set_conf_type       num conf_type
22   success set_garb_nice       num num
23   success get_marks 	    : mark_list
24   success mark_text_old       num num
25   success get_text            num num num : string
26   success get_text_stat_old   num : text_stat_old
27   success mark_as_read        num num_list (param.mark_as_read_chunk)
28   number create_text_old      c_string (param.text_len) misc_info_list (param.max_crea_misc)
29   success delete_text         num
30   success add_recipient       num num num
31   success sub_recipient       num num
32   success add_comment         num num
33   success sub_comment         num num
34   success get_map             num num num : l2g_iterator_as_text_list
35   success get_time            : time_date
36   success get_info_old        : info_old
37   success add_footnote        num num
38   success sub_footnote        num num
39   success who_is_on_old       : who_info_list_old
40   success set_unread          num num
41   success set_motd_of_lyskom  num
42   success enable              num
43   success sync_kom
44   success shutdown_kom        num
45   success broadcast           c_string (param.broadcast_len)
46   success get_membership_old  num num num num : membership_list_old
47   success get_created_texts   num num num : l2g_iterator_as_text_list
48   success get_members_old     num num num : member_list_old
49   success get_person_stat     num : person
50   success get_conf_stat_old   num : conference_old
51   success who_is_on           : who_info_list
52   success get_unread_confs    num : conf_no_list
53   success send_message        num c_string (param.broadcast_len)
54   success get_session_info    num : session_info
55   success disconnect          num
56   success who_am_i            : session_no
#
# Protocol version 2
#
57   success set_user_area       num num
#
# Protocol version 3
#
58   success get_last_text       time_date : text_no
59   number  create_anonymous_text_old   c_string (param.text_len) misc_info_list (param.max_crea_misc)
60   success find_next_text_no num       : text_no
61   success find_previous_text_no num   : text_no
#
# Protocol version 4
#
62   success login               num c_string (param.pwd_len) num
63   success who_is_on_ident     : who_info_ident_list
64   success get_session_info_ident      num : session_info_ident
#
# Protocol version 5
#
65   success re_lookup_person    c_string (param.regexp_len) : conf_no_list
66   success re_lookup_conf      c_string (param.regexp_len) : conf_no_list
#
# Protocol version 6
#
67   success lookup_person       c_string (param.conf_name_len) : conf_no_list
68   success lookup_conf         c_string (param.conf_name_len) : conf_no_list
69   success set_client_version  c_string (param.client_data_len) c_string (param.client_data_len)
70   success get_client_name     num : string
71   success get_client_version  num : string
72   success mark_text           num num
73   success unmark_text         num
#
# Protocol version 7
#
74   success re_z_lookup         c_string (param.regexp_len) num num : conf_z_info_list
75   success get_version_info    : version_info
76   success lookup_z_name       c_string (param.conf_name_len) num num : conf_z_info_list
#
# Protocol version 8
#
77   success set_last_read       num num
78   success get_uconf_stat      num : uconference
#
# Protocol version 9
#
79   success set_info            info
80   success accept_async        num_list (param.accept_async_len)
81   success query_async         : num_list
82   success user_active
83   success who_is_on_dynamic   num num num : dynamic_session_info_list
84   success get_static_session_info     num : static_session_info
#
# Protocol version 10
#
85   success get_collate_table   : string
86   number  create_text         c_string (param.text_len) misc_info_list (param.max_crea_misc) aux_item_list (param.max_add_aux)
87   number  create_anonymous_text   c_string (param.text_len) misc_info_list (param.max_crea_misc) aux_item_list (param.max_add_aux)
88   number  create_conf         c_string (param.conf_name_len) conf_type aux_item_list (param.max_add_aux)
89   number  create_person       c_string (param.conf_name_len) c_string (param.pwd_len) pers_flags aux_item_list (param.max_add_aux)
90   success get_text_stat       num : text_stat
91   success get_conf_stat       num : conference
92   success modify_text_info    num num_list (param.max_delete_aux) aux_item_list (param.max_add_aux)
93   success modify_conf_info    num num_list (param.max_delete_aux) aux_item_list (param.max_add_aux)
94   success get_info            : info
95   success modify_system_info num_list (param.max_delete_aux) aux_item_list (param.max_add_aux)
96   success query_predefined_aux_items : num_list
97   success set_expire          num num
98   success query_read_texts_10 num num : membership_10
99   success get_membership_10 num num num num : membership_list_10
100  success add_member          num num num num membership_type
101  success get_members         num num num : member_list
102  success set_membership_type num num membership_type
103  success local_to_global	    num	num num	: text_mapping
104  success map_created_texts   num	num num	: text_mapping
105  success set_keep_commented  num num
106  success set_pers_flags      num pers_flags
107  success query_read_texts    num num num num : membership
108  success get_membership      num num num num num : membership_list
109  success mark_as_unread	 num num
110  success set_read_ranges     num read_range_list (param.max_read_ranges)
111  success get_stats_description : stats_description
112  success get_stats           c_string (param.stat_name_len) : stats_list
113  success get_boottime_info   : static_server_info
114  success first_unused_conf_no : conf_no
115  success first_unused_text_no : text_no
116  success find_next_conf_no     num : conf_no
117  success find_previous_conf_no num : conf_no
118  success get_scheduling	 num : scheduling_info
119  success set_scheduling	 num num num
120  success set_connection_time_format num
121  success local_to_global_reverse   num num num : text_mapping_reverse
122  success map_created_texts_reverse num num num : text_mapping_reverse

#ifdef DEBUG_CALLS
1000 success get_memory_info          : memory_info
1001 success set_marks           num num
1002 success backdate_text       num num
1003 success start_garb
1004 success cache_sync_start
1005 success cache_sync_finish
1006 success dump_cfg_timevals
1007 success backdate_comment_link num num num
1008 success server_sleep	   num
1009 success disable_client	   num num
#endif
