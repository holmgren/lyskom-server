#ifndef BISON_AUX_ITEM_DEF_PARSE_H
# define BISON_AUX_ITEM_DEF_PARSE_H

#ifndef YYSTYPE
typedef union
{
    String          str;
    unsigned long   num;
    struct aux_item_def_value_type
    {
        int         type;
        union
        {
            String          str;
            unsigned long   num;
        } val;
    } value;
} yystype;
# define YYSTYPE yystype
# define YYSTYPE_IS_TRIVIAL 1
#endif

#ifndef YYLTYPE
typedef struct yyltype
{
  int first_line;
  int first_column;

  int last_line;
  int last_column;
} yyltype;

# define YYLTYPE yyltype
# define YYLTYPE_IS_TRIVIAL 1
#endif

# define	NUMBER	257
# define	BOOLEAN	258
# define	ID	259
# define	STRING	260
# define	DISABLED	261
# define	TEXT	262
# define	CONFERENCE	263
# define	LETTERBOX	264
# define	TOK_SERVER	265
# define	TOK_ANY	266
# define	VOID	267
# define	CREATE	268
# define	MODIFY	269


extern YYSTYPE yylval;

#endif /* not BISON_AUX_ITEM_DEF_PARSE_H */
