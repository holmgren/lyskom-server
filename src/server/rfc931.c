/*
 * $Id: rfc931.c,v 1.28 2003/08/23 16:38:14 ceder Exp $
 * Copyright (C) 1991-1994, 1998-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

/*
 * This function retrieves the real user that owns the TCP/IP link
 * that is connecting via the IscSession "scb". The returned string
 * points to static data which is overwritten on the next call.
 *
 * Link with "-lauthuser".
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <errno.h>
#include <stdio.h>
#ifdef HAVE_LIBAUTHUSER
#  include <authuser.h>
#endif
#include <sys/types.h>
#include <sys/socket.h>
#include "timewrap.h"

#include "oop.h"

#include "log.h"
#include "s-string.h"
#include "isc-interface.h"
#include "misc-types.h"
#include "kom-types.h"
#include "param.h"
#include "rfc931.h"
#include "unused.h"
#include "stats.h"

#ifdef HAVE_LIBAUTHUSER
#  define USE_IF_LIBAUTHUSER(x) x
#else
#  define USE_IF_LIBAUTHUSER(x) UNUSED(x)
#endif

const char *
get_real_username(struct isc_scb *USE_IF_LIBAUTHUSER(scb),
		  char           *USE_IF_LIBAUTHUSER(hostname))
{
#ifdef HAVE_LIBAUTHUSER
    unsigned long inlocal;
    unsigned long inremote;
    unsigned short local;
    unsigned short remote;
    char *result;
    time_t before, after;

    if (param.authentication_level == 0)
	return NULL;

    if (auth_fd2(scb->fd, &inlocal, &inremote,
		 &local, &remote) == -1)
 	return NULL;

    time(&before);
    update_stat(STAT_IDENT_QUEUE, 1);
    result = auth_tcpuser3(inlocal, inremote, local, remote, 20);
    update_stat(STAT_IDENT_QUEUE, -1);
    update_stat(STAT_PROCESSED_IDENT, -1);
    if (result == NULL && errno == ETIMEDOUT)
	kom_log ("Identd request to %s timed out.\n", hostname);
    else
    {
	time(&after);
	if (difftime(after, before) > 15)
	    kom_log ("Identd at %s said %s after %d seconds.\n",
		 hostname,
		 result == NULL ? "(error)" :  result,
		 (int)difftime(after, before));
    }

    return result;
#else
    return "unknown";
#endif
}
