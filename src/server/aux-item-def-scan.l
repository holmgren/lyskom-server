%{
/*
 * $Id: aux-item-def-scan.l,v 1.7 2003/08/23 16:38:18 ceder Exp $
 * Copyright (C) 1994-1996, 1999, 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <ctype.h>

/* Rename hack from the automake 1.4 manual. */

#define	yymaxdepth aid_maxdepth
#define	yyparse	aid_parse
#define	yylex	aid_lex
#define	yyerror	aid_error
#define	yylval	aid_lval
#define	yychar	aid_char
#define	yydebug	aid_debug
#define	yypact	aid_pact
#define	yyr1	aid_r1
#define	yyr2	aid_r2
#define	yydef	aid_def
#define	yychk	aid_chk
#define	yypgo	aid_pgo
#define	yyact	aid_act
#define	yyexca	aid_exca
#define yyerrflag aid_errflag
#define yynerrs	aid_nerrs
#define	yyps	aid_ps
#define	yypv	aid_pv
#define	yys	aid_s
#define	yy_yys	aid_yys
#define	yystate	aid_state
#define	yytmp	aid_tmp
#define	yyv	aid_v
#define	yy_yyv	aid_yyv
#define	yyval	aid_val
#define	yylloc	aid_lloc
#define yyreds	aid_reds
#define yytoks	aid_toks
#define yylhs	aid_yylhs
#define yylen	aid_yylen
#define yydefred aid_yydefred
#define yydgoto	aid_yydgoto
#define yysindex aid_yysindex
#define yyrindex aid_yyrindex
#define yygindex aid_yygindex
#define yytable	 aid_yytable
#define yycheck	 aid_yycheck
#define yyname   aid_yyname
#define yyrule   aid_yyrule

extern int yylex(void);

#include "s-string.h"
#include "aux-item-def-parse.h"
#include "server/smalloc.h"


#if defined(HAVE_VFPRINTF) && defined(HAVE_STDARG_H)
void yyerror(const char * format, int lineno, ...)
#  if HAVE_ATTRIBUTE_FORMAT_PRINTF
    __attribute__ ((format (printf, 1, 3)))
#  endif
    ;
#else
void yyerror();
#endif
static String stringBuffer = EMPTY_STRING_i;
extern YYLTYPE yylloc;
extern int aux_item_def_error_line;
#define RETURN aux_item_def_error_line = yylineno; return
%}


%option yylineno
%option noyywrap

%x string

%%

(true|on|yes)                       {
                                        yylval.num = 1;
                                        yylloc.first_line = yylineno;
                                        RETURN BOOLEAN;
                                    }
(false|off|no)                      {
                                        yylval.num = 0; 
                                        yylloc.first_line = yylineno;
                                        RETURN BOOLEAN;
                                    }
disabled                            {
                                        yylloc.first_line = yylineno;
                                        RETURN DISABLED;
                                    }
text                                {
                                        yylloc.first_line = yylineno;
                                        RETURN TEXT;
                                    }
conference                          {
                                        yylloc.first_line = yylineno; 
                                        RETURN CONFERENCE;
                                    }
letterbox                           {
                                        yylloc.first_line = yylineno;
                                        RETURN LETTERBOX;
                                    }
server                              {
                                        yylloc.first_line = yylineno;
                                        RETURN TOK_SERVER;
                                    }
any                                 {
                                        yylloc.first_line = yylineno;
                                        RETURN TOK_ANY;
                                    }
create                              {
                                        yylloc.first_line = yylineno;
                                        RETURN CREATE;
                                    }

modify                              {
                                        yylloc.first_line = yylineno;
                                        RETURN MODIFY;
                                    }


#[^\n]*                             /* Eat comments */
[ \t\n]+                            /* Eat whitespace */

[[:alpha:]][\-[:alnum:]_]*          {
                                        char *s = yytext;
                                        do { *s=tolower(*s); } while(*(++s));
                                        yylval.str = EMPTY_STRING;
                                        s_crea_str(&yylval.str, yytext);
                                        yylloc.first_line = yylineno;
                                        RETURN ID;
                                    }

([0-9]+)                            {
                                        yylloc.first_line = yylineno;
                                        yylval.num = atoi(yytext);
                                        RETURN NUMBER;
                                    }

\"                                  {
                                        BEGIN(string); 
                                        stringBuffer = EMPTY_STRING;
                                        yylloc.first_line = yylineno;
                                    }
<string>
{
    "[ \t\n]*"                      /* Concatenate strings */
    \"                              {
                                        BEGIN(INITIAL);
                                        yylval.str = stringBuffer;
                                        stringBuffer = EMPTY_STRING;
                                        RETURN STRING;
                                    }
    \\[0-9]{1,3}                    {
                                        unsigned int tmp;
                                        sscanf(yytext+1, "%o", &tmp);
                                        if (tmp > 255)
                                            s_strcat(&stringBuffer, 
                                                     s_fcrea_str(yytext));
                                        else
                                        {
                                            sprintf(yytext, "%c",
                                                    (unsigned char)tmp);
                                            yytext[1] = '\0';
                                            s_strcat(&stringBuffer, 
                                                     s_fcrea_str(yytext));
                                        }
                                    }
    \\n                             s_strcat(&stringBuffer, s_fcrea_str("\n"));
    \\t                             s_strcat(&stringBuffer, s_fcrea_str("\t"));
    \\r                             s_strcat(&stringBuffer, s_fcrea_str("\r"));
    \\b                             s_strcat(&stringBuffer, s_fcrea_str("\b"));
    \\f                             s_strcat(&stringBuffer, s_fcrea_str("\f"));
    \\'                             s_strcat(&stringBuffer, s_fcrea_str("\'"));
    \\\"                            s_strcat(&stringBuffer, s_fcrea_str("\""));
    \\\n[ \t]+                      /* Eat escaped whitespace */
    \\.                             s_strcat(&stringBuffer, s_fcrea_str(yytext+1));
    [^\\\n\"]+                      s_strcat(&stringBuffer, s_fcrea_str(yytext));
    \n                              {
                                        yyerror("unterminated string",
                                                 yylloc.first_line);
                                        RETURN VOID;
                                    }
}

.                                   RETURN yytext[0];

%%
