/*
 * File: local_to_global.c
 *
 * Copyright 1996-1999, 2001-2003 Inge Wallin, Per Cederqvist
 */




#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <assert.h>
#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#endif
#ifndef NULL
#  include <stdio.h>
#endif
#include <sys/types.h>
#include "timewrap.h"

#include "s-string.h"

#include "kom-types.h"
#include "local-to-global.h"
#include "log.h"
#include "ram-parse.h"
#include "ram-output.h"
#include "lyskomd.h"
#include "server/smalloc.h"

/* The test suite requires L2G_BLOCKSIZE to be 10, but that small
   blocks will result in far too much malloc-overhead.  A value of at
   least 100 is probably needed for reasonable performance.  The user
   can call l2g_set_block_size to set this to a reasonable value.  */
static int L2G_BLOCKSIZE = -1;


struct l2g_block_info {
    /* An index into key_block and value_block that indicates the
       first free spot.  This can also be thought of as the number of
       entries in key_block and value_block that are actually in use. */
    int first_free;

    /* Number of entries in the block that contain the value 0.  For
       purposes of calculating this value, data past the end of the
       block is counted as zeroes, so that this->first_free +
       this->zeroes is always at least L2G_BLOCKSIZE.  */
    int zeroes;

    /* First local text no in this block.  Note: this does not
       increase if the first entry of the block is removed. */
    Local_text_no start;

    /* NULL if this is a dense block, or a block of L2G_BLOCKSIZE
       Local_text_nos if this is a sparse block. */
    Local_text_no *key_block;

    /* A block of L2G_BLOCKSIZE Text_nos. */
    Text_no *value_block;
};


static long nr_constructs = 0;
static long nr_l2gs = 0;
static long nr_l2gs_peak = 0;
static long nr_destructs = 0;
static long nr_clears = 0;
static long nr_copies = 0;
static long nr_joins = 0;
static long nr_joined_blocks = 0;
static long nr_blocks = 0;
static long nr_blocks_peak = 0;
static long nr_blocks_sparse = 0;
static long nr_blocks_sparse_peak = 0;
static long sparse_skip_cost = 0;
static long nr_sparse_compactions = 0;
static long nr_sparsifications = 0;



/* ================================================================ */
/* ====                     Static functions                   ==== */
/* ================================================================ */


static inline int
is_dense(const struct l2g_block_info *binfo)
{
    return binfo->key_block == NULL;
}

static inline int
is_empty(const struct l2g_block_info *binfo)
{
    return binfo->zeroes == L2G_BLOCKSIZE;
}

static inline Local_text_no
key_value(const struct l2g_block_info *binfo,
	  int index)
{
    if (is_dense(binfo))
	return binfo->start + index;
    else
	return binfo->key_block[index];
}


static inline int
sparse_skip_deleted(const struct l2g_block_info *binfo,
		    int i)
{
    while (i < binfo->first_free && binfo->value_block[i] == 0)
    {
	++sparse_skip_cost;
	++i;
    }

    return i;
}


/* Search for LNO in BINFO, and return the index for it.  BINFO must
   be a non-empty sparse block.  If LNO isn't present in the block
   this will return the index of next higher entry.  If LNO is larger
   than the last entry, the index past the largest entry will be
   returned.  This function never returns the index of a deleted entry. */
static inline int
sparse_locate_value(const struct l2g_block_info *binfo,
		    Local_text_no lno)
{
    int i;

    assert(!is_empty(binfo));
    assert(!is_dense(binfo));
    
    /* We could to this as a binary search instead, but since a block
       is fairly small, it isn't certain that it would be a gain.  If
       profiling shows that this is a hot spot, a binary search should
       be tried.  */
    for (i = 0; i < binfo->first_free && binfo->key_block[i] < lno; ++i)
	continue;

    /* Skip any lingering deleted entries. */
    i = sparse_skip_deleted(binfo, i);

    assert(i == binfo->first_free || binfo->key_block[i] >= lno);
    assert(i == binfo->first_free || binfo->value_block[i] != 0);

    return i;
}

static void
sparse_compact(struct l2g_block_info *binfo)
{
    int from, to;

    if (binfo->first_free + binfo->zeroes == L2G_BLOCKSIZE)
	return;

    assert(binfo->first_free + binfo->zeroes > L2G_BLOCKSIZE);

    for (from = to = 0; from < binfo->first_free; ++from)
	if (binfo->value_block[from] != 0)
	{
	    if (from != to)
	    {
		binfo->value_block[to] = binfo->value_block[from];
		binfo->key_block[to] = binfo->key_block[from];
	    }
	    ++to;
	}

    binfo->first_free = to;
    assert(binfo->first_free + binfo->zeroes == L2G_BLOCKSIZE);
    ++nr_sparse_compactions;
}


/*
 * Add a new block to the Local_to_global L2G.
 * Since we always add consecutively, we always start out by making 
 * a new block dense.
 *
 * Return a pointer to the newly created block info.
 */

static struct l2g_block_info *
add_block(Local_to_global *l2g)
{
    struct l2g_block_info  * binfo;
#ifndef NDEBUG
    int i;
#endif

    /* Realloc the block pointer array. */
    l2g->num_blocks++;
    l2g->blocks = srealloc(l2g->blocks,
			   l2g->num_blocks * sizeof(struct l2g_block_info));

    /* Create a new block info and fill it in. */
    binfo = &(l2g->blocks[l2g->num_blocks - 1]);
    binfo->first_free  = 0;
    binfo->zeroes      = L2G_BLOCKSIZE;
#ifndef NDEBUG
    binfo->start       = 0xdeadbeef;
#endif
    binfo->key_block   = NULL;
    binfo->value_block = smalloc(L2G_BLOCKSIZE * sizeof(Text_no));
    
#ifndef NDEBUG
    for (i = 0; i < L2G_BLOCKSIZE; ++i)
	binfo->value_block[i] = 0xdeadbeef;
#endif
    if (++nr_blocks > nr_blocks_peak)
	nr_blocks_peak = nr_blocks;
	
    return binfo;
}

/* Insert a new block in the middle of an l2g structure.  This
   is only used by l2g_expensive_set, so it doesn't have to be
   optimally efficient.  */
static void
add_block_before(Local_to_global *l2g,
		 int position)
{
    struct l2g_block_info  * binfo;
    struct l2g_block_info  * new_blocks;
    int ix;

    /* Allocate a new block pointer array. */
    new_blocks = smalloc((l2g->num_blocks+1) * sizeof(struct l2g_block_info));

    /* Copy blocks before and after position. */
    for (ix = 0; ix < position; ix++)
	new_blocks[ix] = l2g->blocks[ix];
    for (ix = position; ix < l2g->num_blocks; ix++)
	new_blocks[ix+1] = l2g->blocks[ix];

    /* Discard the old block pointers. */
    sfree(l2g->blocks);
    l2g->blocks = new_blocks;
    l2g->num_blocks++;

    /* Initialize the new block. */
    binfo = &new_blocks[position];
    binfo->first_free  = 0;
    binfo->zeroes      = L2G_BLOCKSIZE;
#ifndef NDEBUG
    binfo->start       = 0xdeadbeef;
#endif
    binfo->key_block   = NULL;
    binfo->value_block = smalloc(L2G_BLOCKSIZE * sizeof(Text_no));
    
#ifndef NDEBUG
    for (ix = 0; ix < L2G_BLOCKSIZE; ++ix)
	binfo->value_block[ix] = 0xdeadbeef;
#endif
    if (++nr_blocks > nr_blocks_peak)
	nr_blocks_peak = nr_blocks;
}


/*
 * Delete a block from the Local_to_global L2G.  The pointer BINFO
 * points at the block, which is supposed to be empty.
 */

static void
delete_block(Local_to_global *l2g, 
	     struct l2g_block_info  *binfo)
{
    assert(is_empty(binfo));

    if (!is_dense(binfo))
	--nr_blocks_sparse;
    --nr_blocks;

    /* Remove the blocks from the Block Info */
    if (binfo->key_block != NULL)
	sfree(binfo->key_block);
    sfree(binfo->value_block);

    /* Compact the remaining blocks. */
    while (++binfo < l2g->blocks + l2g->num_blocks)
	*(binfo - 1) = *binfo;
    l2g->num_blocks--;
    l2g->blocks = srealloc(l2g->blocks,
			   l2g->num_blocks * sizeof(struct l2g_block_info));
}


/*
 * Make a sparse block from a dense one.
 */
static void
make_sparse(struct l2g_block_info *binfo)
{
    int  next;
    int  i;

    assert(is_dense(binfo));
    if (++nr_blocks_sparse > nr_blocks_sparse_peak)
	nr_blocks_sparse_peak = nr_blocks_sparse;
    ++nr_sparsifications;

    /* Allocate the room for the key block. */
    binfo->key_block = smalloc(L2G_BLOCKSIZE * sizeof(Local_text_no));
#ifndef NDEBUG
    for (i = 0; i < L2G_BLOCKSIZE; ++i)
	binfo->key_block[i] = (Local_text_no)0xdeadbeefUL;
#endif

    /* Compact the block. */
    next = 0;
    for (i = 0; i < binfo->first_free; ++i)
	if (binfo->value_block[i] != 0)
	{
	    binfo->key_block[next]   = binfo->start + i;
	    binfo->value_block[next] = binfo->value_block[i];
	    next++;
	}

    /* Set the rest of the fields. */
    binfo->first_free = next;
    binfo->zeroes = L2G_BLOCKSIZE - next;
}


/*
 * Find the block where the local text no LNO should be and return a
 * pointer to it.  Returning a pointer does not necessarily mean that
 * the text exists, so a search within the block must also be done
 * later.  If LNO is smaller than the smallest number in the structure
 * a pointer to the first block will be returned.  If LNO is larger
 * than the largest number in the structure a pointer to the last
 * block will be returned.
 * 
 * Return NULL if (and only if) the structure is empty.
 */

static struct l2g_block_info *
find_block(const Local_to_global *l2g, Local_text_no lno)
{
    struct l2g_block_info  * binfo;

    /* If empty, the number can not be in here. */
    if (l2g->num_blocks == 0)
	return NULL;

    /* Let binfo point to the last block. */
    binfo = &(l2g->blocks[l2g->num_blocks - 1]);

    /* Find the block where lno *could* be. */
    /* FIXME (bug 152): Binary search? */
    while (binfo > l2g->blocks)
    {
	if (lno >= binfo->start)
	    return binfo;
	binfo--;
    }

    return binfo;
}


/* Find the first local text number higher than LNO and return it.
   Return 0 if LNO is higher than any local text number in the
   structure.  The block where the returned value was found is stored
   in BINFO_OUT, and the position within the block is stored in
   INDEX_OUT.  BINFO_OUT and INDEX_OUT are only filled in if the
   return value is non-zero.  */

static Local_text_no
find_block_index_key(const Local_to_global *l2g, Local_text_no lno,
		     const struct l2g_block_info **binfo_out,
		     int             *index_out)
{
    const struct l2g_block_info  * binfo;
    int               i;

    /* If the number is not to be found, return 0. */
    binfo = find_block(l2g, lno);
    if (binfo == NULL)
	return 0;

    /* If lno is lower than the first existing entry, return the first
       existing entry.  The search in the first block is performed the
       same way as when no entry was found after LNO in binfo. */
    if (lno >= binfo->start)
    {
	/* Check the found block.  See if there is another entry after
	   the entry for LNO.  If so, return it. */
	if (is_dense(binfo))
	{
	    for (i = lno - binfo->start + 1; i < binfo->first_free; ++i)
		if (binfo->value_block[i] != 0)
		{
		    *binfo_out = binfo;
		    *index_out = i;

		    return binfo->start + i;
		}
	}
	else
	{
	    i = sparse_locate_value(binfo, lno);
	    if (i < binfo->first_free && binfo->key_block[i] == lno)
		++i;
	    i = sparse_skip_deleted(binfo, i);
	    if (i < binfo->first_free)
	    {
		*binfo_out = binfo;
		*index_out = i;

		return binfo->key_block[i];
	    }
	}
	/*
	 * If the block didn't contain anything more, try the next
	 * block, if there is one.
	 */
	binfo++;
    }

    /*
     * We want the first existing entry in the block that binfo points
     * to.  However, binfo may point past the last block.
     */
    if (binfo >= l2g->blocks + l2g->num_blocks)
	return 0;

    for (i = 0; i < binfo->first_free; ++i)
	if (binfo->value_block[i] != 0)
	{
	    *binfo_out = binfo;
	    *index_out = i;
	    return key_value(binfo, i);
	}

    restart_kom("find_block_index_key found nothing\n");
}

/* Find the first local text number lower than LNO and return it.
   Return 0 if LNO is lower than any local text number in the
   structure.  The block where the returned value was found is stored
   in BINFO_OUT, and the position within the block is stored in
   INDEX_OUT.  BINFO_OUT and INDEX_OUT are only filled in if the
   return value is non-zero.  */

static Local_text_no
find_block_index_key_reverse(const Local_to_global        *l2g,
			     Local_text_no                 lno,
			     const struct l2g_block_info **binfo_out,
			     int                          *index_out)
{
    const struct l2g_block_info *binfo;
    int i;

    /* Adjust lno to the local number we are hunting for.  */
    if (lno > 0)
	--lno;

    /* If the number is not to be found, return 0. */
    binfo = find_block(l2g, lno);
    if (binfo == NULL)
	return 0;

    /* Check if not present in the first block. */
    if (lno < binfo->start)
	return 0;

    /* Check the found block and return the last entry found, if any. */
    if (is_dense(binfo))
    {
	i = lno - binfo->start;
	if (i >= binfo->first_free)
	    i = binfo->first_free - 1;
	for (; i >= 0; --i)
	    if (binfo->value_block[i] != 0)
	    {
		*binfo_out = binfo;
		*index_out = i;

		return binfo->start + i;
	    }
    }
    else
    {
	i = sparse_locate_value(binfo, lno);
	if (i >= binfo->first_free)
	    i = binfo->first_free - 1;
	while (i > 0 && (binfo->value_block[i] == 0
			 || binfo->key_block[i] > lno))
	{
	    ++sparse_skip_cost;
	    --i;
	}
	if (binfo->value_block[i] != 0 && binfo->key_block[i] <= lno)
	{
	    *binfo_out = binfo;
	    *index_out = i;

	    return binfo->key_block[i];
	}
    }
	
    /*
     * Try the previous block, if there is one.
     */
    if (binfo == l2g->blocks)
	return 0;

    binfo--;

    /*
     * We want the last existing entry in the block that binfo points
     * to.
     */
    for (i = binfo->first_free - 1; i >= 0; --i)
	if (binfo->value_block[i] != 0)
	{
	    *binfo_out = binfo;
	    *index_out = i;
	    return key_value(binfo, i);
	}

    restart_kom("find_block_index_key_reverse found nothing\n");
}

static void
join_range(Local_to_global *l2g,
	   struct l2g_block_info *first,
	   struct l2g_block_info *last)
{
    int next;
    int i;
    struct l2g_block_info *binfo;

    assert(first < last);
    assert(l2g->blocks <= first && first < l2g->blocks + l2g->num_blocks);
    assert(l2g->blocks <= last  && last  < l2g->blocks + l2g->num_blocks);

    if (is_dense(first))
	make_sparse(first);
    else
	sparse_compact(first);

    ++nr_joins;
    next = first->first_free;
    for (binfo = first + 1; binfo <= last; ++binfo)
    {
	++nr_joined_blocks;
	for (i = 0; i < binfo->first_free; ++i)
	    if (binfo->value_block[i] != 0)
	    {
		first->value_block[next] = binfo->value_block[i];
		first->key_block[next]  = key_value(binfo, i);
		next++;
	    }
	if (!is_dense(binfo))
	{
	    --nr_blocks_sparse;
	    sfree(binfo->key_block);
	}
	sfree(binfo->value_block);
    }

    while (binfo < l2g->blocks + l2g->num_blocks)
    {
	assert(binfo - (last - first) > first);
	*(binfo - (last - first)) = *binfo;
	++binfo;
    }

    assert(next <= L2G_BLOCKSIZE);
    first->first_free = next;
    first->zeroes = L2G_BLOCKSIZE - next;

    nr_blocks -= last - first;
    l2g->num_blocks -= last - first;
    l2g->blocks = srealloc(l2g->blocks,
			   l2g->num_blocks * sizeof(struct l2g_block_info));
}
    
    
static int
join_blocks(Local_to_global *l2g,
	    struct l2g_block_info *binfo)
{
    int zeroes;
    int gain;
    int best;
    int i;

    if (binfo->zeroes == 0)
	/* This block is full. */
	return 0;

    /* Note: on rare occasions we might be able to create one dense
       block from two (dense or sparse) blocks or one sparse block.
       We don't bother.  The gain would probably be small, and it
       would complicate the algorithm significantly.  */

    zeroes = L2G_BLOCKSIZE;
    gain = 0;
    best = 0;

    for (i = 0; zeroes > 0 && i <= binfo - l2g->blocks; ++i)
    {
	zeroes -= (L2G_BLOCKSIZE - (binfo-i)->zeroes);
	gain += (is_dense(binfo-i) ? 1 : 2);
	if (gain > 2 && zeroes >= 0)
	    best = i;
    }

    if (best > 0)
    {
	join_range(l2g, binfo - best, binfo);
	return 1;
    }

    zeroes = L2G_BLOCKSIZE;
    gain = 0;
    best = 0;
    for (i = 0; zeroes > 0 && i < l2g->num_blocks - (binfo - l2g->blocks); ++i)
    {
	zeroes -= (L2G_BLOCKSIZE - (binfo+i)->zeroes);
	gain += (is_dense(binfo+i) ? 1 : 2);
	if (gain > 2 && zeroes >= 0)
	    best = i;
    }

    if (best > 0)
    {
	join_range(l2g, binfo, binfo + best);
	return 1;
    }

    return 0;
}

/* ================================================================ */
/* ====                Outside accessible functions            ==== */
/* ================================================================ */


void
l2g_set_block_size(int sz)
{
    assert(L2G_BLOCKSIZE == -1);
    L2G_BLOCKSIZE = sz;
}


void
l2g_destruct(Local_to_global *l2g)
{
    l2g_clear(l2g);
    ++nr_destructs;
    --nr_l2gs;
#ifndef NDEBUG
    l2g->num_blocks = 0xdeadbeef;
    l2g->first_unused = 0xdeadbeef;
    l2g->blocks = (void*)0xdeadbeef;
#endif
}


void
l2g_init(Local_to_global *l2g)
{
    if (L2G_BLOCKSIZE == -1)
	L2G_BLOCKSIZE = 250;

    /* Initialize the main structure */
    l2g->num_blocks = 0;
    l2g->first_unused = 1;
    l2g->blocks = NULL;
    ++nr_constructs;
    if (++nr_l2gs > nr_l2gs_peak)
	nr_l2gs_peak = nr_l2gs;
}


void
l2g_clear(Local_to_global *l2g)
{
    struct l2g_block_info  * binfo;
    int               i;

    /* Free the block info structures. */
    binfo = l2g->blocks;
    for (i = 0; i < l2g->num_blocks; ++i)
    {
	if (binfo->key_block != NULL)
	{
	    --nr_blocks_sparse;
	    sfree(binfo->key_block);
	}
	sfree(binfo->value_block);

	binfo++;
    }
    nr_blocks -= l2g->num_blocks;

    /* Free the block pointers. */
    l2g->num_blocks = 0;
    l2g->first_unused = 1;
    if (l2g->blocks != NULL)
    {
	sfree(l2g->blocks);
	l2g->blocks = NULL;
    }
    ++nr_clears;
}


/*
 * Copy everything from one l2g structure (FROM) into another (TO).
 * TO has to be an initialized structure.
 */

void
l2g_copy(Local_to_global *dest, const Local_to_global *src)
{
    const struct l2g_block_info *binfo;
    int i;

    /* FIXME (bug 153): More efficient code */
    l2g_clear(dest);

    for (binfo = src->blocks; binfo < src->blocks + src->num_blocks; ++binfo)
	for (i = 0; i < binfo->first_free; ++i)
	    l2g_append(dest, key_value(binfo, i), binfo->value_block[i]);
    assert(src->first_unused >= dest->first_unused);
    dest->first_unused = src->first_unused;
    ++nr_copies;
}


typedef Local_text_no Local_text_no_iter;

void
l2g_append(Local_to_global *l2g, 
	   Local_text_no    lno,
	   Text_no          tno)
{
    struct l2g_block_info  * binfo;
    Local_text_no_iter ix;

    if (lno < l2g->first_unused)
    {
	kom_log("l2g_append: won't add %lu<%lu> when first_unused=%lu\n",
	    (unsigned long)tno, (unsigned long)lno,
	    (unsigned long)l2g->first_unused);
	return;
    }

    l2g->first_unused = lno + 1;
	    
    /* Don't add anything if tno == 0. */
    if (tno == 0)
	return;

    if (l2g->num_blocks == 0)
    {
	/* If totally empty, add the first block and set binfo to it. */
	binfo = add_block(l2g);
	binfo->start = lno;
    }
    else
    {
	/* else let binfo point to the last block. */
	binfo = &(l2g->blocks[l2g->num_blocks - 1]);
    }

    /* Try to make room for the new data in the last block (pointed to
       by binfo).  If that fails, allocate a new block and let binfo
       point to it. */
    if (is_dense(binfo))
    {
	if (lno - binfo->start >= (Local_text_no)L2G_BLOCKSIZE)
	{
	    /* The last block is a dense block, and this text does not
	       fit in it.  Or is there anything we can do to make it fit?  */

	    if (binfo->zeroes >= L2G_BLOCKSIZE / 2)
		/* Arbitrary limit: make the block sparse if at least
		   half of it contains zeroes.  (Remeber that any
		   unused entries past binfo->first_free are counted
		   in binfo->zeroes). */
		make_sparse(binfo);
	    else
		/* There is no way to make room in the last block, so
		   allocate a new one.  (There *could* be ways to make
		   this entry fit into the last block: if the next to
		   last block is sparse and has room for an entry, we
		   might be able to slide the last block, and fit in
		   the new entry.  However, we never move data between
		   blocks.  That would be too complex and error-prone,
		   and the gain would be very small.) */
		binfo = add_block(l2g);
	}
    }
    else
    {
	/* The last block was sparse. */
	sparse_compact(binfo);
	if (binfo->first_free == L2G_BLOCKSIZE)
	    binfo = add_block(l2g);
    }

    /* Enter the new value into the last block. */
    if (binfo->first_free == 0)
	binfo->start = lno;

    if (is_dense(binfo))
    {
	for (ix = binfo->first_free; ix < lno - binfo->start; ++ix)
	    binfo->value_block[ix] = 0;
	binfo->value_block[lno - binfo->start] = tno;
	binfo->first_free = lno - binfo->start + 1;
    }
    else
    {
	binfo->key_block  [binfo->first_free] = lno;
	binfo->value_block[binfo->first_free] = tno;
	binfo->first_free++;
    }
    
    binfo->zeroes--;
}


void
l2g_expensive_set(Local_to_global *l2g, 
		  Local_text_no    lno,
		  Text_no          tno)
{
    struct l2g_block_info  * binfo;
    Local_text_no_iter ix;
    int block_no;

    if (lno >= l2g->first_unused)
    {
	l2g_append(l2g, lno, tno);
	return;
    }

    if (tno == 0)
    {
	l2g_delete(l2g, lno);
	return;
    }

    binfo = find_block(l2g, lno);
    if (binfo == NULL)
    {
	assert(l2g->num_blocks == 0);

	/* Append a new block. */
	binfo = add_block(l2g);
	assert(binfo->first_free == 0);
	assert(binfo->key_block == NULL);
	binfo->start = lno;
	binfo->value_block[0] = tno;
	binfo->first_free = 1;
	binfo->zeroes--;
    }
    else if (lno < binfo->start)
    {
	assert(binfo == l2g->blocks);

	/* Prepend a new block. */
	add_block_before(l2g, 0);
	binfo = l2g->blocks;
	binfo->start = lno;
	binfo->value_block[0] = tno;
	binfo->first_free = 1;
	binfo->zeroes--;
    }
    else if (is_dense(binfo))
    {
	assert(binfo->start <= lno);
	if (lno < binfo->start + L2G_BLOCKSIZE)
	{
	    /* The number can be entered into this block.  */
	    while (binfo->start + binfo->first_free <= lno)
	    {
		binfo->value_block[binfo->first_free++] = 0;
		assert(binfo->first_free <= L2G_BLOCKSIZE);
	    }
	    
	    assert(lno < binfo->start + binfo->first_free);
	    assert(binfo->first_free <= L2G_BLOCKSIZE);

	    if (binfo->value_block[lno - binfo->start] == 0)
		binfo->zeroes--;
	    binfo->value_block[lno - binfo->start] = tno;
	}
	else
	{
	    /* There is no room within this block.  Insert a new block
	       after binfo.  */
	    block_no = 1 + binfo - l2g->blocks;
	    add_block_before(l2g, block_no);
	    binfo = l2g->blocks + block_no;
	    binfo->start = lno;
	    binfo->value_block[0] = tno;
	    binfo->first_free = 1;
	    binfo->zeroes--;
	}
    }
    else
    {
	ix = sparse_locate_value(binfo, lno);
	if (ix < (Local_text_no_iter)binfo->first_free
	    && binfo->key_block[ix] == lno)
	{
	    /* We found the value in this sparse block.  */
	    assert(binfo->value_block[ix] != 0);
	    binfo->value_block[ix] = tno;
	}
	else
	{
	    if (binfo->zeroes == 0)
	    {
		/* Split the sparse block to make room. */
		block_no = 1 + binfo - l2g->blocks;
		add_block_before(l2g, block_no);
		binfo = l2g->blocks + block_no;

		assert(binfo[-1].first_free == L2G_BLOCKSIZE);

		if (lno > binfo[-1].key_block[L2G_BLOCKSIZE-1])
		{
		    /* Insert the mapping into the new block.  */
		    binfo->start = lno;
		    binfo->value_block[0] = tno;
		    binfo->first_free = 1;
		    binfo->zeroes--;
		    return;
		}
		else
		{
		    /* Move the last mapping from the block into the new block,
		       and enter the new mapping into the old block.  */

		    /* Set up the new block.  */
		    binfo->start = binfo[-1].key_block[L2G_BLOCKSIZE-1];
		    binfo->value_block[0] = binfo[-1].value_block[
			L2G_BLOCKSIZE-1];
		    binfo->first_free = 1;
		    binfo->zeroes--;

		    /* Fix the old block.  */
		    binfo--;
		    binfo->zeroes++;
		    binfo->first_free--;
		    /* The old block is now compact.  Let the code below
		       enter the new mapping into it.  */
		}
		assert(binfo->zeroes == 1);
	    }
	    else
		sparse_compact(binfo);

	    /* There is room for a new mapping in this block. */
	    assert(binfo->zeroes > 0);
	    for (ix = binfo->first_free; ix > 0; --ix)
	    {
		assert(binfo->key_block[ix-1] != lno);
		if (binfo->key_block[ix-1] > lno)
		{
		    binfo->key_block[ix] = binfo->key_block[ix-1];
		    binfo->value_block[ix] = binfo->value_block[ix-1];
		}
		else
		    break;
	    }

	    assert(ix == 0 || binfo->key_block[ix-1] < lno);
	    assert(ix == (Local_text_no_iter)binfo->first_free
		   || binfo->key_block[ix+1] > lno);

	    binfo->key_block[ix] = lno;
	    binfo->value_block[ix] = tno;
	    binfo->first_free++;
	    binfo->zeroes--;
	}
    }
}


/*
 * Delete the local text LNO from the structure L2G.
 */

void
l2g_delete(Local_to_global *l2g,
	   Local_text_no lno)
{
    struct l2g_block_info *binfo;
    int i;
    
    /* Find block where LNO might be and return if not there. */
    binfo = find_block(l2g, lno);
    if (binfo == NULL)
	return;

    /* Go through the block where it might be and delete LNO */
    if (is_dense(binfo))
    {
	if (binfo->start <= lno && lno < binfo->start + L2G_BLOCKSIZE)
	    if (binfo->value_block[lno - binfo->start] != 0)
	    {
		binfo->value_block[lno - binfo->start] = 0;
		binfo->zeroes++;
	    }
    }
    else
    {
	i = sparse_locate_value(binfo, lno);
	if (i < binfo->first_free && binfo->key_block[i] == lno)
	{
	    assert(binfo->value_block[i] != 0); /* See sparse_locate_value. */
	    binfo->value_block[i] = 0;
	    binfo->zeroes++;
	}
    }

    /* Delete the block if it became empty. */
    if (is_empty(binfo))
    {
	delete_block(l2g, binfo);
	return;
    }

    /* Check if we can save space by joining adjoining blocks. */
    if (!join_blocks(l2g, binfo))
	/* Could not join.  Compact this block if it is a sparse block
           with more than half of the entries zeroes.  */
	if (!is_dense(binfo)
	    && binfo->zeroes + binfo->first_free/2 > L2G_BLOCKSIZE)
	{
	    sparse_compact(binfo);
	}
}


Text_no
l2g_lookup(const Local_to_global *l2g,
	   Local_text_no lno)
{
    const struct l2g_block_info  * binfo;
    int               i;

    binfo = find_block(l2g, lno);
    if (binfo == NULL)
	return 0;

    if (is_dense(binfo))
    {
	if (binfo->start <= lno && lno < binfo->start + binfo->first_free)
	    return binfo->value_block[lno - binfo->start];
    }
    else
    {
	i = sparse_locate_value(binfo, lno);
	if (i < binfo->first_free && binfo->key_block[i] == lno)
	    return binfo->value_block[i];
    }

    return 0;
}


Local_text_no
l2g_next_key(const Local_to_global *l2g,
	     Local_text_no lno)
{
    const struct l2g_block_info  * binfo;
    int               i;

    return find_block_index_key(l2g, lno, &binfo, &i);
}


Local_text_no
l2g_first_appendable_key(const Local_to_global *l2g)
{
    return l2g->first_unused;
}


void
l2g_set_first_appendable_key(Local_to_global *l2g,
			     Local_text_no    key)
{
    if (key < l2g->first_unused)
    {
    	kom_log("l2g_append: won't decrease first_unused from %lu to %lu\n",
		(unsigned long)l2g->first_unused, (unsigned long)key);
	return;
    }

    l2g->first_unused = key;
}


void
l2g_delete_global_in_sorted(Local_to_global *l2g,
			    Text_no tno)
{
    /* This implementation performs a linear search through the list,
       but since we know that both the key and the data are sorted in
       ascending order we could do a binary search instead.
       FIXME (bug 154): profile the code and se if it is worthwhile to
       implement something fancier.  */

    L2g_iterator iter;

    for (l2gi_searchall(&iter, l2g); !iter.search_ended; l2gi_next(&iter))
	if (iter.tno == tno)
	{
	    l2g_delete(l2g, iter.lno);
	    return;
	}
}


void
l2g_dump(FILE *file,
	 const Local_to_global *l2g)
{
    struct l2g_block_info  * binfo;
    int               i, j;

    fprintf(file, "Number of blocks: %d\n", l2g->num_blocks);
    fprintf(file, "First unused: %lu\n", l2g->first_unused);

    binfo = l2g->blocks;
    for (i = 0; i < l2g->num_blocks; ++i) {
	fprintf(file, "%d: %d %d %d (%s) [", i,
		binfo->first_free,
		binfo->zeroes,
		(int) binfo->start,
		(is_dense(binfo) ? "dense" : "sparse"));

	if (is_dense(binfo)) {
	    for (j = 0; j < binfo->first_free; ++j) {
		fprintf(file, "%d ", (int) binfo->value_block[j]);
	    }
	} else {
	    for (j = 0; j < binfo->first_free; ++j) {
		fprintf(file, "%d:%d ", 
			(int) binfo->key_block[j],
			(int) binfo->value_block[j]);
	    }
	}
	fprintf(file, "]\n");

	binfo++;
    }
}


Success
l2g_read(FILE *fp, Local_to_global *l2g)
{
    int            c;
    Local_text_no  lno = 0;
    Text_no        tno;
    Local_text_no  first_unused;

    l2g_clear(l2g);

    /* Read past the start marker */
    fskipwhite(fp);
    if ( (c = getc(fp)) == EOF || c != '[')
    {
	kom_log("l2g_read() failed to find ``['' marker at pos %lu.\n",
                (unsigned long) ftell(fp));
	return FAILURE;
    }

    first_unused = fparse_long(fp);

    /* Read numbers until the EOL (end-of-l2g :-) ) marker is read. */
    while (1)
    {
	if ((c = getc(fp)) == EOF)
	{
	    kom_log("l2g_read(): unexpected EOF at pos %lu.\n",
		(unsigned long) ftell(fp));
	    return FAILURE;
	}

	switch (c)
	{
	case ' ':
	    lno = fparse_long(fp);
	    if (lno == 0)
	    {
		kom_log("l2g_read(): got local number 0 at pos %lu.\n",
		    (unsigned long) ftell(fp));
		return FAILURE;
	    }
	    break;
	case ',':
	    if (lno == 0)
	    {
		kom_log("l2g_read(): missing local number at pos %lu.\n",
		    (unsigned long)ftell(fp));
		return FAILURE;
	    }
	    ++lno;
	    /*FALLTHROUGH*/
	case ':':
	    if (lno == 0)
	    {
		kom_log("l2g_read(): missing local number at pos %lu.\n",
		    (unsigned long)ftell(fp));
		return FAILURE;
	    }
	    tno = fparse_long(fp);
	    l2g_append(l2g, lno, tno);
	    break;
	case ']':
	    assert(first_unused >= l2g->first_unused);
	    l2g->first_unused = first_unused;
	    return OK;
	default:
	    kom_log("l2g_read(): unexpected character ``%c'' at pos %lu.\n",
		c, (unsigned long) ftell(fp));
	    return FAILURE;
	}
    }
}

/* Write the unsigned long value ``l'' to ``fp''. */
static void
put_ulong(unsigned long l,
	  FILE *fp)
{
    static char buf[sizeof(unsigned long) * 3 + 1];
    char       *cp;

    if (l < 10)
	putc("0123456789"[l], fp);
    else
    {
	cp = buf + sizeof(buf);
	while (l > 0)
	{
	    *--cp = (l % 10) + '0';
	    l /= 10;
	}
	fwrite(cp, buf + sizeof(buf) - cp, 1, fp);
    }
}

void
l2g_write(FILE *fp, const Local_to_global *l2g)
{
    const struct l2g_block_info *binfo;
    int ix;
    Local_text_no key;
    Text_no val;
    Local_text_no current = 0;

    putc('[', fp);
    put_ulong(l2g->first_unused, fp);

    for (binfo = l2g->blocks; binfo < l2g->blocks + l2g->num_blocks; ++binfo)
	for (ix = 0; ix < binfo->first_free; ++ix)
	    if ((val = binfo->value_block[ix]) != 0)
	    {
		key = key_value(binfo, ix);
		if (key > current + 3 || current == 0)
		{
		    putc(' ', fp);
		    put_ulong(key, fp);
		    putc(':', fp);
		    put_ulong(val, fp);
		    current = key;
		}
		else
		{
		    while (++current < key)
		    {
			putc(',', fp);
			putc('0', fp);
		    }
		    putc(',', fp);
		    put_ulong(val, fp);
		}
	    }

    putc(']', fp);
}


/* ================================================================ */
/* ===                     Iterator code                        === */
/* ================================================================ */


void
l2gi_searchall (L2g_iterator *l2gi,
		const Local_to_global *l2g)
{
    l2gi_searchsome(l2gi, l2g, 1, 0);
}


void
l2gi_searchsome(L2g_iterator *l2gi,
		const Local_to_global *l2g, 
		Local_text_no begin, 
		Local_text_no end)
{
    const struct l2g_block_info  * binfo;
    int               index;

    if (begin < 1)
    {
	kom_log("l2gi_searchsome(%lu, %lu) called: min is 1\n",
	    (unsigned long)begin, (unsigned long)end);
	begin = 1;
    }

    l2gi->l2g = l2g;
    l2gi->beginval = begin;
    l2gi->endval   = end;
    l2gi->search_ended = 1;

    if (end != 0 && begin >= end)
	return;

    if (find_block_index_key(l2g, begin-1, &binfo, &index) == 0)
	return;

    l2gi->binfo    = binfo;
    l2gi->arrindex = index;

    assert(binfo != NULL);
    
    l2gi->lno = key_value(binfo, index);
    l2gi->tno = binfo->value_block[index];

    if (end == 0)
	l2gi->search_ended = 0;
    else
	l2gi->search_ended = l2gi->lno >= end;
}


void
l2gi_next(L2g_iterator *l2gi)
{
    const Local_to_global *l2g;
    const struct l2g_block_info *binfo;
    int                arrindex;

    assert(!l2gi->search_ended);

    l2g      = l2gi->l2g;
    arrindex = l2gi->arrindex + 1;
    for (binfo = l2gi->binfo;
	 binfo < l2g->blocks + l2g->num_blocks;
	 ++binfo, arrindex = 0)
    {
	for (; arrindex < binfo->first_free; ++arrindex)
	{
	    if (l2gi->endval != 0
		&& key_value(binfo, arrindex) >= l2gi->endval)
	    {
		l2gi->search_ended = 1;
		return;
	    }

	    if (binfo->value_block[arrindex] != 0)
	    {
		l2gi->binfo    = binfo;
		l2gi->arrindex = arrindex;

		l2gi->lno = key_value(binfo, arrindex);
		l2gi->tno = binfo->value_block[arrindex];

		return;
	    }
	}
    }

    l2gi->search_ended = 1;
    return;
}


Local_text_no
l2gi_begin(const L2g_iterator *l2gi)
{
    return l2gi->beginval;
}


Local_text_no
l2gi_end(const L2g_iterator *l2gi)
{
    return l2gi->endval;
}


/* ================================================================ */
/* ===                Reverse Iterator code                     === */
/* ================================================================ */

void
l2gi_searchsome_reverse(L2g_reverse_iterator *l2gi,
			const Local_to_global *l2g, 
			Local_text_no begin,
			Local_text_no end)
{
    const struct l2g_block_info  * binfo;
    int               index;

    l2gi->l2g = l2g;
    l2gi->beginval = begin;
    l2gi->endval   = end;
    l2gi->search_ended = 1;

    if (begin >= end)
	return;

    if (find_block_index_key_reverse(l2g, end, &binfo, &index) == 0)
	return;

    l2gi->binfo    = binfo;
    l2gi->arrindex = index;

    assert(binfo != NULL);
    
    l2gi->lno = key_value(binfo, index);
    l2gi->tno = binfo->value_block[index];
    l2gi->search_ended = l2gi->lno < begin;
}


void
l2gi_prev(L2g_reverse_iterator *l2gi)
{
    const Local_to_global *l2g;
    const struct l2g_block_info *binfo;
    int                arrindex;

    assert(!l2gi->search_ended);

    l2g      = l2gi->l2g;
    arrindex = l2gi->arrindex - 1;
    binfo = l2gi->binfo;
    while (1)
    {
	for (; arrindex >= 0; --arrindex)
	{
	    if (key_value(binfo, arrindex) < l2gi->beginval)
	    {
		l2gi->search_ended = 1;
		return;
	    }

	    if (binfo->value_block[arrindex] != 0)
	    {
		l2gi->binfo    = binfo;
		l2gi->arrindex = arrindex;

		l2gi->lno = key_value(binfo, arrindex);
		l2gi->tno = binfo->value_block[arrindex];

		return;
	    }
	}

	if (binfo == l2g->blocks)
	{
	    l2gi->search_ended = 1;
	    return;
	}
	
	--binfo;
	arrindex = binfo->first_free - 1;
    }
}




/* ---------- */

extern void
dump_l2g_stats(FILE *file)
{
    fprintf(file, "---%s:\n\tExisting l2gs:    %ld\n",
	    __FILE__, nr_l2gs);
    fprintf(file, "\tExisting sparse blocks: %ld\n", nr_blocks_sparse);
    fprintf(file, "\tExisting blocks (total): %ld\n", nr_blocks);
    fprintf(file, "\tPeak l2gs: %ld\n", nr_l2gs_peak);
    fprintf(file, "\tPeak sparse blocks: %ld\n", nr_blocks_sparse_peak);
    fprintf(file, "\tPeak blocks (total): %ld\n", nr_blocks_peak);
    fprintf(file, "\tctor calls: %ld\n", nr_constructs);
    fprintf(file, "\tdtor calls: %ld\n", nr_destructs);
    fprintf(file, "\tclear calls: %ld\n", nr_clears);
    fprintf(file, "\tcopy calls: %ld\n", nr_copies);
    fprintf(file, "\tjoin operations: %ld\n", nr_joins);
    fprintf(file, "\tjoined blocks: %ld\n", nr_joined_blocks);
    fprintf(file, "\tsparse skip cost: %ld\n", sparse_skip_cost);
    fprintf(file, "\tsparse compactions: %ld\n", nr_sparse_compactions);
    fprintf(file, "\tsparsifications: %ld\n", nr_sparsifications);
}
