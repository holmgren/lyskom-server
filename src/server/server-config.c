/*
 * $Id: server-config.c,v 0.100 2003/08/29 10:40:46 ceder Exp $
 * Copyright (C) 1991-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 *  server-config.c
 *
 *  This is in a .c file to make it possible to change a value without having
 *  to recompile the entire server (or, in fact, anything!)
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <limits.h>
#include <stdio.h>
#include <sys/types.h>
#ifdef HAVE_STRING_H
#  include <string.h>
#endif
#include "timewrap.h"
#include <assert.h>
#include <setjmp.h>

#include "server/smalloc.h"
#include "kom-config.h"
#include "kom-types.h"
#include "com.h"
#include "async.h"
#include "connections.h"
#include "kom-errno.h"
#include "manipulate.h"
#include "server-config.h"
#include "misc-types.h"
#include "s-string.h"
#include "kom-types.h"
#include "param.h"
#include "conf-file.h"
#include "admin.h"
#include "log.h"
#include "lyskomd.h"
#include "unused.h"
#include "timeval-util.h"
#ifdef DEBUG_CALLS
#  include "services.h"
#endif
#include "paths.h"

struct kom_par param;

static Success log_param(const char *val, const struct parameter *par);
static Success jubel(const char *val, const struct parameter *par);
static Success ident_param(const char *val, const struct parameter *par);

/* Paths are a special kind of strings.  For now, just use these
   defines to make it easier to spot paths in the table below. */
#define assign_path assign_string
#define unassign_path unassign_string

/* See lyskomd.texi for more info about the parameters.
   Please remember to update lyskomd.texi if you add more parameters!
   Try to keep this list and the list in lyskomd.texi in the same order. */
static const struct parameter parameters[] = {

    /* "Normal" configuration */

    {"Locale",
	 assign_string,  unassign_string,  0, 1, NULL, &param.use_locale,
         NULL},
    {"Force ISO 8859-1",
         assign_bool,    NULL,             0, 1, "no", &param.force_iso_8859_1,
         NULL},
    {"Prefix",
	 assign_path,    unassign_path,    0, 1, DEFAULT_PREFIX,
         &param.dbase_dir,
         NULL},
    {"Send async",
  	 assign_bool,    NULL,             0, 1, "1",
         &param.send_async_messages,
         NULL},
    {"Client host",
	 assign_string,  unassign_string,  0, 1, NULL, &param.ip_client_host,
         NULL},
    {"Client port",
	 assign_string,  unassign_string,  1, 1, NULL, &param.ip_client_port,
         NULL},
    {"Presentation of conferences",
	 assign_conf_no, NULL,             0, 1, "1", &kom_info.conf_pres_conf,
         NULL},
    {"Presentation of persons",
	 assign_conf_no, NULL,             0, 1, "2", &kom_info.pers_pres_conf,
         NULL},
    {"Motd-conference",
	 assign_conf_no, NULL,             0, 1, "3", &kom_info.motd_conf,
         NULL},
    {"News-conference", 
	 assign_conf_no, NULL,             0, 1, "4", &kom_info.kom_news_conf,
         NULL},
    {"Message of the day",
	 assign_text_no, NULL,             0, 1, "0", &kom_info.motd_of_lyskom,
         NULL},
    {"Garb",
	 assign_bool,    NULL,             0, 1, "on", &param.garb_enable,
         NULL},
    {"Never save",
	 assign_bool,    NULL,             0, 1, "no", &param.never_save,
         NULL},
#ifdef LOGACCESSES
    {"Log accesses", 
	 assign_path,    unassign_path,    0, 1, NULL,
         &param.logaccess_file,
         NULL},
#endif

    /* The database files. */

    {"Data file",
	 assign_path,    unassign_path,    0, 1, DATA_FILE,
         &param.datafile_name,
         NULL},
    {"Backup file",
	 assign_path,    unassign_path,    0, 1, BACKUP_FILE,
         &param.backupfile_name,
         NULL},
    {"Backup file 2",
	 assign_path,    unassign_path,    0, 1, PREV_BACKUP_FILE,
         &param.backupfile_name_2,
         NULL},
    {"Lock file",
	 assign_path,    unassign_path,    0, 1, LOCK_FILE,
         &param.lockfile_name,
         NULL},
    {"Text file",
	 assign_path,    unassign_path,    0, 1, TEXT_FILE,
         &param.textfile_name,
         NULL},
    {"Number file",
	 assign_path,    unassign_path,    0, 1, NUMBER_FILE,
         &param.numberfile_name,
         NULL},
    {"Number temp file",
	 assign_path,    unassign_path,    0, 1, NUMBER_FILE_TMP,
         &param.numberfile_tmp_name,
         NULL},
    {"Text backup file",
	 assign_path,    unassign_path,    0, 1, TEXT_BACKUP_FILE,
         &param.textbackupfile_name,
         NULL},
    {"Backup export directory",
	 assign_path,    unassign_path,    0, 1, EXPORT_DIR,
         &param.backup_dir,
         NULL},

    /* Various log files */

    {"Log file",
	 assign_path,    unassign_path,    0, 1, LYSKOMD_LOG,
         &param.logfile_name,
         NULL},
    {"Log statistics",
	 assign_path,    unassign_path,    0, 1, LYSKOMD_STATS,
         &param.statistic_name,
         NULL},
    {"Pid file",
	 assign_path,    unassign_path,    0, 1, LYSKOMD_PID,
         &param.pid_name,
         NULL},
    {"Memory usage file",
	 assign_path,    unassign_path,    0, 1, MEMORY_USAGE,
         &param.memuse_name,
         NULL},

    /* Other files. */

    {"Aux-item definition file",
	 assign_path,    unassign_path,    0, 1, AUX_FILE,
         &param.aux_def_file,
         NULL},
    {"Status file",
         assign_path,    unassign_path,    0, 1, STATUS_FILE,
         &param.status_file,
         NULL},

    {"Connection status file",
         assign_path,    unassign_path,    0, 1, CONNECTIONS_FILE,
         &param.connection_status_file,
         NULL},

    {"Connection status temp file",
         assign_path,    unassign_path,    0, 1, CONNECTIONS_TMP,
         &param.connection_status_file_tmp,
         NULL},

    /* Where to dump core. */

    {"Core directory",
	 assign_path,    unassign_path,    0, 1, CORE_DIR,
         &param.core_dir,
         NULL},
    {"Nologin file",
         assign_path,    unassign_path,    0, 1, "/etc/nologin",
         &param.nologin_file,
         NULL},

    /* Performance tuning parameters (milliseconds) */

    {"Garb busy postponement",
	 assign_timeval, NULL,             0, 1, "50",
         &param.garb_busy_postponement, "milliseconds"},

    {"Garb timeout",
	 assign_timeval, NULL,             0, 1, "0", &param.garbtimeout,
         "milliseconds"},
    {"Sync timeout",
	 assign_timeval, NULL,             0, 1, "0", &param.synctimeout,
         "milliseconds"},

    /* Performance tuning parameters (minutes) */

    {"Garb interval",
	 assign_timeval, NULL,             0, 1, "1440", &param.garb_interval,
         "minutes"},
    {"Permissive sync",
         assign_bool,    NULL,             0, 1, "off", &param.permissive_sync,
         NULL},
    {"Sync interval",
	 assign_timeval, NULL,             0, 1, "5", &param.sync_interval,
         "minutes"},
    
    {"Sync retry interval",
	 assign_timeval, NULL,             0, 1, "1",
         &param.sync_retry_interval,
         "minutes"},

    {"Saved items per call",
         assign_int,     NULL,             0, 1, "5",
         &param.saved_items_per_call,
         NULL},

    {"Penalty per call",
         assign_uint,    NULL,             0, 1, "10",
         &param.penalty_per_call,
         NULL},

    {"Penalty per read",
         assign_uint,    NULL,             0, 1, "1",
         &param.penalty_per_read,
         NULL},

    {"Max penalty",
         assign_uint,    NULL,             0, 1, "100",
         &param.max_penalty,
         NULL},

    {"Low penalty",
         assign_uint,    NULL,             0, 1, "20",
         &param.low_penalty,
         NULL},

    {"Default priority",
         assign_uint,    NULL,             0, 1, "0",
         &param.default_priority,
         NULL},

    {"Max priority",
         assign_uint,    NULL,             0, 1, "0",
         &param.max_priority,
         NULL},

    {"Default weight",
         assign_uint,    NULL,             0, 1, "20",
         &param.default_weight,
         NULL},

    {"Max weight",
         assign_uint,    NULL,             0, 1, "100",
         &param.max_weight,
         NULL},

    /* Client inactivity timeouts. */

    {"Connect timeout",
	 assign_timeval, NULL,             0, 1, "30",
         &param.connect_timeout,
         "seconds"},

    {"Login timeout",
	 assign_timeval, NULL,             0, 1, "30",
         &param.login_timeout,
         "minutes"},

    {"Active timeout",
	 assign_timeval, NULL,             0, 1, "11.5",
         &param.active_timeout,
         "days"},

    
    /* More performance tuning. */

    {"Max client message size",
	 assign_int,     NULL,             0, 1, "8176", &param.maxmsgsize,
         NULL},

    {"Max client transmit queue messages",
	 assign_int,     NULL,             0, 1, "50", &param.maxqueuedsize,
         NULL},

    {"Max client transmit queue bytes",
	 assign_int,     NULL,             0, 1, "100000",
         &param.maxqueuedsize_bytes,
         NULL},

    {"Stale timeout",
	 assign_timeval, NULL,             0, 1, "60",
         &param.stale_timeout,
         "minutes"},

    {"Max simultaneous client replies",
	 assign_int,     NULL,             0, 1, "10", &param.maxdequeuelen,
         NULL},
    {"Open files",
         assign_int,     NULL,             0, 1, "-1", &param.no_files,
         NULL},
    {"Use DNS",
         assign_bool,    NULL,             0, 1, "yes", &param.use_dns,
         NULL},
    {"DNS log threshold",
         assign_double,  NULL,             0, 1, "1.5",
         &param.dns_log_threshold,
         NULL},

    /* String limits */

    {"Max conference name length",
	 assign_int,     NULL,             0, 1, "60", &param.conf_name_len,
         NULL},
    {"Max client data length",
         assign_int,     NULL,             0, 1, "60", &param.client_data_len,
         NULL},
    {"Max password length",
	 assign_int,     NULL,             0, 1, "128", &param.pwd_len,
         NULL},
    {"Max what am I doing length",
	 assign_int,     NULL,             0, 1, "60", &param.what_do_len,
         NULL},
    {"Max username length",
	 assign_int,     NULL,             0, 1, "128", &param.username_len,
         NULL},
    {"Max text length",
	 assign_int,     NULL,             0, 1, "131072", &param.text_len,
         NULL},
    {"Max aux_item length",
	 assign_int,     NULL,             0, 1, "16384", &param.aux_len,
         NULL},
    {"Max broadcast length",
	 assign_int,     NULL,             0, 1, "1024", &param.broadcast_len,
         NULL},
    {"Max regexp length",
	 assign_int,     NULL,             0, 1, "1024", &param.regexp_len,
         NULL},
    {"Statistic name length",
	 assign_int,     NULL,             0, 1, "64", &param.stat_name_len,
         NULL},

    /* Text_stat limits */

    {"Max marks per person",
	 assign_int,     NULL,             0, 1, "2048",
         &param.max_marks_person,
         NULL},
    {"Max marks per text",
	 assign_int,     NULL,             0, 1, "1024", &param.max_marks_text,
         NULL},
    {"Max recipients per text",
	 assign_int,     NULL,             0, 1, "512", &param.max_recipients,
         NULL},
    {"Max comments per text",
	 assign_int,     NULL,             0, 1, "128", &param.max_comm,
         NULL},
    {"Max footnotes per text",
	 assign_int,     NULL,             0, 1, "32", &param.max_foot,
         NULL},
    {"Max links per text",
	 assign_int,     NULL,             0, 1, "512", &param.max_crea_misc,
         NULL},

    /* Other client-visible configuration */

    {"Max mark_as_read chunks",
	 assign_int,     NULL,             0, 1, "128",
         &param.mark_as_read_chunk,
         NULL},
    {"Max accept_async len",
	 assign_int,     NULL,             0, 1, "128",
         &param.accept_async_len,
         NULL},
    {"Max aux_items added per call",
         assign_int,     NULL,             0, 1, "128", &param.max_add_aux,
         NULL},
    {"Max aux_items deleted per call",
         assign_int,     NULL,             0, 1, "128", &param.max_delete_aux,
         NULL},
    {"Max read_ranges per call",
         assign_int,     NULL,             0, 1, "512", &param.max_read_ranges,
         NULL},
    {"Max super_conf loop",
	 assign_int,     NULL,             0, 1, "17",
         &param.max_super_conf_loop,
         NULL},
    {"Default garb nice",
	 assign_int,     NULL,             0, 1, "77", &param.default_nice,
         NULL},
    {"Default keep commented nice",
	 assign_int,     NULL,             0, 1, "77",
         &param.default_keep_commented,
         NULL},

    /* Security options */

    {"Anyone can create new persons",
	 assign_bool,    NULL,             0, 1, "yes",
         &param.anyone_can_create_new_persons,
         NULL},
    {"Anyone can create new conferences",
	 assign_bool,    NULL,             0, 1, "yes",
         &param.anyone_can_create_new_confs,
         NULL},
    {"Allow creation of persons before login",
	 assign_bool,    NULL,             0, 1, "yes",
         &param.create_person_before_login,
         NULL},
    {"Default change name capability",
	 assign_bool,    NULL,             0, 1, "on",
         &param.default_change_name,
         NULL},
    {"Add members by invitation",
        assign_bool,     NULL,             0, 1, "on",
         &param.invite_by_default,
         NULL},
    {"Allow secret memberships",
        assign_bool,     NULL,             0, 1, "on",
         &param.secret_memberships,
         NULL},
    {"Allow reinvitations",
        assign_bool,     NULL,             0, 1, "off", &param.allow_reinvite,
         NULL},
    {"Log login",
	 assign_bool,    NULL,             0, 1, "off", &param.log_login,
         NULL},
    {"Ident-authentication",
	ident_param,     NULL,             0, 1, "try",
         &param.authentication_level,
         NULL},

    /* Cache configuration */

    {"Cache conference limit",
	 assign_int,     NULL,             0, 1, "20",
         &param.cache_conferences,
         NULL},
    {"Cache person limit",
	 assign_int,     NULL,             0, 1, "20", &param.cache_persons,
         NULL},
    {"Cache text_stat limit",
	 assign_int,     NULL,             0, 1, "20", &param.cache_text_stats,
         NULL},

    /* Echo the value to the log. */

    {"Echo",
	 log_param,      NULL,             0, -1, NULL, NULL,
         NULL},

    /* Register a forbidden text number. */

    {"Jubel",
         jubel,          NULL,             0, -1, NULL, NULL,
         NULL},

    {"Max conferences",
        assign_ulong,    NULL,             1, 1, "4765", &param.max_conf,
         NULL},
    {"Max texts",
        assign_ulong,    NULL,             1, 1, "2000000", &param.max_text,
         NULL},

    /* Configuration for support programs.  */

    {"Normal shutdown time",
	 assign_int,     NULL,             0, 1, "21",
         &param.normal_shutdown_time,
         NULL},
    {"Mail after downtime",
	 assign_int,     NULL,             0, 1, "60",
         &param.downtime_mail_start,
         NULL},
    {"Mail until downtime",
	 assign_int,     NULL,             0, 1, "120",
         &param.downtime_mail_end,
         NULL},
    {"sendmail path",
         assign_path,    unassign_path,    0, 1, SENDMAIL_PATH,
         &param.sendmail_path,
         NULL},
    {"lyskomd path",
	 assign_path,    unassign_path,    0, 1, LYSKOMD_PATH,
         &param.lyskomd_path,
         NULL},
    {"savecore path",
	 assign_path,    unassign_path,    0, 1, SAVECORE_PATH,
         &param.savecore_path,
         NULL},
    
    /* end marker */

    {NULL, NULL, NULL, 0, 0, NULL, NULL, NULL}};

/* Where to save things. */

static const char compiled_config_file[] = CONFIG_FILE;
static const char *default_config = NULL;

const char *
get_default_config_file_name(void)
{
    if (default_config == NULL)
    {
	if (compiled_config_file[0] == '/')
	    default_config = compiled_config_file;
	else
	{
	    char *cfg = smalloc(strlen(DEFAULT_PREFIX)
				+ strlen(compiled_config_file) + 2);
	    sprintf(cfg, "%s/%s", DEFAULT_PREFIX, compiled_config_file);
	    default_config = cfg;
	}
    }
    return default_config;
}


void
free_default_config_file_name(void)
{
    if (default_config != NULL && default_config != compiled_config_file)
    {
	/* cast away const; this string was allocated by
	   get_default_config_file_name().  */

	sfree((char*)default_config);
    }
    default_config = NULL;
}


/* This file descriptor, and any above it, will not be used by lyskomd. */
int fd_ceiling = 0;	/* Initialized by main(). */

/* What is whitespace? */
const char *WHITESPACE = " \t\n\r";

static Success
log_param(const char *val, const struct parameter *UNUSED(par))
{
    if (val != NULL)
	kom_log ("config: %s\n", val);
    return OK;
}

static Success
jubel(const char *val, const struct parameter *par)
{
    long a, b, c;
    int res;
    Bool public = FALSE;
    
    if (val == NULL)
        return OK;

    if (!strncmp(val, "public ", 7))
    {
	public = TRUE;
	val += 7;
    }

    res = sscanf(val, "%ld %ld %ld", &a, &b, &c);
    switch (res)
    {
    case 3:
	register_jubel(a, b, c, public);
	break;
    case 2:
	register_jubel(a, 0, b, public);
	break;
    default:
	kom_log("%s expecting [public ] x y [z]\n", par->name);
	return  FAILURE;
    }
    return OK;
}

static Success
ident_param(const char *val, const struct parameter *par)
{
    if (val == NULL)
	restart_kom("ident_param(): val == NULL\n");
    if (!strcmp(val, "off")
	|| !strcmp(val, "never"))
    {
	*(int*)par->value = 0;
    }
    else if (!strcmp(val, "on")
	     || !strcmp(val, "try"))
    {
	*(int*)par->value = 1;
    }
    else if (!strcmp(val, "require")
	     || !strcmp(val, "required"))
    {
	*(int*)par->value = 2;
    }
    else
    {
	kom_log ("%s expects \"never\", \"try\" or \"required\" as argument\n",
	     par->name);
	return FAILURE;
    }
    return OK;
}

static void
add_prefix(char **name)
{
    char *s;

    if (**name == '/')
	return;			/* Don't alter full paths. */

    s = smalloc(2 + strlen(param.dbase_dir) + strlen(*name));
    sprintf(s, "%s/%s", param.dbase_dir, *name);
    sfree(*name);
    *name = s;
}

static const char *
param_name(void *value)
{
    int ix;

    for (ix = 0; parameters[ix].name != NULL; ix++)
	if (parameters[ix].value == value)
	    return parameters[ix].name;

    restart_kom("Internal error: non-existing config param in param_name.\n");
    /* notreached */
    return NULL;
}

static Bool
check_abs_path(char **path)
{
    if (**path == '/')
	return FALSE;
	
    kom_log("Parameter '%s' must be an absolute path when 'Prefix' is empty.\n",
	param_name(path));
    return TRUE;
}

static void
require_less(void *low,
	     void *high)
{
    kom_log("Parameter '%s' must be less than parameter '%s'.\n",
	    param_name(low),
	    param_name(high));
}    

static void
require_less_eq(void *low,
		void *high)
{
    kom_log("Parameter '%s' must be less than or equal to parameter '%s'.\n",
	    param_name(low),
	    param_name(high));
}    

void
read_configuration(const char *conf_file)
{
    Bool err = FALSE;

    read_config(conf_file, parameters);
    
    assert(param.dbase_dir != NULL);
    assert(param.datafile_name != NULL);
    assert(param.backupfile_name != NULL);
    assert(param.backupfile_name_2 != NULL);
    assert(param.lockfile_name != NULL);
    assert(param.textfile_name != NULL);
    assert(param.numberfile_name != NULL);
    assert(param.numberfile_tmp_name != NULL);
    assert(param.textbackupfile_name != NULL);
    assert(param.backup_dir != NULL);
    assert(param.statistic_name != NULL);
    assert(param.pid_name != NULL);
    assert(param.memuse_name != NULL);
    assert(param.logfile_name != NULL);
    assert(param.connection_status_file != NULL);
    assert(param.connection_status_file_tmp != NULL);
    assert(param.aux_def_file != NULL);
    assert(param.status_file != NULL);
    assert(param.core_dir != NULL);
    assert(param.lyskomd_path != NULL);
    assert(param.savecore_path != NULL);
    assert(param.sendmail_path != NULL);

    if (strlen(param.dbase_dir) > 0) 
    {
	if (param.dbase_dir[0] != '/')
	{
	    kom_log("The 'Prefix' parameter must be an absolute path.\n");
	    err = TRUE;
	}

	add_prefix(&param.datafile_name);
	add_prefix(&param.backupfile_name);
	add_prefix(&param.backupfile_name_2);
	add_prefix(&param.lockfile_name);
	add_prefix(&param.textfile_name);
	add_prefix(&param.numberfile_name);
	add_prefix(&param.numberfile_tmp_name);
	add_prefix(&param.textbackupfile_name);
	add_prefix(&param.backup_dir);
	add_prefix(&param.statistic_name);
	add_prefix(&param.pid_name);
	add_prefix(&param.memuse_name);
	add_prefix(&param.logfile_name);
	add_prefix(&param.connection_status_file);
	add_prefix(&param.connection_status_file_tmp);
	add_prefix(&param.aux_def_file);
	add_prefix(&param.status_file);
	add_prefix(&param.core_dir);
	add_prefix(&param.lyskomd_path);
	add_prefix(&param.savecore_path);
	if (strcmp(param.sendmail_path, ":") != 0)
	    add_prefix(&param.sendmail_path);
    }
    else
    {
	err |= check_abs_path(&param.datafile_name);
	err |= check_abs_path(&param.backupfile_name);
	err |= check_abs_path(&param.backupfile_name_2);
	err |= check_abs_path(&param.lockfile_name);
	err |= check_abs_path(&param.textfile_name);
	err |= check_abs_path(&param.numberfile_name);
	err |= check_abs_path(&param.numberfile_tmp_name);
	err |= check_abs_path(&param.textbackupfile_name);
	err |= check_abs_path(&param.backup_dir);
	err |= check_abs_path(&param.statistic_name);
	err |= check_abs_path(&param.pid_name);
	err |= check_abs_path(&param.memuse_name);
	err |= check_abs_path(&param.logfile_name);
	err |= check_abs_path(&param.connection_status_file);
	err |= check_abs_path(&param.connection_status_file_tmp);
	err |= check_abs_path(&param.aux_def_file);
	err |= check_abs_path(&param.status_file);
	err |= check_abs_path(&param.core_dir);
	err |= check_abs_path(&param.lyskomd_path);
	err |= check_abs_path(&param.savecore_path);
	if (strcmp(param.sendmail_path, ":") != 0)
	    err |= check_abs_path(&param.sendmail_path);
    }

    if (param.saved_items_per_call < 1)
    {
	err = TRUE;
	kom_log("Parameter '%s' must be at least 1.\n",
		param_name(&param.saved_items_per_call));
    }

    /* We want max_texts and max_confs to hold the first forbidden
       number, but the documentation states that they hold the number
       of objects that are allowed to be created.  Adjust. */
    if (++param.max_text < 2)
    {
	err = TRUE;
	kom_log("Parameter '%s' must be at least 1.\n",
		param_name(&param.max_text));
    }
    if (++param.max_conf < 6)
    {
	err = TRUE;
	kom_log("Parameter '%s' must be at least 5.\n",
		param_name(&param.max_conf));
    }

    if (param.low_penalty >= param.max_penalty)
    {
	err = TRUE;
	require_less(&param.low_penalty, &param.max_penalty);
    }

    if (param.default_weight < 1)
    {
	err = TRUE;
	kom_log("Parameter '%s' must be at least 1.\n",
		param_name(&param.default_weight));
    }

    if (param.default_weight > param.max_weight)
    {
	err = TRUE;
	require_less_eq(&param.default_weight,
			&param.max_weight);
    }

    if (param.default_priority > param.max_priority)
    {
	err = TRUE;
	require_less_eq(&param.default_priority,
			&param.max_priority);
    }

    if (param.max_priority > 0)
    {
	err = TRUE;
	kom_log("Parameter '%s' must be at most 0.\n",
		param_name(&param.max_priority));
    }

    /* This limit stops a potential overflow in adjust_penalty(). */
    if (param.max_weight >= 0x10000)
    {
	err = TRUE;
	kom_log("Parameter '%s' must be at most %d.\n",
		param_name(&param.max_weight), 0x10000);
    }

    /* This limit stops a potential overflow in adjust_penalty(). */
    if (param.max_penalty >= 0x8000)
    {
	err = TRUE;
	kom_log("Parameter '%s' must be at most %d.\n",
		param_name(&param.max_weight), 0x8000);
    }

    /* FIXME (bug 165): Check config parameters for sanity.
       One thing to check is:
          The following should always be true:
          0 <= SYNCTIMEOUT <= GARBTIMEOUT <= TIMEOUT
          Times in milliseconds.
       There are probably many more things to check. */

    if (err)
	restart_kom("Please fix the above configuration errors.\n");
}

void
free_configuration(void)
{
    int i = 0;

    while (parameters[i].name != NULL)
    {
        if (parameters[i].freer != NULL)
        {
            (*parameters[i].freer)(&parameters[i]);
        }
        i += 1;
    }
}

#ifdef DEBUG_CALLS

static void
dump_timeval(const struct parameter *par)
{
    struct timeval *tv = par->value;
    kom_log("Name: %s\n", par->name);
    kom_log("  Default suffix: %s\n", par->default_suffix);
    kom_log("  Seconds: %ld\n", tv->tv_sec);
    kom_log("  Microseconds: %ld\n", tv->tv_usec);
}


Success
dump_cfg_timevals(void)
{
    int ix;

    kom_log("Configuration timeval dump\n");
    for (ix = 0; parameters[ix].name != NULL; ix++)
	if (parameters[ix].assigner == assign_timeval)
	    dump_timeval(&parameters[ix]);
    kom_log("End of timeval dump\n");

    return OK;
}

#endif
