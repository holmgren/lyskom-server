/*
 * Copyright (C) 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * stats.c -- Handle statistical information, such as load averages.
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <assert.h>
#include <stdlib.h>
#include "timewrap.h"
#include <string.h>
#include <stdio.h>
#include <setjmp.h>
#include <float.h>
#include <math.h>

#include "stats.h"
#include "log.h"
#include "lyskomd.h"
#include "kom-types.h"
#include "services.h"
#include "async.h"
#include "com.h"
#include "connections.h"
#include "kom-errno.h"
#include "manipulate.h"
#include "server/smalloc.h"
#include "timeval-util.h"

#define FACT_0 (1)		/*  1 second */
#define FACT_1 (15)		/* 15 seconds */
#define FACT_2 ( 4 * FACT_1)	/*  1 minute */
#define FACT_3 ( 5 * FACT_2)	/*  5 minutes */
#define FACT_4 ( 3 * FACT_3)	/* 15 minutes */
#define N_FACTS 5
#define HISTORY_LENGTH (FACT_4)

enum value_type { VT_AVG, VT_ASCEND, VT_DESCEND, NUM_VT };

struct avg_status {
    /* When was something last added to this? */
    struct timeval when;

    /* The current value. */
    long value;

    /* The accumulated average value for the current second.  For
       VT_AVG, the values for fractional seconds are accumulated
       here.  For VT_DESCEND and VT_ASCEND, this is the same as
       value. */
    double acc;

    /* A circular buffer of the acc values for the past several
       seconds.  Use ind() to figure out which index you should use.  */
    float avg_history[HISTORY_LENGTH];

    /* When somebody fetches an average value, it is computed.  In
       case the value is used twice during the same second, we cache
       the values in avenrun.  Check the avenrun_updated to see if the
       cached value is up-to-date.

       We tried to keep avenrun_updated up-to-date, but rounding
       errors made that hard.  We could use an exponentially decaying
       average, but I don't like them. */
    time_t avenrun_updated;
    double avenrun[N_FACTS];
};

static struct avg_status status[NUM_STAT][NUM_VT];

static const int factors[] = {
    FACT_0,
    FACT_1,
    FACT_2,
    FACT_3,
    FACT_4,
};

static int
ind(unsigned long sec,
    unsigned int offset)
{
    int ret = (sec - offset) % HISTORY_LENGTH;
    assert(ret >= 0);
    assert(ret < HISTORY_LENGTH);
    return ret;
}

    
void
init_stats(void)
{
    enum stat_type st;
    enum value_type vt;
    int i;
    struct timeval now;

    assert(sizeof(factors)/sizeof(factors[0]) == N_FACTS);
    assert(factors[N_FACTS-1] == HISTORY_LENGTH);

    gettimeofday(&now, NULL);

    for (st = 0; st < NUM_STAT; st++)
	for (vt = 0; vt < NUM_VT; vt++)
	{
	    status[st][vt].when = now;
	    status[st][vt].value = 0;
	    status[st][vt].acc = 0.0;
	    status[st][vt].avenrun_updated = now.tv_sec;
	    for (i = 0; i < HISTORY_LENGTH; i++)
		status[st][vt].avg_history[i] = 0.0;
	    for (i = 0; i < N_FACTS; i++)
		status[st][vt].avenrun[i] = 0.0;
	}
}


static void
update_history(struct avg_status *s,
	       time_t end,
	       float value)
{
    for (; s->when.tv_sec < end; s->when.tv_sec++, s->when.tv_usec = 0)
	s->avg_history[ind(s->when.tv_sec, 0)] = value;
}

	       
static void
update_one_stat(enum stat_type st,
		enum value_type vt,
		long delta,
		struct timeval now)
{
    long time_delta;
    int i;
    struct avg_status *s = &status[st][vt];
    static struct timeval notified;
    static int logged = 0;

    if (timeval_less(now, s->when))
    {
	if (!logged || !timeval_less(now, notified))
	{
	    kom_log("Time moved backward at least %g seconds."
		    "  Statistics will be wrong for that amount of time.\n",
		    timeval_diff_d(s->when, now));
	    notified = s->when;
	    logged = 1;
	}

	s->value += delta;
	return;
    }

    if (now.tv_sec == s->when.tv_sec)
    {
	if (vt == VT_AVG)
	{
	    time_delta = now.tv_usec - s->when.tv_usec;
	    s->acc += 1.0e-6 * (double)s->value * time_delta;
	    s->when.tv_usec = now.tv_usec;
	}
    }
    else if (now.tv_sec - s->when.tv_sec <= HISTORY_LENGTH)
    {
	if (vt == VT_AVG)
	{
	    time_delta = 1000000 - s->when.tv_usec;
	    s->acc += 1.0e-6 * (double)s->value * time_delta;
	}
	else
	    s->acc = s->value;

	update_history(s, s->when.tv_sec+1, s->acc);
	if (vt != VT_AVG)
	{
	    s->acc = 0;
	    s->value = 0;
	}
	update_history(s, now.tv_sec, s->value);
	assert(s->when.tv_sec == now.tv_sec);
	if (vt == VT_AVG)
	{
	    s->acc = 1.0e-6 * (double)s->value * now.tv_usec;
	    s->when.tv_usec = now.tv_usec;
	}
    }
    else
    {
	if (vt != VT_AVG)
	{
	    s->acc = 0;
	    s->value = 0;
	}
	for (i = 0; i < N_FACTS; i++)
	    s->avenrun[i] = s->value;
	for (i = 0; i < HISTORY_LENGTH; i++)
	    s->avg_history[i] = s->value;
	if (vt == VT_AVG)
	    s->acc = 1.0e-6 * (double)s->value * now.tv_usec;
	s->when = now;
	s->avenrun_updated = now.tv_sec;
    }

    s->value += delta;
    if (s->value < 0)
    {
	kom_log("update_stat(%d, %ld): current value became negative: %ld\n",
		st, delta, s->value);
	s->value = 0;
    }
}

void
update_stat(enum stat_type st,
	    long delta)
{
    struct timeval now;

    gettimeofday(&now, NULL);

    update_one_stat(st, VT_AVG, delta, now);
    if (delta >= 0)
	update_one_stat(st, VT_ASCEND, delta, now);
    if (delta <= 0)
	update_one_stat(st, VT_DESCEND, -delta, now);
}

static void
check_one_stat(enum stat_type st,
	       enum value_type vt)
{
    struct avg_status *s;
    double acc;
    float val;
    int f;
    int i;

    s = &status[st][vt];
    if (s->avenrun_updated == s->when.tv_sec)
	return;

    s->avenrun_updated = s->when.tv_sec;
    assert(factors[N_FACTS-1] == HISTORY_LENGTH);

    f = 0;
    acc = 0;
    for (i = 0; i < HISTORY_LENGTH; i++)
    {
	val = s->avg_history[ind(s->when.tv_sec, i+1)];
	assert(val >= 0);
	acc += val;

	assert(f < N_FACTS);
	if (i == factors[f] - 1)
	{
	    s->avenrun[f] = acc / factors[f];
	    f++;
	    assert(f < N_FACTS || i + 1== HISTORY_LENGTH);
	}
    }
    
    return;
}

static void
check_stat(enum stat_type st)
{
    enum value_type vt;

    for (vt = 0; vt < NUM_VT; vt++)
	check_one_stat(st, vt);
}


static const char *
name(enum stat_type st)
{
    switch (st)
    {
    case STAT_RUN_QUEUE:
	return "run-queue-length";

    case STAT_DNS_QUEUE:
	return "pending-dns";

    case STAT_IDENT_QUEUE:
	return "pending-ident";

    case STAT_CLIENTS:
	return "clients";

    case STAT_REQUESTS:
	return "reqs";

    case STAT_TEXTS:
	return "texts";

    case STAT_CONFS:
	return "confs";

    case STAT_PERSONS:
	return "persons";

    case STAT_SEND_QUEUE:
	return "send-queue-bytes";

    case STAT_RECV_QUEUE:
	return "recv-queue-bytes";

    case NUM_STAT:
	abort();
    }

    abort();
}

long
read_stat_value(enum stat_type st)
{
    return status[st][VT_AVG].value;
}

extern Success
get_stats_description(Stats_description *result)
{
    enum stat_type st;
    int i;

    CHK_CONNECTION(FAILURE);

    result->no_of_stats = NUM_STAT;
    result->stat_names = tmp_alloc(NUM_STAT * sizeof(result->stat_names[0]));

    for (st = 0; st < NUM_STAT; st++)
	result->stat_names[st] = s_fcrea_str(name(st));

    result->intervals.length = N_FACTS + 1;
    result->intervals.data = tmp_alloc(
	(N_FACTS + 1) * sizeof(result->intervals.data[0]));

    result->intervals.data[0] = 0; /* The first momentaneous value. */
    for (i = 0; i < N_FACTS; i++)
	result->intervals.data[i+1] = factors[i];

    return OK;
}

extern Success
get_stats(const String what,
	  Stats_list *result)
{
    enum stat_type st;
    int i;

    CHK_CONNECTION(FAILURE);

    for (st = 0; st < NUM_STAT; st++)
	if (s_streq(what, s_fcrea_str(name(st))))
	    break;

    if (st == NUM_STAT)
    {
	kom_errno = KOM_UNDEFINED_MEASUREMENT;
	err_stat = 0;
	return FAILURE;
    }

    /* Make sure we get the curren statistics, not some old value. */
    update_stat(st, 0);
    check_stat(st);

    result->no_of_stats = N_FACTS + 1;
    result->stats = tmp_alloc((N_FACTS + 1) * sizeof(result->stats[0]));

    result->stats[0].average = status[st][VT_AVG].value;
    result->stats[0].ascent_rate = 0;
    result->stats[0].descent_rate = 0;
    
    for (i = 0; i < N_FACTS; i++)
    {
	result->stats[i+1].average = status[st][VT_AVG].avenrun[i];
	result->stats[i+1].ascent_rate = status[st][VT_ASCEND].avenrun[i];
	result->stats[i+1].descent_rate = status[st][VT_DESCEND].avenrun[i];
    }

    return OK;
}
