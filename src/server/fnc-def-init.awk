#
# $Id: fnc-def-init.awk,v 0.12 2003/08/23 16:38:17 ceder Exp $
# Copyright (C) 1991, 1996, 1999, 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 
#
# $Id: fnc-def-init.awk,v 0.12 2003/08/23 16:38:17 ceder Exp $
BEGIN {
    printf("/* Don't edit this file - it is generated automatically");
    printf(" from\n   fnc-def-init.awk and fncdef.txt */\n\n");
    m = 0;
}
$1 == "#ifdef" {
    printf("#ifdef %s\n", $2);
    next;
}
$1 == "#endif" {
    printf("#endif\n");
    next;
}
$1 != "#" && $1 != "" {
    if (m < $1)
        m = $1;
    
    printf("    { ");

    printf("%s, ", $1);
    if ( $(NF-1) == ":" )
	printf("%7s, ", "rt_" $NF);
    else
	printf("%7s, ", "rt_" $2);

    printf("prot_a_parse_arg_%s },\n", $3);
}
END {
    printf("    { %d, rt_success, prot_a_hunt_nl }\n", m + 1);
}
