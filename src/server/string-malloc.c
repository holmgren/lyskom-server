/*
 * $Id: string-malloc.c,v 0.21 2003/08/23 16:38:13 ceder Exp $
 * Copyright (C) 1991, 1993-1995, 1998-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * Malloc wrappers for the string package.
 *
 * These functions call smalloc and also counts
 * how many allocated strings there are.
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#include <stdio.h>
#include <sys/types.h>

#include "exp.h"
#include "server/smalloc.h"
#include "string-malloc.h"

static int no_of_allocated_blocks = 0;

EXPORT  void *
string_malloc(size_t size)
{
   ++no_of_allocated_blocks;
   return smalloc (size);
}


EXPORT  void
string_free(void * ptr)
{
    --no_of_allocated_blocks;
    sfree(ptr);
}

EXPORT  void *
string_realloc (void * ptr,
		size_t size)
{
    if ( ptr == NULL )
	return string_malloc (size);

    return srealloc (ptr, size);
}


EXPORT void
dump_string_alloc_counts(FILE *stat_file)
{
    fprintf(stat_file, "---%s:\n\tAllocated strings:    %d\n",
	    __FILE__, no_of_allocated_blocks);
}
