/*
 * $Id: async.h,v 0.25 2003/08/23 16:38:18 ceder Exp $
 * Copyright (C) 1991, 1994-1999, 2001, 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: async.h,v 0.25 2003/08/23 16:38:18 ceder Exp $
 *
 * This file contains the parts of the async protocol that are common
 * to the server and the client.
 */


#ifndef LYSKOM_ASYNC_H
#define LYSKOM_ASYNC_H

/*
 * What does this packet contain? (See doc/Protocol-A.texi)
 */
enum async
{
	ay_new_text_old = 0,
			 	 /* 1 is no longer used. */
			 	 /* 2 is no longer used. */
#if 0
	ay_conf_deleted = 3,	/* Reserved for future use. */
	ay_conf_created = 4,	/* Reserved for future use. */
#endif	
	ay_new_name = 5,
	ay_i_am_on = 6,		/* Sends a Who_info when changeinge
				   conference or what-am-i-doing. */
	ay_sync_db = 7,		/* Database is syncing. */
	ay_leave_conf = 8,
	ay_login = 9,		/* Sends a Pers_no and connection
				   when someone logs in. */
				/* 10 is no longer used. */
	ay_rejected_connection = 11,
				/* A connection attempt was rejected
	       			   because LysKOM is full. */
	ay_send_message = 12,	/* A message is sent. */
	ay_logout = 13,		/* Someone logs out. */
        ay_deleted_text = 14,   /* A text is deleted */
        ay_new_text = 15,       /* New format created text */
        ay_new_recipient = 16,  /* New recipient added */
        ay_sub_recipient = 17,  /* Recipient removed */
        ay_new_membership = 18, /* Membership added or modified */
	ay_new_user_area = 19,	/* User-area changed. */
	ay_new_presentation = 20,
				/* Presentation changed. */
	ay_new_motd = 21,	/* Motd of conference changed. */
	ay_text_aux_changed = 22, /* Added and/or removed items. */

#ifdef DEBUG_CALLS
	ay_garb_ended = 1000,	/* Garb cycle complete */
#endif
        /* When you add stuff here, don't forget to add it to the
          switch in accept_async in session.c */

        ay_dummy_last           /* Keep this last in the list.
				   When adding new async messages you might
				   want to increase the default value for
				   parameter "Max accept_async len" in
				   server-config.c.   If this becomes
				   too large you may want to change
				   query_async to use a tmp_alloc()d
				   buffer instead of the static buffer
				   it currently uses. */
};

#define ASYNC_DEFAULT_MESSAGES  { ay_new_text_old, \
                                  ay_new_name, \
                                  ay_sync_db, \
                                  ay_leave_conf, \
                                  ay_login, \
                                  ay_rejected_connection, \
                                  ay_send_message, \
                                  ay_logout }



#endif
