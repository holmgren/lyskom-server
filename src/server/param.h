/*
 * $Id: param.h,v 1.49 2003/08/23 16:38:15 ceder Exp $
 * Copyright (C) 1994-1999, 2002-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * LysKOM parameters read from the configuration file. 
 */
#ifndef PARAM_H_INCLUDED
#define PARAM_H_INCLUDED

/* See server-config.c and lyskomd.texi for more info about the
   fields in this structure. */
struct kom_par
{
    char *dbase_dir;
    char *use_locale;
    Bool  send_async_messages;
    Bool  garb_enable;
    Bool  never_save;
    char *logaccess_file;
    char *ip_client_host;	/* IP to listen to (default: INADDR_ANY) */
    char *ip_client_port;       /* Port to listen to for clients */
    Bool  use_dns;
    double dns_log_threshold;
    Conf_no conf_pres_conf;
    Conf_no pers_pres_conf;
    Conf_no motd_conf;
    Conf_no kom_news_conf;
    Text_no motd_of_lyskom;
    char *datafile_name;
    char *backupfile_name;
    char *backupfile_name_2;
    char *lockfile_name;
    char *textfile_name;
    char *numberfile_name;
    char *numberfile_tmp_name;
    char *textbackupfile_name;	/* Only used in dbck */
    char *backup_dir;		/* Only used in splitkomdb */
    char *statistic_name;
    char *pid_name;
    char *memuse_name;
    char *logfile_name;
    char *connection_status_file;
    char *connection_status_file_tmp;
    char *aux_def_file;
    char *status_file;		/* Only used in komrunning and updateLysKOM */
    char *core_dir;
    char *nologin_file;
    char *lyskomd_path;
    char *savecore_path;
    struct timeval garb_busy_postponement;
    struct timeval garbtimeout;
    struct timeval synctimeout;
    struct timeval garb_interval;
    Bool permissive_sync;
    struct timeval sync_interval;
    struct timeval sync_retry_interval;
    struct timeval stale_timeout;
    int saved_items_per_call;
    unsigned int penalty_per_call;
    unsigned int penalty_per_read;
    unsigned int max_penalty;
    unsigned int low_penalty;
    unsigned int default_priority;
    unsigned int max_priority;
    unsigned int default_weight;
    unsigned int max_weight;
    struct timeval connect_timeout;
    struct timeval login_timeout;
    struct timeval active_timeout;
    int client_data_len;
    int conf_name_len;
    int pwd_len;
    int what_do_len;
    int username_len;
    int text_len;
    int aux_len;
    int broadcast_len;
    int regexp_len;
    int stat_name_len;
    int max_marks_person;
    int max_marks_text;
    int max_recipients;
    int max_comm;
    int max_foot;
    int max_crea_misc;
    int mark_as_read_chunk;
    int accept_async_len;
    int max_delete_aux;
    int max_add_aux;
    int max_read_ranges;
    int max_super_conf_loop;
    int default_nice;
    int default_keep_commented;
    int maxmsgsize;
    int maxqueuedsize_bytes;
    int maxqueuedsize;
    int maxdequeuelen;
    Bool anyone_can_create_new_persons;
    Bool anyone_can_create_new_confs;
    Bool create_person_before_login;
    Bool default_change_name;
    int cache_conferences;
    int cache_persons;
    int cache_text_stats;

    /* 0=never use IDENT,
       1=try to use IDENT,
       2=only allow login if IDENT succeeded. */    
    int authentication_level;
    Bool log_login;
    Bool force_iso_8859_1;
    int no_files;		/* -1=default */
    int normal_shutdown_time;	/* In minutes. */
    int downtime_mail_start;	/* In minutes. */
    int downtime_mail_end;	/* In minutes */
    char *sendmail_path;
    Bool invite_by_default;
    Bool secret_memberships;
    Bool allow_reinvite;
    unsigned long max_conf;
    unsigned long max_text;
};

extern struct kom_par param;

#endif
