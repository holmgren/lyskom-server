/*
 * Lock and unlock the database.
 * Copyright (C) 1999, 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

#include <config.h>

#include <sys/param.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#ifdef HAVE_STRING_H
#  include <string.h>
#else
#  ifdef HAVE_STRINGS_H
#    include <strings.h>
#  endif
#endif
#ifndef HAVE_STRCHR
#  define strchr index
#endif
#include <stdlib.h>
#include <signal.h>
#include <netdb.h>
#include "timewrap.h"

#include "lockdb.h"
#include "log.h"
#include "kom-types.h"
#include "param.h"
#include "lyskomd.h"

int lock_db(void)
{
    char *new_lock;
    char *current_lock;
    char *end;
    int retry;
    size_t sz;
    size_t ix;
    pid_t pid;

    size_t lock_sz = 16;
    do {
	new_lock = malloc(lock_sz + 3 + 3 * sizeof(pid_t));
	if (!new_lock) restart_kom("malloc failed (%d)\n", errno);
	int ret = gethostname(new_lock, lock_sz);
	if (ret == -1) {
	    if (errno == ENAMETOOLONG) {
		free(new_lock);
		lock_sz *= 2;
	    } else {
		restart_kom("gethostname failed (%d)\n", errno);
	    }
	} else break;
    } while (1);

    end = strchr(new_lock, '\0');
    sprintf(end, ":%ld", (long)getpid());

    for (retry = 0; retry < 2; ++retry)
    {
	if (symlink(new_lock, param.lockfile_name) == 0)
	{
	    kom_log("Created lock %s\n", param.lockfile_name);
	    return 0;
	}
	
	if (errno != EEXIST)
	    restart_kom("Failed to create lock symlink %s "
			"pointing to %s: %d\n", param.lockfile_name,
			new_lock, errno);

	if (retry)
	{
	    kom_log("Lock file recreated by some other party\n");
	    return -1;
	}

	lock_sz = 16;
	do {
	    current_lock = malloc(lock_sz);
	    sz = readlink(param.lockfile_name, current_lock, lock_sz);
	    if (sz >= lock_sz) {
		free(current_lock);
		lock_sz *= 2;
	    } else {
		break;
	    }
	} while (1);
	current_lock[sz] = '\0';

	for (ix = 0; ix < lock_sz && current_lock[ix] && new_lock[ix]; ++ix)
	{
	    if (new_lock[ix] != current_lock[ix])
	    {
		kom_log("Database already locked by %s\n", current_lock);
		free(new_lock); free(current_lock);
		return -1;
	    }

	    if (new_lock[ix] == ':')
		break;
	}

	pid = strtol(&current_lock[ix+1], &end, 10);
	if (end == &current_lock[ix+1])
	    restart_kom("Found a broken lock symlink %s: %s\n",
			param.lockfile_name, current_lock);

	if (kill(pid, 0) == 0 || errno != ESRCH)
	{
	    kom_log("Database already locked by %s\n", current_lock);
	    free(new_lock); free(current_lock);
	    return -1;
	}

	/* The process is dead.  Remove the stale lock file.  If it
	   was just removed by a dying process -- fine.  */
	if (remove(param.lockfile_name) < 0 && errno != ENOENT)
	    restart_kom("Failed to remove stale lock symlink %s\n",
			param.lockfile_name);
	else
	    kom_log("Removed stale lock file left by %s.\n", current_lock);

	free(new_lock); free(current_lock);
    }
    restart_kom("Unreachable code reached in lock_db.\n");
}

void
unlock_db(void)
{
    if (remove(param.lockfile_name) < 0)
	restart_kom("Failed to remove lock file %s\n", param.lockfile_name);
}
