/*
 * $Id: connections.h,v 0.77 2003/08/23 16:38:17 ceder Exp $
 * Copyright (C) 1991-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: connections.h,v 0.77 2003/08/23 16:38:17 ceder Exp $
 *
  * connections.h -- The top level of the communication packet.
  *
  * Requires kom-types.h, com.h, async.h
  */


#if 0    /* This comment isn't true yet. */
/* Connection establishment
   ========================

   When a new connection is accepted, the server starts to collect
   some information about the client: its domainname and IDENT
   information.  Until that information gathering is complete, the
   client will only be able to do its initial handshake.  It will not
   be visible to any other client, because the information is needed
   by the Static-Session-Info structure.  The client will, however, be
   noted in var/lyskomd.clients as soon as accept() returns.

   Once the client has performed the initial handshake (sent "A" plus
   the user%host hollerith string and received "LysKOM\n") the
   async-slow-external-system asynchronous messages will be sent to
   the client if an external system (currently only DNS or IDENT) is
   blocking the client.  Until all external systems has responded (or
   the server has given up on them) no input from the client will be
   accepted once the initial handshake is performed.

   If the initial handshake is not completed within a few minutes
   (configurable via FIXME (bug 627): what?) the client will be
   disconnected.

   DNS resolution
   ==============

   If no reply from the DNS system is received for a reasonable time,
   or if a NXDOMAIN error is returned, the raw IP address will be used
   instead.

   IDENT checking
   ==============

   The ``Ident-authentication'' configuration parameter controls if
   IDENT checking is done, and what happens if the IDENT checking
   returns an error or a timeout.

   At most ``Parallell Ident connections'' IDENT requests is performed
   at once.  If more clients are connected at once, the later clients
   will have to wait for the earlier IDENT requests to finish.
   Rationale: we reserve that many file descriptors to this task.
*/
#endif
    
enum kill_state
{
    ks_none,			/* Not yet killed. */
    ks_pending,			/* Kill pending. */
    ks_dying,			/* check_kill_flg() is killing this. */
};

typedef struct connection {
    struct connection * prev;
    struct connection * next;

    /* The following are used by services.c (which has been split
       into many files such as conference.c and person.c). */

    Pers_no		pers_no;         /* 0 if not yet logged in. */
    Person	      * person;	         /* NULL if not yet logged in. */
    struct timeval      connect_time;    /* When did the client connect? */
    struct timeval      login_time;      /* Time of connect, login or logout */
    Conf_no		cwc;	         /* Current working conference */
    String		what_am_i_doing;
    unsigned char	ena_level;       /* Enable level */
    String		username;        /* Given userid (from handshake). */
    String		ident_user; 	 /* Real username (IDENT lookup) */
    String              client_name;     /* See doc/clients.assigned */
    String              client_version;
    Session_flags       flags;
    Bool		username_valid;	 /* Set once username is set.
					    Under protocol A, no async
					    messages are sent until
					    this bit is set. */
    Bool                dns_done;        /* Set once DNS reverse
					    lookup is completed. */
    Bool                blocked_by_dns;	 /* Disabled waiting for DNS reply? */
    Bool                want_async[ay_dummy_last]; /* Async messages to get */
    
    /* The following are used by server/connections.c */
    
    struct isc_scb     *isc_session;
    String              remote_ip;       /* The IP address. */
    char               *peer;		 /* For dump_connections().
					    This holds the IP address,
					    a space, and the port
					    number of the peer.*/
    unsigned char	protocol;

    /* Should times be expressed in UTC or in the local timezone of
       the server?  */
    Bool                use_utc;

    int			parse_pos;
    int			fnc_parse_pos;
    int			array_parse_pos;
    int			array_parse_index;
    int                 array_parse_parsed_length;
    int			struct_parse_pos;
    int			string_parse_pos;
    int                 hunt_parse_pos;

    int                 array_hunt_num;
    int                 array_hunt_depth;

    int			ref_no;
    enum call_header    function;       /* Function to call. */
    int                 function_index; /* Index of function in server tables*/

    /* Gather data in these variables. */

    Number_list         num_list;
    long		num0;
    long		num1;
    long		num2;
    long		num3;
    long		num4;
    String		c_string0; /* These strings are used for strings
				    * that are *NOT* freed by services.c.
				    * They are freed by free_parsed() in
				    * connections.c */
    String		c_string1;
    String		string0; /* This string is used for strings that
				   * are freed by the routines in services.c */
    Misc_info_list      misc_info_list;
    Aux_item_list       aux_item_list;  /* Freed by free_parsed() */
    Aux_item            aux_item;       /* Freed by free_parsed() */
    Aux_item            dummy_aux_item;       /* Freed by free_parsed() */
    struct read_range_list read_range_list; /* Freed by free_parsed(). */
    Priv_bits		priv_bits;
    Conf_type		conf_type;
    Membership_type     membership_type;
    struct tm		time;
    Info                info;
    Personal_flags      pers_flags;

    /* Protocol independent things. */
    
    String              unparsed;
    String_size		first_to_parse; /* Index into unparsed. */
    Bool		more_to_parse;	/* Any chance that there is anything
					   left in unparsed? */
    enum kill_state     kill_status;    /* Is this client dying? */
    unsigned int        penalty;        /* Number of penalty points
					   for this session. */
    unsigned int        penalty_generation; /* Penalties are aged. */

    /* Once the penalty points are too high, the session will be
       disabled and put on a wait queue, which is a double linked list
       implemented via these fields.  */
    struct connection  *queue_prev;
    struct connection  *queue_next;
    Bool                on_queue;

    Scheduling_info     schedule;
    
    struct timeval      active_time;    /* Set every time the client sends
					   user_active. */
    
    Session_no		session_no;      /* A unique number. */
} Connection;
/*
 * It is guaranteed that in the Connection struct the pers_no and person 
 * fields are either both 0/NULL, or both non-0/non-NULL.
 */


/*
 * Data for the isc-server.
 */
extern struct isc_mcb *kom_server_mcb;

/*
 * active_connection points to the info about the client who initiated this
 * call. This is set by connectins.c whenever a complete message is parsed
 * and a function in services.c is called.
 */
extern Connection * active_connection;


/*
 * This enum describes the result of a function in services.c.
 */
enum res_type
{
    rt_number,			/* E.g. Pers_no, Conf_no, Text_no.
				   This is somewhat special in that the
				   function indicates an error by returning
				   0 and not FAILURE. */
    rt_success,			/* Only return a Success. */
    /* Functions of the following types returns a Success _and_ data
       of the indicated type. The corresponding types are found in
       kom-types.h */
    rt_person,
    rt_membership,
    rt_membership_old,
    rt_membership_10,
    rt_conf_list,
    rt_conf_no_list,
    rt_conference_old,
    rt_string,
    rt_mark_list,
    rt_text_stat_old,
    rt_who_info_list,
    rt_who_info_list_old,
    rt_info_old,
    rt_membership_list,
    rt_membership_list_old,
    rt_membership_list_10,
    rt_member_list,
    rt_member_list_old,
    rt_time_date,
    rt_session_info,
    rt_session_no,
    rt_text_no,
    rt_conf_no,
    rt_session_info_ident,
    rt_who_info_ident_list,
    rt_conf_z_info_list,
    rt_version_info,
    rt_uconference,
    rt_num_list,
    rt_dynamic_session_info_list,
    rt_static_session_info,
    rt_text_stat,
    rt_conference,
    rt_info,
    rt_l2g_iterator_as_text_list,
    rt_text_mapping,
    rt_text_mapping_reverse,
    rt_stats_description,
    rt_stats_list,
    rt_static_server_info,
    rt_scheduling_info,
#ifdef DEBUG_CALLS
    rt_memory_info,
#endif
};

/*
 * The result from a call is stored in this union.
 *
 * The pointers in these structures are normally set to point into
 * data from the cache, and should thus not be free()d. In those cases
 * where this actually contains newly allocated memory (and it is not
 * allocated via tmp_alloc()) it should be free()d in prot_a_reply() in
 * prot-a.c.
 */
union result_holder
{
        unsigned long		  number;
    	Person			  person;
	Membership		  membership;
	Membership		  membership_10;
	Membership		  membership_old;
	Conf_list_old		  conf_list;
	Conf_no_list		  conf_no_list;
	Conference		  conference;
	Conference		  conference_old;
	Mark_list		  mark_list;
	String			  string;
	Text_stat		  text_stat;
        Text_stat                 text_stat_old;
	Info			  info;
	Info			  info_old;
	Who_info_list		  who_info_list;
	Who_info_list_old	  who_info_list_old;
	Membership_list		  membership_list;
	Membership_list		  membership_list_10;
	Membership_list		  membership_list_old;
	Member_list		  member_list;
        Member_list               member_list_old;
	time_t			  time_date;
	Session_info		  session_info;
	Session_no		  session_no;
	Text_no			  text_no;
	Conf_no			  conf_no;
	Who_info_ident_list	  who_info_ident_list;
	Session_info_ident	  session_info_ident;
	Conf_z_info_list          conf_z_info_list;
        Version_info              version_info;
	Small_conf		  uconference;
        Number_list               num_list;
	Dynamic_session_info_list dynamic_session_info_list;
	Static_session_info	  static_session_info;
	L2g_iterator		  l2g_iterator_as_text_list;
	Text_mapping              text_mapping;
	Text_mapping_reverse      text_mapping_reverse;
	Stats_description         stats_description;
	Stats_list                stats_list;
	Static_server_info        static_server_info;
	Scheduling_info           scheduling_info;
#ifdef DEBUG_CALLS
        Memory_info               memory_info;
#endif
};


typedef struct {
    enum call_header function;
    enum res_type    result;
    /* The function that is used to parse args for this function: */
    void          (*parser)(Connection *client);
} Fnc_descriptor;

extern const Fnc_descriptor fnc_defs[];
extern const int num_fnc_defs;

/*
 * This array holds number of calls to each service. It is dumped to the
 * log file every time the server syncs.
 */
extern unsigned long service_statistics[];

extern jmp_buf parse_env;

extern void
toploop(void);

extern void
dump_statistics(void);

extern void
logout_all_clients(void);

extern void
add_to_kill_list(Connection *conn);

/* Functions used as ISC callbacks. */
extern void *
handle_accept_event(struct isc_scb *accepting_session,
		    struct isc_scb *new_session);

/* Return true if the server is currently idle. */
extern Bool
server_idle(void);

/* Set the current_time global variable. */
extern void
set_time(void);
