# Test suite for lyskomd.
# Copyright (C) 2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test the bad-bool error code.

lyskomd_start

client_start 0
send "A[holl "foo%bar"]\n"
simple_expect "LysKOM" "connected"
send "1000 80 1 { 9 }\n"
simple_expect "=1000"

# Try stuff without logging in.

# get-membership-old
send "1001 46 5 0 1 2\n"
simple_expect "%1001 61 0"

# login
send "1002 62 5 [holl "gazonk"] 2\n"
simple_expect "%1002 61 0"

# re-z-lookup
send "1003 74 [holl "."] 1 2\n"
simple_expect "%1003 61 0"
send "1004 74 [holl "."] 2 1\n"
simple_expect "%1004 61 0"
send "1005 74 [holl "."] 2 2\n"
simple_expect "%1005 61 0"

# lookup-z-name
send "1006 76 [holl "n"] 2 1\n"
simple_expect "%1006 61 0"
send "1007 76 [holl "n"] 1 2\n"
simple_expect "%1007 61 0"
send "1008 76 [holl "n"] 2 2\n"
simple_expect "%1008 61 0"

# who-is-on-dynamic
send "1009 83 2 1 0\n"
simple_expect "%1009 61 0"
send "1010 83 1 2 0\n"
simple_expect "%1010 61 0"
send "1011 83 2 2 0\n"
simple_expect "%1011 61 0"

# get-membership-10
send "1012 99 5 0 1 2\n"
simple_expect "%1012 61 0"

# query-read-texts
send "1013 107 5 5 2 0\n"
simple_expect "%1013 61 0"

# get-membership
send "1014 108 5 0 1 2 0\n"
simple_expect "%1014 61 0"

# set-connection-time-format
send "1015 120 2\n"
simple_expect "%1015 61 0"
send "1016 120 3\n"
simple_expect "%1016 61 0"
send "1017 120 4\n"
simple_expect "%1017 61 0"
send "1018 120 255\n"
simple_expect "%1018 61 0"
send "1019 120 256\n"
simple_expect "%1019 61 0"
send "1020 120 257\n"
simple_expect "%1020 61 0"
send "1021 120 65536\n"
simple_expect "%1021 61 0"
send "1022 120 65537\n"
simple_expect "%1022 61 0"

# Log in as admin.
send "1023 0 5 [holl "gazonk"]\n"
simple_expect ":2 9 5 1"
simple_expect "=1023"

# Try the stuff again.

# get-membership-old
send "1024 46 5 0 1 2\n"
simple_expect "%1024 61 0"

# login
send "1025 62 5 [holl "gazonk"] 2\n"
simple_expect "%1025 61 0"

# re-z-lookup
send "1026 74 [holl "."] 1 2\n"
simple_expect "%1026 61 0"
send "1027 74 [holl "."] 2 1\n"
simple_expect "%1027 61 0"
send "1028 74 [holl "."] 2 2\n"
simple_expect "%1028 61 0"

# lookup-z-name
send "1029 76 [holl "n"] 2 1\n"
simple_expect "%1029 61 0"
send "1030 76 [holl "n"] 1 2\n"
simple_expect "%1030 61 0"
send "1031 76 [holl "n"] 2 2\n"
simple_expect "%1031 61 0"

# who-is-on-dynamic
send "1032 83 2 1 0\n"
simple_expect "%1032 61 0"
send "1033 83 1 2 0\n"
simple_expect "%1033 61 0"
send "1034 83 2 2 0\n"
simple_expect "%1034 61 0"

# get-membership-10
send "1035 99 5 0 1 2\n"
simple_expect "%1035 61 0"

# query-read-texts
send "1036 107 5 5 2 0\n"
simple_expect "%1036 61 0"

# get-membership
send "1037 108 5 0 1 2 0\n"
simple_expect "%1037 61 0"

# set-connection-time-format
send "1038 120 2\n"
simple_expect "%1038 61 0"
send "1039 120 3\n"
simple_expect "%1039 61 0"
send "1040 120 4\n"
simple_expect "%1040 61 0"
send "1041 120 255\n"
simple_expect "%1041 61 0"
send "1042 120 256\n"
simple_expect "%1042 61 0"
send "1043 120 257\n"
simple_expect "%1043 61 0"
send "1044 120 65536\n"
simple_expect "%1044 61 0"
send "1045 120 65537\n"
simple_expect "%1045 61 0"

# And now run the tests with valid bools, just to make sure the test
# cases are OK.

# get-membership-old
send "1046 46 5 0 1 0\n"
simple_expect "=1046 1 { $any_time 5 255 0 0 \\* }"

# login (no need to give proper password, as we are admin).
send "1047 62 5 [holl "dobedo"] 0\n"
simple_expect ":2 9 5 1"
simple_expect "=1047"

# re-z-lookup
send "1048 74 [holl "."] 1 1\n"
simple_expect "=1048 5 { [holl "Presentation .av nya. m�ten"] 0000 1 [holl "Presentation .av nya. medlemmar"] 0000 2 [holl "Lappar .p�. d�rren"] 0000 3 [holl "Nyheter om LysKOM"] 0000 4 [holl "Administrat�r .f�r. LysKOM"] 1001 5 }"
send "1049 74 [holl "."] 0 1\n"
simple_expect "=1049 4 { [holl "Presentation .av nya. m�ten"] 0000 1 [holl "Presentation .av nya. medlemmar"] 0000 2 [holl "Lappar .p�. d�rren"] 0000 3 [holl "Nyheter om LysKOM"] 0000 4 }"
send "1050 74 [holl "."] 1 0\n"
simple_expect "=1050 1 { [holl "Administrat�r .f�r. LysKOM"] 1001 5 }"

# lookup-z-name
send "1051 76 [holl "n"] 0 1\n"
simple_expect "=1051 1 { [holl "Nyheter om LysKOM"] 0000 4 }"
send "1052 76 [holl "n"] 1 0\n"
simple_expect "=1052 0 \\*"
send "1053 76 [holl "n"] 1 1\n"
simple_expect "=1053 1 { [holl "Nyheter om LysKOM"] 0000 4 }"

# who-is-on-dynamic
send "1054 83 0 1 0\n"
simple_expect "=1054 0 \\*"
send "1055 83 1 0 0\n"
simple_expect "=1055 1 { 1 5 0 $any_num 00000000 [holl ""] }"
send "1056 83 1 1 0\n"
simple_expect "=1056 1 { 1 5 0 $any_num 00000000 [holl ""] }"

# get-membership-10
send "1057 99 5 0 1 1\n"
simple_expect "=1057 1 { 0 $any_time 5 255 0 0 \\* 5 $any_time 00000000 }"
send "1058 99 5 0 1 0\n"
simple_expect "=1058 1 { 0 $any_time 5 255 0 0 \\* 5 $any_time 00000000 }"

# query-read-texts
send "1059 107 5 5 1 0\n"
simple_expect "=1059 0 $any_time 5 255 0 \\* 5 $any_time 00000000"
send "1060 107 5 5 0 0\n"
simple_expect "=1060 0 $any_time 5 255 0 \\* 5 $any_time 00000000"

# get-membership
send "1061 108 5 0 1 1 0\n"
simple_expect "=1061 1 { 0 $any_time 5 255 0 \\* 5 $any_time 00000000 }"
send "1062 108 5 0 1 0 0\n"
simple_expect "=1062 1 { 0 $any_time 5 255 0 \\* 5 $any_time 00000000 }"

# set-connection-time-format
send "1063 120 1\n"
simple_expect "=1063"
send "1064 120 0\n"
simple_expect "=1064"

# Shut down.
send "1065 42 255\n"
simple_expect "=1065"

send "1066 44 0\n"
simple_expect "=1066"

lyskomd_death
client_death 0
