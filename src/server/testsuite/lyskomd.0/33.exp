# Test suite for lyskomd.
# Copyright (C) 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test async-new-user-area.


lyskomd_start

client_start 0
talk_to client 0
send "A0H\n"
simple_expect "LysKOM"
send "1000 80 1 { 19 }\n"
simple_expect "=1000"
send "1001 62 5 6Hgazonk 0\n"
simple_expect "=1001"
send "1002 42 255\n"
simple_expect "=1002"

# Create the user "Supervisor".  Log in as it.
client_start 1
talk_to client 1
send "A0H\n"
simple_expect "LysKOM"
send "1003 80 1 { 19 }\n"
simple_expect "=1003"
send "1004 89 [holl "Supervisor"] [holl "none"] 00000000 0 { }\n"
simple_expect "=1004 6"
send "1005 62 6 [holl "none"] 0\n"
simple_expect "=1005"

# As "Supervisor", create "Slave".
send "1006 89 [holl "Slave"] [holl "none"] 00000000 0 { }\n"
simple_expect "=1006 7"

# Log in as "Slave"
client_start 2
talk_to client 2
send "A0H\n"
simple_expect "LysKOM"
send "1007 80 1 { 19 }\n"
simple_expect "=1007"
send "1008 62 7 [holl "none"] 0\n"
simple_expect "=1008"

# Create the user "Bystander".  Log in as it.
client_start 3
talk_to client 3
send "A0H\n"
simple_expect "LysKOM"
send "1009 80 1 { 19 }\n"
simple_expect "=1009"
send "1010 89 [holl "Bystander"] [holl "none"] 00000000 0 { }\n"
simple_expect "=1010 8"
send "1011 62 8 [holl "none"] 0\n"
simple_expect "=1011"

# Set a user-area for "Supervisor".
talk_to client 1
send "1012 86 [holl "user-area 1 for Supervisor"] 0 { } 0 { }\n"
simple_expect "=1012 1"
send "1013 57 6 1\n"
simple_expect ":3 19 6 0 1"
simple_expect "=1013"
talk_to client 0
simple_expect ":3 19 6 0 1"

# Change user-area for "Supervisor".
talk_to client 1
send "1014 86 [holl "user-area 2 for Supervisor"] 0 { } 0 { }\n"
simple_expect "=1014 2"
send "1015 57 6 2\n"
simple_expect ":3 19 6 1 2"
simple_expect "=1015"
talk_to client 0
simple_expect ":3 19 6 1 2"

# Set a user-area for "Slave".
talk_to client 2
send "1016 86 [holl "user-area 1 for Slave"] 0 { } 0 { }\n"
simple_expect "=1016 3"
send "1017 57 7 3\n"
simple_expect ":3 19 7 0 3"
simple_expect "=1017"
talk_to client 0
simple_expect ":3 19 7 0 3"
talk_to client 1
simple_expect ":3 19 7 0 3"

# Change user-area for "Slave".
talk_to client 2
send "1018 86 [holl "user-area 2 for Slave"] 0 { } 0 { }\n"
simple_expect "=1018 4"
send "1019 57 7 4\n"
simple_expect ":3 19 7 3 4"
simple_expect "=1019"
talk_to client 0
simple_expect ":3 19 7 3 4"
talk_to client 1
simple_expect ":3 19 7 3 4"

# Set a user-area for "Bystander".
talk_to client 3
send "1016 86 [holl "user-area 1 for Bystander"] 0 { } 0 { }\n"
simple_expect "=1016 5"
send "1017 57 8 5\n"
simple_expect ":3 19 8 0 5"
simple_expect "=1017"
talk_to client 0
simple_expect ":3 19 8 0 5"

# Change user-area for "Bystander".
talk_to client 3
send "1016 86 [holl "user-area 2 for Bystander"] 0 { } 0 { }\n"
simple_expect "=1016 6"
send "1017 57 8 6\n"
simple_expect ":3 19 8 5 6"
simple_expect "=1017"
talk_to client 0
simple_expect ":3 19 8 5 6"

# Don't change user-area for "Bystander".
talk_to client 3
send "1017 57 8 6\n"
simple_expect "=1017"

# Shut everything down.
talk_to client 0
send "1020 44 0\n"
simple_expect "=1020"
client_death 0
client_death 1
client_death 2
client_death 3
lyskomd_death
