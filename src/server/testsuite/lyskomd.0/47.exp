# Test suite for lyskomd.
# Copyright (C) 2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test client disconnect while logged in and receiving async-logout.

lyskomd_start

# Log in as admin on client 2.
client_start 2
send "A[holl "two"]\n"
simple_expect "LysKOM" "connected"
send "1000 80 1 { 13 }\n"
simple_expect "=1000"
send "1001 0 5 [holl "gazonk"]\n"
simple_expect "=1001"

# Put something in the input buffer of the client, but don't read it.
# Without this, the bug isn't triggered.  With this, it is.  At least
# when running Linux 2.4.18pre1 with glibc-2.3.1.  I suspect that the
# exact behaviour is highly depent on your TCP/IP stack, but I don't
# have a deep enough understanding of that to be really sure.

send "#suspend socket\n"
simple_expect "suspended" "" meta
send "1002 35\n"

# Disconnect client 2.
kill_client 2

# Log in as admin on client 0.
client_start 0
send "A[holl "zero"]\n"
simple_expect "LysKOM" "connected"
send "1003 80 1 { 13 }\n"
simple_expect "=1003"
send "1004 0 5 [holl "gazonk"]\n"
simple_expect "=1004"

# Log in as admin on client 1.
client_start 1
send "A[holl "one"]\n"
simple_expect "LysKOM" "connected"
send "1005 80 1 { 13 }\n"
simple_expect "=1005"
send "1006 0 5 [holl "gazonk"]\n"
simple_expect "=1006"

talk_to client 0
send "#suspend socket\n"
simple_expect "suspended" "" meta
send "1007 35\n"

# Disconnect client 0.
kill_client 0

talk_to client 1
simple_expect ":2 13 5 2"

# Shut down.
send "1008 42 255\n"
simple_expect "=1008"

send "1009 44 0\n"
simple_expect "=1009"

lyskomd_death
client_death 1
