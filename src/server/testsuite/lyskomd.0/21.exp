# Test suite for lyskomd.
# Copyright (C) 2001, 2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Check the owner-delete feature of aux-items.
# Note: there is currently no aux-item with owner-delete that can be
# used on texts, so that functionality is not tested.

# Database setup.
#
# Persons:
#   0: person  5: administrator.
#   1: person  6: supervisor of 7.
#   2: person  7: owner of aux-items: creator of conf 12, text 1, person 8.
#   3: person  8: subpersona of person 7.
#   4: person  9: innocent bystander.
#   5: person 10: supervisor of person 11.
#   6: person 11: creator of aux-items: subpersona of 10.
#
# Conferences:
#   conference 12: created by 7.
#
# Texts:
#   text 1: created by 7.

read_versions

lyskomd_start

# Start client 0: person 5 (administrator), enabled.

client_start 0
talk_to client 0
send "A3Hfoo\n"
simple_expect "LysKOM" "connected"
send "1000 80 0 { }\n"
simple_expect "=1000"
send "1001 62 5 [holl "gazonk"] 1\n"
simple_expect "=1001"
send "1002 42 255\n"
simple_expect "=1002"

# Start client 1: person 6.

client_start 1
talk_to client 1
send "A3Hbar\n"
simple_expect "LysKOM"
send "1003 80 0 { }\n"
simple_expect "=1003"
send "1004 89 [holl "Person 6"] [holl "password"] 00000000 0 { }\n"
simple_expect "=1004 6"
send "1005 62 6 [holl "password"] 0\n"
simple_expect "=1005"

# Create person 7.
send "1006 89 [holl "Person 7"] [holl "password"] 00000000 0 { }\n"
simple_expect "=1006 7"
send "1007 91 7\n"
simple_expect "=1007 [holl "Person 7"] 10011000 $any_time $any_time 6 0 6 0 6 0 77 77 1 1 0 0 0 \\*"

# Start client 2: person 7.

client_start 2
talk_to client 2
send "A3Hbar\n"
simple_expect "LysKOM"
send "1008 80 0 { }\n"
simple_expect "=1008"
send "1009 62 7 [holl "password"] 0\n"
simple_expect "=1009"

# Create person 8.
send "1010 89 [holl "Person 8"] [holl "password"] 00000000 0 { }\n"
simple_expect "=1010 8"
send "1011 91 8\n"
simple_expect "=1011 [holl "Person 8"] 10011000 $any_time $any_time 7 0 7 0 7 0 77 77 1 1 0 0 0 \\*"

# Start client 3: person 8.

client_start 3
talk_to client 3
send "A3Hbar\n"
simple_expect "LysKOM"
send "1012 80 0 { }\n"
simple_expect "=1012"
send "1013 62 8 [holl "password"] 0\n"
simple_expect "=1013"

# Start client 4: person 9.

client_start 4
talk_to client 4
send "A3Hfoo\n"
simple_expect "LysKOM"
send "1014 80 0 { }\n"
simple_expect "=1014"
send "1015 89 [holl "Person 9"] [holl "password"] 00000000 0 { }\n"
simple_expect "=1015 9"
send "1016 62 9 [holl "password"] 0\n"
simple_expect "=1016"

# Start client 5: person 10.

client_start 5
talk_to client 5
send "A3Hbar\n"
simple_expect "LysKOM"
send "1017 80 0 { }\n"
simple_expect "=1017"
send "1018 89 [holl "Person 10"] [holl "password"] 00000000 0 { }\n"
simple_expect "=1018 10"
send "1019 62 10 [holl "password"] 0\n"
simple_expect "=1019"

# Create person 11.
send "1020 89 [holl "Person 11"] [holl "password"] 00000000 0 { }\n"
simple_expect "=1020 11"
send "1021 91 11\n"
simple_expect "=1021 [holl "Person 11"] 10011000 $any_time $any_time 10 0 10 0 10 0 77 77 1 1 0 0 0 \\*"

# Start client 6: person 11.

client_start 6
talk_to client 6
send "A3Hbar\n"
simple_expect "LysKOM"
send "1022 80 0 { }\n"
simple_expect "=1022"
send "1023 62 11 [holl "password"] 0\n"
simple_expect "=1023"

# Create conference 12 and text 1.

talk_to client 2
send "1024 88 [holl "Conference 12"] 00000000 0 { }\n"
simple_expect "=1024 12"
send "1025 86 [holl "Text 1"] 1 { 0 12 } 0 { }\n"
simple_expect "=1025 1"

#
# Now that the database is set up, perform some real tests.
#

#
# Test 8: redirect.
#

# Delete aux-item with owner-delete when creator.

talk_to client 6
# Check that conference 12 and text 1 are devoid of aux-items.
send "1026 90 1\n"
simple_expect "=1026 $any_time 7 0 6 0 2 { 0 12 6 1 } 0 \\*"
send "1027 91 12\n"
simple_expect "=1027 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 0 \\*"

# Set supervisor of 12 to 11.
talk_to client 2
send "1028 18 12 11\n"
simple_expect "=1028"

talk_to client 6
send "1029 93 12 0 { } 1 { 8 00000000 1 [holl "E-mail:ceder@lysator.liu.se"] }\n"
simple_expect "=1029"

# Reset supervisor of 12 to 7.
send "1030 18 12 7\n"
simple_expect "=1030"

send "1031 91 12\n"
simple_expect "=1031 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 1 { 1 8 11 $any_time 00000000 1 [holl "E-mail:ceder@lysator.liu.se"] }"

send "1032 93 12 1 { 1 } 0 { }\n"
simple_expect "=1032"

# Delete aux-item with owner-delete when owner.

talk_to client 6
# Check that conference 12 and text 1 are devoid of aux-items.
send "1033 90 1\n"
simple_expect "=1033 $any_time 7 0 6 0 2 { 0 12 6 1 } 0 \\*"
send "1034 91 12\n"
simple_expect "=1034 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 0 \\*"

# Set supervisor of 12 to 11.
talk_to client 2
send "1035 18 12 11\n"
simple_expect "=1035"

talk_to client 6
send "1036 93 12 0 { } 1 { 8 00000000 1 [holl "E-mail:ceder@lysator.liu.se"] }\n"
simple_expect "=1036"

# Reset supervisor of 12 to 7.
send "1037 18 12 7\n"
simple_expect "=1037"

send "1038 91 12\n"
simple_expect "=1038 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 1 { 2 8 11 $any_time 00000000 1 [holl "E-mail:ceder@lysator.liu.se"] }"

talk_to client 2

send "1039 93 12 1 { 2 } 0 { }\n"
simple_expect "=1039"

# Delete aux-item with owner-delete when supervisor of creator.

talk_to client 6
# Check that conference 12 and text 1 are devoid of aux-items.
send "1040 90 1\n"
simple_expect "=1040 $any_time 7 0 6 0 2 { 0 12 6 1 } 0 \\*"
send "1041 91 12\n"
simple_expect "=1041 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 0 \\*"

# Set supervisor of 12 to 11.
talk_to client 2
send "1042 18 12 11\n"
simple_expect "=1042"

talk_to client 6
send "1043 93 12 0 { } 1 { 8 00000000 1 [holl "E-mail:ceder@lysator.liu.se"] }\n"
simple_expect "=1043"

# Reset supervisor of 12 to 7.
send "1044 18 12 7\n"
simple_expect "=1044"

send "1045 91 12\n"
simple_expect "=1045 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 1 { 3 8 11 $any_time 00000000 1 [holl "E-mail:ceder@lysator.liu.se"] }"

talk_to client 5

send "1046 93 12 1 { 3 } 0 { }\n"
simple_expect "=1046"

# Delete aux-item with owner-delete when supervisor of owner.

talk_to client 6
# Check that conference 12 and text 1 are devoid of aux-items.
send "1047 90 1\n"
simple_expect "=1047 $any_time 7 0 6 0 2 { 0 12 6 1 } 0 \\*"
send "1048 91 12\n"
simple_expect "=1048 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 0 \\*"

# Set supervisor of 12 to 11.
talk_to client 2
send "1049 18 12 11\n"
simple_expect "=1049"

talk_to client 6
send "1050 93 12 0 { } 1 { 8 00000000 1 [holl "E-mail:ceder@lysator.liu.se"] }\n"
simple_expect "=1050"

# Reset supervisor of 12 to 7.
send "1051 18 12 7\n"
simple_expect "=1051"

send "1052 91 12\n"
simple_expect "=1052 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 1 { 4 8 11 $any_time 00000000 1 [holl "E-mail:ceder@lysator.liu.se"] }"

talk_to client 6

send "1053 93 12 1 { 4 } 0 { }\n"
simple_expect "=1053"

# Delete aux-item with owner-delete when ENA.

talk_to client 6
# Check that conference 12 and text 1 are devoid of aux-items.
send "1054 90 1\n"
simple_expect "=1054 $any_time 7 0 6 0 2 { 0 12 6 1 } 0 \\*"
send "1055 91 12\n"
simple_expect "=1055 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 0 \\*"

# Set supervisor of 12 to 11.
talk_to client 2
send "1056 18 12 11\n"
simple_expect "=1056"

talk_to client 6
send "1057 93 12 0 { } 1 { 8 00000000 1 [holl "E-mail:ceder@lysator.liu.se"] }\n"
simple_expect "=1057"

# Reset supervisor of 12 to 7.
send "1058 18 12 7\n"
simple_expect "=1058"

send "1059 91 12\n"
simple_expect "=1059 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 1 { 5 8 11 $any_time 00000000 1 [holl "E-mail:ceder@lysator.liu.se"] }"

talk_to client 0

send "1060 93 12 1 { 5 } 0 { }\n"
simple_expect "=1060"

# Delete aux-item with owner-delete when someone else.

talk_to client 6
# Check that conference 12 and text 1 are devoid of aux-items.
send "1061 90 1\n"
simple_expect "=1061 $any_time 7 0 6 0 2 { 0 12 6 1 } 0 \\*"
send "1062 91 12\n"
simple_expect "=1062 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 0 \\*"

# Set supervisor of 12 to 11.
talk_to client 2
send "1063 18 12 11\n"
simple_expect "=1063"

talk_to client 6
send "1064 93 12 0 { } 1 { 8 00000000 1 [holl "E-mail:ceder@lysator.liu.se"] }\n"
simple_expect "=1064"

# Reset supervisor of 12 to 7.
send "1065 18 12 7\n"
simple_expect "=1065"

send "1066 91 12\n"
simple_expect "=1066 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 1 { 6 8 11 $any_time 00000000 1 [holl "E-mail:ceder@lysator.liu.se"] }"

talk_to client 4

send "1067 93 12 1 { 6 } 0 { }\n"
simple_expect "%1067 49 0"

# Check that Conference 12 looks OK.
send "1068 91 12\n"
simple_expect "=1068 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 1 { 6 8 11 $any_time 00000000 1 [holl "E-mail:ceder@lysator.liu.se"] }"


#
# Test 14: faq-text
#

# Delete aux-item with owner-delete when creator.

talk_to client 6
# Ensure that conference 12 and text 1 are devoid of aux-items.
send "1069 93 12 1 { 6 } 0 { }\n"
simple_expect "=1069"
send "1070 90 1\n"
simple_expect "=1070 $any_time 7 0 6 0 2 { 0 12 6 1 } 0 \\*"
send "1071 91 12\n"
simple_expect "=1071 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 0 \\*"

# Set supervisor of 12 to 11.
talk_to client 2
send "1072 18 12 11\n"
simple_expect "=1072"

talk_to client 6
send "1073 93 12 0 { } 1 { 14 00000000 1 [holl "1"] }\n"
simple_expect "=1073"

# Reset supervisor of 12 to 7.
send "1074 18 12 7\n"
simple_expect "=1074"

send "1075 91 12\n"
simple_expect "=1075 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 1 { 7 14 11 $any_time 00000000 1 [holl "1"] }"

send "1076 93 12 1 { 7 } 0 { }\n"
simple_expect "=1076"

# Delete aux-item with owner-delete when owner.

talk_to client 6
# Check that conference 12 and text 1 are devoid of aux-items.
send "1077 90 1\n"
simple_expect "=1077 $any_time 7 0 6 0 2 { 0 12 6 1 } 0 \\*"
send "1078 91 12\n"
simple_expect "=1078 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 0 \\*"

# Set supervisor of 12 to 11.
talk_to client 2
send "1079 18 12 11\n"
simple_expect "=1079"

talk_to client 6
send "1080 93 12 0 { } 1 { 14 00000000 1 [holl "1"] }\n"
simple_expect "=1080"

# Reset supervisor of 12 to 7.
send "1081 18 12 7\n"
simple_expect "=1081"

send "1082 91 12\n"
simple_expect "=1082 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 1 { 8 14 11 $any_time 00000000 1 [holl "1"] }"

talk_to client 2

send "1083 93 12 1 { 8 } 0 { }\n"
simple_expect "=1083"

# Delete aux-item with owner-delete when supervisor of creator.

talk_to client 6
# Check that conference 12 and text 1 are devoid of aux-items.
send "1084 90 1\n"
simple_expect "=1084 $any_time 7 0 6 0 2 { 0 12 6 1 } 0 \\*"
send "1085 91 12\n"
simple_expect "=1085 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 0 \\*"

# Set supervisor of 12 to 11.
talk_to client 2
send "1086 18 12 11\n"
simple_expect "=1086"

talk_to client 6
send "1087 93 12 0 { } 1 { 14 00000000 1 [holl "1"] }\n"
simple_expect "=1087"

# Reset supervisor of 12 to 7.
send "1088 18 12 7\n"
simple_expect "=1088"

send "1089 91 12\n"
simple_expect "=1089 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 1 { 9 14 11 $any_time 00000000 1 [holl "1"] }"

talk_to client 5

send "1090 93 12 1 { 9 } 0 { }\n"
simple_expect "=1090"

# Delete aux-item with owner-delete when supervisor of owner.

talk_to client 6
# Check that conference 12 and text 1 are devoid of aux-items.
send "1091 90 1\n"
simple_expect "=1091 $any_time 7 0 6 0 2 { 0 12 6 1 } 0 \\*"
send "1092 91 12\n"
simple_expect "=1092 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 0 \\*"

# Set supervisor of 12 to 11.
talk_to client 2
send "1093 18 12 11\n"
simple_expect "=1093"

talk_to client 6
send "1094 93 12 0 { } 1 { 14 00000000 1 [holl "1"] }\n"
simple_expect "=1094"

# Reset supervisor of 12 to 7.
send "1095 18 12 7\n"
simple_expect "=1095"

send "1096 91 12\n"
simple_expect "=1096 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 1 { 10 14 11 $any_time 00000000 1 [holl "1"] }"

talk_to client 6

send "1097 93 12 1 { 10 } 0 { }\n"
simple_expect "=1097"

# Delete aux-item with owner-delete when ENA.

talk_to client 6
# Check that conference 12 and text 1 are devoid of aux-items.
send "1098 90 1\n"
simple_expect "=1098 $any_time 7 0 6 0 2 { 0 12 6 1 } 0 \\*"
send "1099 91 12\n"
simple_expect "=1099 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 0 \\*"

# Set supervisor of 12 to 11.
talk_to client 2
send "1100 18 12 11\n"
simple_expect "=1100"

talk_to client 6
send "1101 93 12 0 { } 1 { 14 00000000 1 [holl "1"] }\n"
simple_expect "=1101"

# Reset supervisor of 12 to 7.
send "1102 18 12 7\n"
simple_expect "=1102"

send "1103 91 12\n"
simple_expect "=1103 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 1 { 11 14 11 $any_time 00000000 1 [holl "1"] }"

talk_to client 0

send "1104 93 12 1 { 11 } 0 { }\n"
simple_expect "=1104"

# Delete aux-item with owner-delete when someone else.

talk_to client 6
# Check that conference 12 and text 1 are devoid of aux-items.
send "1105 90 1\n"
simple_expect "=1105 $any_time 7 0 6 0 2 { 0 12 6 1 } 0 \\*"
send "1106 91 12\n"
simple_expect "=1106 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 0 \\*"

# Set supervisor of 12 to 11.
talk_to client 2
send "1107 18 12 11\n"
simple_expect "=1107"

talk_to client 6
send "1108 93 12 0 { } 1 { 14 00000000 1 [holl "1"] }\n"
simple_expect "=1108"

# Reset supervisor of 12 to 7.
send "1109 18 12 7\n"
simple_expect "=1109"

send "1110 91 12\n"
simple_expect "=1110 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 1 { 12 14 11 $any_time 00000000 1 [holl "1"] }"

talk_to client 4

send "1111 93 12 1 { 12 } 0 { }\n"
simple_expect "%1111 49 0"

# Check that Conference 12 and text 1 looks OK.
send "1112 91 12\n"
simple_expect "=1112 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 1 { 12 14 11 $any_time 00000000 1 [holl "1"] }"
send "1113 90 1\n"
simple_expect "=1113 $any_time 7 0 6 0 2 { 0 12 6 1 } 1 { 6 28 11 $any_time 00001000 0 [holl "12"] }"

# Attempt to set a duplicate faq-text on Conference 12.

talk_to client 2

send "1114 93 12 0 { } 1 { 14 00000000 1 [holl "1"] }\n"
simple_expect "%1114 68 0"

# Attempt to remote a faq-text and add it back later.

send "1115 93 12 1 { 12 } 0 { }\n"
simple_expect "=1115"
send "1116 93 12 0 { } 1 { 14 00000000 1 [holl "1"] }\n"
simple_expect "=1116"

# Attempt to remote a faq-text and add it back in the same operation.
# This should work, but currently fails.

send "1117 93 12 1 { 13 } 1 { 14 00000000 1 [holl "1"] }\n"
if {0} {
    simple_expect "=1117"
    set aux 14
} else {
    simple_expect "%1117 68 0"
    setup_xfail "*-*-*" "Bug 1614"
    fail "Removing and adding back the same faq-text aux-item in a single operation (got KOM_AUX_DATA_EXISTS)"
    set aux 13
}

send "1118 91 12\n"
simple_expect "=1118 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 1 { $aux 14 7 $any_time 00000000 1 [holl "1"] }"

# Attempt to remove one faq-text and add back two instances of the same faq-text.

send "1119 93 12 1 { $aux } 2 { 14 00000000 1 [holl "1"] 14 00000000 1 [holl "1"] }\n"
extracting_expect "%1119 68 ($any_num)" dupaux 1
set test "correct aux-item flagged as duplicate when adding back two instances of the same faq-text"
if {$dupaux == 1} {
    pass "$test"
} else {
    if {$dupaux == 0} {
	setup_xfail "*-*-*" "Bug 1615"
    }
    fail "$test (got $dupaux instead of 1)"
}
unset test

send "1120 91 12\n"
simple_expect "=1120 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 1 { $aux 14 7 $any_time 00000000 1 [holl "1"] }"

# Remove the faq-text.  Then, attempt to add it two identical faq-texts at once.

send "1121 93 12 1 { $aux } 0 { }\n"
simple_expect "=1121"
send "1122 93 12 0 { } 2 { 14 00000000 1 [holl "1"] 14 00000000 1 [holl "1"] }\n"
extracting_expect "%1122 68 ($any_num)" dupaux 1
set test "correct aux-item flagged as duplicate when adding two identixal faq-texts"
if {$dupaux == 1} {
    pass "$test"
} else {
    if {$dupaux == 0} {
	setup_xfail "*-*-*" "Bug 1615"
    }
    fail "$test (got $dupaux instead of 1)"
}
unset test

send "1123 91 12\n"
simple_expect "=1123 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 1 0 0 \\*"

# Add two faq-texts in a single operation.  One points to an non-existing text.
send "1124 93 12 0 { } 2 { 14 00000000 1 [holl "1"] 14 00000000 1 [holl "2"] }\n"
simple_expect "%1124 69 1"
send "1125 93 12 0 { } 2 { 14 00000000 1 [holl "2"] 14 00000000 1 [holl "1"] }\n"
simple_expect "%1125 69 0"

# Create text 2
send "1126 86 [holl "Text 2"] 1 { 0 12 } 0 { }\n"
simple_expect "=1126 2"

# Add two faq-texts in a single operation.
send "1127 93 12 0 { } 2 { 14 00000000 1 [holl "2"] 14 00000000 1 [holl "1"] }\n"
simple_expect "=1127"

# Check that Conference 12 and texts 1 and 2 looks OK.
send "1128 91 12\n"
simple_expect "=1128 [holl "Conference 12"] 00000000 $any_time $any_time 7 0 7 0 7 0 77 77 0 1 2 0 2 { [expr {$aux + 1}] 14 7 $any_time 00000000 1 [holl "2"] [expr {$aux + 2}] 14 7 $any_time 00000000 1 [holl "1"] }"
send "1129 90 1\n"
simple_expect "=1129 $any_time 7 0 6 0 2 { 0 12 6 1 } 1 { 8 28 7 $any_time 00001000 0 [holl "12"] }"
send "1130 90 2\n"
simple_expect "=1130 $any_time 7 0 6 0 2 { 0 12 6 2 } 1 { 1 28 7 $any_time 00001000 0 [holl "12"] }"



#
# Shut down
#

talk_to client 6
send "1131 55 0\n"
simple_expect "=1131"
client_death 6

talk_to client 5
send "1132 55 0\n"
simple_expect "=1132"
client_death 5

talk_to client 4
send "1133 55 0\n"
simple_expect "=1133"
client_death 4

talk_to client 3
send "1134 55 0\n"
simple_expect "=1134"
client_death 3

talk_to client 2
send "1135 55 0\n"
simple_expect "=1135"
client_death 2

talk_to client 1
send "1136 55 0\n"
simple_expect "=1136"
client_death 1

talk_to client 0
send "1137 42 255\n"
simple_expect "=1137"

send "1138 44 0\n"
simple_expect "=1138"
client_death 0

lyskomd_death
