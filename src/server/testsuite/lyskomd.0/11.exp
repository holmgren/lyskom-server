# Test suite for lyskomd.
# Copyright (C) 1999, 2001-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test the lock file of lyskomd and dbck.

# Start a server that locks the database.
lyskomd_start

# Start dbck in read-only mode.  No lock is required.
spawn ../dbck -d config/lyskomd-config
set test "dbck started in read-only mode"
expect {
    -re "^Where does the trace want to go today. .stderr.$nl" {
        pass "Tracing is activated (/dev/null)"
        send "/dev/null\n"
        exp_continue
    }
    -re "^MSG: init_cache: using datafile\.$nl" {
        pass "$test" 
    }
}
set test "read-only dbck sent second line"
expect {
    -re "^Read $any_num confs/persons and $any_num texts, eof at $any_num$nl" {
	pass "$test"
    }
}
set test "read-only dbck sent final line"
expect {
    -re "^Press enter to terminate dbck$nl" {
	pass "$test"
    }
}
send "\n"
set test "read-only dbck died"
expect {
    eof { pass "$test"; wait }
}
unset test

# Attempt to start dbck in read-write mode.
spawn ../dbck -d -i config/lyskomd-config
set test "dbck finds a lock"
expect {
    -re "^Where does the trace want to go today. .stderr.$nl" {
        pass "Tracing is activated (/dev/null)"
        send "/dev/null\n"
        exp_continue
    }
    -re "^Database already locked by $any*\.$nl" { pass "$test" }
}
set test "read-write dbck sent final line"
expect {
    -re "^Press enter to terminate dbck$nl" {
	pass "$test"
    }
}
send "\n"
set test "read-write dbck died"
expect {
    eof { pass "$test"; wait }
}
unset test

# Attempt to start a second lyskomd.
spawn ../lyskomd -f config/lyskomd-config
set test "lyskomd finds a lock"
expect {
    -re "^Where does the trace want to go today. .stderr.$nl" {
        pass "Tracing is activated (/dev/null)"
        send "/dev/null\n"
        exp_continue
    }
    -re "Database already locked by $any*:$lyskomd_pid$nl" { pass "$test" }
}
set test "lyskomd writes an exiting message."
expect {
    -re "Cannot obtain database lock\.  Exiting\.$nl" { pass "$test" }
}
set test "read-write lyskomd sent final line"
expect {
    -re "^\[0-9 :\]* Press enter to terminate lyskomd$nl" {
	pass "$test"
    }
}
send "\n"
set test "read-write lyskomd died"
expect {
    eof { pass "$test"; wait }
}
unset test

# Shut down lyskomd.
system "kill -TERM $lyskomd_pid"
lyskomd_death "" signal

# Start a new lyskomd.
lyskomd_start

# Kill it so that it cannot remove the lock file.
system "kill -KILL $lyskomd_pid"
set test "lyskomd was killed"
expect {
    eof { pass "$test"; wait }
}

# Start a new lyskomd.  Check that it removes the stale lock file.
set new_pid [spawn ../lyskomd -f config/lyskomd-config]
set line_leader "\[0-9\]\[0-9\]\[0-9\]\[0-9\]\[0-9\]\[0-9\] \[0-9\]\[0-9\]:\[0-9\]\[0-9\]:\[0-9\]\[0-9\] $new_pid "
set test "lyskomd startup"
expect {
    -re "^Where does the trace want to go today. .stderr.$nl" {
        pass "Tracing is activated (/dev/null)"
        send "/dev/null\n"
        exp_continue
    }
    -re "^${line_leader}... Version $any* \\(process $new_pid\\) started.$nl" {
	pass "$test"
    }
    timeout        {fail "$test (timeout)"}
    full_buffer    {fail "$test (full_buffer)"}
    eof 	   {fail "$test (eof)"; wait}
}
set test "stale lock file removed"

expect {
    -re "^${line_leader}WARNING: This server was compiled with --with-debug-calls\\.$nl" {
	expect -re "^${line_leader}It isn.t safe to use in a production environment.$nl"
	pass "debug calls are enabled"
	set debug_calls 1
	exp_continue
    }
    -re "^${line_leader}Debug calls are disabled, as they should be\\.$nl" {
	pass "debug calls are disabled"
	set debug_calls 0
	exp_continue
    }
    -re "^${line_leader}Removed stale lock file left by $any*:$lyskomd_pid\.$nl" {
	pass "$test"
    }
    timeout             {fail "$test (timeout)"}
    full_buffer 	{fail "$test (full_buffer)"}
    eof 		{fail "$test (eof)"; wait}
}

set test "New lock created"
expect {
    -re "^${line_leader}Created lock ($any*)$nl" {
	pass "$test (lock file $expect_out(1,string)"
    }
    timeout             {fail "$test (timeout)"}
    full_buffer 	{fail "$test (full_buffer)"}
    eof 		{fail "$test (eof)"; wait}
}
simple_expect "Listening for clients on 0.0.0.0:$clientport."
simple_expect "Database = [pwd]/db/lyskomd-data"
simple_expect "Backup = [pwd]/db/lyskomd-backup"
simple_expect "2nd Backup = [pwd]/db/lyskomd-backup-prev"
simple_expect "Lock File = [pwd]/db/lyskomd-lock"
simple_expect "MSG: init_cache: using datafile."
simple_expect "Database saved on $any*"
simple_expect "Read 5 confs/persons and 0 texts"
simple_expect "MSG: garb started."
simple_expect "MSG: garb ready. 0 texts deleted."

system "kill -TERM $new_pid"
set bypass 0
set test "non-stale lyskomd sent final line"
simple_expect "Signal TERM received. Shutting down server."
simple_expect "../lyskomd terminated normally."
simple_expect "Press enter to terminate lyskomd"

send "\n"
set test "non-stale lyskomd died"
expect {
    eof { pass "$test"; wait }
}
unset test

# Start a new lyskomd while a lock file for a remote system exists.
system "ln -s inet.lysator.liu.se:4711 db/lyskomd-lock"
spawn ../lyskomd -f config/lyskomd-config
set test "remote lock file found"
expect {
    -re "^Where does the trace want to go today. .stderr.$nl" {
        pass "Tracing is activated (/dev/null)"
        send "/dev/null\n"
        exp_continue
    }
    -re "Database already locked by inet\.lysator\.liu\.se:4711$nl" {
	pass "$test"
    }
}
set test "lock complaint"
expect {
    -re "^\[0-9 :\]* Cannot obtain database lock.  Exiting.$nl" {
	pass "$test"
    }
}
set test "local lyskomd sent final line"
expect {
    -re "^\[0-9 :\]* Press enter to terminate lyskomd$nl" {
	pass "$test"
    }
}
send "\n"
set test "local lyskomd died"
expect {
    eof { pass "$test"; wait }
}
unset test

# Since we use lyskomd_start, but kill the server ourself, we are
# left with a dangling lock.  Release it.
release_lock
