# Test suite for lyskomd.
# Copyright (C) 2001-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 



# Check client disconnect.

read_versions

set tests {
    {"Disconnect while parsing a string."
	{"A3Hfoo\n" "LysKOM" "0 4 10Hxyzzy"}}

    {"Disconnect while parsing the end of an overly-long string."
	{"A3Hfoo\n" "LysKOM"
	    "0 4 30HUUUUUUUUUUUUUUUUUUUUUUUU"}}

    {"Disconnect while parsing a c_string."
	{"A3Hfoo\n" "LysKOM" "0 12 10Hxyzzy"}}

    {"Disconnect while parsing the end of an overly-long c_string."
	{"A3Hfoo\n" "LysKOM"
	    "0 12 30HUUUUUUUUUUUUUUUUUUUUUUUU"}}

    {"Disconnect after parsing the first greeting."
	{"A3Hfoo\n" "LysKOM"}}
    
    {"Disconnect after parsing the ref-no."
	{"A3Hfoo\n" "LysKOM" "982 "}}
    
    {"Disconnect while parsing the ref-no."
	{"A3Hfoo\n" "LysKOM" "982"}}
    
    {"Disconnect after parsing the request number."
	{"A3Hfoo\n" "LysKOM" "982 0 "}}

    {"Disconnect while parsing the request number."
	{"A3Hfoo\n" "LysKOM" "982 0"}}
    
    {"Disconnect after the initial A."
	{"A"}}
    
    {"Disconnect before sending anything."
	{}}
    
    {"Disconnect while parsing a number."
	{"A3Hfoo\n" "LysKOM" "0 0 5"}}
    
    {"Disconnect while parsing a priv_bits."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 7 5 0000"}}
    
    {"Disconnect while parsing a string length."
	{"A3Hfoo\n" "LysKOM" "0 4 10"}}
    
    {"Disconnect while parsing a c_string length."
	{"A3Hfoo\n" "LysKOM" "0 12 10"}}
    
    {"Disconnect while parsing a time_date (get_last_text)."
	{"A3Hfoo\n" "LysKOM" "0 58 59 59 23 31 12 101 3 365 0"}}
    
    {"Disconnect while parsing a time_date (get_last_text)."
	{"A3Hfoo\n" "LysKOM" "0 58 59 59 23 31 12 101 3 365 "}}
    
    {"Disconnect while parsing a time_date (get_last_text)."
	{"A3Hfoo\n" "LysKOM" "0 58 59 59 23 31 12 101 3 365"}}
    
    {"Disconnect while parsing a time_date (get_last_text)."
	{"A3Hfoo\n" "LysKOM" "0 58 59 59 23 31 12 101 3 "}}
    
    {"Disconnect while parsing a time_date (get_last_text)."
	{"A3Hfoo\n" "LysKOM" "0 58 59 59 23 31 12 101 3"}}
    
    {"Disconnect while parsing a time_date (get_last_text)."
	{"A3Hfoo\n" "LysKOM" "0 58 59 59 23 31 12 101 "}}
    
    {"Disconnect while parsing a time_date (get_last_text)."
	{"A3Hfoo\n" "LysKOM" "0 58 59 59 23 31 12 101"}}
    
    {"Disconnect while parsing a time_date (get_last_text)."
	{"A3Hfoo\n" "LysKOM" "0 58 59 59 23 31 12 "}}
    
    {"Disconnect while parsing a time_date (get_last_text)."
	{"A3Hfoo\n" "LysKOM" "0 58 59 59 23 31 12"}}
    
    {"Disconnect while parsing a time_date (get_last_text)."
	{"A3Hfoo\n" "LysKOM" "0 58 59 59 23 31 "}}
    
    {"Disconnect while parsing a time_date (get_last_text)."
	{"A3Hfoo\n" "LysKOM" "0 58 59 59 23 31"}}
    
    {"Disconnect while parsing a time_date (get_last_text)."
	{"A3Hfoo\n" "LysKOM" "0 58 59 59 23 "}}
    
    {"Disconnect while parsing a time_date (get_last_text)."
	{"A3Hfoo\n" "LysKOM" "0 58 59 59 23"}}
    
    {"Disconnect while parsing a time_date (get_last_text)."
	{"A3Hfoo\n" "LysKOM" "0 58 59 59 "}}
    
    {"Disconnect while parsing a time_date (get_last_text)."
	{"A3Hfoo\n" "LysKOM" "0 58 59 59"}}
    
    {"Disconnect while parsing a time_date (get_last_text)."
	{"A3Hfoo\n" "LysKOM" "0 58 59 "}}
    
    {"Disconnect while parsing a time_date (get_last_text)."
	{"A3Hfoo\n" "LysKOM" "0 58 59"}}
    
    {"Disconnect while parsing a time_date (get_last_text)."
	{"A3Hfoo\n" "LysKOM" "0 58 "}}
    
    {"Disconnect while parsing a time_date (get_last_text)."
	{"A3Hfoo\n" "LysKOM" "0 58"}}
    
    {"Disconnect while parsing pers_flags (0)."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 106 5 "}}
    
    {"Disconnect while parsing pers_flags (1)."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 106 5 0"}}
    
    {"Disconnect while parsing pers_flags (7)."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 106 5 0000000"}}
    
    {"Disconnect while parsing pers_flags (8)."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 106 5 00000000"}}
    
    {"Disconnect while parsing the conf_type of create_conf_old (8)."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 10 3Hfoo 00001000"}}
    
    {"Disconnect while parsing the conf_type of create_conf_old (0)."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 10 3Hfoo "}}
    
    {"Disconnect while parsing the conf_type of create_conf_old (1)."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 10 3Hfoo 0"}}
    
    {"Disconnect while parsing the conf_type of create_conf_old (4)."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 10 3Hfoo 0000"}}
    
    {"Disconnect while parsing the conf_type of create_conf_old (5)."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 10 3Hfoo 00001"}}
    
    {"Disconnect while parsing the conf_type of create_conf_old (8)."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 10 3Hfoo 00001000"}}
    
    {"Disconnect while parsing the fifth argument of add_member."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 100 1 5 200 1 00000"}}
    
    {"Disconnect while parsing the fourth argument of add_member_old."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 14 1 5 200 1"}}
    
    {"Disconnect while parsing the initial string length."
	{"A3"}}
    
    {"Disconnect while parsing the initial string."
	{"A3H"}}

    {"Disconnect while parsing the initial string."
	{"A3Hf"}}
    
    {"Disconnect while parsing the initial string."
	{"A3Hfo"}}

    {"Disconnect while parsing the initial string."
	{"A3Hfoo" "LysKOM"}}
    
    {"Disconnect while parsing the second string of set_passwd."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 8 5 6Hgazonk 6Hfoo"}}
    
    {"Disconnect while parsing a aux_item_list, before \{."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 1 "}}
    
    {"Disconnect while parsing a aux_item_list, after \{."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 1 \{"}}
    
    {"Disconnect while parsing a aux_item_list, after \{."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 1 \{ "}}
    
    {"Disconnect while parsing a aux_item_list, in middle."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 1 \{ 19 "}}
    
    {"Disconnect while parsing a aux_item_list, in middle."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 1 \{ 19 00000000 "}}
    
    {"Disconnect while parsing a aux_item_list, in middle."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 1 \{ 19 00000000 1 "}}
    
    {"Disconnect while parsing a aux_item_list, in middle."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 1 \{ 19 00000000 1 3"}}
    
    {"Disconnect while parsing a aux_item_list, in middle."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 1 \{ 19 00000000 1 3H"}}
    
    {"Disconnect while parsing a aux_item_list, in middle."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 1 \{ 19 00000000 1 3Hfo"}}
    
    {"Disconnect while parsing a aux_item_list, in middle."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 1 \{ 19 00000000 1 3Hfoo"}}
    
    {"Disconnect while parsing a aux_item_list, in middle."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 2 \{ 19 00000000 1 3Hbar "}}
    
    {"Disconnect while parsing a aux_item_list, in middle."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 2 \{ 19 00000000 1 3Hbar 19 "}}
    
    {"Disconnect while parsing a aux_item_list, in middle."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 2 \{ 19 00000000 1 3Hbar 19 00000000 "}}
    
    {"Disconnect while parsing a aux_item_list, in middle."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 2 \{ 19 00000000 1 3Hbar 19 00000000 1 "}}
    
    {"Disconnect while parsing a aux_item_list, in middle."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 2 \{ 19 00000000 1 3Hbar 19 00000000 1 3"}}
    
    {"Disconnect while parsing a aux_item_list, in middle."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 2 \{ 19 00000000 1 3Hbar 19 00000000 1 3H"}}
    
    {"Disconnect while parsing a aux_item_list, in middle."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 2 \{ 19 00000000 1 3Hbar 19 00000000 1 3Hfo"}}
    
    {"Disconnect while parsing a aux_item_list, in middle."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 2 \{ 19 00000000 1 3Hbar 19 00000000 1 3Hfoo"}}
    
    {"Disconnect while parsing a aux_item_list, before \}."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 2 \{ 19 00000000 1 3Hbar 19 00000000 1 3Hfoo "}}

    {"Disconnect while parsing a aux_item_list, overly long, before \{."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 150 "}}
    
    {"Disconnect while parsing a aux_item_list, overly long, after \{."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 150 \{ "}}
    
    {"Disconnect while parsing a aux_item_list, overly long, first half."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 150 \{ 19 00000000 1 3Hbar "}}
    
    {"Disconnect while parsing a aux_item_list, overly long, middle."
 	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 150 \{ 19 00000000 1 3Hbar 19 00000000 1 3Hba2 19 00000000 1 3Hba3 19 00000000 1 3Hba4 19 00000000 1 3Hba"}}

    {"Disconnect while parsing a aux_item_list, overly long, second half."
 	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 150 \{ 19 00000000 1 3Hbar 19 00000000 1 3Hba2 19 00000000 1 3Hba3 19 00000000 1 3Hba4 19 00000000 1 3Hba5 19 00000000 1 3Hba"}}

    {"Disconnect while parsing a aux_item_list, overly long, middle, with overly long string."
 	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 150 \{ 19 00000000 1 3Hbar 19 00000000 1 3Hba2 19 00000000 1 3Hba3 19 00000000 1 3Hba4 19 00000000 1 15Hba"}}

    {"Disconnect while parsing a aux_item_list, overly long, second half, with overly long string."
 	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 150 \{ 19 00000000 1 3Hbar 19 00000000 1 3Hba2 19 00000000 1 3Hba3 19 00000000 1 3Hba4 19 00000000 1 3Hba5 19 00000000 1 15Hba"}}

    {"Disconnect while parsing a aux_item_list, overly long, middle, with overly long string, second half."
 	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 150 \{ 19 00000000 1 3Hbar 19 00000000 1 3Hba2 19 00000000 1 3Hba3 19 00000000 1 3Hba4 19 00000000 1 15Habcdeabcdeab"}}

    {"Disconnect while parsing a aux_item_list, overly long, second half, with overly long string, second half."
 	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 150 \{ 19 00000000 1 3Hbar 19 00000000 1 3Hba2 19 00000000 1 3Hba3 19 00000000 1 3Hba4 19 00000000 1 3Hba5 19 00000000 1 15Habcdeabcdeab"}}

    {"Disconnect while parsing a aux_item_list, barely overly long, before \}."
 	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 5 \{ 19 00000000 1 3Hbar 19 00000000 1 3Hba2 19 00000000 1 3Hba3 19 00000000 1 3Hba4 19 00000000 1 3Hba5 "}}

    {"Disconnect while parsing a aux_item_list, overly long, before \}."
 	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 0 { } 6 \{ 19 00000000 1 3Hbar 19 00000000 1 3Hba2 19 00000000 1 3Hba3 19 00000000 1 3Hba4 19 00000000 1 3Hba5 19 00000000 1 3Hba6 "}}

    {"Disconnect while parsing a misc_info_list, before \{."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 86 3Hfoo 2 "}}
    
    {"Disconnect while parsing a misc_info_list, after \{."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 28 3Hfoo 2 \{"}}
    
    {"Disconnect while parsing a misc_info_list, after \{."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 28 3Hfoo 2 \{ "}}
    
    {"Disconnect while parsing a misc_info_list, in middle."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 28 3Hfoo 2 \{ 0 1 "}}
    
    {"Disconnect while parsing a misc_info_list, in middle."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 28 3Hfoo 2 \{ 0 1 0 "}}
    
    {"Disconnect while parsing a misc_info_list, before \}."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 28 3Hfoo 2 \{ 0 1 0 2 "}}
    
    {"Disconnect while parsing a misc_info_list, overly long, before \{."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 28 3Hfoo 5 "}}
    
    {"Disconnect while parsing a misc_info_list, overly long, after \{."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 28 3Hfoo 5 \{ "}}
    
    {"Disconnect while parsing a misc_info_list, overly long, first half."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 28 3Hfoo 5 \{ 0 1 0 2 "}}
    
    {"Disconnect while parsing a misc_info_list, overly long, middle."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 28 3Hfoo 520 \{ 0 1 0 2 0 3 0 4 0 "}}
    
    {"Disconnect while parsing a misc_info_list, overly long, second half."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 28 3Hfoo 520 \{ 0 1 0 2 0 3 0 4 0 5 "}}
    
    {"Disconnect while parsing a misc_info_list, overly long, second half."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 28 3Hfoo 520 \{ 0 1 0 2 0 3 0 4 0 5 0 "}}
    
    {"Disconnect while parsing a misc_info_list, overly long, second half."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 28 3Hfoo 520 \{ 0 1 0 2 0 3 0 4 0 5 0 6 "}}
    
    {"Disconnect while parsing a misc_info_list, barely overly long, before \}."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 28 3Hfoo 5 \{ 0 1 0 2 0 3 0 4 0 5 "}}

    {"Disconnect while parsing a misc_info_list, overly long, before \}."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 28 3Hfoo 6 \{ 0 1 0 2 0 3 0 4 0 5 0 6 "}}

    {"Disconnect while parsing a num_list, before \{."
	{"A3Hfoo\n" "LysKOM" "0 80 2 "}}
    
    {"Disconnect while parsing a num_list, after \{."
	{"A3Hfoo\n" "LysKOM" "0 80 2 \{"}}
    
    {"Disconnect while parsing a num_list, in middle."
	{"A3Hfoo\n" "LysKOM" "0 80 2 \{ 1 "}}
    
    {"Disconnect while parsing a num_list, before \}."
	{"A3Hfoo\n" "LysKOM" "0 80 2 \{ 1 2 "}}
    
    {"Disconnect while parsing a num_list, overly long, before \{."
	{"A3Hfoo\n" "LysKOM" "0 80 5 "}}
    
    {"Disconnect while parsing a num_list, overly long, after \{."
	{"A3Hfoo\n" "LysKOM" "0 80 5 \{ "}}
    
    {"Disconnect while parsing a num_list, overly long, first half."
	{"A3Hfoo\n" "LysKOM" "0 80 5 \{ 1 2 3 "}}
    
    {"Disconnect while parsing a num_list, overly long, second half."
	{"A3Hfoo\n" "LysKOM" "0 80 6 \{ 1 2 3 4 5 "}}
    
    {"Disconnect while parsing a num_list, overly long, before \}."
	{"A3Hfoo\n" "LysKOM" "0 80 6 \{ 1 2 3 4 5 6 "}}
    
    {"Disconnect while parsing a num_list, barely overly long, before \}."
	{"A3Hfoo\n" "LysKOM" "0 80 5 \{ 1 2 3 4 5 "}}
    
    {"Disconnect while parsing a c_local_text_no_p array, before \{."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 27 5 1 "}}
    
    {"Disconnect while parsing a c_local_text_no_p array, after \{."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 27 5 1 \{ "}}
    
    {"Disconnect while parsing a c_local_text_no_p array, in middle."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 27 5 2 \{ 1 "}}
    
    {"Disconnect while parsing a c_local_text_no_p array, before \}."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 27 5 1 \{ 1 "}}
    
    {"Disconnect parsing a c_local_text_no_p array, overly long, before \{."
	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
	    "2 27 5 6 "}}

    {"Try to give an insanely large greeting (A47114711H...)"
	{"A47114711Hxyzzy"}}
    
    {"Disconnect parsing a read-range array, in length."
    	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
    	    "2 110 1 2"}}

    {"Disconnect parsing a read-range array, after length."
    	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
    	    "2 110 1 2 "}}

    {"Disconnect parsing a read-range array, after \{."
    	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
    	    "2 110 1 2 \{"}}

    {"Disconnect parsing a read-range array, after \{."
    	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
    	    "2 110 1 2 \{ "}}

    {"Disconnect parsing a read-range array, in first number of first pair."
    	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
    	    "2 110 1 2 \{ 1"}}

    {"Disconnect parsing a read-range array, between numbers of first pair."
    	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
    	    "2 110 1 2 \{ 1 "}}

    {"Disconnect parsing a read-range array, in second number of first pair."
    	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
    	    "2 110 1 2 \{ 1 2"}}

    {"Disconnect parsing a read-range array, between first pairs."
    	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
    	    "2 110 1 2 \{ 1 2 "}}

    {"Disconnect parsing a read-range array, before \}."
    	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
    	    "2 110 1 2 \{ 1 2 4 4 "}}

    {"Disconnect parsing a read-range array, overly long, after \{."
    	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
    	    "2 110 1 6 \{ "}}

    {"Disconnect parsing a read-range array, overly long, first half."
    	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
    	    "2 110 1 6 \{ 1 1 3 3 5 "}}

    {"Disconnect parsing a read-range array, overly long, second half."
    	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
    	    "2 110 1 6 \{ 1 1 3 3 5 5 7 7 9 9 11 "}}

    {"Disconnect parsing a read-range array, overly long, before \}."
    	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
    	    "2 110 1 6 \{ 1 1 3 3 5 5 7 7 9 9 11 11 "}}

    {"Disconnect parsing a local_text_no_list, overly long, after \{."
    	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
    	    "2 27 5 6 \{ "}}
    
    {"Disconnect parsing a local_text_no_list, overly long, first half."
    	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
    	    "2 27 5 6 \{ 1 2 3 "}}
    
    {"Disconnect parsing a local_text_no_list, overly long, second half."
    	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
    	    "2 27 5 6 \{ 1 2 3 4 5 "}}
    
    {"Disconnect parsing a local_text_no_list, overly long, before \}."
    	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
    	    "2 27 5 6 \{ 1 2 3 4 5 6 "}}

    {"Disconnect parsing a local_text_no_list, barely overly long, before \}."
    	{"A3Hfoo\n" "LysKOM" "1 62 5 6Hgazonk 1\n" "=1"
    	    "2 27 5 5 \{ 1 2 3 4 5 "}}
}


set next_client 2
set hanging_clients {}

proc startup {} {
    lyskomd_start "" \
"Max accept_async len: 4
Max aux_items added per call: 4
Max aux_items deleted per call: 4
Max what am I doing length: 20
Max conference name length: 20
Max mark_as_read chunks: 4
Max aux_item length: 10
Max links per text: 4
Max read_ranges per call: 5
Connect timeout: 6 hours
Login timeout: 6 hours
Active timeout: 6 hours
Sync interval: 6 hours
"
}

proc shutdown {} {
    global hanging_clients

    client_start 0
    talk_to client 0
    send "A3Hfoo\n"
    simple_expect "LysKOM" "connected"
    send "1000 62 5 [holl "gazonk"] 1\n"
    simple_expect "=1000"
    send "1001 42 255\n"
    simple_expect "=1001"
    send "1002 44 0\n"
    simple_expect "=1002"
    client_death 0
    send_user "Total of [llength $hanging_clients] clients connected\n"
    foreach clnt $hanging_clients {
	client_death $clnt
    }
    set hanging_clients {}
    
    lyskomd_death
}

startup

foreach test $tests {
    global next_client
    global hanging_clients
    
    verbose -log "Running test [lindex $test 0]"
    set chat [lindex $test 1]
    client_start 1
    client_start $next_client
    set hanging_clients [lappend hanging_clients $next_client]
    for {set i 0} {$i < [llength $chat]} {set i [expr $i + 2]} {
	set snd "[lindex $chat $i]"
	if {[string index $snd [expr {[string length $snd] - 1}]] != "\n"} {
	    set snd "#nocr $snd\n"
	}
	    
	#send_user "SEND $snd\n"
	talk_to client 1
	send "$snd"
	talk_to client $next_client
	send "$snd"
	if {$i + 1 < [llength $chat]} {
	    set rcv "[lindex $chat [expr $i + 1]]"
	    #send_user "RECV $rcv\n"
	    talk_to client 1
	    simple_expect "$rcv"
	    talk_to client $next_client
	    simple_expect "$rcv"
	}
    }
    kill_client 1
    set next_client [expr $next_client + 1]
    if {[llength $hanging_clients] > 15} {
	verbose -log "Restarting server."
	shutdown
	startup
	verbose -log "Server restarted."
    }
}

shutdown
