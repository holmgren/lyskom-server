# Test suite for lyskomd.
# Copyright (C) 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test bug 349: set_supervisor returns KOM_UNDEF_CONF instead of
# KOM_PERM when a member of a secret conference tries to set a new
# supervisor for the secret conference, and the member isn't a
# supervisor of the conference.


# Start the server.
lyskomd_start

# Log in as admin (person 5).
client_start 0
talk_to client 0
send "A3Hfoo\n"
simple_expect "LysKOM"
send "1000 0 5 [holl "gazonk"]\n"
simple_expect ":2 9 5 1"
simple_expect "=1000"

# Create a secret conference: "secret" (conf 6).
send "1001 88 [holl "secret"] 10101000 0 { }\n"
simple_expect "=1001 6"

# Create and log in as "member" (person 7).
client_start 1
talk_to client 1
send "A[holl "member"]\n"
simple_expect "LysKOM"
send "1002 81\n"
simple_expect "=1002 8 { 0 5 7 8 9 11 12 13 }"
send "1003 80 9 { 0 5 7 8 9 11 12 13 18 }\n"
simple_expect "=1003"
send "1004 89 [holl "member"] [holl "secrt"] 00000000 0 { }\n"
simple_expect "=1004 7"
send "1005 62 7 [holl "secrt"] 0\n"
simple_expect ":2 9 7 2"
simple_expect "=1005"

talk_to client 0
simple_expect ":2 9 7 2"

# Create and log in as "nonmember" (person 8).
client_start 2
talk_to client 2
send "A[holl "nonmember"]\n"
simple_expect "LysKOM"
send "1006 89 [holl "nonmember"] [holl "Secrt"] 00000000 0 { }\n"
simple_expect "=1006 8"
send "1007 62 8 [holl "Secrt"] 0\n"
simple_expect ":2 9 8 3"
simple_expect "=1007"

talk_to client 0
simple_expect ":2 9 8 3"

talk_to client 1
simple_expect ":2 9 8 3"

# As admin, add member to the secret conference.
talk_to client 0
send "1008 100 6 7 200 1 00000000\n"
lyskomd_expect "Person 7 added to conference 6 by 5."
simple_expect "=1008"

talk_to client 1
simple_expect ":2 18 7 6"

# As "nonmember", try to change the supervisor of "secret" to
# "nonmember".  Should get KOM_UNDEF_CONF.
talk_to client 2
send "1009 18 6 8\n"
simple_expect "%1009 9 6"

# As "member", try to change the supervisor of "secret" to "member".
# Should get KOM_PERM.
talk_to client 1
send "1010 18 6 7\n"
extracting_expect "%1010 (12|9) 6" errorcode 1
setup_xfail "*-*-*" "Bug 349"
set test "Got KOM_PERM"
if {$errorcode == 12} {
    pass "$test"
} else {
    fail "$test"
}
unset test

# Shut down everything.
talk_to client 2
send "1011 55 0\n"
simple_expect "=1011"
simple_expect ":2 13 8 3"
client_death 2

talk_to client 1
simple_expect ":2 13 8 3"

talk_to client 0
simple_expect ":2 13 8 3"

talk_to client 1
send "1012 55 0\n"
simple_expect "=1012"
simple_expect ":2 13 7 2"
client_death 1

talk_to client 0
simple_expect ":2 13 7 2"
send "1013 42 255\n"
simple_expect "=1013"
send "1014 44 0\n"
simple_expect "=1014"
client_death 0

lyskomd_death
