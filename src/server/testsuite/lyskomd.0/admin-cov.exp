# Test suite for lyskomd.
# Copyright (C) 1999, 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 


#
# Supplementary test suites to get statement coverage of admin.c
#
# Note: 100% coverage depends on being able to put the database into
#       an inconsistent state. Call 1000 (set-marks) does this.
#


read_versions
source "$srcdir/config/prot-a.exp"


lyskomd_start "" \
"Max marks per text: 1
Max broadcast length: 10"

client_start 0
talk_to client 0

send "A[holl "DejaGnu test suite"]\n"
simple_expect "LysKOM"
kom_accept_async "1 { 12 }"


# Set up

kom_login 5 "gazonk" 0
kom_create_conference "C6" "10100000" "0 { }"
kom_create_person "P7" "PW7" "00000000" "0 { }"
kom_create_person "P8" "PW8" "00000000" "0 { }"
kom_create_conference "C9" "00000000" "0 { }"

kom_add_member 9 8 100 0 "01000000"
lyskomd_expect "Person 8 added to conference 9 by 5."

# Attempt to do set-info of non-existent text

kom_login 5 "gazonk" 0
kom_enable 255
send "1000 79 0 1 2 3 4 1\n"
simple_expect "%1000 14 1"

# Attempt to do set-info without privs

kom_enable 0
send "1001 86 [holl "Text"] 1 { 0 1 } 0 { }\n"
simple_expect "=1001 1"

send "1002 79 0 1 2 3 4 1\n"
simple_expect "%1002 12 0"

# Attempt to do set-info with too many marks on the text

kom_enable 255
send "1003 72 1 1\n"
simple_expect "=1003"

send "1005 79 0 1 2 3 4 1\n"
lyskomd_expect "LIMIT: set_motd_of_lyskom.1.: New motd has 1 marks."
simple_expect "%1005 36 1"
kom_enable 0

# Attempt to do set-info with an old motd that isn't marked

kom_enable 255
send "1100 86 [holl "Another text"] 1 { 0 1 } 0 { }\n"
simple_expect "=1100 2"

send "1101 86 [holl "Message of the day!"] 1 { 0 1 } 0 { }\n"
simple_expect "=1101 3"

send "1102 79 0 1 2 3 4 2\n"
simple_expect "=1102"

if {$debug_calls} {
send "1103 1001 2 0\n"
simple_expect "=1103"
} else {
unsupported "Use configure --with-debug-calls to enable"
}

send "1104 79 0 1 2 3 4 3\n"
if {$debug_calls} {
    lyskomd_expect "ERROR: set_motd_of_lyskom\\(\\): Old motd not marked\\."
}
simple_expect "=1104"
kom_enable 0


# Send a long message

send "2000 53 0 [holl "1234567890X"]\n"
simple_expect "%2000 5 10"

# Send a message to a non-existent conference (9999)

send "2001 53 9999 [holl "FUBAR"]\n"
simple_expect "%2001 9 9999"

# Attempt to sent a message to a secret conference

kom_login 7 "PW7" 0
send "2002 53 6 [holl "FUBAR"]\n"
simple_expect "%2002 9 6"

# Send message to conference with passive members

client_start 1
talk_to client 1
kom_connect "DejaGnu Test Suite"
kom_accept_async "0 { }"
kom_login 8 "PW8" 0

talk_to client 0
send "2003 53 9 [holl "SILENT"]\n"
simple_expect "%2003 53 0"

talk_to client 1
kom_ping_server


# Attempt to shut down LysKOM without privs

send "3000 44 0\n"
simple_expect "%3000 12 0"



# Finish

kom_logout
kom_login 5 "gazonk" 0
kom_enable 255
send "9999 44 0\n"
simple_expect "=9999"

client_death 1
client_death 0
lyskomd_death
