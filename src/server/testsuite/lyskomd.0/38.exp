# Test suite for lyskomd.
# Copyright (C) 2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test what the server does under severe load.

lyskomd_start "" "Stale timeout: 10 seconds
Max client message size: 512
Sync interval: 1 days
Max client transmit queue bytes: 2505"

proc monitor_progress {} {
    global test
    global nl
    global any_num

    set aborted 0

    set test "monitoring progress"
    expect {
	-re "Progress: $any_num pending reqs$nl" { exp_continue }
	-re "End of progress reports$nl" {
	    pass "$test"
	}
	timeout {
	    if {$aborted == 1} {
		fail "$test (timeout after shutdown)"
	    } else {
		fail "$test (timeout - shutting down client)"
		send "shutdown\n"
		set aborted 1
	    }
	    exp_continue
	}
    }
    unset test
}

proc req_rate {} {
    global test
    global spawn_id
    global any_num
    global any_float
    global req_rate_rate

    if {[info exists test]} {
	set oldtest $test
	unset test
    }
    set id $spawn_id
    talk_to client 0

    send "998 112 [holl "reqs"]\n"
    extracting_expect "=998 6 { $any_num 0 0 $any_float $any_float $any_float $any_float $any_float ($any_float) $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float }" req_rate_rate 1

    if {[info exists oldtest]} {
	set test $oldtest
    }

    set spawn_id $id

    return $req_rate_rate
}

proc send_queue_size {} {
    global test
    global spawn_id
    global any_num
    global any_float
    global send_queue_size_size

    if {[info exists test]} {
	set oldtest $test
	unset test
    }
    set id $spawn_id
    talk_to client 0

    send "999 112 [holl "send-queue-bytes"]\n"
    extracting_expect "=999 6 { ($any_num) 0 0 $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float }" send_queue_size_size 1

    if {[info exists oldtest]} {
	set test $oldtest
    }

    set spawn_id $id

    return $send_queue_size_size
}

client_start 0
talk_to client 0
send "A\n"
send "[holl "DejaGnu test suite"]\n"
simple_expect "LysKOM" "connected"

set oldsize [send_queue_size]

get_time_client_start 0 {"--time-limit"}
monitor_progress
extracting_expect "($any_num) reqs, $any_num bytes, $any_num reads" reqs 1
simple_expect "$reqs reqs, $any_num bytes, $any_num writes"
simple_expect "0 pending reqs"
simple_expect ""
get_time_client_death 0

get_time_client_start 1 {"--time-abort"}
monitor_progress
simple_expect "$any_num reqs, $any_num bytes, $any_num reads"
simple_expect "$any_num reqs, $any_num bytes, $any_num writes"
simple_expect "\[1-9\]\[0-9\]* pending reqs"
simple_expect ""
get_time_client_death 1

get_time_client_start 2 {"--time-abort"}
get_time_client_start 3 {"--time-limit"}
talk_to get_time_client 2
monitor_progress
simple_expect "$any_num reqs, $any_num bytes, $any_num reads"
simple_expect "$any_num reqs, $any_num bytes, $any_num writes"
simple_expect "\[1-9\]\[0-9\]* pending reqs"
simple_expect ""
get_time_client_death 2
talk_to get_time_client 3
monitor_progress
extracting_expect "($any_num) reqs, $any_num bytes, $any_num reads" reqs 1
simple_expect "$reqs reqs, $any_num bytes, $any_num writes"
simple_expect "0 pending reqs"
simple_expect ""
get_time_client_death 3

get_time_client_start 4 {"--write-only"}
send "ping\n"
simple_expect "pong"
talk_to lyskomd

# The server will wait 10 seconds before it kills the stalled client.
# Give it 4 extra seconds.
set test "server kills client 5"
expect {
    -re "Client 6 from $any* has stalled\.  Killing it\.$nl" {
	pass "$test"
    }
    timeout {
	set size [send_queue_size]
	set rate [req_rate]

	# Check to see if the client was killed after the timeout
	# fired but before execution arrives here.
	set old_timeout $timeout
	set timeout 0
	set got_it 0
	expect {
	    -re "Client 6 from $any* has stalled\.  Killing it\.$nl" {
		set got_it 1
	    }
	    timeout {
	    }
	}
	set timeout $old_timeout

	if {$got_it == 1} {
	    pass "$test"
	} elseif {$size > $oldsize} {
	    set oldsize $size
	    send_user "Queue is growing... $size\n"
	    exp_continue
	} elseif {$rate > 10} {
	    send_user "Server is processing... $rate reqs/second (15 s avg) (size $size)\n"
	    exp_continue
	} else {
	    fail "$test (output queue not growing)"
	    talk_to get_time_client 4
	    send "shutdown\n"
	}
    }
}
unset test

talk_to get_time_client 4
send "ping\n"
simple_expect "pong"
send "start-reading\n"
simple_expect "ok"
monitor_progress
simple_expect "$any_num reqs, $any_num bytes, $any_num reads"
extracting_expect "($any_num) reqs, $any_num bytes, $any_num writes" reqs 1
extracting_expect "($any_num) pending reqs" pend 1
simple_expect ""
set test "proper number of pending and sent requests"
if {$pend > 0 && $reqs > $pend} {
    pass "$test"
} else {
    fail "$test ($reqs sent, $pend pending)"
}
unset test
get_time_client_death 4

talk_to client 0
send "1000 112 [holl "recv-queue-bytes"]\n"
simple_expect "=1000 6 { $any_num 0 0 $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float }"

system "kill -TERM $lyskomd_pid"
client_death 0
lyskomd_death "" signal
