# Test suite for lyskomd.
# Copyright (C) 1998-1999, 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 


# Test get_map and local_to_global.
# Test get_created_texts and map_created_texts.
# Test local_to_global_reverse and map_created_texts_reverse.
# As a side effect a lot of other things are also tested.

# The following texts are created by this test case:
#
# Author Text_no Recipients
#   5       1     1<1>
#   5       2     2<1>
#   5       3     1<2> 3<1>
#    6      4     1<3>
#    6      5     1<4>
#   5       6     1<5>
#   5       7     1<6>
#   5       8     1<7>
#   5       9     1<8>
#   5      10     1<9>
#    6     11     1<10>
#    6     12     1<11>
#    6     13     1<11>
lyskomd_start

client_start 0
talk_to client 0
send "A3Hfoo\n"
simple_expect "LysKOM" "connected"

# Log in
send "1000 0 5 [holl gazonk]\n"
simple_expect ":2 9 5 1" "0=login-old caused a login async msg"
simple_expect "=1000" "0=login-old succeeded"

# Become a member of conference 1 using 14=add-member-old
send "1001 14 1 5 100 1\n"
simple_expect "=1001" "14=add-member-old succeeded"

# Get the map using 34=get-map and 103=local-to-global
#   Also test 121=local-to-global-reverse, in indented lines.
send "1002 34 1 0 50\n"
simple_expect "=1002 1 0 \\*" "empty map (old-style 0)"

send "1003 103 1 0 50\n"
simple_expect "%1003 17 0" "empty map (new-style 0)"

  send "2000 121 1 0 50\n"
  simple_expect "=2000 1 1 0 0 0 \\*"

send "1004 34 1 1 50\n"
simple_expect "%1004 16 1" "empty map (old-style 1)"

send "1005 103 1 1 50\n"
simple_expect "%1005 16 1" "empty map (new-style 1)"

  send "2001 121 1 1 50\n"
  simple_expect "=2001 1 1 0 0 0 \\*"

send "1006 34 1 2 50\n"
simple_expect "%1006 16 2" "empty map (old-style 2)"

send "1007 103 1 2 50\n"
simple_expect "%1007 16 2" "empty map (new-style 2)"

  send "2002 121 1 2 50\n"
  simple_expect "=2002 1 1 0 0 0 \\*"

# Create a text
send "1008 28 [holl text0001] 1 { 0 1 }\n"
simple_expect ":16 0 1 $any_time 5 0 8 0 2 { 0 1 6 1 }" "async text 1 created"
simple_expect "=1008 1" "text 1 created"

# Get the map using 34=get-map and 103=local-to-global
send "1009 34 1 0 50\n"
simple_expect "=1009 1 1 { 1 }" "one map (old-style 0)"

send "1010 103 1 0 50\n"
simple_expect "%1010 17 0" "one map (new-style 0)"

  send "2003 121 1 0 50\n"
  simple_expect "=2003 1 2 0 1 1 1 { 1 }"

send "1011 34 1 1 50\n"
simple_expect "=1011 1 1 { 1 }" "one map (old-style)"

send "1012 103 1 1 50\n"
simple_expect "=1012 1 2 0 1 1 1 { 1 }" "one map (new-style)"

  send "2004 121 1 1 50\n"
  simple_expect "=2004 1 1 0 0 0 \\*"

send "1013 34 1 2 50\n"
simple_expect "%1013 16 2" "one map (old-style 2)"

send "1014 103 1 2 50\n"
simple_expect "%1014 16 2" "one map (new-style 2)"

  send "2005 121 1 2 50\n"
  simple_expect "=2005 1 2 0 1 1 1 { 1 }"

# Create yet another text in a conference we are not subscribed to.
# Make sure that no async message arrives.
send "1015 28 [holl text002] 1 { 0 2 }\n"
simple_expect "=1015 2" "text 2 created"

# Create text 3.  Make sure that loc_no is ignored.
send "1016 28 [holl text0003] 4 { 0 1 6 99 1 3 6 32 }\n"
simple_expect ":16 0 3 $any_time 5 0 8 0 4 { 0 1 6 2 1 3 6 1 }" \
	"async text 3 created"
simple_expect "=1016 3" "text 3 created"

# Get the map using 34=get-map and 103=local-to-global
send "1017 34 1 0 50\n"
simple_expect "=1017 1 2 { 1 3 }" "one map (old-style 0)"

send "1018 103 1 0 50\n"
simple_expect "%1018 17 0" "one map (new-style 0)"

  send "2006 121 1 0 50\n"
  simple_expect "=2006 1 3 0 1 1 2 { 1 3 }"

send "1019 34 1 1 50\n"
simple_expect "=1019 1 2 { 1 3 }" "one map (old-style)"

send "1020 103 1 1 50\n"
simple_expect "=1020 1 3 0 1 1 2 { 1 3 }" "one map (new-style)"

  send "2007 121 1 1 50\n"
  simple_expect "=2007 1 1 0 0 0 \\*"

send "1021 34 1 3 50\n"
simple_expect "%1021 16 3" "one map (old-style 3)"

send "1022 103 1 3 50\n"
simple_expect "%1022 16 3" "one map (new-style 3)"

  send "2008 121 1 3 50\n"
  simple_expect "=2008 1 3 0 1 1 2 { 1 3 }"

send "1023 34 1 2 50\n"
simple_expect "=1023 2 1 { 3 }" "one map (old-style 2)"

send "1024 103 1 2 50\n"
simple_expect "=1024 2 3 0 1 2 1 { 3 }" "one map (new-style 2)"

  send "2009 121 1 2 50\n"
  simple_expect "=2009 1 2 0 1 1 1 { 1 }"

send "1025 34 1 1 2\n"
simple_expect "=1025 1 2 { 1 3 }" "one map (old-style)"

send "1026 103 1 1 2\n"
simple_expect "=1026 1 3 0 1 1 2 { 1 3 }" "one map (new-style)"

  send "2010 121 1 3 2\n"
  simple_expect "=2010 1 3 0 1 1 2 { 1 3 }"

send "1027 34 1 1 1\n"
simple_expect "=1027 1 1 { 1 }" "one map (old-style)"

send "1028 103 1 1 1\n"
simple_expect "=1028 1 2 1 1 1 1 { 1 }" "one map (new-style)"

  send "2011 121 1 3 1\n"
  simple_expect "=2011 2 3 1 1 2 1 { 3 }"

send "1029 34 1 1 0\n"
simple_expect "=1029 1 0 \\*" "one map (old-style)"

send "1030 103 1 1 0\n"
simple_expect "=1030 1 1 1 0 0 \\*" "one map (new-style)"

  send "2012 121 1 3 0\n"
  simple_expect "=2012 3 3 1 0 0 \\*"

# Connect a second client that is aux-info-aware
client_start 1
talk_to client 1
send "A3Hbar\n"
simple_expect "LysKOM" "connected"

# Enable new-style async messages.
send "1031 81\n"
simple_expect "=1031 8 { 0 5 7 8 9 11 12 13 }" "the default list of async messages"

send "1032 80 10 { 5 6 8 9 11 12 13 14 15 9999 }\n"
simple_expect "%1032 50 9999" "async-message 9999 does not exist"

send "1033 81\n"
simple_expect "=1033 9 { 5 6 8 9 11 12 13 14 15 }" "the failed call succeeded"

send "1034 80 10 { 5 6 8 9 11 12 7 13 14 15 }\n"
simple_expect "=1034" "setting wanted async"

send "1035 81\n"
simple_expect "=1035 10 { 5 6 7 8 9 11 12 13 14 15 }" "setting async"

# Create the person "Eskil Block, FOA"

# lookup
send "1036 76 [holl "Eskil Block, FOA"] 0 1\n"
simple_expect "=1036 0 \\*" "No Eskil Block yet"

send "1037 89 [holl "Eskil Block, FOA"] [holl "liksE"] 00000000 2 { 9 00000000 1 [holl "simulated compface data"] 12 00000000 1 [holl "simulated pgp public key"] }\n"
simple_expect "=1037 6" "created Eskil, got 6"
  send "2013 62 6 [holl "liksE"] 0\n"
  simple_expect ":2 9 6 2" "Eskil logged in (async c1)"
  simple_expect "=2013"

talk_to client 0
simple_expect ":2 9 6 2" "Eskil logged in (async c0)"

talk_to client 1

# Check that Eskil is a member of his letterbox and nothing else.
send "1038 99 6 0 9 0\n"
simple_expect "=1038 1 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 }" "Eskil is a member of his letterbox (only)"

# Eskil joins conf 1 and creates a text.

send "1039 2 1\n"
simple_expect "%1039 13 1" "Eskil not member yet; thus cannot change conference"

send "1040 100 1 6 100 0 00000000\n"
simple_expect "=1040" "Eskil joins ok"

send "1041 2 1\n"
simple_expect ":5 6 6 1 2 [holl ""] [idholl "bar"]"
simple_expect "=1041" "Eskil member; thus can change conference"

send "1042 86 [holl "text0004"] 1 { 0 1 } 0 { }\n"
simple_expect ":18 15 4 $any_time 6 0 8 0 2 { 0 1 6 3 } 0 \\*" "async to c1"
simple_expect "=1042 4" "Eskil created text 4"
talk_to client 0
simple_expect ":16 0 4 $any_time 6 0 8 0 2 { 0 1 6 3 }" "async to c0"

send "1043 34 1 0 50\n"
simple_expect "=1043 1 3 { 1 3 4 }" "one map (old-style 0)"

send "1044 103 1 0 50\n"
simple_expect "%1044 17 0" "one map (new-style 0)"

  send "2014 121 1 0 50\n"
  simple_expect "=2014 1 4 0 1 1 3 { 1 3 4 }"

send "1045 34 1 1 50\n"
simple_expect "=1045 1 3 { 1 3 4 }" "one map (old-style)"

send "1046 103 1 1 50\n"
simple_expect "=1046 1 4 0 1 1 3 { 1 3 4 }" "one map (new-style)"

  send "2015 121 1 1 50\n"
  simple_expect "=2015 1 1 0 0 0 \\*"

send "1047 34 1 4 50\n"
simple_expect "%1047 16 4" "one map (old-style 3)"

send "1048 103 1 4 50\n"
simple_expect "%1048 16 4" "one map (new-style 3)"

  send "2016 121 1 4 50\n"
  simple_expect "=2016 1 4 0 1 1 3 { 1 3 4 }"

send "1049 34 1 2 50\n"
simple_expect "=1049 2 2 { 3 4 }" "one map (old-style 2)"

send "1050 103 1 2 50\n"
simple_expect "=1050 2 4 0 1 2 2 { 3 4 }" "one map (new-style 2)"

  send "2017 121 1 3 50\n"
  simple_expect "=2017 1 3 0 1 1 2 { 1 3 }"

  send "2018 121 1 2 50\n"
  simple_expect "=2018 1 2 0 1 1 1 { 1 }"

send "1051 34 1 1 2\n"
simple_expect "=1051 1 2 { 1 3 }" "one map (old-style)"

send "1052 103 1 1 2\n"
simple_expect "=1052 1 3 1 1 1 2 { 1 3 }" "one map (new-style)"

  send "2019 121 1 3 2\n"
  simple_expect "=2019 1 3 0 1 1 2 { 1 3 }"

  send "2020 121 1 4 2\n"
  simple_expect "=2020 2 4 1 1 2 2 { 3 4 }"

send "1053 34 1 1 1\n"
simple_expect "=1053 1 1 { 1 }" "one map (old-style)"

send "1054 103 1 1 1\n"
simple_expect "=1054 1 2 1 1 1 1 { 1 }" "one map (new-style)"

  send "2021 121 1 4 1\n"
  simple_expect "=2021 3 4 1 1 3 1 { 4 }"

  send "2022 121 1 3 1\n"
  simple_expect "=2022 2 3 1 1 2 1 { 3 }"

  send "2023 121 1 2 1\n"
  simple_expect "=2023 1 2 0 1 1 1 { 1 }"

  send "2024 121 1 1 1\n"
  simple_expect "=2024 1 1 0 0 0 \\*"

  send "2025 121 1 0 1\n"
  simple_expect "=2025 3 4 1 1 3 1 { 4 }"

send "1055 34 1 3 0\n"
simple_expect "=1055 3 0 \\*" "one map (old-style)"

send "1056 103 1 3 0\n"
simple_expect "=1056 3 3 1 0 0 \\*" "one map (new-style)"

  send "2026 121 1 1 0\n"
  simple_expect "=2026 1 1 0 0 0 \\*"

  send "2027 121 1 4 0\n"
  simple_expect "=2027 4 4 1 0 0 \\*"

  send "2028 121 1 0 0\n"
  simple_expect "=2028 4 4 1 0 0 \\*"

talk_to client 1

send "1057 86 [holl "text0005 failed attempt"] 1 { 0 1 } 1 { 9 00000000 1 [holl "simulated compface data"] }\n"
simple_expect "%1057 63 0" "not allowed to add compface to texts"

send "1058 86 [holl "text0005"] 1 { 0 1 } 1 { 1 00000000 1 [holl "text/plain"] }\n"
simple_expect ":18 15 5 $any_time 6 0 8 0 2 { 0 1 6 4 } 1 { 1 1 6 $any_time 00000000 1 [holl "text/plain"] }" "async to c1"
simple_expect "=1058 5" "Eskil created text 5"
talk_to client 0
simple_expect ":16 0 5 $any_time 6 0 8 0 2 { 0 1 6 4 }" "async to c0"

# Create some more texts
send "1059 28 [holl text0006] 1 { 0 1 }\n"
simple_expect ":16 0 6 $any_time 5 0 8 0 2 { 0 1 6 5 }" "async text 6 created"
simple_expect "=1059 6"
talk_to client 1
simple_expect ":18 15 6 $any_time 5 0 8 0 2 { 0 1 6 5 } 0 \\*" "async to c1"
talk_to client 0

send "1060 28 [holl text0007] 1 { 0 1 }\n"
simple_expect ":16 0 7 $any_time 5 0 8 0 2 { 0 1 6 6 }" "async text 7 created"
simple_expect "=1060 7"
talk_to client 1
simple_expect ":18 15 7 $any_time 5 0 8 0 2 { 0 1 6 6 } 0 \\*" "async to c1"
talk_to client 0

send "1061 28 [holl text0008] 1 { 0 1 }\n"
simple_expect ":16 0 8 $any_time 5 0 8 0 2 { 0 1 6 7 }" "async text 8 created"
simple_expect "=1061 8"
talk_to client 1
simple_expect ":18 15 8 $any_time 5 0 8 0 2 { 0 1 6 7 } 0 \\*" "async to c1"
talk_to client 0

send "1062 28 [holl text0009] 1 { 0 1 }\n"
simple_expect ":16 0 9 $any_time 5 0 8 0 2 { 0 1 6 8 }" "async text 9 created"
simple_expect "=1062 9"
talk_to client 1
simple_expect ":18 15 9 $any_time 5 0 8 0 2 { 0 1 6 8 } 0 \\*" "async to c1"
talk_to client 0

send "1063 28 [holl text0010] 1 { 0 1 }\n"
simple_expect ":16 0 10 $any_time 5 0 8 0 2 { 0 1 6 9 }" "async text 10 created"
simple_expect "=1063 10"
talk_to client 1
simple_expect ":18 15 10 $any_time 5 0 8 0 2 { 0 1 6 9 } 0 \\*" "async to c1"

# We now have 9 texts in conference 1.  Check get_map.

send "1064 34 1 0 50\n"
simple_expect "=1064 1 9 { 1 3 4 5 6 7 8 9 10 }"

send "1065 34 1 1 50\n"
simple_expect "=1065 1 9 { 1 3 4 5 6 7 8 9 10 }"

send "1066 34 1 2 50\n"
simple_expect "=1066 2 8 { 3 4 5 6 7 8 9 10 }"

send "1067 34 1 3 50\n"
simple_expect "=1067 3 7 { 4 5 6 7 8 9 10 }"

send "1068 34 1 4 50\n"
simple_expect "=1068 4 6 { 5 6 7 8 9 10 }"

send "1069 34 1 5 50\n"
simple_expect "=1069 5 5 { 6 7 8 9 10 }"

send "1070 34 1 6 50\n"
simple_expect "=1070 6 4 { 7 8 9 10 }"

send "1071 34 1 7 50\n"
simple_expect "=1071 7 3 { 8 9 10 }"

send "1072 34 1 8 50\n"
simple_expect "=1072 8 2 { 9 10 }"

send "1073 34 1 9 50\n"
simple_expect "=1073 9 1 { 10 }"

send "1074 34 1 10 50\n"
simple_expect "%1074 16 10"

send "1075 34 1 0 11\n"
simple_expect "=1075 1 9 { 1 3 4 5 6 7 8 9 10 }"

send "1076 34 1 0 10\n"
simple_expect "=1076 1 9 { 1 3 4 5 6 7 8 9 10 }"

send "1077 34 1 0 9\n"
simple_expect "=1077 1 8 { 1 3 4 5 6 7 8 9 }"

send "1078 34 1 1 10\n"
simple_expect "=1078 1 9 { 1 3 4 5 6 7 8 9 10 }"

send "1079 34 1 1 9\n"
simple_expect "=1079 1 9 { 1 3 4 5 6 7 8 9 10 }"

send "1080 34 1 1 8\n"
simple_expect "=1080 1 8 { 1 3 4 5 6 7 8 9 }"

send "1081 34 1 8 2\n"
simple_expect "=1081 8 2 { 9 10 }"

send "1082 34 1 9 1\n"
simple_expect "=1082 9 1 { 10 }"

# Check local_to_global

send "1083 103 1 0 50\n"
simple_expect "%1083 17 0"

send "1084 103 1 1 50\n"
simple_expect "=1084 1 10 0 1 1 9 { 1 3 4 5 6 7 8 9 10 }"

send "1085 103 1 2 50\n"
simple_expect "=1085 2 10 0 1 2 8 { 3 4 5 6 7 8 9 10 }"

send "1086 103 1 3 50\n"
simple_expect "=1086 3 10 0 1 3 7 { 4 5 6 7 8 9 10 }"

send "1087 103 1 4 50\n"
simple_expect "=1087 4 10 0 1 4 6 { 5 6 7 8 9 10 }"

send "1088 103 1 5 50\n"
simple_expect "=1088 5 10 0 1 5 5 { 6 7 8 9 10 }"

send "1089 103 1 6 50\n"
simple_expect "=1089 6 10 0 1 6 4 { 7 8 9 10 }"

send "1090 103 1 7 50\n"
simple_expect "=1090 7 10 0 1 7 3 { 8 9 10 }"

send "1091 103 1 8 50\n"
simple_expect "=1091 8 10 0 1 8 2 { 9 10 }"

send "1092 103 1 9 50\n"
simple_expect "=1092 9 10 0 1 9 1 { 10 }"

send "1093 103 1 10 50\n"
simple_expect "%1093 16 10"

send "1094 103 1 0 11\n"
simple_expect "%1094 17 0"

send "1095 103 1 0 10\n"
simple_expect "%1095 17 0"

send "1096 103 1 0 9\n"
simple_expect "%1096 17 0"

send "1097 103 1 1 10\n"
simple_expect "=1097 1 10 0 1 1 9 { 1 3 4 5 6 7 8 9 10 }"

send "1098 103 1 1 9\n"
simple_expect "=1098 1 10 0 1 1 9 { 1 3 4 5 6 7 8 9 10 }"

send "1099 103 1 1 8\n"
simple_expect "=1099 1 9 1 1 1 8 { 1 3 4 5 6 7 8 9 }"

send "1100 103 1 3 5\n"
simple_expect "=1100 3 8 1 1 3 5 { 4 5 6 7 8 }"

send "1101 103 1 8 2\n"
simple_expect "=1101 8 10 0 1 8 2 { 9 10 }"

send "1102 103 1 9 1\n"
simple_expect "=1102 9 10 0 1 9 1 { 10 }"

# Check local_to_global_reverse

  send "2029 121 1 0 50\n"
  simple_expect "=2029 1 10 0 1 1 9 { 1 3 4 5 6 7 8 9 10 }"

  send "2030 121 1 12 50\n"
  simple_expect "=2030 1 10 0 1 1 9 { 1 3 4 5 6 7 8 9 10 }"

  send "2031 121 1 11 50\n"
  simple_expect "=2031 1 10 0 1 1 9 { 1 3 4 5 6 7 8 9 10 }"

  send "2032 121 1 10 50\n"
  simple_expect "=2032 1 10 0 1 1 9 { 1 3 4 5 6 7 8 9 10 }"

  send "2033 121 1 9 50\n"
  simple_expect "=2033 1 9 0 1 1 8 { 1 3 4 5 6 7 8 9 }"

  send "2034 121 1 8 50\n"
  simple_expect "=2034 1 8 0 1 1 7 { 1 3 4 5 6 7 8 }"

  send "2035 121 1 7 50\n"
  simple_expect "=2035 1 7 0 1 1 6 { 1 3 4 5 6 7 }"

  send "2036 121 1 6 50\n"
  simple_expect "=2036 1 6 0 1 1 5 { 1 3 4 5 6 }"

  send "2037 121 1 5 50\n"
  simple_expect "=2037 1 5 0 1 1 4 { 1 3 4 5 }"

  send "2038 121 1 4 50\n"
  simple_expect "=2038 1 4 0 1 1 3 { 1 3 4 }"

  send "2039 121 1 3 50\n"
  simple_expect "=2039 1 3 0 1 1 2 { 1 3 }"

  send "2040 121 1 2 50\n"
  simple_expect "=2040 1 2 0 1 1 1 { 1 }"

  send "2041 121 1 1 50\n"
  simple_expect "=2041 1 1 0 0 0 \\*"

  send "2042 121 1 12 10\n"
  simple_expect "=2042 1 10 0 1 1 9 { 1 3 4 5 6 7 8 9 10 }"

  send "2043 121 1 12 9\n"
  simple_expect "=2043 1 10 0 1 1 9 { 1 3 4 5 6 7 8 9 10 }"

  send "2044 121 1 12 8\n"
  simple_expect "=2044 2 10 1 1 2 8 { 3 4 5 6 7 8 9 10 }"

  send "2045 121 1 11 8\n"
  simple_expect "=2045 2 10 1 1 2 8 { 3 4 5 6 7 8 9 10 }"

  send "2046 121 1 10 8\n"
  simple_expect "=2046 2 10 1 1 2 8 { 3 4 5 6 7 8 9 10 }"

  send "2047 121 1 10 5\n"
  simple_expect "=2047 5 10 1 1 5 5 { 6 7 8 9 10 }"

  send "2048 121 1 10 4\n"
  simple_expect "=2048 6 10 1 1 6 4 { 7 8 9 10 }"

  send "2049 121 1 10 2\n"
  simple_expect "=2049 8 10 1 1 8 2 { 9 10 }"

  send "2050 121 1 10 1\n"
  simple_expect "=2050 9 10 1 1 9 1 { 10 }"

# Remove a few texts to provoke sparse maps.

send "1103 29 4\n"
simple_expect ":18 14 4 $any_time 6 0 8 0 2 { 0 1 6 3 } 0 \\*"
simple_expect "=1103"

send "1104 31 5 1\n"
simple_expect "=1104"

# Now, these local text numbers exist:
# lno: 1 2 . . 5 6 7 8  9
# tno: 1 3 . . 6 7 8 9 10

send "1105 34 1 1 50\n"
simple_expect "=1105 1 9 { 1 3 0 0 6 7 8 9 10 }"

send "1106 34 1 2 50\n"
simple_expect "=1106 2 8 { 3 0 0 6 7 8 9 10 }"

send "1107 34 1 3 50\n"
simple_expect "=1107 3 7 { 0 0 6 7 8 9 10 }"

send "1108 34 1 4 50\n"
simple_expect "=1108 4 6 { 0 6 7 8 9 10 }"

send "1109 34 1 5 50\n"
simple_expect "=1109 5 5 { 6 7 8 9 10 }"

send "1110 34 1 6 50\n"
simple_expect "=1110 6 4 { 7 8 9 10 }"

send "1111 103 1 3 1\n"
simple_expect "=1111 3 6 1 1 5 1 { 6 }"

send "1112 103 1 4 1\n"
simple_expect "=1112 4 6 1 1 5 1 { 6 }"

send "1113 103 1 5 1\n"
simple_expect "=1113 5 6 1 1 5 1 { 6 }"

send "1114 103 1 2 1\n"
simple_expect "=1114 2 3 1 1 2 1 { 3 }"

send "1115 103 1 2 2\n"
simple_expect "=1115 2 6 1 0 2 { 2 3 5 6 }"

send "1116 103 1 2 3\n"
simple_expect "=1116 2 7 1 1 2 5 { 3 0 0 6 7 }"

  send "2051 121 1 0 50\n"
  simple_expect "=2051 1 10 0 1 1 9 { 1 3 0 0 6 7 8 9 10 }"

  send "2052 121 1 7 1\n"
  simple_expect "=2052 6 7 1 1 6 1 { 7 }"

  send "2053 121 1 6 1\n"
  simple_expect "=2053 5 6 1 1 5 1 { 6 }"

  send "2054 121 1 5 1\n"
  simple_expect "=2054 2 5 1 1 2 1 { 3 }"

  send "2055 121 1 4 1\n"
  simple_expect "=2055 2 4 1 1 2 1 { 3 }"

  send "2056 121 1 3 1\n"
  simple_expect "=2056 2 3 1 1 2 1 { 3 }"

  send "2057 121 1 2 1\n"
  simple_expect "=2057 1 2 0 1 1 1 { 1 }"

  send "2058 121 1 6 2\n"
  simple_expect "=2058 2 6 1 0 2 { 2 3 5 6 }"

  send "2059 121 1 6 3\n"
  simple_expect "=2059 1 6 0 1 1 5 { 1 3 0 0 6 }"

  send "2060 121 1 7 3\n"
  simple_expect "=2060 2 7 1 1 2 5 { 3 0 0 6 7 }"

# Texts written by person 5:
# ano: 1 2 3 4 5 6 7  8
# tno: 1 2 3 6 7 8 9 10

# Texts written by person 6:
# ano: . 2
# tno: . 5

send "1117 47 5 1 50\n"
simple_expect "=1117 1 8 { 1 2 3 6 7 8 9 10 }"

send "1118 47 5 1 8\n"
simple_expect "=1118 1 8 { 1 2 3 6 7 8 9 10 }"

send "1119 47 5 1 7\n"
simple_expect "=1119 1 7 { 1 2 3 6 7 8 9 }"

send "1120 47 5 1 6\n"
simple_expect "=1120 1 6 { 1 2 3 6 7 8 }"

send "1121 47 5 1 1\n"
simple_expect "=1121 1 1 { 1 }"

send "1122 47 5 0 1\n"
simple_expect "=1122 1 1 { 1 }"

send "1123 47 5 0 2\n"
simple_expect "=1123 1 2 { 1 2 }"

send "1124 47 5 5 3\n"
simple_expect "=1124 5 3 { 7 8 9 }"

send "1125 47 5 5 4\n"
simple_expect "=1125 5 4 { 7 8 9 10 }"

send "1126 47 5 5 5\n"
simple_expect "=1126 5 4 { 7 8 9 10 }"

send "1127 47 5 8 4\n"
simple_expect "=1127 8 1 { 10 }"

send "1128 47 5 9 4\n"
simple_expect "%1128 16 9"

send "1129 47 5 10 4\n"
simple_expect "%1129 16 10"

send "1130 47 6 1 10\n"
simple_expect "=1130 2 1 { 5 }"

send "1131 104 5 1 50\n"
simple_expect "=1131 1 9 0 1 1 8 { 1 2 3 6 7 8 9 10 }"

send "1132 104 5 1 9\n"
simple_expect "=1132 1 9 0 1 1 8 { 1 2 3 6 7 8 9 10 }"

send "1133 104 5 1 8\n"
simple_expect "=1133 1 9 0 1 1 8 { 1 2 3 6 7 8 9 10 }"

send "1134 104 5 1 7\n"
simple_expect "=1134 1 8 1 1 1 7 { 1 2 3 6 7 8 9 }"

send "1135 104 5 1 6\n"
simple_expect "=1135 1 7 1 1 1 6 { 1 2 3 6 7 8 }"

send "1136 104 5 1 2\n"
simple_expect "=1136 1 3 1 1 1 2 { 1 2 }"

send "1137 104 5 1 1\n"
simple_expect "=1137 1 2 1 1 1 1 { 1 }"

send "1138 104 5 1 0\n"
simple_expect "=1138 1 1 1 0 0 \\*"

send "1139 104 5 0 0\n"
simple_expect "%1139 17 0"

send "1140 104 5 0 1\n"
simple_expect "%1140 17 0"

send "1141 104 5 0 50\n"
simple_expect "%1141 17 0"

send "1142 104 6 1 10\n"
simple_expect "=1142 1 3 0 1 2 1 { 5 }"

  send "2061 122 5 0 50\n"
  simple_expect "=2061 1 9 0 1 1 8 { 1 2 3 6 7 8 9 10 }"

  send "2062 122 5 10 50\n"
  simple_expect "=2062 1 9 0 1 1 8 { 1 2 3 6 7 8 9 10 }"

  send "2063 122 5 10 50\n"
  simple_expect "=2063 1 9 0 1 1 8 { 1 2 3 6 7 8 9 10 }"

  send "2064 122 5 9 50\n"
  simple_expect "=2064 1 9 0 1 1 8 { 1 2 3 6 7 8 9 10 }"

  send "2065 122 5 8 50\n"
  simple_expect "=2065 1 8 0 1 1 7 { 1 2 3 6 7 8 9 }"

  send "2066 122 5 2 50\n"
  simple_expect "=2066 1 2 0 1 1 1 { 1 }"

  send "2067 122 5 8 7\n"
  simple_expect "=2067 1 8 0 1 1 7 { 1 2 3 6 7 8 9 }"

  send "2068 122 5 8 6\n"
  simple_expect "=2068 2 8 1 1 2 6 { 2 3 6 7 8 9 }"

  send "2069 122 5 8 0\n"
  simple_expect "=2069 8 8 1 0 0 \\*"

# Remove some more

talk_to client 0
send "1143 29 6\n"
simple_expect "=1143"

send "1144 29 7\n"
simple_expect "=1144"

send "1145 29 8\n"
simple_expect "=1145"

send "1146 29 9\n"
simple_expect "=1146"

talk_to client 1
simple_expect ":18 14 6 $any_time 5 0 8 0 2 { 0 1 6 5 } 0 \\*"
simple_expect ":18 14 7 $any_time 5 0 8 0 2 { 0 1 6 6 } 0 \\*"
simple_expect ":18 14 8 $any_time 5 0 8 0 2 { 0 1 6 7 } 0 \\*"
simple_expect ":18 14 9 $any_time 5 0 8 0 2 { 0 1 6 8 } 0 \\*"

# Now, these local text numbers exist:
# lno: 1 2 . . . . . .  9
# tno: 1 3 . . . . . . 10

send "1147 103 1 1 2\n"
simple_expect "=1147 1 3 1 1 1 2 { 1 3 }"

send "1148 103 1 2 2\n"
simple_expect "=1148 2 10 0 0 2 { 2 3 9 10 }"

send "1149 103 1 2 3\n"
simple_expect "=1149 2 10 0 0 2 { 2 3 9 10 }"

send "1150 103 1 1 3\n"
simple_expect "=1150 1 10 0 0 3 { 1 1 2 3 9 10 }"

send "1151 103 1 1 4\n"
simple_expect "=1151 1 10 0 0 3 { 1 1 2 3 9 10 }"

send "1152 103 1 1 50\n"
simple_expect "=1152 1 10 0 0 3 { 1 1 2 3 9 10 }"

send "1153 103 1 6 50\n"
simple_expect "=1153 6 10 0 1 9 1 { 10 }"

send "1154 34 1 6 50\n"
simple_expect "=1154 6 4 { 0 0 0 10 }"

send "1155 34 1 1 50\n"
simple_expect "=1155 1 9 { 1 3 0 0 0 0 0 0 10 }"

  send "2070 121 1 3 2\n"
  simple_expect "=2070 1 3 0 1 1 2 { 1 3 }"

  send "2071 121 1 4 2\n"
  simple_expect "=2071 1 4 0 1 1 2 { 1 3 }"

  send "2072 121 1 8 2\n"
  simple_expect "=2072 1 8 0 1 1 2 { 1 3 }"

  send "2073 121 1 9 2\n"
  simple_expect "=2073 1 9 0 1 1 2 { 1 3 }"

  send "2074 121 1 10 2\n"
  simple_expect "=2074 2 10 1 0 2 { 2 3 9 10 }"

  send "2075 121 1 10 3\n"
  simple_expect "=2075 1 10 0 0 3 { 1 1 2 3 9 10 }"

  send "2076 121 1 10 50\n"
  simple_expect "=2076 1 10 0 0 3 { 1 1 2 3 9 10 }"

  send "2077 121 1 40 50\n"
  simple_expect "=2077 1 10 0 0 3 { 1 1 2 3 9 10 }"

  send "2078 121 1 0 50\n"
  simple_expect "=2078 1 10 0 0 3 { 1 1 2 3 9 10 }"

  send "2079 121 1 6 50\n"
  simple_expect "=2079 1 6 0 1 1 2 { 1 3 }"

# Test the limit on no_of_texts
send "1156 103 1 3 256\n"
simple_expect "%1156 46 255"

send "1157 103 1 3 255\n"
simple_expect "=1157 3 10 0 1 9 1 { 10 }"

  send "2080 121 1 0 256\n"
  simple_expect "%2080 46 255"

  send "2081 121 1 0 255\n"
  simple_expect "=2081 1 10 0 0 3 { 1 1 2 3 9 10 }"

# Test to get the map from a conference we are not a member of
send "1158 34 3 1 10\n"
simple_expect "=1158 1 1 { 3 }"

send "1159 103 3 1 10\n"
simple_expect "=1159 1 2 0 1 1 1 { 3 }"

  send "2082 121 3 0 10\n"
  simple_expect "=2082 1 2 0 1 1 1 { 3 }"

# Texts written by person 5:
# ano: 1 2 3 . . . .  8
# tno: 1 2 3 . . . . 10

# Texts written by person 6:
# ano: . 2
# tno: . 5

send "1160 104 5 1 3\n"
simple_expect "=1160 1 4 1 1 1 3 { 1 2 3 }"

send "1161 104 5 2 2\n"
simple_expect "=1161 2 4 1 1 2 2 { 2 3 }"

send "1162 104 5 1 4\n"
simple_expect "=1162 1 9 0 0 4 { 1 1 2 2 3 3 8 10 }"

send "1163 104 5 4 50\n"
simple_expect "=1163 4 9 0 1 8 1 { 10 }"

send "1164 104 5 1 50\n"
simple_expect "=1164 1 9 0 0 4 { 1 1 2 2 3 3 8 10 }"

send "1165 104 5 3 50\n"
simple_expect "=1165 3 9 0 0 2 { 3 3 8 10 }"

send "1166 104 5 3 2\n"
simple_expect "=1166 3 9 0 0 2 { 3 3 8 10 }"

send "1167 104 5 3 1\n"
simple_expect "=1167 3 4 1 1 3 1 { 3 }"

  send "2083 122 5 4 3\n"
  simple_expect "=2083 1 4 0 1 1 3 { 1 2 3 }"

  send "2084 122 5 10 1\n"
  simple_expect "=2084 8 9 1 1 8 1 { 10 }"

  send "2085 122 5 9 1\n"
  simple_expect "=2085 8 9 1 1 8 1 { 10 }"

  send "2086 122 5 8 1\n"
  simple_expect "=2086 3 8 1 1 3 1 { 3 }"

  send "2087 122 5 10 2\n"
  simple_expect "=2087 3 9 1 0 2 { 3 3 8 10 }"

  send "2088 122 5 10 3\n"
  simple_expect "=2088 2 9 1 0 3 { 2 2 3 3 8 10 }"

  send "2089 122 5 10 4\n"
  simple_expect "=2089 1 9 0 0 4 { 1 1 2 2 3 3 8 10 }"

  send "2090 122 5 10 5\n"
  simple_expect "=2090 1 9 0 0 4 { 1 1 2 2 3 3 8 10 }"

  send "2091 122 5 10 50\n"
  simple_expect "=2091 1 9 0 0 4 { 1 1 2 2 3 3 8 10 }"

  send "2092 122 5 10 255\n"
  simple_expect "=2092 1 9 0 0 4 { 1 1 2 2 3 3 8 10 }"

  send "2093 122 5 10 256\n"
  simple_expect "%2093 46 255"

  send "2094 122 5 9 255\n"
  simple_expect "=2094 1 9 0 0 4 { 1 1 2 2 3 3 8 10 }"

  send "2095 122 5 8 255\n"
  simple_expect "=2095 1 8 0 1 1 3 { 1 2 3 }"

send "1168 47 5 1 3\n"
simple_expect "=1168 1 3 { 1 2 3 }"

send "1169 47 5 1 4\n"
simple_expect "=1169 1 4 { 1 2 3 0 }"

send "1170 47 5 1 5\n"
simple_expect "=1170 1 5 { 1 2 3 0 0 }"

send "1171 47 5 1 6\n"
simple_expect "=1171 1 6 { 1 2 3 0 0 0 }"

send "1172 47 5 1 7\n"
simple_expect "=1172 1 7 { 1 2 3 0 0 0 0 }"

send "1173 47 5 1 8\n"
simple_expect "=1173 1 8 { 1 2 3 0 0 0 0 10 }"

send "1174 47 5 1 9\n"
simple_expect "=1174 1 8 { 1 2 3 0 0 0 0 10 }"

send "1175 47 5 2 6\n"
simple_expect "=1175 2 6 { 2 3 0 0 0 0 }"

send "1176 47 5 2 7\n"
simple_expect "=1176 2 7 { 2 3 0 0 0 0 10 }"

send "1177 47 5 3 7\n"
simple_expect "=1177 3 6 { 3 0 0 0 0 10 }"

send "1178 47 5 4 7\n"
simple_expect "=1178 4 5 { 0 0 0 0 10 }"

send "1179 47 5 5 4\n"
simple_expect "=1179 5 4 { 0 0 0 10 }"

send "1180 47 5 7 4\n"
simple_expect "=1180 7 2 { 0 10 }"

send "1181 47 5 8 4\n"
simple_expect "=1181 8 1 { 10 }"

send "1182 104 6 1 50\n"
simple_expect "=1182 1 3 0 1 2 1 { 5 }"

  send "2096 122 6 0 50\n"
  simple_expect "=2096 2 3 0 1 2 1 { 5 }"

send "1183 47 6 1 50\n"
simple_expect "=1183 2 1 { 5 }"

# Test what happens when the oldest texts are removed and there exists
# a few more texts.
talk_to client 0

send "1184 29 1\n"
simple_expect "=1184"

send "1185 29 3\n"
simple_expect "=1185"

talk_to client 1
simple_expect ":18 14 1 $any_time 5 0 8 0 2 { 0 1 6 1 } 0 \\*"
simple_expect ":18 14 3 $any_time 5 0 8 0 4 { 0 1 6 2 1 3 6 1 } 0 \\*"

send "1186 86 [holl "text0011"] 1 { 0 1 } 0 { }\n"
simple_expect ":18 15 11 $any_time 6 0 8 0 2 { 0 1 6 10 } 0 \\*" "async to c1"
simple_expect "=1186 11" "Eskil created text 11"

send "1187 86 [holl "text0012"] 1 { 0 1 } 0 { }\n"
simple_expect ":18 15 12 $any_time 6 0 8 0 2 { 0 1 6 11 } 0 \\*" "async to c1"
simple_expect "=1187 12" "Eskil created text 12"

send "1188 86 [holl "text0013"] 1 { 0 1 } 0 { }\n"
simple_expect ":18 15 13 $any_time 6 0 8 0 2 { 0 1 6 12 } 0 \\*" "async to c1"
simple_expect "=1188 13" "Eskil created text 13"

talk_to client 0
simple_expect ":16 0 11 $any_time 6 0 8 0 2 { 0 1 6 10 }" "async to c0"
simple_expect ":16 0 12 $any_time 6 0 8 0 2 { 0 1 6 11 }" "async to c0"
simple_expect ":16 0 13 $any_time 6 0 8 0 2 { 0 1 6 12 }" "async to c0"

talk_to client 1

send "1189 29 11\n"
simple_expect ":18 14 11 $any_time 6 0 8 0 2 { 0 1 6 10 } 0 \\*"
simple_expect "=1189"

# Now, these local text numbers exist:
# lno:  9 . 11 12
# tno: 10 . 12 13

send "1190 34 1 0 1\n"
simple_expect "=1190 9 0 \\*"

send "1191 34 1 1 1\n"
simple_expect "=1191 9 0 \\*"

send "1192 34 1 1 8\n"
simple_expect "=1192 9 0 \\*"

send "1193 34 1 1 9\n"
simple_expect "=1193 9 1 { 10 }"

send "1194 34 1 1 10\n"
simple_expect "=1194 9 2 { 10 0 }"

send "1195 34 1 1 11\n"
simple_expect "=1195 9 3 { 10 0 12 }"

send "1196 34 1 1 12\n"
simple_expect "=1196 9 4 { 10 0 12 13 }"

send "1197 34 1 1 13\n"
simple_expect "=1197 9 4 { 10 0 12 13 }"

send "1198 34 1 9 13\n"
simple_expect "=1198 9 4 { 10 0 12 13 }"

# Test to get the map from a completely empty conference
# Text 5 was a member of this conference.
send "1199 34 3 1 10\n"
simple_expect "=1199 2 0 \\*"

send "1200 103 3 1 10\n"
simple_expect "=1200 1 2 0 0 0 \\*"

  send "2097 121 3 0 10\n"
  simple_expect "=2097 2 2 0 0 0 \\*"

  send "2098 121 3 400 10\n"
  simple_expect "=2098 2 2 0 0 0 \\*"

  send "2099 121 3 3 10\n"
  simple_expect "=2099 2 2 0 0 0 \\*"

  send "2100 121 3 2 10\n"
  simple_expect "=2100 2 2 0 0 0 \\*"

  send "2101 121 3 1 10\n"
  simple_expect "=2101 1 1 0 0 0 \\*"

# Texts written by person 5:
# ano: . 2 . . . . .  8
# tno: . 2 . . . . . 10

# Texts written by person 6:
# ano: . 2 .  4  5
# tno: . 5 . 12 13

send "1201 104 5 1 50\n"
simple_expect "=1201 1 9 0 0 2 { 2 2 8 10 }"

send "1202 104 5 1 2\n"
simple_expect "=1202 1 9 0 0 2 { 2 2 8 10 }"

send "1203 104 5 8 50\n"
simple_expect "=1203 8 9 0 1 8 1 { 10 }"

send "1204 104 5 9 50\n"
simple_expect "%1204 16 9"

send "1205 104 5 1 1\n"
simple_expect "=1205 1 3 1 1 2 1 { 2 }"

  send "2102 122 5 0 50\n"
  simple_expect "=2102 2 9 0 0 2 { 2 2 8 10 }"

  send "2103 122 5 0 2\n"
  simple_expect "=2103 2 9 0 0 2 { 2 2 8 10 }"

  send "2104 122 5 3 50\n"
  simple_expect "=2104 2 3 0 1 2 1 { 2 }"

  send "2105 122 5 25 1\n"
  simple_expect "=2105 8 9 1 1 8 1 { 10 }"

  send "2106 122 5 2 50\n"
  simple_expect "=2106 2 2 0 0 0 \\*"

send "1206 104 6 1 1\n"
simple_expect "=1206 1 3 1 1 2 1 { 5 }"

send "1207 104 6 1 2\n"
simple_expect "=1207 1 5 1 1 2 3 { 5 0 12 }"

send "1208 104 6 1 3\n"
simple_expect "=1208 1 6 0 1 2 4 { 5 0 12 13 }"

  send "2107 122 6 9 1\n"
  simple_expect "=2107 5 6 1 1 5 1 { 13 }"

  send "2108 122 6 9 2\n"
  simple_expect "=2108 4 6 1 1 4 2 { 12 13 }"

  send "2109 122 6 9 3\n"
  simple_expect "=2109 2 6 0 1 2 4 { 5 0 12 13 }"

  send "2110 122 6 9 50\n"
  simple_expect "=2110 2 6 0 1 2 4 { 5 0 12 13 }"

  send "2111 122 6 0 1\n"
  simple_expect "=2111 5 6 1 1 5 1 { 13 }"

  send "2112 122 6 0 2\n"
  simple_expect "=2112 4 6 1 1 4 2 { 12 13 }"

  send "2113 122 6 0 3\n"
  simple_expect "=2113 2 6 0 1 2 4 { 5 0 12 13 }"

  send "2114 122 6 0 4\n"
  simple_expect "=2114 2 6 0 1 2 4 { 5 0 12 13 }"

  send "2115 122 6 0 50\n"
  simple_expect "=2115 2 6 0 1 2 4 { 5 0 12 13 }"

send "1209 104 6 2 1\n"
simple_expect "=1209 2 3 1 1 2 1 { 5 }"

send "1210 104 6 2 2\n"
simple_expect "=1210 2 5 1 1 2 3 { 5 0 12 }"

send "1211 104 6 2 3\n"
simple_expect "=1211 2 6 0 1 2 4 { 5 0 12 13 }"

  send "2116 122 6 5 1\n"
  simple_expect "=2116 4 5 1 1 4 1 { 12 }"

  send "2117 122 6 5 2\n"
  simple_expect "=2117 2 5 0 1 2 3 { 5 0 12 }"

  send "2118 122 6 5 3\n"
  simple_expect "=2118 2 5 0 1 2 3 { 5 0 12 }"

send "1212 104 6 3 1\n"
simple_expect "=1212 3 5 1 1 4 1 { 12 }"

send "1213 104 6 3 2\n"
simple_expect "=1213 3 6 0 1 4 2 { 12 13 }"

send "1214 104 6 3 3\n"
simple_expect "=1214 3 6 0 1 4 2 { 12 13 }"

  send "2119 122 6 4 1\n"
  simple_expect "=2119 2 4 0 1 2 1 { 5 }"

  send "2120 122 6 4 2\n"
  simple_expect "=2120 2 4 0 1 2 1 { 5 }"

  send "2121 122 6 4 40\n"
  simple_expect "=2121 2 4 0 1 2 1 { 5 }"

send "1215 104 6 4 1\n"
simple_expect "=1215 4 5 1 1 4 1 { 12 }"

send "1216 104 6 4 2\n"
simple_expect "=1216 4 6 0 1 4 2 { 12 13 }"

send "1217 104 6 4 3\n"
simple_expect "=1217 4 6 0 1 4 2 { 12 13 }"

  send "2122 122 6 3 40\n"
  simple_expect "=2122 2 3 0 1 2 1 { 5 }"

send "1218 47 5 0 7\n"
simple_expect "=1218 2 7 { 2 0 0 0 0 0 10 }"

send "1219 47 5 1 7\n"
simple_expect "=1219 2 7 { 2 0 0 0 0 0 10 }"

send "1220 47 5 2 7\n"
simple_expect "=1220 2 7 { 2 0 0 0 0 0 10 }"

send "1221 47 5 3 7\n"
simple_expect "=1221 3 6 { 0 0 0 0 0 10 }"

send "1222 47 5 1 6\n"
simple_expect "=1222 2 6 { 2 0 0 0 0 0 }"

send "1223 47 5 2 8\n"
simple_expect "=1223 2 7 { 2 0 0 0 0 0 10 }"

send "1224 47 5 3 8\n"
simple_expect "=1224 3 6 { 0 0 0 0 0 10 }"

send "1225 47 5 2 50\n"
simple_expect "=1225 2 7 { 2 0 0 0 0 0 10 }"

send "1226 47 5 3 50\n"
simple_expect "=1226 3 6 { 0 0 0 0 0 10 }"

send "1227 47 6 1 2\n"
simple_expect "=1227 2 2 { 5 0 }"

send "1228 47 6 1 1\n"
simple_expect "=1228 2 1 { 5 }"

send "1229 47 6 3 4\n"
simple_expect "=1229 3 3 { 0 12 13 }"

send "1230 47 6 3 2\n"
simple_expect "=1230 3 2 { 0 12 }"

send "1231 47 6 3 1\n"
simple_expect "=1231 3 1 { 0 }"

send "1232 47 6 4 1\n"
simple_expect "=1232 4 1 { 12 }"

send "1233 47 6 4 4\n"
simple_expect "=1233 4 2 { 12 13 }"

send "1234 47 6 4 2\n"
simple_expect "=1234 4 2 { 12 13 }"

send "1235 47 6 4 1\n"
simple_expect "=1235 4 1 { 12 }"

send "1236 47 6 5 1\n"
simple_expect "=1236 5 1 { 13 }"

send "1237 47 6 5 50\n"
simple_expect "=1237 5 1 { 13 }"

send "1238 47 6 6 1\n"
simple_expect "%1238 16 6"

# Delete text 5
send "1239 29 5\n"
# No async message, since there are no members of the text.
# simple_expect ":18 14 5 $any_time 6 0 8 0 0 \\* 1 { 1 1 6 $any_time 00000000 1 [holl "text/plain"] }"
simple_expect "=1239"

# Texts written by person 5:
# ano: . 2 . . . . .  8
# tno: . 2 . . . . . 10

# Texts written by person 6:
# ano: . . .  4  5
# tno: . . . 12 13

send "1240 104 6 1 2\n"
simple_expect "=1240 1 6 0 1 4 2 { 12 13 }"

send "1241 104 6 1 1\n"
simple_expect "=1241 1 5 1 1 4 1 { 12 }"

  send "2123 122 6 6 2\n"
  simple_expect "=2123 4 6 0 1 4 2 { 12 13 }"

  send "2124 122 6 6 1\n"
  simple_expect "=2124 5 6 1 1 5 1 { 13 }"

  send "2125 122 6 0 2\n"
  simple_expect "=2125 4 6 0 1 4 2 { 12 13 }"

  send "2126 122 6 0 1\n"
  simple_expect "=2126 5 6 1 1 5 1 { 13 }"

send "1242 47 6 1 2\n"
simple_expect "=1242 4 2 { 12 13 }"

send "1243 47 6 1 1\n"
simple_expect "=1243 4 1 { 12 }"

# Delete all remaining texts that person 5 have created.

talk_to client 0
send "1244 29 2\n"
simple_expect "=1244"

send "1245 29 10\n"
simple_expect "=1245"

talk_to client 1
# Text 2 is sent to conference 2, so we get no async message about it.
simple_expect ":18 14 10 $any_time 5 0 8 0 2 { 0 1 6 9 } 0 \\*"

# Texts written by person 5:
# ano: . . . . . . . . (9)
# tno: . . . . . . . .

# Texts written by person 6:
# ano: . . .  4  5
# tno: . . . 12 13

send "1246 104 5 1 2\n"
simple_expect "=1246 1 9 0 0 0 \\*"

send "1247 104 5 1 1\n"
simple_expect "=1247 1 9 0 0 0 \\*"

send "1248 104 5 1 50\n"
simple_expect "=1248 1 9 0 0 0 \\*"

send "1249 104 5 7 1\n"
simple_expect "=1249 7 9 0 0 0 \\*"

send "1250 104 5 8 1\n"
simple_expect "=1250 8 9 0 0 0 \\*"

send "1251 104 5 8 2\n"
simple_expect "=1251 8 9 0 0 0 \\*"

send "1252 104 5 9 1\n"
simple_expect "%1252 16 9"

  send "2127 122 5 0 50\n"
  simple_expect "=2127 9 9 0 0 0 \\*"

  send "2128 122 5 9 50\n"
  simple_expect "=2128 9 9 0 0 0 \\*"

  send "2129 122 5 10 50\n"
  simple_expect "=2129 9 9 0 0 0 \\*"

  send "2130 122 5 8 50\n"
  simple_expect "=2130 8 8 0 0 0 \\*"

  send "2131 122 5 2 50\n"
  simple_expect "=2131 2 2 0 0 0 \\*"

  send "2132 122 5 1 50\n"
  simple_expect "=2132 1 1 0 0 0 \\*"

  send "2133 122 5 0 1\n"
  simple_expect "=2133 9 9 0 0 0 \\*"

  send "2134 122 5 9 1\n"
  simple_expect "=2134 9 9 0 0 0 \\*"

  send "2135 122 5 10 1\n"
  simple_expect "=2135 9 9 0 0 0 \\*"

  send "2136 122 5 8 1\n"
  simple_expect "=2136 8 8 0 0 0 \\*"

  send "2137 122 5 2 1\n"
  simple_expect "=2137 2 2 0 0 0 \\*"

  send "2138 122 5 1 1\n"
  simple_expect "=2138 1 1 0 0 0 \\*"

send "1253 47 5 1 2\n"
simple_expect "=1253 9 0 \\*"

send "1254 47 5 7 2\n"
simple_expect "=1254 9 0 \\*"

send "1255 47 5 7 1\n"
simple_expect "=1255 9 0 \\*"

send "1256 47 5 8 1\n"
simple_expect "=1256 9 0 \\*"

send "1257 47 5 0 50\n"
simple_expect "=1257 9 0 \\*"

send "1258 47 5 1 50\n"
simple_expect "=1258 9 0 \\*"

send "1259 47 5 9 50\n"
simple_expect "%1259 16 9*"

# Shut down the server.

talk_to client 0

send "1260 42 255\n"
simple_expect "=1260" "42=enable succeeded"
send "1261 44 0\n"
simple_expect "=1261" "44=shutdown-kom succeeded"
client_death 0

client_death 1

lyskomd_death
