# Test suite for lyskomd.
# Copyright (C) 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test the passive-message-invert bit of Membership-Type.


lyskomd_start

client_start 0
talk_to client 0
send "A0H\n"
simple_expect "LysKOM"
send "1000 62 5 6Hgazonk 1\n"
simple_expect "=1000"

# Create the user "Listener".  Log in as it.
client_start 1
talk_to client 1
send "A0H\n"
simple_expect "LysKOM"
send "1001 89 [holl "Listener"] [holl "none"] 00000000 0 { }\n"
simple_expect "=1001 6"
send "1002 62 6 [holl "none"] 1\n"
simple_expect "=1002"

# Join conferences:
#
# Conf_no   passive   passive-message-invert
#    1         0             0
#    2         0             1
#    3         1             0
#    4         1             1

send "1003 100 1 6 200 10 00000000\n"
simple_expect "=1003"
send "1004 100 2 6 200 10 00010000\n"
simple_expect "=1004"
send "1005 100 3 6 200 10 01000000\n"
simple_expect "=1005"
send "1006 100 4 6 200 10 01010000\n"
simple_expect "=1006"

# Send messages to the four conferences and ensure they arrive properly.
talk_to client 0
send "1007 53 1 [holl "msg 1"]\n"
simple_expect "=1007"
talk_to client 1
simple_expect ":3 12 1 5 [holl "msg 1"]"

talk_to client 0
send "1008 53 2 [holl "msg 2"]\n"
simple_expect "%1008 53 0"

send "1009 53 3 [holl "msg 3"]\n"
simple_expect "%1009 53 0"

talk_to client 0
send "1010 53 4 [holl "msg 4"]\n"
simple_expect "=1010"
talk_to client 1
simple_expect ":3 12 4 5 [holl "msg 4"]"

# Shut everything down.
talk_to client 0
send "1011 42 255\n"
simple_expect "=1011"
send "1012 44 0\n"
simple_expect "=1012"
client_death 0
client_death 1
lyskomd_death
