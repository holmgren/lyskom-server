# Test suite for lyskomd.
# Copyright (C) 2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test conversion of recipients from one type to another.
#
# Persons:
#     5: admin
#     6: super: creator of 10 and 11
#     7: member: a member of 10 and 11
#     8: bystander: not a member
#     9: provocateur: not a member
#
# Conferences:
#    10: public
#    11: secret
#
# Texts:
#     1:
#     2:
#     3:
#     4:

lyskomd_start

# Log in as admin, and enable.
client_start 5
send "A[holl "five"]\n"
simple_expect "LysKOM" "connected"
# 114:first-unused-conf-no
send "1000 114\n"
simple_expect "=1000 6"
# 115:first-unused-text-no
send "1001 115\n"
simple_expect "=1001 1"
send "1002 80 1 { 21 }\n"
simple_expect "=1002"
send "1003 0 5 [holl "gazonk"]\n"
simple_expect "=1003"
send "1004 42 255\n"
simple_expect "=1004"

# Create person 6 and log in.
client_start 6
send "A[holl "six"]\n"
simple_expect "LysKOM" "connected"
send "1005 80 1 { 21 }\n"
simple_expect "=1005"
send "1006 89 [holl "super"] [holl "super"] 00000000 0 { }\n"
simple_expect "=1006 6"
send "1007 0 6 [holl "super"]\n"
simple_expect "=1007"

# Create person 7 and log in.
client_start 7
send "A[holl "seven"]\n"
simple_expect "LysKOM" "connected"
send "1008 80 1 { 21 }\n"
simple_expect "=1008"
send "1009 89 [holl "member"] [holl "member"] 00000000 0 { }\n"
simple_expect "=1009 7"
send "1010 0 7 [holl "member"]\n"
simple_expect "=1010"

# Create person 8 and log in.
client_start 8
send "A[holl "eight"]\n"
simple_expect "LysKOM" "connected"
send "1011 80 1 { 21 }\n"
simple_expect "=1011"
send "1012 89 [holl "bystander"] [holl "bystander"] 00000000 0 { }\n"
simple_expect "=1012 8"
send "1013 0 8 [holl "bystander"]\n"
simple_expect "=1013"

# Create person 9 and log in.
client_start 9
send "A[holl "nine"]\n"
simple_expect "LysKOM" "connected"
send "1014 80 1 { 21 }\n"
simple_expect "=1014"
send "1015 89 [holl "provocateur"] [holl "provocateur"] 00000000 0 { }\n"
simple_expect "=1015 9"
send "1016 0 9 [holl "provocateur"]\n"
simple_expect "=1016"

# Create conferences 10 and 11, and let person 6 and 7 join them.
talk_to client 6
send "1017 88 [holl "public"] 00000000 0 { }\n"
simple_expect "=1017 10"
send "1018 88 [holl "secret"] 10100000 0 { }\n"
simple_expect "=1018 11"
send "1019 100 10 6 200 1 00000000\n"
simple_expect "=1019"
send "1020 100 10 7 200 1 00000000\n"
lyskomd_expect "Person 7 added to conference 10 by 6\."
simple_expect "=1020"
send "1021 100 11 6 200 1 00000000\n"
simple_expect "=1021"
send "1022 100 11 7 200 1 00000000\n"
lyskomd_expect "Person 7 added to conference 11 by 6\."
simple_expect "=1022"

# Person 7 accepts the invitations.
talk_to client 7
send "1023 100 10 7 200 1 00000000\n"
simple_expect "=1023"
send "1024 100 11 7 200 1 00000000\n"
simple_expect "=1024"

# Person 6 creates texts 1 and 2.
talk_to client 6
send "1025 86 [holl "text 1"] 1 { 0 1 } 0 { }\n"
simple_expect "=1025 1"
send "1026 86 [holl "text 2"] 1 { 0 1 } 0 { }\n"
simple_expect "=1026 2"

# Person 8 creates text 3 and 4.
talk_to client 8
send "1027 86 [holl "text 3"] 1 { 0 1 } 0 { }\n"
simple_expect "=1027 3"
send "1028 86 [holl "text 4"] 1 { 0 1 } 0 { }\n"
simple_expect "=1028 4"

# Person 9 is not able to transform either of the recipients.
talk_to client 9
foreach text {1 2 3 4} {
    send "1 30 $text 1 0\n"
    simple_expect "%1 27 1"
    send "2 90 $text\n"
    simple_expect "=2 $any_time $any_num 0 6 0 2 { 0 1 6 $text } 0 \\*"
    foreach rcpt {1 15} {
	send "3 30 $text 1 $rcpt\n"
	simple_expect "%3 12 1"
	send "4 90 $text\n"
	simple_expect "=4 $any_time $any_num 0 6 0 2 { 0 1 6 $text } 0 \\*"
    }
}

# Person 8 is not able to transform the texts created by person 6.
talk_to client 9
foreach text {1 2} {
    send "5 30 $text 1 0\n"
    simple_expect "%5 27 1"
    send "6 90 $text\n"
    simple_expect "=6 $any_time $any_num 0 6 0 2 { 0 1 6 $text } 0 \\*"
    foreach rcpt {1 15} {
	send "7 30 $text 1 $rcpt\n"
	simple_expect "%7 12 1"
	send "8 90 $text\n"
	simple_expect "=8 $any_time $any_num 0 6 0 2 { 0 1 6 $text } 0 \\*"
    }
}

# Person 7 is not able to transform any text.
talk_to client 9
foreach text {1 2 3 4} {
    send "9 30 $text 1 0\n"
    simple_expect "%9 27 1"
    send "10 90 $text\n"
    simple_expect "=10 $any_time $any_num 0 6 0 2 { 0 1 6 $text } 0 \\*"
    foreach rcpt {1 15} {
	send "11 30 $text 1 $rcpt\n"
	simple_expect "%11 12 1"
	send "12 90 $text\n"
	simple_expect "=12 $any_time $any_num 0 6 0 2 { 0 1 6 $text } 0 \\*"
    }
}

# Person 8 can transform the texts he wrote.
talk_to client 8
foreach text {3 4} {
    send "13 30 $text 1 0\n"
    simple_expect "%13 27 1"
    send "14 90 $text\n"
    simple_expect "=14 $any_time $any_num 0 6 0 2 { 0 1 6 $text } 0 \\*"
    foreach rcpt {1 15 0} {
	send "15 30 $text 1 $rcpt\n"
	simple_expect "=15"
	send "16 90 $text\n"
	simple_expect "=16 $any_time 8 0 6 0 2 { $rcpt 1 6 $text } 0 \\*"
    }
}

# Person 5 can transform all texts.
talk_to client 5
foreach text {1 2 3 4} {
    send "17 30 $text 1 0\n"
    simple_expect "%17 27 1"
    send "18 90 $text\n"
    simple_expect "=18 $any_time $any_num 0 6 0 2 { 0 1 6 $text } 0 \\*"
    foreach rcpt {1 15 0} {
	send "19 30 $text 1 $rcpt\n"
	simple_expect "=19"
	send "20 90 $text\n"
	simple_expect "=20 $any_time $any_num 0 6 0 2 { $rcpt 1 6 $text } 0 \\*"
    }
}

# Person 6 can transform the texts he wrote, but not the others.
talk_to client 6
foreach text {1 2} {
    send "21 30 $text 1 0\n"
    simple_expect "%21 27 1"
    send "22 90 $text\n"
    simple_expect "=22 $any_time $any_num 0 6 0 2 { 0 1 6 $text } 0 \\*"
    foreach rcpt {1 15 0} {
	send "23 30 $text 1 $rcpt\n"
	simple_expect "=23"
	send "24 90 $text\n"
	simple_expect "=24 $any_time $any_num 0 6 0 2 { $rcpt 1 6 $text } 0 \\*"
    }
}
foreach text {3 4} {
    send "25 30 $text 1 0\n"
    simple_expect "%25 27 1"
    send "26 90 $text\n"
    simple_expect "=26 $any_time $any_num 0 6 0 2 { 0 1 6 $text } 0 \\*"
    foreach rcpt {1 15} {
	send "27 30 $text 1 $rcpt\n"
	simple_expect "%27 12 1"
	send "28 90 $text\n"
	simple_expect "=28 $any_time $any_num 0 6 0 2 { 0 1 6 $text } 0 \\*"
    }
}

# The fun is about to start.  Just a quick recollection of the state first.
#
# Text 1: 0 1 6 1   author=6
# Text 2: 0 1 6 2   author=6
# Text 3: 0 1 6 3   author=8
# Text 4: 0 1 6 4   author=8
send "1029 90 1\n"
simple_expect "=1029 $any_time 6 0 6 0 2 { 0 1 6 1 } 0 \\*"
send "1030 90 2\n"
simple_expect "=1030 $any_time 6 0 6 0 2 { 0 1 6 2 } 0 \\*"
send "1031 90 3\n"
simple_expect "=1031 $any_time 8 0 6 0 2 { 0 1 6 3 } 0 \\*"
send "1032 90 4\n"
simple_expect "=1032 $any_time 8 0 6 0 2 { 0 1 6 4 } 0 \\*"

talk_to client 6
send "1033 30 1 10 0\n"
simple_expect "=1033"
send "1034 90 1\n"
simple_expect "=1034 $any_time 6 0 6 0 5 { 0 1 6 1 0 10 6 1 9 $any_time } 0 \\*"

send "1035 30 3 10 1\n"
simple_expect "=1035"
send "1036 90 3\n"
simple_expect "=1036 $any_time 8 0 6 0 6 { 0 1 6 3 1 10 6 2 8 6 9 $any_time } 0 \\*"

talk_to client 8
send "1037 30 4 11 15\n"
simple_expect "%1037 9 11"
send "1038 30 4 10 15\n"
simple_expect "=1038"
send "1039 31 4 1\n"
simple_expect "=1039"
send "1040 90 4\n"
simple_expect "=1040 $any_time 8 0 6 0 3 { 15 10 6 3 9 $any_time } 0 \\*"

talk_to client 7
send "1041 32 4 2\n"
simple_expect "=1041"
send "1042 90 4\n"
simple_expect "=1042 $any_time 8 0 6 0 6 { 15 10 6 3 9 $any_time 2 2 8 7 9 $any_time } 0 \\*"
send "1043 90 2\n"
simple_expect "=1043 $any_time 6 0 6 0 3 { 0 1 6 2 3 4 } 0 \\*"

# Current state:
# Text 1: 0 1 6 1  0 10 6 1     9 *              author=6 (can: 5 6)
# Text 2: 0 1 6 2  3  4                          author=6
# Text 3: 0 1 6 3  1 10 6 2 8 6 9 *              author=8 (can: 5 6 8)
# Text 4:         15 10 6 3     9 * 2 2 8 7 9 *  author=8 (can: 5 6 8)

# Check that person 7 cannot modify the recipient of text 4, even
# though there is a "sent-by 7" later on in that text status.
send "1044 30 4 10 1\n"
simple_expect "%1044 12 10"

# Text 1: recipient 10.  Cannot change:
foreach c {7 8 9} {
    talk_to client $c
    send "29 30 1 10 1\n"
    simple_expect "%29 12 10"
}

# Text 3: recipient 10.  Cannot change:
foreach c {7 9} {
    talk_to client $c
    send "30 30 3 10 0\n"
    simple_expect "%30 12 10"
}

# Text 4: recipient 10.  Cannot change:
foreach c {7 9} {
    talk_to client $c
    send "31 30 4 10 1\n"
    simple_expect "%31 12 10"
}

# Text 1: recipient 10.  Can change:
foreach c {5 6} {
    talk_to client $c
    foreach rcpt {1 15 0} {
	send "32 30 1 10 $rcpt\n"
	simple_expect "=32"
    }
}

# Text 3: recipient 10.  Can change:
foreach c {5 6 8} {
    talk_to client $c
    foreach rcpt {15 0 1} {
	send "33 30 3 10 $rcpt\n"
	simple_expect "=33"
    }
}

# Text 4: recipient 10.  Can change:
foreach c {5 6 8} {
    talk_to client $c
    foreach rcpt {1 0 15} {
	send "34 30 4 10 $rcpt\n"
	simple_expect "=34"
    }
}

# Make person 7 supervisor of person 6.  This gives person 7 the
# ability to modify text 1 (because 7 is now supervisor of author)
# and text 3 (7 is supervisor of sender), but not 4 (even though 7 is
# supervisor of the supervisor of the recipient).
talk_to client 6
send "1045 18 6 7\n"
simple_expect "=1045"
talk_to client 7
foreach rcpt {1 15 0} {
    send "35 30 1 10 $rcpt\n"
    good_bad_expect "=35" "%(12|27) 10"
}
foreach rcpt {15 0 1} {
    send "36 30 3 10 $rcpt\n"
    good_bad_expect "=36" "%(12|27) 10"
}
send "1046 30 4 10 0\n"
simple_expect "%1046 12 10"

# If you can alter it, you should be able to remove it.
send "1047 31 1 10\n"
simple_expect "=1047"
send "1048 31 3 10\n"
good_bad_expect "=1048" "%12 3"
send "1049 31 4 10\n"
simple_expect "%1049 12 4"

# Make text 2 a footnote of text 1.
talk_to client 6
send "1050 37 2 1\n"
simple_expect "=1050"

# Attempt to make text 2 a comment of text 1.  This should not be
# possible, since it already is a footnote.
send "1051 32 2 1\n"
good_bad_expect "%1051 29 2" "="

# Make text 4 a comment of text 3.
talk_to client 8
send "1052 32 4 3\n"
simple_expect "=1052"

# And now try to make it a footnote as well.
send "1053 37 4 3\n"
good_bad_expect "%1053 28 4" "="



# Shut down.
talk_to client 5
send "1054 44 0\n"
simple_expect "=1054"
client_death 5
client_death 6
client_death 7
client_death 8

lyskomd_death
