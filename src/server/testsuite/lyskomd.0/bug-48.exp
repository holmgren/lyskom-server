# Test suite for lyskomd.
# Copyright (C) 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 


# Check that mark-as-read, set-read-ranges, mark-as-unread and
# query-read-texts doesn't leak info about secret conferences.

lyskomd_start
client_start 0
talk_to client 0
send "A3Hfoo\n"
simple_expect "LysKOM"
send "1000 62 5 [holl "gazonk"] 0\n"
simple_expect ":2 9 5 1"
simple_expect "=1000"

# Create a secret conference.
send "1001 88 [holl "secret"] 10101000 0 { }\n"
simple_expect "=1001 6"

# Join it.
send "1002 100 6 5 100 0 00000000\n"
simple_expect "=1002"

# Log out.
send "1003 1\n"
simple_expect ":2 13 5 1"
simple_expect "=1003"

# Create an unprivileged observer, that isn't allowed to know anything
# about the secret conference.
send "1004 89 [holl "observer"] [holl "passwd"] 00000000 0 { }\n"
simple_expect "=1004 7"

send "1005 62 7 [holl "passwd"] 0\n"
simple_expect ":2 9 7 1"
simple_expect "=1005"

# We want the error code "undefined-conference", not anything else
# such as "not-member".

# mark-as-read
send "1006 27 6 1 { 1 }\n"
simple_expect "%1006 9 6"

# mark-as-unread
send "1007 109 6 1\n"
simple_expect "%1007 9 6"

# set-read-ranges
send "1008 110 6 1 { 1 1 }\n"
simple_expect "%1008 9 6"

# query-read-texts, self.
send "1009 107 7 6 1 0\n"
simple_expect "%1009 9 6"

# query-read-texts, somebody who *is* a member.
send "1010 107 5 6 1 0\n"
simple_expect "%1010 9 6"

# query-read-texts-10, self.
send "1011 98 7 6\n"
simple_expect "%1011 9 6"

# query-read-texts-10, somebody who *is* a member.
send "1012 98 5 6\n"
simple_expect "%1012 9 6"

# query-read-texts-old, self.
send "1013 9 7 6\n"
simple_expect "%1013 9 6"

# query-read-texts-old, somebody who *is* a member.
send "1014 9 5 6\n"
simple_expect "%1014 9 6"

# Shut everything down.
system "kill -TERM $lyskomd_pid"
lyskomd_death {} signal
client_death 0

