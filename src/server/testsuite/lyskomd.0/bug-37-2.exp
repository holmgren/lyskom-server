# Test suite for lyskomd.
# Copyright (C) 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test bug 37: get_unread_confs does not check that the caller is
# allowed to see the person, and returns the correct result even for
# secret persons, including secret confs!
#
# Variant two: test with all memberships being secret.
#
# We also check get-membership, get-membership-10, get-membership-old,
# query-read-texts, query-read-texts-10 and query-read-texts-old.
#
# While we are at it, we also test the unread-is-secret personal flag.

#
# Startup and create the players
#

# Start the server.
lyskomd_start

# Create and log in as foo (person 6).
client_start 0
send "A3Hfoo\n"
simple_expect "LysKOM"
send "1000 80 0 { }\n"
simple_expect "=1000"
send "1001 89 [holl "foo"] [holl "p6"] 00000000 0 { }\n"
simple_expect "=1001 6"
send "1002 62 6 [holl "p6"] 0\n"
simple_expect "=1002"

# Create and log in as bar (person 7; secret).
client_start 1
send "A3Hbar\n"
simple_expect "LysKOM"
send "1003 80 0 { }\n"
simple_expect "=1003"
send "1004 89 [holl "bar"] [holl "p7"] 00000000 0 { }\n"
simple_expect "=1004 7"
send "1005 62 7 [holl "p7"] 0\n"
simple_expect "=1005"
send "1006 21 7 10111000\n"
simple_expect "=1006"

# Create and log in as citrus (person 8; has unread-is-secret set).
client_start 2
send "A6Hcitrus\n"
simple_expect "LysKOM"
send "1007 80 0 { }\n"
simple_expect "=1007"
send "1008 89 [holl "citrus"] [holl "p8"] 10000000 0 { }\n"
simple_expect "=1008 8"
send "1009 62 8 [holl "p8"] 0\n"
simple_expect "=1009"

# Create and log in as gazonk (person 9).  This is the observer.
client_start 3
send "A6Hgazonk\n"
simple_expect "LysKOM"
send "1010 80 0 { }\n"
simple_expect "=1010"
send "1011 89 [holl "gazonk"] [holl "p9"] 00000000 0 { }\n"
simple_expect "=1011 9"
send "1012 62 9 [holl "p9"] 0\n"
simple_expect "=1012"

#
# Create conferences.
#

# As bar, create secret (conf 10; secret).
talk_to client 1
send "1013 88 [holl "secret"] 10101000 0 { }\n"
simple_expect "=1013 10"

# As bar, create rd-prot (conf 11; rd-prot).
send "1014 88 [holl "rd-prot"] 10001000 0 { }\n"
simple_expect "=1014 11"

# As bar, create public (conf 12).
send "1015 88 [holl "public"] 00001000 0 { }\n"
simple_expect "=1015 12"

#
# Join conferences
#

# bar joins secret, rd-prot and public.
send "1016 100 10 7 200 1 00100000\n"
simple_expect "=1016"
send "1017 100 11 7 200 2 00100000\n"
simple_expect "=1017"
send "1018 100 12 7 200 3 00100000\n"
simple_expect "=1018"

# bar invites foo to secret, rd-prot and public.
send "1019 100 10 6 200 1 00100000\n"
lyskomd_expect "Person 6 added to conference 10 by 7."
simple_expect "=1019"
send "1020 100 11 6 200 2 00100000\n"
lyskomd_expect "Person 6 added to conference 11 by 7."
simple_expect "=1020"
send "1021 100 12 6 200 3 00100000\n"
lyskomd_expect "Person 6 added to conference 12 by 7."
simple_expect "=1021"

# bar invites citrus to secret, rd-prot and public.
send "1022 100 10 8 200 1 00100000\n"
lyskomd_expect "Person 8 added to conference 10 by 7."
simple_expect "=1022"
send "1023 100 11 8 200 2 00100000\n"
lyskomd_expect "Person 8 added to conference 11 by 7."
simple_expect "=1023"
send "1024 100 12 8 200 3 00100000\n"
lyskomd_expect "Person 8 added to conference 12 by 7."
simple_expect "=1024"

#
# foo examines foo.
#

talk_to client 0

# foo does get-unread-confs of foo.  Should return the empty list.
send "1025 52 6\n"
simple_expect "=1025 0 \\*"

# foo does get-membership-10 of foo.  Should return 4 { 6 10 11 12 }.
send "1026 99 6 0 100 1\n"
simple_expect "=1026 4 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 10 200 0 0 \\* 7 $any_time 10100000 2 $any_time 11 200 0 0 \\* 7 $any_time 10100000 3 $any_time 12 200 0 0 \\* 7 $any_time 10100000 }"

# foo does get-membership-old of foo.  Should return 4 { 6 10 11 12 }.
send "1027 46 6 0 100 1\n"
simple_expect "=1027 4 { $any_time 6 255 0 0 \\* $any_time 10 200 0 0 \\* $any_time 11 200 0 0 \\* $any_time 12 200 0 0 \\* }"

# foo does get-membership of foo.  Should return 4 { 6 10 11 12 }.
send "1028 108 6 0 100 1 0\n"
simple_expect "=1028 4 { 0 $any_time 6 255 0 \\* 6 $any_time 00000000 1 $any_time 10 200 0 \\* 7 $any_time 10100000 2 $any_time 11 200 0 \\* 7 $any_time 10100000 3 $any_time 12 200 0 \\* 7 $any_time 10100000 }"

# foo does query-read-texts-10 on foo and 10, 11 and 12.  Should get results.
send "1029 98 6 10\n"
simple_expect "=1029 1 $any_time 10 200 0 0 \\* 7 $any_time 10100000"
send "1030 98 6 11\n"
simple_expect "=1030 2 $any_time 11 200 0 0 \\* 7 $any_time 10100000"
send "1031 98 6 12\n"
simple_expect "=1031 3 $any_time 12 200 0 0 \\* 7 $any_time 10100000"

# foo does query-read-texts-old on foo and 10, 11 and 12.  Should get results.
send "1032 9 6 10\n"
simple_expect "=1032 $any_time 10 200 0 0 \\*"
send "1033 9 6 11\n"
simple_expect "=1033 $any_time 11 200 0 0 \\*"
send "1034 9 6 12\n"
simple_expect "=1034 $any_time 12 200 0 0 \\*"

# foo does query-read-texts on foo and 10, 11 and 12.  Should get results.
send "1035 107 6 10 1 0\n"
simple_expect "=1035 1 $any_time 10 200 0 \\* 7 $any_time 10100000"
send "1036 107 6 11 1 0\n"
simple_expect "=1036 2 $any_time 11 200 0 \\* 7 $any_time 10100000"
send "1037 107 6 12 1 0\n"
simple_expect "=1037 3 $any_time 12 200 0 \\* 7 $any_time 10100000"

#
# gazonk examines foo.
#

talk_to client 3

# gazonk does get-unread-confs of foo.  Should return the empty list.
send "1038 52 6\n"
simple_expect "=1038 0 \\*"

# gazonk does get-membership-10 of foo.  Should return 1 { 6 }.
send "1039 99 6 0 100 1\n"
simple_expect "=1039 1 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 }"
# (Just a quick check: does the renumber work properly even if we don't
# start from the beginning of the list?)
send "1040 99 6 2 100 1\n"
simple_expect "%1040 19 2"
send "1041 99 6 1 100 1\n"
simple_expect "%1041 19 1"

# gazonk does get-membership-old of foo.  Should return 1 { 6 }.
send "1042 46 6 0 100 1\n"
simple_expect "=1042 1 { $any_time 6 255 0 0 \\* }"

# gazonk does get-membership of foo.  Should return 1 { 6 }.
send "1043 108 6 0 100 1 0\n"
simple_expect "=1043 1 { 0 $any_time 6 255 0 \\* 6 $any_time 00000000 }"
# (Just a quick check: does the renumber work properly even if we don't
# start from the beginning of the list?)
send "1044 108 6 2 100 1 0\n"
simple_expect "%1044 19 2"
send "1045 108 6 1 100 1 0\n"
simple_expect "%1045 19 1"

# gazonk does query-read-texts-10 on foo and 10, 11 and 12.  All should fail.
send "1046 98 6 10\n"
simple_expect "%1046 9 10"
send "1047 98 6 11\n"
simple_expect "%1047 13 11"
send "1048 98 6 12\n"
simple_expect "%1048 13 12"

# gazonk does query-read-texts-old on foo and 10, 11 and 12.  10 should fail.
send "1049 9 6 10\n"
simple_expect "%1049 9 10"
send "1050 9 6 11\n"
simple_expect "%1050 13 11"
send "1051 9 6 12\n"
simple_expect "%1051 13 12"

# gazonk does query-read-texts on foo and 10, 11 and 12.  All should fail.
send "1052 107 6 10 1 0\n"
simple_expect "%1052 9 10"
send "1053 107 6 11 1 0\n"
simple_expect "%1053 13 11"
send "1054 107 6 12 1 0\n"
simple_expect "%1054 13 12"

#
# gazonk examines bar.
#

# gazonk does get-unread-confs of bar.  Should return undefined-person.
send "1055 52 7\n"
good_bad_expect "%1055 10 7" "=0 \\*" "Bug 37"

# gazonk does get-membership-10 of bar.  Should return undefined-person.
send "1056 99 7 0 100 1\n"
simple_expect "%1056 10 7"

# gazonk does get-membership-old of bar.  Should return undefined-person.
send "1057 46 7 0 100 1\n"
simple_expect "%1057 10 7"

# gazonk does get-membership of bar.  Should return undefined-person.
send "1058 108 7 0 100 1 0\n"
simple_expect "%1058 10 7"

# gazonk does query-read-texts-10 on bar and 10, 11 and 12.  undefined-person.
send "1059 98 7 10\n"
simple_expect "%1059 10 7"
send "1060 98 7 11\n"
simple_expect "%1060 10 7"
send "1061 98 7 12\n"
simple_expect "%1061 10 7"

# gazonk does query-read-texts-old on bar and 10, 11 and 12.  undefined-person.
send "1062 9 7 10\n"
simple_expect "%1062 10 7"
send "1063 9 7 11\n"
simple_expect "%1063 10 7"
send "1064 9 7 12\n"
simple_expect "%1064 10 7"

# gazonk does query-read-texts on bar and 10, 11 and 12.  undefined-person.
send "1065 107 7 10 1 0\n"
simple_expect "%1065 10 7"
send "1066 107 7 11 1 0\n"
simple_expect "%1066 10 7"
send "1067 107 7 12 1 0\n"
simple_expect "%1067 10 7"

#
# gazonk examines citrus.
#

# gazonk does get-unread-confs of citrus.  Should return the empty list.
send "1068 52 8\n"
simple_expect "=1068 0 \\*"

# gazonk does get-membership-10 of citrus.  Should return 1 { 6 }.
send "1069 99 8 0 100 1\n"
simple_expect "=1069 1 { 0 $any_time 8 255 0 0 \\* 8 $any_time 00000000 }"

# gazonk does get-membership-old of citrus.  Should return 1 { 6 }.
send "1070 46 8 0 100 1\n"
simple_expect "=1070 1 { $any_time 8 255 0 0 \\* }"

# gazonk does get-membership of citrus.  Should return 1 { 6 }.
send "1071 108 8 0 100 1 0\n"
simple_expect "=1071 1 { 0 $any_time 8 255 0 \\* 8 $any_time 00000000 }"

# gazonk does query-read-texts-10 on citrus and 10, 11 and 12.  All fails.
send "1072 98 8 10\n"
simple_expect "%1072 9 10"
send "1073 98 8 11\n"
simple_expect "%1073 13 11"
send "1074 98 8 12\n"
simple_expect "%1074 13 12"

# gazonk does query-read-texts-old on citrus and 10, 11 and 12.  All fails.
send "1075 9 8 10\n"
simple_expect "%1075 9 10"
send "1076 9 8 11\n"
simple_expect "%1076 13 11"
send "1077 9 8 12\n"
simple_expect "%1077 13 12"

# gazonk does query-read-texts on citrus and 10, 11 and 12.  All should fail.
send "1078 107 8 10 1 0\n"
simple_expect "%1078 9 10"
send "1079 107 8 11 1 0\n"
simple_expect "%1079 13 11"
send "1080 107 8 12 1 0\n"
simple_expect "%1080 13 12"

#
# foo writes a text with secret, rd-prot and public as recipients.
#

talk_to client 0
send "1081 86 [holl "foo"] 3 { 0 10 0 11 0 12 } 0 { }\n"
simple_expect "=1081 1"

#
# foo examines foo
#

# foo does get-unread-confs of foo.  Should return 3 { 10 11 12 }.
send "1082 52 6\n"
simple_expect "=1082 3 { 10 11 12 }"

# foo does get-membership-10 of foo.  Should return 4 { 6 10 11 12 }.
send "1083 99 6 0 100 1\n"
simple_expect "=1083 4 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 10 200 0 0 \\* 7 $any_time 10100000 2 $any_time 11 200 0 0 \\* 7 $any_time 10100000 3 $any_time 12 200 0 0 \\* 7 $any_time 10100000 }"

# foo does get-membership-old of foo.  Should return 4 { 6 10 11 12 }.
send "1084 46 6 0 100 1\n"
simple_expect "=1084 4 { $any_time 6 255 0 0 \\* $any_time 10 200 0 0 \\* $any_time 11 200 0 0 \\* $any_time 12 200 0 0 \\* }"

# foo does get-membership of foo.  Should return 4 { 6 10 11 12 }.
send "1085 108 6 0 100 1 0\n"
simple_expect "=1085 4 { 0 $any_time 6 255 0 \\* 6 $any_time 00000000 1 $any_time 10 200 0 \\* 7 $any_time 10100000 2 $any_time 11 200 0 \\* 7 $any_time 10100000 3 $any_time 12 200 0 \\* 7 $any_time 10100000 }"

# foo does query-read-texts-10 on foo and 10, 11 and 12.  Should get results.
send "1086 98 6 10\n"
simple_expect "=1086 1 $any_time 10 200 0 0 \\* 7 $any_time 10100000"
send "1087 98 6 11\n"
simple_expect "=1087 2 $any_time 11 200 0 0 \\* 7 $any_time 10100000"
send "1088 98 6 12\n"
simple_expect "=1088 3 $any_time 12 200 0 0 \\* 7 $any_time 10100000"

# foo does query-read-texts-old on foo and 10, 11 and 12.  Should get results.
send "1089 9 6 10\n"
simple_expect "=1089 $any_time 10 200 0 0 \\*"
send "1090 9 6 11\n"
simple_expect "=1090 $any_time 11 200 0 0 \\*"
send "1091 9 6 12\n"
simple_expect "=1091 $any_time 12 200 0 0 \\*"

# foo does query-read-texts on foo and 10, 11 and 12.  Should get results.
send "1092 107 6 10 1 0\n"
simple_expect "=1092 1 $any_time 10 200 0 \\* 7 $any_time 10100000"
send "1093 107 6 11 1 0\n"
simple_expect "=1093 2 $any_time 11 200 0 \\* 7 $any_time 10100000"
send "1094 107 6 12 1 0\n"
simple_expect "=1094 3 $any_time 12 200 0 \\* 7 $any_time 10100000"

#
# gazonk examines foo.
#

talk_to client 3

# gazonk does get-unread-confs of foo.  Should return the empty list.
send "1095 52 6\n"
simple_expect "=1095 0 \\*"

# gazonk does get-membership-10 of foo.  Should return 1 { 6 }.
send "1096 99 6 0 100 1\n"
simple_expect "=1096 1 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 }"

# gazonk does get-membership-old of foo.  Should return 1 { 6 }.
send "1097 46 6 0 100 1\n"
simple_expect "=1097 1 { $any_time 6 255 0 0 \\* }"

# gazonk does get-membership of foo.  Should return 1 { 6 }.
send "1098 108 6 0 100 1 0\n"
simple_expect "=1098 1 { 0 $any_time 6 255 0 \\* 6 $any_time 00000000 }"

# gazonk does query-read-texts-10 on foo and 10, 11 and 12.  All should fail.
send "1099 98 6 10\n"
simple_expect "%1099 9 10"
send "1100 98 6 11\n"
simple_expect "%1100 13 11"
send "1101 98 6 12\n"
simple_expect "%1101 13 12"

# gazonk does query-read-texts-old on foo and 10, 11 and 12.  All should fail.
send "1102 9 6 10\n"
simple_expect "%1102 9 10"
send "1103 9 6 11\n"
simple_expect "%1103 13 11"
send "1104 9 6 12\n"
simple_expect "%1104 13 12"

# gazonk does query-read-texts on foo and 10, 11 and 12.  All should fail.
send "1105 107 6 10 1 0\n"
simple_expect "%1105 9 10"
send "1106 107 6 11 1 0\n"
simple_expect "%1106 13 11"
send "1107 107 6 12 1 0\n"
simple_expect "%1107 13 12"

#
# gazonk examines bar
#

# gazonk does get-unread-confs of bar.  Should return undefined-person.
send "1108 52 7\n"
good_bad_expect "%1108 10 7" "=0 \\*" "Bug 37"

# gazonk does get-membership-10 of bar.  Should return undefined-person.
send "1109 99 7 0 100 1\n"
simple_expect "%1109 10 7"

# gazonk does get-membership-old of bar.  Should return undefined-person.
send "1110 46 7 0 100 1\n"
simple_expect "%1110 10 7"

# gazonk does get-membership of bar.  Should return undefined-person.
send "1111 108 7 0 100 1 0\n"
simple_expect "%1111 10 7"

# gazonk does query-read-texts-10 on bar and 10, 11 and 12.  undefined-person.
send "1112 98 7 10\n"
simple_expect "%1112 10 7"
send "1113 98 7 11\n"
simple_expect "%1113 10 7"
send "1114 98 7 12\n"
simple_expect "%1114 10 7"

# gazonk does query-read-texts-old on bar and 10, 11 and 12.  undefined-person.
send "1115 9 7 10\n"
simple_expect "%1115 10 7"
send "1116 9 7 11\n"
simple_expect "%1116 10 7"
send "1117 9 7 12\n"
simple_expect "%1117 10 7"

# gazonk does query-read-texts on bar and 10, 11 and 12.  undefined-person.
send "1118 107 7 10 1 0\n"
simple_expect "%1118 10 7"
send "1119 107 7 11 1 0\n"
simple_expect "%1119 10 7"
send "1120 107 7 12 1 0\n"
simple_expect "%1120 10 7"

#
# gazonk examines citrus.
#

# gazonk does get-unread-confs of citrus.  Should return the empty list.
send "1121 52 8\n"
simple_expect "=1121 0 \\*"

# gazonk does get-membership-10 of citrus.  Should return 1 { 6 }.
send "1122 99 8 0 100 1\n"
simple_expect "=1122 1 { 0 $any_time 8 255 0 0 \\* 8 $any_time 00000000 }"

# gazonk does get-membership-old of citrus.  Should return 1 { 6 }.
send "1123 46 8 0 100 1\n"
simple_expect "=1123 1 { $any_time 8 255 0 0 \\* }"

# gazonk does get-membership of citrus.  Should return 1 { 6 }.
send "1124 108 8 0 100 1 0\n"
simple_expect "=1124 1 { 0 $any_time 8 255 0 \\* 8 $any_time 00000000 }"

# gazonk does query-read-texts-10 on citrus and 10, 11 and 12.  All fails.
send "1125 98 8 10\n"
simple_expect "%1125 9 10"
send "1126 98 8 11\n"
simple_expect "%1126 13 11"
send "1127 98 8 12\n"
simple_expect "%1127 13 12"

# gazonk does query-read-texts-old on citrus and 10, 11 and 12.  All fails.
send "1128 9 8 10\n"
simple_expect "%1128 9 10"
send "1129 9 8 11\n"
simple_expect "%1129 13 11"
send "1130 9 8 12\n"
simple_expect "%1130 13 12"

# gazonk does query-read-texts on citrus and 10, 11 and 12.  All should fail.
send "1131 107 8 10 1 0\n"
simple_expect "%1131 9 10"
send "1132 107 8 11 1 0\n"
simple_expect "%1132 13 11"
send "1133 107 8 12 1 0\n"
simple_expect "%1133 13 12"


#
# mark as read
#

# foo marks the text as read
talk_to client 0
send "1134 27 10 1 { 1 }\n"
simple_expect "=1134"
send "1135 27 11 1 { 1 }\n"
simple_expect "=1135"
send "1136 27 12 1 { 1 }\n"
simple_expect "=1136"

# bar marks the text as read
talk_to client 1
send "1137 27 10 1 { 1 }\n"
simple_expect "=1137"
send "1138 27 11 1 { 1 }\n"
simple_expect "=1138"
send "1139 27 12 1 { 1 }\n"
simple_expect "=1139"

# citrus marks the text as read
talk_to client 2
send "1140 27 10 1 { 1 }\n"
simple_expect "=1140"
send "1141 27 11 1 { 1 }\n"
simple_expect "=1141"
send "1142 27 12 1 { 1 }\n"
simple_expect "=1142"

#
# foo examines foo.
#

talk_to client 0

# foo does get-unread-confs of foo.  Should return the empty list.
send "1143 52 6\n"
simple_expect "=1143 0 \\*"

# foo does get-membership-10 of foo.  Should return 4 { 6 10 11 12 }.
send "1144 99 6 0 100 1\n"
simple_expect "=1144 4 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 10 200 1 0 \\* 7 $any_time 10100000 2 $any_time 11 200 1 0 \\* 7 $any_time 10100000 3 $any_time 12 200 1 0 \\* 7 $any_time 10100000 }"

# foo does get-membership-old of foo.  Should return 4 { 6 10 11 12 }.
send "1145 46 6 0 100 1\n"
simple_expect "=1145 4 { $any_time 6 255 0 0 \\* $any_time 10 200 1 0 \\* $any_time 11 200 1 0 \\* $any_time 12 200 1 0 \\* }"

# foo does get-membership of foo.  Should return 4 { 6 10 11 12 }.
send "1146 108 6 0 100 1 0\n"
simple_expect "=1146 4 { 0 $any_time 6 255 0 \\* 6 $any_time 00000000 1 $any_time 10 200 1 { 1 1 } 7 $any_time 10100000 2 $any_time 11 200 1 { 1 1 } 7 $any_time 10100000 3 $any_time 12 200 1 { 1 1 } 7 $any_time 10100000 }"

# foo does query-read-texts-10 on foo and 10, 11 and 12.  Should get results.
send "1147 98 6 10\n"
simple_expect "=1147 1 $any_time 10 200 1 0 \\* 7 $any_time 10100000"
send "1148 98 6 11\n"
simple_expect "=1148 2 $any_time 11 200 1 0 \\* 7 $any_time 10100000"
send "1149 98 6 12\n"
simple_expect "=1149 3 $any_time 12 200 1 0 \\* 7 $any_time 10100000"

# foo does query-read-texts-old on foo and 10, 11 and 12.  Should get results.
send "1150 9 6 10\n"
simple_expect "=1150 $any_time 10 200 1 0 \\*"
send "1151 9 6 11\n"
simple_expect "=1151 $any_time 11 200 1 0 \\*"
send "1152 9 6 12\n"
simple_expect "=1152 $any_time 12 200 1 0 \\*"

# foo does query-read-texts on foo and 10, 11 and 12.  Should get results.
send "1153 107 6 10 1 0\n"
simple_expect "=1153 1 $any_time 10 200 1 { 1 1 } 7 $any_time 10100000"
send "1154 107 6 11 1 0\n"
simple_expect "=1154 2 $any_time 11 200 1 { 1 1 } 7 $any_time 10100000"
send "1155 107 6 12 1 0\n"
simple_expect "=1155 3 $any_time 12 200 1 { 1 1 } 7 $any_time 10100000"

#
# gazonk examines foo.
#

talk_to client 3

# gazonk does get-unread-confs of foo.  Should return the empty list.
send "1156 52 6\n"
simple_expect "=1156 0 \\*"

# gazonk does get-membership-10 of foo.  Should return 1 { 6 }.
send "1157 99 6 0 100 1\n"
simple_expect "=1157 1 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 }"

# gazonk does get-membership-old of foo.  Should return 1 { 6 11 12 }.
send "1158 46 6 0 100 1\n"
simple_expect "=1158 1 { $any_time 6 255 0 0 \\* }"

# gazonk does get-membership of foo.  Should return 1 { 6 }.
send "1159 108 6 0 100 1 0\n"
simple_expect "=1159 1 { 0 $any_time 6 255 0 \\* 6 $any_time 00000000 }"

# gazonk does query-read-texts-10 on foo and 10, 11 and 12.  All should fail.
send "1160 98 6 10\n"
simple_expect "%1160 9 10"
send "1161 98 6 11\n"
simple_expect "%1161 13 11"
send "1162 98 6 12\n"
simple_expect "%1162 13 12"

# gazonk does query-read-texts-old on foo and 10, 11 and 12.  All should fail.
send "1163 9 6 10\n"
simple_expect "%1163 9 10"
send "1164 9 6 11\n"
simple_expect "%1164 13 11"
send "1165 9 6 12\n"
simple_expect "%1165 13 12"

# gazonk does query-read-texts on foo and 10, 11 and 12.  All should fail.
send "1166 107 6 10 1 0\n"
simple_expect "%1166 9 10"
send "1167 107 6 11 1 0\n"
simple_expect "%1167 13 11"
send "1168 107 6 12 1 0\n"
simple_expect "%1168 13 12"

#
# gazonk examines bar.
#

# gazonk does get-unread-confs of bar.  Should return undefined-person.
send "1169 52 7\n"
good_bad_expect "%1169 10 7" "=0 \\*" "Bug 37"

# gazonk does get-membership-10 of bar.  Should return undefined-person.
send "1170 99 7 0 100 1\n"
simple_expect "%1170 10 7"

# gazonk does get-membership-old of bar.  Should return undefined-person.
send "1171 46 7 0 100 1\n"
simple_expect "%1171 10 7"

# gazonk does get-membership of bar.  Should return undefined-person.
send "1172 108 7 0 100 1 0\n"
simple_expect "%1172 10 7"

# gazonk does query-read-texts-10 on bar and 10, 11 and 12.  undefined-person.
send "1173 98 7 10\n"
simple_expect "%1173 10 7"
send "1174 98 7 11\n"
simple_expect "%1174 10 7"
send "1175 98 7 12\n"
simple_expect "%1175 10 7"

# gazonk does query-read-texts-old on bar and 10, 11 and 12.  undefined-person.
send "1176 9 7 10\n"
simple_expect "%1176 10 7"
send "1177 9 7 11\n"
simple_expect "%1177 10 7"
send "1178 9 7 12\n"
simple_expect "%1178 10 7"

# gazonk does query-read-texts on bar and 10, 11 and 12.  undefined-person.
send "1179 107 7 10 1 0\n"
simple_expect "%1179 10 7"
send "1180 107 7 11 1 0\n"
simple_expect "%1180 10 7"
send "1181 107 7 12 1 0\n"
simple_expect "%1181 10 7"

#
# gazonk examines citrus.
#

# gazonk does get-unread-confs of citrus.  Should return the empty list.
send "1182 52 8\n"
simple_expect "=1182 0 \\*"

# gazonk does get-membership-10 of citrus.  Should return 1 { 6 }.
send "1183 99 8 0 100 1\n"
simple_expect "=1183 1 { 0 $any_time 8 255 0 0 \\* 8 $any_time 00000000 }"

# gazonk does get-membership-old of citrus.  Should return 1 { 6 }.
send "1184 46 8 0 100 1\n"
simple_expect "=1184 1 { $any_time 8 255 0 0 \\* }"

# gazonk does get-membership of citrus.  Should return 1 { 6 }.
send "1185 108 8 0 100 1 0\n"
simple_expect "=1185 1 { 0 $any_time 8 255 0 \\* 8 $any_time 00000000 }"

# gazonk does query-read-texts-10 on citrus and 10, 11 and 12.  All should fail.
send "1186 98 8 10\n"
simple_expect "%1186 9 10"
send "1187 98 8 11\n"
simple_expect "%1187 13 11"
send "1188 98 8 12\n"
simple_expect "%1188 13 12"

# gazonk does query-read-texts-old on citrus and 10, 11 and 12.  All fails.
send "1189 9 8 10\n"
simple_expect "%1189 9 10"
send "1190 9 8 11\n"
simple_expect "%1190 13 11"
send "1191 9 8 12\n"
simple_expect "%1191 13 12"

# gazonk does query-read-texts on citrus and 10, 11 and 12.  All should fail.
send "1192 107 8 10 1 0\n"
simple_expect "%1192 9 10"
send "1193 107 8 11 1 0\n"
simple_expect "%1193 13 11"
send "1194 107 8 12 1 0\n"
simple_expect "%1194 13 12"

#
# gazonk examines the conferences
#

# Test get-members-old
send "1195 48 10 0 100\n"
simple_expect "%1195 9 10"
send "1196 48 11 0 100\n"
simple_expect "=1196 3 { 0 0 0 }"
send "1197 48 12 0 100\n"
simple_expect "=1197 3 { 0 0 0 }"

# Test get-members.
send "1198 101 10 0 100\n"
simple_expect "%1198 9 10"
send "1199 101 11 0 100\n"
simple_expect "=1199 3 { 0 0 $any_time 00100000 0 0 $any_time 00100000 0 0 $any_time 00100000 }"
send "1200 101 12 0 100\n"
simple_expect "=1200 3 { 0 0 $any_time 00100000 0 0 $any_time 00100000 0 0 $any_time 00100000 }"

# Shut down.
talk_to client 3
send "1201 0 5 [holl "gazonk"]\n"
simple_expect "=1201"
send "1202 42 255\n"
simple_expect "=1202"
send "1203 44 0\n"
simple_expect "=1203"
client_death 3
client_death 2
client_death 1
client_death 0
lyskomd_death
