# Test suite for lyskomd.
# Copyright (C) 1999, 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 


# Supplemental tests for membership.c

# Second iteration (possibly)
# Call to do_sub_member with conf_c, pers_p set to NULL
# Generate a call to access_perm, fast_access_perm with viewer_p set to NULL
# --- I don't think there are any such calls in the server
# Generate a call to locate_member that fails to locate a member
# --- Can't be done unless the membership and member lists are out of sync

# Stuff to ignore
# Everything in check_membership
# Error statements in DEBUG_MARK_AS_READ
# Membership and member record mismatches


source "$srcdir/config/prot-a.exp"
read_versions
lyskomd_start "" "\
Add members by invitation: false"


client_start 0
talk_to client 0

kom_connect "DejaGnu Test Suite"
kom_accept_async "0 { }"

kom_create_person "P6" "PW6" "00000000" "0 { }"
kom_create_person "P7" "PW7" "00000000" "0 { }"
kom_create_person "P8" "PW8" "00000000" "0 { }"
kom_create_person "P9" "PW9" "00000000" "0 { }"

kom_login 7 "PW7" 0
kom_set_conf_type 7 "10110000"

kom_login 6 "PW6" 0

kom_create_conference "C10" "00000000" "0 { }"
kom_create_conference "C11" "00000000" "0 { }"
kom_create_conference "C12" "00000000" "0 { }"
kom_create_conference "C13" "00000000" "0 { }"
kom_create_conference "C14" "10000000" "0 { }"
kom_create_conference "C15" "10100000" "0 { }"
kom_create_conference "C16" "00000000" "0 { }"
kom_add_member 16 6 200 9999 "00000000"

kom_logout
kom_create_person "P17" "PW17" "00000000" "0 { }"
kom_create_person "P18" "PW18" "00000000" "0 { }"
kom_create_person "P19" "PW19" "00000000" "0 { }"

kom_login 6 "PW6" 0
kom_add_member 10 17 200 9999 "00000000"
lyskomd_expect "Person 17 added to conference 10 by 6."

kom_login 9 "PW9" 0
send "1000 106 9 1\n"
simple_expect "=1000"

send "1001 100 10 9 100 9999 00000000\n"
simple_expect "=1001"

send "1002 100 11 9 100 9999 00000000\n"
simple_expect "=1002"

send "1003 100 12 9 100 9999 00000000\n"
simple_expect "=1003"

send "1004 86 [holl "T1"] 4 { 0 1 0 10 0 11 0 12 } 0 { }\n"
simple_expect "=1004 1"

send "1005 86 [holl "T2"] 4 { 0 1 0 10 0 11 0 12 } 0 { }\n"
simple_expect "=1005 2"

send "1006 86 [holl "T3"] 4 { 0 1 0 10 0 11 0 12 } 0 { }\n"
simple_expect "=1006 3"

send "1007 27 10 2 { 1 3 }\n"
simple_expect "=1007"

send "1008 27 11 2 { 1 3 }\n"
simple_expect "=1008"

send "1009 27 12 2 { 1 3 }\n"
simple_expect "=1009"

kom_login 6 "PW6" 0
kom_add_member 10 18 200 9999 "00000000"
lyskomd_expect "Person 18 added to conference 10 by 6."
kom_add_member 10 19 200 9999 "00000000"
lyskomd_expect "Person 19 added to conference 10 by 6."

# Get membership from a person with unread-is-secret set

kom_login 8 "PW8" 0

send "1100 99 9 0 9999 1\n"
simple_expect "=1100 4 { 0 $any_time 9 255 0 0 \\* 9 $any_time 00000000 1 $any_time 10 100 0 0 \\* 9 $any_time 00000000 2 $any_time 11 100 0 0 \\* 9 $any_time 00000000 3 $any_time 12 100 0 0 \\* 9 $any_time 00000000 }"

# Call add_membership with WHERE set way too high

kom_login 9 "PW9" 0

send "1200 100 13 9 100 9999 00000000\n"
simple_expect "=1200"


# Move an existing membership from N to N-2, N-2 to N

send "1201 100 13 9 100 1 00000000\n"
simple_expect "=1201"

send "1202 100 13 9 100 4 00000000\n"
simple_expect "=1202"

send "1203 15 10 9\n"
simple_expect "=1203"


# Add a member with priority 0 and fake_passive ON
# This *really* should have been tested in 05.exp!

send "1300 14 10 9 0 9999\n"
simple_expect "=1300"

# Restore the flags

kom_login 9 "PW9" 0
send "1301 102 9 10 00000000\n"
simple_expect "=1301"


# Remove a member from conf X when member is logged on with CWC X

client_start 1
talk_to client 1
kom_connect "DejaGnu Test Suite"
kom_accept_async "1 { 8 }"

kom_login 6 "PW6" 0
kom_add_member 10 6 100 9999 00000000
kom_change_conference 10

talk_to client 0
kom_login 6 "PW6" 0
send "1401 15 10 6\n"
simple_expect "=1401"

talk_to client 1
simple_expect ":1 8 10"
send "1400 55 0\n"
simple_expect "=1400"
client_death 1

talk_to client 0


# Remove a membership in the middle of the membership list

kom_login 9 "PW9" 0
send "1500 15 11 9\n"
simple_expect "=1500"


# Remove a member without being supervisor of conf or person

kom_login 6 "PW6" 0
send "1600 100 15 9 100 9999 00000000\n"
lyskomd_expect "Person 9 added to conference 15 by 6."
simple_expect "=1600"

send "1601 100 11 9 100 9999 00100000\n"
lyskomd_expect "Person 9 added to conference 11 by 6."
simple_expect "=1601"

# Remove with read access to conf and membership

kom_login 8 "PW8" 0
send "1602 15 12 9\n"
simple_expect "%1602 12 12"

# Remove secret conf

send "1603 15 15 9\n"
simple_expect "%1603 9 15"

# Remove secret membership

send "1604 15 11 9\n"
simple_expect "%1604 13 11"



# Add a member to a rd_prot conference without access to conf

kom_login 8 "Pw8" 0
send "1700 100 14 8 100 9999 00000000\n"
simple_expect "%1700 11 14"

# Add a member to a secret conf we don't know about

send "1701 100 15 8 100 9999 00000000\n"
simple_expect "%1701 9 15"

# Try to add a member who is already a secret member

kom_login 9 "PW9" 0
send "1702 99 9 0 100 0\n"
extracting_expect "=1702 ($any*)" tmp 1
regsub -all "\\*" "$tmp" "\\*" mship

kom_login 8 "PW8" 0
send "1703 100 11 9 100 9999 00000000\n"
simple_expect "=1703"

kom_login 9 "PW9" 0
send "1704 99 9 0 100 0\n"
simple_expect "=1704 $mship"

# Try to change someone else's priorities

kom_login 8 "PW8" 0
send "1705 100 12 9 200 9999 00000000\n"
simple_expect "%1705 12 9"


# Attempt to mark a text as read in a conf the person is not a member of

kom_login 9 "PW9" 0

send "1800 27 1 3 { 1 2 3 }\n"
simple_expect "%1800 13 1"

# Attempt to mark local text zero as read

send "1801 27 10 1 { 0 }\n"
simple_expect "%1801 17 0"

# As person 6 mark text with rcpt,cc,bcc 6 as read

send "1802 86 [holl "T4"] 2 { 0 6 0 9 } 0 { }\n"
simple_expect "=1802 4"

send "1803 86 [holl "T5"] 2 { 1 6 0 9 } 0 { }\n"
simple_expect "=1803 5"

send "1804 86 [holl "T6"] 2 { 15 6 0 9 } 0 { }\n"
simple_expect "=1804 6"

kom_login 6 "PW6" 0

send "1805 27 6 3 { 1 2 3 }\n"
simple_expect "=1805"


# Create a conf with 5 text, with conf as CWC
# Create a bunch of texts, read a nonconsecutive range
# Delete all texts in that range and one more
# Read the first existing text

kom_login 6 "PW6" 0

send "1900 86 [holl "T7"] 1 { 0 16 } 0 { }\n"
simple_expect "=1900 7"

send "1901 86 [holl "T8"] 1 { 0 16 } 0 { }\n"
simple_expect "=1901 8"

send "1902 86 [holl "T9"] 1 { 0 16 } 0 { }\n"
simple_expect "=1902 9"

send "1903 86 [holl "T10"] 1 { 0 16 } 0 { }\n"
simple_expect "=1903 10"

send "1904 86 [holl "T11"] 1 { 0 16 } 0 { }\n"
simple_expect "=1904 11"

send "1905 2 16\n"
simple_expect "=1905"

send "1906 27 16 1 { 1 }\n"
simple_expect "=1906"

send "1907 27 16 1 { 3 }\n"
simple_expect "=1907"

send "1908 29 7\n"
simple_expect "=1908"

send "1909 29 8\n"
simple_expect "=1909"

send "1910 29 9\n"
simple_expect "=1910"

send "1911 29 10\n"
simple_expect "=1911"

send "1912 27 16 1 { 5 }\n"
simple_expect "=1912"


# Try to get the membership of a secret person

kom_login 6 "PW6" 0

send "2000 99 7 0 9999 0\n"
simple_expect "%2000 10 7"

# Try to get membership with first WAY too high

send "2001 99 6 999 9999 0\n"
simple_expect "%2001 19 999"


# Try to get the members of a secret conference

kom_login 8 "PW8" 0

send "2100 101 15 0 9999\n"
simple_expect "%2100 9 15"


# Call set-unread on a conf we are not a member of
# Call set-last-read on a conf we are not a member of

kom_login 8 "PW8" 0

send "2200 40 10 0\n"
simple_expect "%2200 13 10"

send "2201 77 10 1\n"
simple_expect "%2201 13 10"


# Call set-membership-type for a secret conf we are not member of
# Call set-membership-type for an open conf we are not member of

send "2300 102 8 15 00000000\n"
simple_expect "%2300 9 15"

send "2301 102 8 10 00000000\n"
simple_expect "%2301 13 10"



# ======================================================================
# Shut it all down


kom_login 5 "gazonk" 0
kom_enable 255

send "9999 44 0\n"
simple_expect "=9999"

client_death 0
lyskomd_death
