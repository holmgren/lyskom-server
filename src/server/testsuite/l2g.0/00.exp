# Test suite for Local_to_global.
# Copyright (C) 1996, 1998-1999, 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

l2g_start

l2g_send "I0"
l2g_send "I1"
l2g_send "D0"
l2g_send "I0"
l2g_send "C1 0"
l2g_send "C1 0"
l2g_send "C1 0"
# Attempt to look up entries in an empty set.
l2g_send "l 0 0"

simple_expect "0" "test 0"
l2g_send "l 0 1"
simple_expect "0" "test 1"
l2g_send "l 0 2"
simple_expect "0" "test 2"
l2g_send "l 0 3"
simple_expect "0" "test 3"
l2g_send "l 0 4"
simple_expect "0" "test 4"
l2g_send "l 0 5"
simple_expect "0" "test 5"

# Insert 3:103
l2g_send "a 0 3 103"

# Look up the first few numbers in a single-valued set.
l2g_send "l 0 1"
simple_expect "0" "test 6"
l2g_send "l 0 2"
simple_expect "0" "test 7"
l2g_send "l 0 3"
simple_expect "103" "test 8"
l2g_send "l 0 4"
simple_expect "0" "test 9"
l2g_send "l 0 5"
simple_expect "0" "test 10"

# Append 4:104, resuling in the set 3:103, 4:104
l2g_send "a 0 4 104"

# Look up again.
l2g_send "l 0 1"
simple_expect "0" "test 11"
l2g_send "l 0 2"
simple_expect "0" "test 12"
l2g_send "l 0 3"
simple_expect "103" "test 13"
l2g_send "l 0 4"
simple_expect "104" "test 14"
l2g_send "l 0 5"
simple_expect "0" "test 15"

# Append 8 more, resulting in a single full dense block.
l2g_send "a 0 5 105"
l2g_send "a 0 6 106"
l2g_send "a 0 7 107"
l2g_send "a 0 8 108"
l2g_send "a 0 9 109"
l2g_send "a 0 10 1010"
l2g_send "a 0 11 1011"
l2g_send "a 0 12 1012"

l2g_send "u 0"
simple_expect "Number of blocks: 1" "test 16"
simple_expect "First unused: 13" "test 16b"
simple_expect "0: 10 0 3 \\(dense\\) \\\[103 104 105 106 107 108 109 1010 1011 1012 \\\]" "test 17"

# Append 11 more, resulting in two full dense blocks and one dense
# block with a single entry.
l2g_send "a 0 13 1013"
l2g_send "a 0 14 1014"
l2g_send "a 0 15 1015"
l2g_send "a 0 16 1016"
l2g_send "a 0 17 1017"
l2g_send "a 0 18 1018"
l2g_send "a 0 19 1019"
l2g_send "a 0 20 1020"
l2g_send "a 0 21 1021"
l2g_send "a 0 22 1022"
l2g_send "a 0 23 1023"

l2g_send "l 0 1"
simple_expect "0" "test 18"
l2g_send "l 0 2"
simple_expect "0" "test 19"
l2g_send "l 0 3"
simple_expect "103" "test 20"
l2g_send "l 0 4"
simple_expect "104" "test 21"
l2g_send "l 0 5"
simple_expect "105" "test 22"
l2g_send "l 0 24"
simple_expect "0" "test 23"
l2g_send "l 0 23"
simple_expect "1023" "test 24"
l2g_send "l 0 22"
simple_expect "1022" "test 25"
l2g_send "l 0 20"
simple_expect "1020" "test 26"

l2g_send "d 0 20"

l2g_send "l 0 20"
simple_expect "0" "deletion works test 27"

l2g_send "l 0 1"
simple_expect "0" "test 28"
l2g_send "l 0 2"
simple_expect "0" "test 29"
l2g_send "l 0 3"
simple_expect "103" "test 30"
l2g_send "l 0 4"
simple_expect "104" "test 31"
l2g_send "l 0 5"
simple_expect "105" "test 32"
l2g_send "l 0 24"
simple_expect "0" "test 33"
l2g_send "l 0 23"
simple_expect "1023" "test 34"
l2g_send "l 0 22"
simple_expect "1022" "test 35"
l2g_send "l 0 20"
simple_expect "0" "test 36"
l2g_send "l 0 21"
simple_expect "1021" "test 37"
l2g_send "l 0 19"
simple_expect "1019" "test 38"

l2g_send "d 0 19"
l2g_send "l 0 18"
simple_expect "1018" "test 39"
l2g_send "l 0 19"
simple_expect "0" "test 40"
l2g_send "l 0 20"
simple_expect "0" "test 41"
l2g_send "l 0 21"
simple_expect "1021" "test 42"

proc check_state {map} {
    l2g_send "n $map 0"
    simple_expect "3" "checking index 0 in $map (test 43)"
    foreach n {3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 20 21 22} {
	l2g_send "n $map $n"
	simple_expect "[expr {$n + 1}]" "checking $n in $map (test 44)"
    }
    l2g_send "n $map 18"
    simple_expect "21" "checking 18 in $map (test 45)"
    l2g_send "n $map 19"
    simple_expect "21" "checking 19 in $map (test 46)"
    l2g_send "n $map 23"
    simple_expect "0" "checking 23 in $map (test 47)"
    l2g_send "n $map 24"
    simple_expect "0" "checking 24 in $map (test 48)"
}

check_state 0

l2g_send "n 1 0"
simple_expect "0" "test 49"

l2g_send "C1 0"

check_state 1

l2g_send "d 0 14"
check_state 1

l2g_send "d 1 13"
l2g_send "l 0 14"
simple_expect "0" "test 50"
l2g_send "l 1 13"
simple_expect "0" "test 51"

# Test the representation.
l2g_send "I2"
l2g_send "u 2"
simple_expect "Number of blocks: 0" "test 52"
simple_expect "First unused: 1" "test 52b"

l2g_send "a2 10 139"
l2g_send "u 2"
simple_expect "Number of blocks: 1" "test 53"
simple_expect "First unused: 11" "test 53b"
simple_expect "0: 1 9 10 \\(dense\\) \\\[139 \\\]" "test 54"

l2g_send "a2 12 141"
l2g_send "u 2"
simple_expect "Number of blocks: 1" "test 55"
simple_expect "First unused: 13" "test 55b"
simple_expect "0: 3 8 10 \\(dense\\) \\\[139 0 141 \\\]" "test 56"

l2g_send "a2 14 147"
l2g_send "u 2"
simple_expect "Number of blocks: 1" "test 57"
simple_expect "First unused: 15" "test 57b"
simple_expect "0: 5 7 10 \\(dense\\) \\\[139 0 141 0 147 \\\]" "test 58"

l2g_send "a2 16 153"
l2g_send "u 2"
simple_expect "Number of blocks: 1" "test 59"
simple_expect "First unused: 17" "test 59b"
simple_expect "0: 7 6 10 \\(dense\\) \\\[139 0 141 0 147 0 153 \\\]" "test 60"

l2g_send "a2 18 157"
l2g_send "u 2"
simple_expect "Number of blocks: 1" "test 61"
simple_expect "First unused: 19" "test 61b"
simple_expect "0: 9 5 10 \\(dense\\) \\\[139 0 141 0 147 0 153 0 157 \\\]" "test 62"

l2g_send "a2 21 167"
l2g_send "u 2"
simple_expect "Number of blocks: 1" "test 63"
simple_expect "First unused: 22" "test 63b"
simple_expect "0: 6 4 10 \\(sparse\\) \\\[10:139 12:141 14:147 16:153 18:157 21:167 \\\]" "test 64"

l2g_send "a2 28 170"
l2g_send "a2 92 171"
l2g_send "a2 99 172"
l2g_send "a2 102 173"

l2g_send "u 2"
simple_expect "Number of blocks: 1" "test 65"
simple_expect "First unused: 103" "test 65b"
simple_expect "0: 10 0 10 \\(sparse\\) \\\[10:139 12:141 14:147 16:153 18:157 21:167 28:170 92:171 99:172 102:173 \\\]" "test 66"

set block1 ""
set nf 0
set zeroes 10
foreach x {{103 177} {104 178} {105 179} {106 180} {107 181} {108 182} {109 183} {110 184} {111 185} {112 186}} {
    
    l2g_send "a2 $x"
    append block1 "[lindex $x 1] "

    l2g_send "u 2"
    incr nf 1
    incr zeroes -1

    simple_expect "Number of blocks: 2" "test 67"
    simple_expect "First unused: [expr 1 + [lindex $x 0]]" "test 67b"
    simple_expect "0: 10 0 10 \\(sparse\\) \\\[10:139 12:141 14:147 16:153 18:157 21:167 28:170 92:171 99:172 102:173 \\\]" "test 68"
    simple_expect "1: $nf $zeroes 103 \\(dense\\) \\\[$block1\\\]" "test 69"
}

l2g_send "a2 113 189"
l2g_send "u2"
simple_expect "Number of blocks: 3" "test 70"
simple_expect "First unused: 114" "test 70b"
simple_expect "0: 10 0 10 \\(sparse\\) \\\[10:139 12:141 14:147 16:153 18:157 21:167 28:170 92:171 99:172 102:173 \\\]" "test 71"
simple_expect "1: 10 0 103 \\(dense\\) \\\[$block1\\\]" "test 72"
simple_expect "2: 1 9 113 \\(dense\\) \\\[189 \\\]" "test 73"

l2g_send "l2 113"
simple_expect "189" "test 74"
l2g_send "l2 112"
simple_expect 186 "test 75"
l2g_send "l2 103"
simple_expect 177 "test 76"
l2g_send "l2 102"
simple_expect 173 "test 77"
l2g_send "l2 101"
simple_expect 0 "test 78"
l2g_send "D1"
l2g_send "D0"
l2g_send "D2"

l2g_stop
