/*
 * Copyright (C) 2002-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

/*
 * Test various ways of obtaining the number of available file descriptors.
 * Return failure if the assumptions used by lyskomd do not hold.
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h>
#ifdef HAVE_SYS_RESOURCE_H
#  ifdef TIME_WITH_SYS_TIME
#    include <sys/time.h>
#    include <time.h>
#  else
#    ifdef HAVE_SYS_TIME_H
#      include <sys/time.h>
#    else
#      include <time.h>
#    endif
#  endif
#  include <sys/resource.h>
#else
#  include <time.h>
#endif

#if defined(HAVE_SETRLIMIT) && defined(RLIMIT_OFILE) && !defined(RLIMIT_NOFILE)
#  define RLIMIT_NOFILE RLIMIT_OFILE
#endif

#if !HAVE_RLIM_T
typedef int rlim_t;
#endif

#define LOW_LIMIT (32)

struct data {
    long getdtable;
    long openmax;
    long rlimmax;
    long rlimcur;
    long sysconfmax;
    long max_seen;
};
    
struct all_data {
    struct data pre;
    struct data post;
};


static void
find_limits(const char *s,
	    struct data *p)
{
    long fd;
    long max_seen = 0;

#if defined(HAVE_SETRLIMIT) && defined(RLIMIT_NOFILE)
    struct rlimit rlim;
#endif

#if HAVE_GETDTABLESIZE
    p->getdtable = getdtablesize();
    printf("%sgetdtablesize: %ld\n", s, p->getdtable);
#else
    printf("%sgetdtablesize: n/a\n", s);
    p->getdtable = 0;
#endif

#ifdef OPEN_MAX
    p->openmax = OPEN_MAX;
    printf("%sOPEN_MAX: %ld\n", s, p->openmax);
#else
    printf("%sOPEN_MAX: n/a\n", s);
    p->openmax = 0;
#endif

#if defined(HAVE_SETRLIMIT) && defined(RLIMIT_NOFILE)
    if (getrlimit(RLIMIT_NOFILE, &rlim) < 0)
    {
	fflush(stdout);
	perror("ERROR: testfd: getrlimit(RLIMIT_NOFILE) failed");
	exit(1);
    }

    p->rlimmax = rlim.rlim_max;
    p->rlimcur = rlim.rlim_cur;
    printf("%sgetrlimit-current: %ld\n", s, p->rlimcur);
    printf("%sgetrlimit-max: %ld\n", s, p->rlimmax);
#else
    printf("%sgetrlimit-current: n/a\n", s);
    printf("%sgetrlimit-max: n/a\n", s);
    p->rlimmax = 0;
    p->rlimcur = 0;
#endif

#if defined (HAVE_SYSCONF)
    p->sysconfmax = sysconf(_SC_OPEN_MAX);
    printf("%ssysconf: %ld\n", s, p->sysconfmax);
#else
    printf("%ssysconf: n/a\n", s);
    p->sysconfmax = 0;
#endif
#if defined(FD_SETSIZE)
    printf("%sFD_SETSIZE: %ld\n", s, (long)FD_SETSIZE);
#else
    printf("%sFD_SETSIZE: n/a\n", s);
#endif    

    while(1)
    {
	fd = open("/dev/null", O_RDONLY);
	if (fd < 0)
	    break;
	if (fd >= max_seen)
	    max_seen = fd + 1;
    }

    printf("%sopen: %ld\n", s, max_seen);
    p->max_seen = max_seen;

    for (fd = 10; fd < max_seen; fd++)
	close(fd);
}

int
main(void)
{
    int errs = 0;
    struct all_data s;

#if defined(HAVE_SETRLIMIT) && defined(RLIMIT_NOFILE)
    struct rlimit rlim;
#endif

    find_limits("pre-", &s.pre);
    
#if defined(HAVE_SETRLIMIT) && defined(RLIMIT_NOFILE)
    if (getrlimit(RLIMIT_NOFILE, &rlim) < 0)
    {
	fflush(stdout);
	perror("ERROR: testfd: post-setrlimit getrlimit failed");
	exit(1);
    }
    rlim.rlim_cur = LOW_LIMIT;
    if (setrlimit(RLIMIT_NOFILE, &rlim) < 0)
    {
	fflush(stdout);
	perror("ERROR: testfd: post-setrlimit getrlimit failed");
	exit(1);
    }
#endif

    find_limits("post-", &s.post);

    fflush(stdout);

#if defined(HAVE_SETRLIMIT) && defined(RLIMIT_NOFILE) \
    && !defined(HAVE_BROKEN_NOFILE)
    if (s.post.max_seen != 0 && s.post.max_seen != LOW_LIMIT)
    {
	fprintf(stderr, "ERROR: open-post not equal to LOW_LIMIT\n");
	errs++;
    }
#else
    if (s.post.getdtable != 0)
    {
	if (s.post.getdtable != s.post.max_seen)
	{
	    fprintf(stderr, "ERROR: open-post not equal to getdtable-post\n");
	    errs++;
	}
    }
    else if (s.pre.openmax != 0)
    {
	if (s.pre.openmax != s.post.openmax)
	{
	    fprintf(stderr, "ERROR: openmax is mutable\n");
	    errs++;
	}
	if (s.post.openmax != s.post.max_seen)
	{
	    fprintf(stderr, "ERROR: open-post not equal to openmax\n");
	    errs++;
	}
    }
#endif

    exit(!!errs);
}
