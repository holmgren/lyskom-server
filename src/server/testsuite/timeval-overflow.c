/*
 * Emit a lyskomd that causes a struct timeval to overflow.
 * Copyright (C) 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <limits.h>
#include <stdio.h>
#include <sys/types.h>
#include "../timewrap.h"

int
main(void)
{
    struct timeval tv;
    struct timeval next;

    tv.tv_sec = ~0;
    if (tv.tv_sec < 0)
    {
	for (next.tv_sec = 0; ; next.tv_sec = 2 * next.tv_sec + 1)
	{
	    if (next.tv_sec < 0)
		break;
	    tv = next;
	}
    }

    /* No overflow. */
    printf("Garb busy postponement: %g seconds\n", (double)tv.tv_sec);

    /* Overflow. */
    printf("Garb timeout: %g seconds\n", 1.5 * (double)tv.tv_sec);

    /* Overflow. */
    printf("Sync timeout: %g seconds\n", 1.1 * (double)tv.tv_sec);
    
    /* Overflow. */
    printf("Garb interval: %g days\n", (double)tv.tv_sec / 24.0 / 3599.0);
    
    /* No overflow. */
    printf("Sync interval: %g days\n", (double)tv.tv_sec / 24.0 / 3601.0);
    
    /* Overflow. */
    printf("Sync retry interval: %g us\n", 1.001e6 * tv.tv_sec);

    return 0;
}
