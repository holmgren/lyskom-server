/*
 * Testbench for the Local_to_global structure.
 * Copyright (C) 1996, 1998-1999, 2002-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

#include <config.h>

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <ctype.h>

#include "s-string.h"
#include "kom-types.h"
#include "linkansi.h"
#include "../local-to-global.h"
#ifdef TRACED_ALLOCATIONS
#  include "../trace-alloc.h"
#endif


#define MAXMAPS 10
#define LINSIZE 1024
#define ARGS 3

/*
  D m	  destruct
  I m     init
  C m	  clear
  C d s   copy
  a m l g append
  d m l   delete
  l m l   lookup
  n m l   next-key
  u m     dump
  r m     read
  s m l g expensive_set
  w m     write
  i m     iterate through all texts
  i m f e iterate through f..e-1
  b m f e reverse iterate through f..e-1
  q       quit
 */
int
main(void)
{
    Local_to_global maps[MAXMAPS];
    char input[LINSIZE];
    int num[ARGS];
    int n;
    int got;
    int ok;
    char *s;

#ifdef TRACED_ALLOCATIONS
    /* We must do this before we allocate any memory... */
    {
      char buf[1024];
      char *nl;

      fputs("Where does the trace want to go today? [stderr]\n", stdout);
      fflush(stdout);
      if (fgets(buf, sizeof(buf), stdin) != buf)
      {
	  fprintf(stderr, "unable to read trace location\n");
	  exit(1);
      }

      if ((nl = strchr(buf, '\n')) != NULL)
	  *nl = '\0';
      trace_alloc_file(buf);
    }
#endif

    link_ansi();

    l2g_set_block_size(10);

    printf("l2g> ");
    fflush(stdout);
    while (fgets(input, LINSIZE, stdin) != NULL)
    {
	s = strchr(input, '\n');
	assert(s);
	*s = '\0';

	if (*input == '\0')
	{
	    printf("EMPTY LINE\nl2g> ");
	    fflush(stdout);
	    continue;
	}

	for (n = 0; n < ARGS; n++)
	    num[n] = 0;
	n = 0;
	got = 0;

	for (s=input + 1; *s && *s != '\n'; ++s)
	{
	    if (isdigit((int)(unsigned char)*s))
	    {
		if (n < ARGS)
		{
		    got = 1;
		    num[n] = 10*num[n] + *s - '0';
		}
		else
		{
		    printf("BAD INPUT (too many numbers)\nl2g> ");
		    fflush(stdout);
		    continue;
		}
	    }
	    else if (*s == ' ')
	    {
		if (got)
		    n++;
		got = 0;
	    }
	    else
	    {
		printf("BAD INPUT (bad character)\nl2g> ");
		fflush(stdout);
		continue;
	    }
	}

	if (got)
	    n++;
	ok = 0;

	switch(*input)
	{
	case 'D':
	    if (n == 1 && num[0] < MAXMAPS)
	    {
		l2g_destruct(&maps[num[0]]);
		ok = 1;
	    }
	    break;
	case 'I':
	    if (n == 1 && num[0] < MAXMAPS)
	    {
		l2g_init(&maps[num[0]]);
		ok = 1;
	    }
	    break;
	case 'C':
	    if (n == 1 && num[0] < MAXMAPS)
	    {
		l2g_clear(&maps[num[0]]);
		ok = 1;
	    }
	    else if (n == 2 && num[0] < MAXMAPS && num[1] < MAXMAPS)
	    {
		l2g_copy(&maps[num[0]], &maps[num[1]]);
		ok = 1;
	    }
	    break;
	case 'a':
	    if (n == 3 && num[0] < MAXMAPS)
	    {
		l2g_append(&maps[num[0]], num[1], num[2]);
		ok = 1;
	    }
	    break;
	case 'd':
	    if (n == 2 && num[0] < MAXMAPS)
	    {
		l2g_delete(&maps[num[0]], num[1]);
		ok = 1;
	    }
	    break;
	case 'l':
	    if (n == 2 && num[0] < MAXMAPS)
	    {
		printf("%lu\n",
		       (unsigned long)l2g_lookup(&maps[num[0]], num[1]));
		ok = 1;
	    }
	    break;
	case 'n':
	    if (n == 2 && num[0] < MAXMAPS)
	    {
		printf("%lu\n",
		       (unsigned long)l2g_next_key(&maps[num[0]], num[1]));
		ok = 1;
	    }
	    break;
	case 'i':
	    if (n == 1 && num[0] < MAXMAPS)
	    {
		L2g_iterator iter;
		for (l2gi_searchall(&iter, &maps[num[0]]);
		     !iter.search_ended;
		     l2gi_next(&iter))
		    printf("%lu:%lu\n",
			   (unsigned long)iter.lno,
			   (unsigned long)iter.tno);
		ok = 1;
	    }
	    else if (n == 3 && num[0] < MAXMAPS)
	    {
		L2g_iterator iter;
		for (l2gi_searchsome(&iter, &maps[num[0]],
				     num[1], num[2]);
		     !iter.search_ended;
		     l2gi_next(&iter))
		    printf("%lu:%lu\n",
			   (unsigned long)iter.lno,
			   (unsigned long)iter.tno);
		ok = 1;
	    }
	    break;
	case 'b':
	    if (n == 1 && num[0] < MAXMAPS)
	    {
		/* This is not yet implemented by local-to-global.c. */
#if 0
		L2g_reverse_iterator iter;
		for (l2gi_searchall_reverse(&iter, &maps[num[0]]);
		     !iter.search_ended;
		     l2gi_prev(&iter))
		    printf("%lu:%lu\n",
			   (unsigned long)iter.lno,
			   (unsigned long)iter.tno);
		ok = 1;
#endif
	    }
	    else if (n == 3 && num[0] < MAXMAPS)
	    {
		L2g_reverse_iterator iter;
		for (l2gi_searchsome_reverse(&iter, &maps[num[0]],
					     num[1], num[2]);
		     !iter.search_ended;
		     l2gi_prev(&iter))
		    printf("%lu:%lu\n",
			   (unsigned long)iter.lno,
			   (unsigned long)iter.tno);
		ok = 1;
	    }
	    break;
	case 'u':
	    if (n == 1 && num[0] < MAXMAPS)
	    {
		l2g_dump(stdout, &maps[num[0]]);
		ok = 1;
	    }
	    break;
	case 'r':
	    if (n == 1 && num[0] < MAXMAPS)
	    {
		int c;
		l2g_read(stdin, &maps[num[0]]);
		if ((c = getchar()) != '\n')
		{
		    printf("BAD INPUT: got char %d instead of newline\n", c);
		    fflush(stdout);
		}
		ok = 1;
	    }
	    break;
	case 's':
	    if (n == 3 && num[0] < MAXMAPS)
	    {
		l2g_expensive_set(&maps[num[0]], num[1], num[2]);
		ok = 1;
	    }
	    break;
	case 'w':
	    if (n == 1 && num[0] < MAXMAPS)
	    {
		l2g_write(stdout, &maps[num[0]]);
		putchar('\n');
		ok = 1;
	    }
	    break;
	case 'q':
	    printf("test-l2g quitting\n");
	    fflush(stdout);
	    return 0;
	}

	if (ok == 0)
	{
	    printf("BAD LINE (bad params)\nl2g> ");
	    fflush(stdout);
	    continue;
	}

	printf("l2g> ");
	fflush(stdout);
    }
    return 0;
}
