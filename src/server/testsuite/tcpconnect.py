# Connect the test suite to a TCP port.
# Copyright (C) 1998-1999, 2001-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

import socket
import select
import sys
import string

s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
host=sys.argv[1]
port=string.atoi(sys.argv[2])
pfx = ""
metapfx = ""
if len(sys.argv) > 3:
   pfx = sys.argv[3] + ": "
   metapfx = sys.argv[3] + "meta: "
print metapfx + "Connecting to", host, port
sys.stdout.flush()
s.connect((host, port))
print metapfx + "Connected"
sys.stdout.flush()
needpfx = 1
fdset = [s, sys.stdin]
while 1:
   (i, o, e) = select.select(fdset, [], [])
   if s in i:
      try:
	  d = s.recv(512)
      except socket.error:
	  d = ""
      if d == "":
	 if needpfx == 0:
	    print
	 if pfx == "":
	    sys.exit(0)
	 else:
	    print metapfx + "EOF on socket"
            sys.stdout.flush()
	    sys.stdin.readline()
	    sys.exit(0)
      line = ""
      if needpfx:
	 line = pfx
      needpfx = 0
      if d[-1] == '\n':
	 d = d[:-1]
	 needpfx = 1
      line = line + string.replace(d, "\n", "\n" + pfx)
      if needpfx == 1:
	 line = line + "\n"
      sys.stdout.write(line)
      sys.stdout.flush()
   if sys.stdin in i:
      d = sys.stdin.readline()
      if d == "":
	 print metapfx + "EOF on stdin"
	 sys.exit(1)
      elif d == "#suspend socket\n":
         fdset = [sys.stdin]
         print metapfx + "suspended"
         sys.stdout.flush()
      elif d == "#resume socket\n":
         fdset = [s, sys.stdin]
         print metapfx + "resumed"
         sys.stdout.flush()
      elif d == "#hose socket\n":
         s.send(string.zfill(0, 2000))
      elif d[:6] == "#nocr ":
         s.send(d[6:-1])
      else:
         s.send(d)
