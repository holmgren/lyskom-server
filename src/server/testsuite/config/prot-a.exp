# Test suite for lyskomd.
# Copyright (C) 1998-1999, 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 



#
# Functions to make protocol A a little simpler
#

set ref_no 0

proc kom_next_call {} {
    global ref_no

    set ref_no [expr $ref_no + 1]
}

proc cres { result dflt} {
    global ref_no

    if { $result == "" } {
        return $dflt
    } else {
        return [eval concat "$result"]
    }
}


proc kom_connect { user {result ""} {testname ""}} {
    global ref_no

    send "A[holl $user]\n"
    set result [cres $result "LysKOM"]
    simple_expect $result $testname
}

proc kom_accept_async { str {result ""} {testname ""}} {
    global ref_no

    kom_next_call
    send "$ref_no 80 $str\n"
    set result [cres $result "=$ref_no"]

    simple_expect $result $testname
}

proc kom_create_person { name password flags aux {result ""} {testname ""}} {
    global ref_no
    global any_num
    global pers_no

    kom_next_call
    send "$ref_no 89 [holl "$name"] [holl "$password"] $flags $aux\n"
    set result [cres $result "=$ref_no ($any_num)" ]
    extracting_expect $result pers_no 1

    return $pers_no
}

proc kom_logout {{result ""} {testname ""}} {
    global ref_no

    kom_next_call
    send "$ref_no 1\n"
    set result [cres $result "=$ref_no"]
    simple_expect $result $testname
}

proc kom_login_old {person password {result ""} {testname ""}} {
    global ref_no

    kom_next_call
    send "$ref_no 0 $person [holl "$password"]\n"
    set result [cres $result "=$ref_no"]
    simple_expect $result $testname
}

proc kom_login {person password invisible {result ""} {testname ""}} {
    global ref_no

    kom_next_call
    send "$ref_no 62 $person [holl "$password"] $invisible\n"
    set result [cres $result "=$ref_no"]
    simple_expect $result $testname
}

proc kom_create_conference { name type aux {result ""} {testname ""}} {
    global ref_no
    global any_num
    global conf_no

    kom_next_call
    send "$ref_no 88 [holl "$name"] $type $aux\n"
    set result [cres $result "=$ref_no ($any_num)"]
    extracting_expect $result conf_no 1

    return $conf_no
}

proc kom_enable { value {result ""} {testname ""} } {
    global ref_no

    kom_next_call
    send "$ref_no 42 $value\n"
    set result [cres $result "=$ref_no"]
    simple_expect $result $testname
}

proc kom_set_supervisor { conf admin {result ""} {testname ""} } {
    global ref_no

    kom_next_call
    send "$ref_no 18 $conf $admin\n"
    set result [cres $result "=$ref_no"]
    simple_expect $result $testname
}    

proc kom_set_permitted_submitters { conf permitted {result ""} {testname ""}} {
    global ref_no

    kom_next_call
    send "$ref_no 19 $conf $permitted\n"
    set result [cres $result "=$ref_no"]
    simple_expect $result $testname
}

proc kom_set_super_conf { conf super {result ""} {testname ""}} {
    global ref_no

    kom_next_call
    send "$ref_no 20 $conf $super\n"
    set result [cres $result "=$ref_no"]
    simple_expect $result $testname
}


proc kom_add_member { conf pers prio where type {result ""} {testname ""}} {
    global ref_no
    
    kom_next_call
    send "$ref_no 100 $conf $pers $prio $where $type\n"
    set result [cres $result "=$ref_no"]
    simple_expect $result $testname
}

proc kom_mark_text { text mark { result "" } { testname "" }} {
    global ref_no

    kom_next_call
    send "$ref_no 72 $text $mark\n"
    set result [cres $result "=$ref_no"]
    simple_expect $result $testname
}

proc kom_sub_recipient { text recpt { result "" } { testname "" }} {
    global ref_no

    kom_next_call
    send "$ref_no 31 $text $recpt\n"
    set result [cres $result "=$ref_no"]
    simple_expect $result $testname
}

proc kom_set_conf_type { conf type { result "" } { testname "" }} {
    global ref_no

    kom_next_call
    send "$ref_no 21 $conf $type\n"
    set result [cres $result "=$ref_no"]
    simple_expect $result $testname
}

proc kom_change_conference { conf { result "" } { testname "" }} {
    global ref_no

    kom_next_call
    send "$ref_no 2 $conf\n"
    set result [cres $result "=$ref_no"]
    simple_expect $result $testname
}

proc kom_set_priv_bits { pers bits { result "" } { testname "" }} {
    global ref_no

    kom_next_call
    send "$ref_no 7 $pers $bits\n"
    set result [cres $result "=$ref_no"]
    simple_expect $result $testname
}

proc kom_set_user_area { pers text { result "" } { testname "" } } {
    global ref_no;

    kom_next_call
    send "$ref_no 57 $pers $text\n"
    set result [cres $result "=$ref_no"]
    simple_expect $result $testname
}

proc kom_create_text_simple { text recpt { result "" } { testname "" } } {
    global ref_no;
    global any_num;
    global text_no;

    kom_next_call
    send "$ref_no 86 [holl "$text"] 1 { 0 $recpt } 0 { }\n" 
    set result [cres $result "=$ref_no ($any_num)"]
    extracting_expect $result text_no 1
    return $text_no
}

proc kom_shutdown_server { { result "" } { testname "" } } {
    global ref_no;

    kom_next_call
    send "$ref_no 44 0\n"
    set result [cres $result "=$ref_no"]
    simple_expect $result $testname
}

proc kom_lookup_z_name  { name want_pers want_conf { result "" } { testname "" } } {
    global ref_no;

    kom_next_call
    send "$ref_no 76 [holl "$name"] $want_pers $want_conf\n"
    set result [cres $result "=$ref_no"]
    simple_expect $result $testname
}

proc kom_delete_conf { conf_no  { result "" } { testname "" } } {
    global ref_no
    global any_num

    kom_next_call
    send "$ref_no 11 $conf_no\n"
    set result [cres $result "=$ref_no"]
    simple_expect $result $testname
    lyskomd_expect "User $any_num deleted conference $conf_no\\."
    lyskomd_expect "Conference $conf_no was named '.*'\\."
}

proc kom_delete_text { text_no  { result "" } { testname "" } } {
    global ref_no;

    kom_next_call
    send "$ref_no 29 $text_no\n"
    set result [cres $result "=$ref_no"]
    simple_expect $result $testname
}


proc kom_ping_server { } {
    global ref_no
    global any_time

    kom_next_call
    send "$ref_no 35\n"
    simple_expect "=$ref_no $any_time"
}
