/*
 * Copyright (C) 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

/*
 * Issue as many get-time requests to the specified server as it can
 * accept.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#ifdef TIME_WITH_SYS_TIME
#  include <sys/time.h>
#  include <time.h>
#else
#  ifdef HAVE_SYS_TIME_H
#    include <sys/time.h>
#  else
#    include <time.h>
#  endif
#endif
#include <unistd.h>
#include <sys/socket.h>
#include <fcntl.h>

#include "oop.h"

#include "s-string.h"
#include "isc.h"

#include "timeval-util.h"
#include "getopt.h"
#include "linkansi.h"
#include "unused.h"

#define BLOCKSIZE (65536)

static long sent_writes = 0;
static long sent_requests = 0;
static long sent_bytes = 0;
static long rcvd_reads = 0;
static long rcvd_requests = 0;
static long rcvd_bytes = 0;

static int server;

static int writing_to_server;
static int reading_from_server;

static long last_progress_status = 0;
static int abort_pending = 0;

static int do_write_only = 0;
static long request_limit = -1;

static oop_call_fd read_stdin;
static oop_call_fd read_server;
static oop_call_fd write_server;
static oop_call_time report_stats;
static oop_call_time report_progress;
static oop_call_time end_it;

static void
fail(const char *reason)
{
    fprintf(stderr, "get-time-often: ");
    perror(reason);
    exit(1);
}

static void
start_writing(oop_source *oop)
{
    if (!writing_to_server)
    {
	oop->on_fd(oop, server, OOP_WRITE, write_server, NULL);
	writing_to_server = 1;
    }
}

static void
stop_writing(oop_source *oop)
{
    if (writing_to_server)
    {
	oop->cancel_fd(oop, server, OOP_WRITE);
	writing_to_server = 0;
    }
}

static void
start_reading(oop_source *oop)
{
    if (!reading_from_server)
    {
	oop->on_fd(oop, server, OOP_READ, read_server, NULL);
	reading_from_server = 1;
    }
}

static void
stop_reading(oop_source *oop)
{
    if (reading_from_server)
    {
	oop->cancel_fd(oop, server, OOP_READ);
	reading_from_server = 0;
    }
}

static int
tcp_connect(const char *host,
	    const char *port)
{
    int sock;
    union isc_address *ia;

    if ((ia = isc_mktcpaddress(host, port)) == NULL)
	fail("isc_mktcpaddress");

    if ((sock = socket(isc_addressfamily(ia), SOCK_STREAM, 0)) < 0)
	fail("socket");

    if (connect(sock, isc_addresspointer(ia), isc_addresssize(ia)) < 0)
	fail("connect");

    isc_freeaddress(ia);

    return sock;
}

static void *
read_stdin(oop_source *source,
	   int fd,
	   oop_event UNUSED(event),
	   void *UNUSED(user))
{
    static char readbuf[128];
    ssize_t rv;

    if ((rv = read(fd, readbuf, sizeof(readbuf) - 1)) < 0)
    {
	if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR)
	    fail("read(stdin)");
	else
	    rv = 0;
    }
    else if (rv == 0)
    {
	fprintf(stderr, "eof on stdin");
	exit(1);
    }

    readbuf[rv] = '\0';
    while (rv > 1
	   && (readbuf[rv-1] == '\r'
	       || readbuf[rv-1] == '\n'))
    {
	rv--;
	readbuf[rv] = '\0';
    }

    if (!strcmp(readbuf, "shutdown"))
	return OOP_HALT;

    if (!strcmp(readbuf, "ping"))
    {
	puts("pong");
	fflush(stdout);
	return OOP_CONTINUE;
    }

    if (!strcmp(readbuf, "start-reading"))
    {
	start_reading(source);
	if (do_write_only)
	    request_limit = sent_requests;

	puts("ok");
	fflush(stdout);
	return OOP_CONTINUE;
    }

    fprintf(stderr, "unknown command on stdin: %s.\n", readbuf);
    return OOP_HALT;
}

static const char *
parse_ok(const char *beg,
	 const char *end)
{
    const char *hunt;

    for (hunt = beg; hunt < end; hunt++)
	if (*hunt == '\n')
	    return hunt + 1;

    return NULL;
}

static const char *
parse_async(const char *beg,
	    const char *end)
{
    const char *hunt;
    const char *number;
    long len;
    
    for (hunt = beg; hunt < end; hunt++)
    {
	if (isdigit((unsigned char)*hunt))
	{
	    number = hunt;
	    for (hunt++; hunt < end; hunt++)
		if (!isdigit((unsigned char)*hunt))
		    break;
	    if (hunt == end)
		return NULL;
	    if (*hunt == 'H')
	    {
		hunt++;
		len = strtol(number, NULL, 10);
		if (len > end - hunt)
		    return NULL;
		hunt += len;
	    }
	    else
		hunt--;
	}
	else if (*hunt == '\n')
	    return hunt + 1;
	else
	{
	    switch (*hunt)
	    {
	    case ' ':
	    case '{':
	    case '}':
	    case '*':
		break;
	    default:
		fprintf(stderr, "unexpected char from server in async: %c\n",
			*hunt);
		fprintf(stderr, "Current block looks like this (%d chars):\n",
			(int)(end - beg));
		fprintf(stderr, "%.*s\n", (int)(end - beg), beg);
		exit(1);
	    }
	}
    }

    return NULL;
}

static const char *
parse_result(const char *beg,
	     const char *end)
{
    const char *next;
    const char *good_seqno = NULL;

    while (beg < end)
    {
	if (*beg == '=')
	{
	    next = parse_ok(beg + 1, end);
	    if (next != NULL)
		good_seqno = beg + 1;
	}
	else if (*beg == ':')
	    next = parse_async(beg+1, end);
	else
	{
	    fprintf(stderr, "unexpected server output: %c\n", *beg);
	    exit(1);
	}
	if (next == NULL)
	    break;
	beg = next;
    }

    if (good_seqno != NULL)
	rcvd_requests = strtol(good_seqno, NULL, 10);

    return beg;
}

static void *
read_server(oop_source *UNUSED(source),
	    int fd,
	    oop_event UNUSED(event),
	    void *UNUSED(user))
{
    static char readbuf[8 * BLOCKSIZE];
    static char *pos = readbuf;
    char *end;
    const char *src;
    ssize_t rv;

    if ((rv = read(fd, pos, sizeof(readbuf) - (pos - readbuf))) < 0)
    {
	if ((errno == EPIPE || errno == ECONNRESET) && do_write_only)
	    return OOP_HALT;

	if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR)
	    fail("read(server)");
	else
	    rv = 0;
    }
    else if (rv == 0)
    {
	if (do_write_only)
	    return OOP_HALT;
	fprintf(stderr, "eof on server");
	exit(1);
    }
    else
    {
	rcvd_bytes += rv;
	rcvd_reads++;
    }

    end = pos + rv;
    src = parse_result(readbuf, end);

    /* Move any trailing incomplete result to the head of the buffe.  */
    for (pos = readbuf; src < end; )
	*pos++ = *src++;

    if (request_limit != -1 && rcvd_requests >= request_limit)
    {
	fprintf(stderr, "Only expected %ld replies, but got at least %ld.\n",
		request_limit, rcvd_requests);
	request_limit = -1;
    }

    if (writing_to_server)
	return OOP_CONTINUE;
    if (rcvd_requests < sent_requests)
	return OOP_CONTINUE;
    return OOP_HALT;
}

static void *
write_server(oop_source *source,
	    int fd,
	    oop_event UNUSED(event),
	    void *UNUSED(user))
{
    static char writebuf[BLOCKSIZE + 100];
    static char *pos = NULL;
    static char *end = NULL;
    ssize_t rv;
    char *hunt;
    long seq = sent_requests;

    /* Fill the output buffer. */
    if (pos == end)
    {
	pos = writebuf;
	while (pos < writebuf + BLOCKSIZE)
	    pos += sprintf(pos, "%ld 35\n", seq++);
	end = pos;
	pos = writebuf;
    }

    rv = write(fd, pos, end - pos);
    if (rv < 0)
    {
	if ((errno == ECONNRESET || errno == EPIPE) && do_write_only)
	{
	    stop_writing(source);
	    return OOP_CONTINUE;
	}
	if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR)
	    fail("write(server)");
	else
	    rv = 0;
    }
    else if (rv == 0)
    {
	if (do_write_only)
	    return OOP_HALT;
	fprintf(stderr, "eof on server (write)");
	exit(1);
    }
    else
    {
	sent_bytes += rv;
	sent_writes++;
    }
    
    pos += rv;
    for (hunt = pos; hunt > writebuf + 1 && hunt[-1] != '\n'; hunt--);
    if (hunt > writebuf + 1)
    {
	for (hunt--; hunt > writebuf + 1 && hunt[-1] != '\n'; hunt--);
	sent_requests = strtol(hunt, NULL, 10);
    }
    
    return OOP_CONTINUE;
}

static void
arm_timer(oop_source *source,
	  time_t sec,
	  long usec,
	  oop_call_time *call,
	  struct timeval *tv)
{
    struct timeval ival;
    ival.tv_sec = sec;
    ival.tv_usec = usec;
    if (setup_timer(tv, ival) < 0)
	fprintf(stderr, "gettimeofday failed: %s\n", strerror(errno));
    source->on_time(source, *tv, call, tv);
}


static void
do_report_stats(void)
{
    printf("%ld reqs, %ld bytes, %ld reads\n",
	   rcvd_requests, rcvd_bytes, rcvd_reads);

    printf("%ld reqs, %ld bytes, %ld writes\n",
	   sent_requests, sent_bytes, sent_writes);

    printf("%ld pending reqs\n\n",
	   sent_requests - rcvd_requests);
}


static void *
report_stats(oop_source *source,
	     struct timeval UNUSED(tv),
	     void *user)
{
    do_report_stats();
    arm_timer(source, 0, 100000, report_stats, user);
    return OOP_CONTINUE;
}

static void *
report_progress(oop_source *source,
		struct timeval UNUSED(tv),
		void *user)
{
    if (rcvd_requests > last_progress_status)
    {
	last_progress_status = rcvd_requests;
	printf("Progress: %ld pending reqs\n", sent_requests - rcvd_requests);
	fflush(stdout);
    }
    arm_timer(source, 2, 0, report_progress, user);
    return OOP_CONTINUE;
}

static void *
end_it(oop_source *UNUSED(source),
       struct timeval UNUSED(tv),
       void *UNUSED(user))
{
    abort_pending = 0;
    return OOP_HALT;
}

static void *
limit_it(oop_source *oop,
	 struct timeval UNUSED(tv),
	 void *UNUSED(user))
{
    stop_writing(oop);
    return OOP_CONTINUE;
}

static struct option longopts[] = {
    {"verbose", 0, 0, 'v'},
    {"silent", 0, 0, 's'},
    {"time-abort", 0, 0, 'a'},
    {"time-limit", 0, 0, 'l'},
    {"write-only", 0, 0, 'w'},
    {"priority", 0, 0, 'p'},
    {"weight", 0, 0, 'W'},

    { 0, 0, 0, 0 }
};

int
main(int argc,
     char **argv)
{
    int optc;
    int oldflags;
    ssize_t rv;
    oop_source_sys *sys;
    oop_source *oop;
    struct timeval progress_timer;
    struct timeval report_timer;
    struct timeval end_timer;
    char buf[8192];

    /* What should we do? */
    int do_limit_time = 0;
    int do_abort_time = 0;
    int verbose = 0;

    /* Alter scheduling? */
    int priority = -1;
    int weight = -1;

    if (signal(SIGPIPE, SIG_IGN) == SIG_ERR)
	fail("signal");

    while ((optc = getopt_long(argc, argv, "W:wvalp:",
			       longopts, (int *) 0)) != EOF)
    {
	switch (optc)
	{
	case 'v':
	    verbose = 1;
	    break;

	case 's':
	    verbose = 0;
	    break;

	case 'a':
	    do_abort_time = 1;
	    break;

	case 'l':
	    do_limit_time = 1;
	    break;

	case 'w':
	    do_write_only = 1;
	    break;

	case 'W':
	    weight = atoi(optarg);
	    break;

	case 'p':
	    priority = atoi(optarg);
	    break;

	case '?':
	    fail("usage");
	}
    }

    if (optind + 2 != argc)
	fail("usage");
    
    if ((priority == -1) + (weight == -1) == 1)
	fail("usage");

    server = tcp_connect(argv[optind], argv[optind + 1]);
    puts("connected");
    if (write(server, "A3Hfoo\n", 7) != 7)
	fail("write");
    if ((rv = read(server, buf, sizeof(buf))) < 0)
	fail("read");
    if (rv == 0)
    {
	fprintf(stderr, "unexpected EOF from server");
	exit(1);
    }
    if (rv != 7 || strncmp(buf, "LysKOM\n", 7) != 0)
    {
	fprintf(stderr, "Got bad handshake ``%.*s'' from the server\n",
		(int)rv, buf);
	exit(1);
    }

    puts("handshake OK");

    if (priority != -1)
    {
	sprintf(buf, "1 119 0 %d %d\n", priority, weight);
	if (write(server, buf, strlen(buf)) != (ssize_t)strlen(buf))
	    fail("set-scheduling write");
	if ((rv = read(server, buf, sizeof(buf))) < 0)
	    fail("set-scheduling read");
	if (rv == 0)
	{
	    fprintf(stderr, "unexpected EOF from server after set-schedule\n");
	    exit(1);
	}
	if (rv != 3 || strncmp(buf, "=1\n", 3) != 0)
	{
	    fprintf(stderr, "Got bad reply ``%.*s'' to set-schedule\n",
		    (int)rv, buf);
	    exit(1);
	}
	puts("set-schedule ok");
    }

    if ((sys = oop_sys_new()) == NULL)
	fail("oop_sys_new");

    /* Make server connection nonblocking. */
    if ((oldflags = fcntl(server, F_GETFL, 0)) < 0)
	fail("fcntl(F_GETFL)");
    if (fcntl(server, F_SETFL, oldflags|O_NONBLOCK) < 0)
	fail("fcntl(F_SETFL)");

    oop = oop_sys_source(sys);

    oop->on_fd(oop, 0, OOP_READ, read_stdin, NULL);
    if (!do_write_only)
	start_reading(oop);
    start_writing(oop);
    if (verbose)
	arm_timer(oop, 0, 0, report_stats, &report_timer);

    arm_timer(oop, 0, 500000, report_progress, &progress_timer);

    if (do_limit_time)
	arm_timer(oop, 5, 0, limit_it, &end_timer);
    else if (do_abort_time)
    {
	abort_pending = 1;
	arm_timer(oop, do_write_only ? 15 : 5, 0, end_it, &end_timer);
    }

    oop_sys_run(sys);

    if (verbose)
	oop->cancel_time(oop, report_timer, report_stats, &report_timer);

    if (abort_pending)
	oop->cancel_time(oop, end_timer, end_it, &end_timer);

    printf("End of progress reports\n");
    oop->cancel_time(oop, progress_timer, report_progress, &progress_timer);

    do_report_stats();

    oop->cancel_fd(oop, 0, OOP_READ);
    stop_reading(oop);
    stop_writing(oop);
    oop_sys_delete(sys);

    return 0;
}
