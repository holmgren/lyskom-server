/*
 * $Id: text-garb.c,v 0.54 2003/08/29 10:43:33 ceder Exp $
 * Copyright (C) 1991-1995, 1997-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * This file contains the functions that deletes old texts.
 *
 * Author: Per Cederqvist.
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#include "timewrap.h"
#include <sys/types.h>
#include <setjmp.h>
#include <errno.h>
#include <string.h>

#include "oop.h"

#include "ldifftime.h"
#include "s-string.h"
#include "timeval-util.h"
#include "kom-types.h"
#include "text-garb.h"
#include "kom-errno.h"
#include "misc-types.h"
#include "debug.h"
#include "cache.h"
#include "lyskomd.h"
#include "log.h"
#include "param.h"
#include "server-time.h"
#include "com.h"
#include "async.h"
#include "connections.h"
#include "manipulate.h"
#include "text.h"
#include "unused.h"
#include "isc-interface.h"
#ifdef DEBUG_CALLS
#  include "send-async.h"
#  include "services.h"
#endif

BUGDECL;

/*
 * This comment is a description of how this _could_ be done in a more
 * efficient way.  It is not yet implemented.  Design by Inge Wallin
 * and Per Cederqvist 1992-10-07.  FIXME (bug 176).
 *
 * Today, all text statuses are read into the core during the garbage
 * collection phase. Due to step 3 below we think that we can reduce
 * the number of text-status-fetches by 95 %.
 *
 * 1) Allocate a bitmap with one bit for each text. Clear all bits.
 *    This bit is set as soon as it is certain that this text should
 *    not be deleted during this garbage collection pass.
 * 2) Loop over all persons:
 *	+ Set the bit for all his/her marked texts.
 *    (This step may, or may not, be efficient. Profile your code!)
 * 3) Loop over all conferences:
 *	+ Loop over all texts in the conference:
 *		+ If the bit is already set, skip the text.
 *		+ Retrieve the text status from the data base.
 *		+ If it is old enough (in all the recipient conferences)
 *			delete it.
 *		  else
 *		        set the bit.
 *		+ If it is too young to be deleted (only
 *    		  considering this conferene)
 *			+ Set the bit for this text and all subsequent
 *    			  texts in this conference.
 * 4) Loop over all remaining texts:
 *	+ If the bit is not set,
 *		+ delete the text. (It has no recipients).
 * 5) Deallocate the bitmap and wait 24 hours.
 */

static Text_no last_checked = 0;

static struct timeval garb_timer;
static Bool garb_timer_running = FALSE;

static const double day_to_sec = 24 * 3600;
static const double default_save = 1.0 * 24 * 3600;

static Bool
saved_by_aux(Text_stat *text_s)
{
    unsigned short naux;
    
    for (naux = 0; naux < text_s->aux_item_list.length; ++naux)
        if (text_s->aux_item_list.items[naux].flags.dont_garb)
            return TRUE;

    return FALSE;
}

static Bool
saved_by_recipient(Text_stat *text_s,
		   double age)
{
    unsigned short nmisc;
    Misc_info 	  *misc;
    double         limit = 0;
    Bool           in_rec_group = FALSE;

    for (nmisc = text_s->no_of_misc, misc = text_s->misc_items;
	 nmisc > 0;
	 --nmisc, ++misc)
    {
	switch (misc->type)
	{
	case recpt:
	case cc_recpt:
	case bcc_recpt:
	    if (cached_conf_exists(misc->datum.recipient))
	    {
		limit = (day_to_sec
			 * cached_get_garb_nice(misc->datum.recipient));
		if (age < limit)
		    return TRUE;
		in_rec_group = TRUE;
	    }
	    else
		in_rec_group = FALSE;
	    break;

	case comm_to:
	case comm_in:
	case footn_to:
	case footn_in:
	    in_rec_group = FALSE;
	    break;
	    
	case loc_no:
	case rec_time:
	case sent_by:
	    break;
	    
	case sent_at:
	    if (in_rec_group
		&& ldifftime(current_time.tv_sec, misc->datum.sent_at) < limit)
	    {
		return TRUE;
	    }
	    break;

#ifndef COMPILE_CHECKS
	default:
#endif
	case unknown_info:
	    restart_kom("saved_by_recipient(): Illegal misc-item.\n");
	}
    }

    return FALSE;
}

static Bool
saved_by_comment(Text_stat *text_s)
{
    unsigned short nmisc;
    Misc_info 	  *misc;
    Bool           in_comm_group = FALSE;

    for (nmisc = text_s->no_of_misc, misc = text_s->misc_items;
	 nmisc > 0;
	 --nmisc, ++misc)
    {
	switch (misc->type)
	{
	case recpt:
	case cc_recpt:
	case bcc_recpt:
	    in_comm_group = FALSE;
	    break;

	case comm_to:
	case comm_in:
	case footn_to:
	case footn_in:
	    in_comm_group = TRUE;
	    break;
	    
	case loc_no:
	case rec_time:
	case sent_by:
	    break;
	    
	case sent_at:
	    if (in_comm_group && ldifftime(current_time.tv_sec,
					   misc->datum.sent_at) < default_save)
	    {
		return TRUE;
	    }
	    break;

#ifndef COMPILE_CHECKS
	default:
#endif
	case unknown_info:
	    restart_kom("saved_by_comment(): Illegal misc-item.\n");
	}
    }

    return FALSE;
}

/* Examine ``text_no''.  (``text_s'' is the text-stat of ``text_no'',
   or NULL).

   If ``text_no'' has a comment or footnote, return TRUE, else FALSE.

   If ``text_no'' is a comment or footnote to ``parent'', the number
   of seconds that has passed since it became so will be returned in
   ``comment_age''.

   Set ``limit'' to the greatest keep-commented value found on any of
   the recipients of ``text_no'', converted to seconds.  (``parent''
   is not involved in this computation.) */

static Bool
find_comment_limit_and_age(Text_no    text_no,
			   Text_stat *text_s,
			   Text_no    parent,
			   double    *comment_age,
			   double    *limit)
{
    unsigned short nmisc;
    Misc_info 	  *misc;
    Bool           in_comm_group = FALSE;
    Bool           has_comments = FALSE;
    double         tmp;

    if (text_s == NULL)
	text_s = cached_get_text_stat(text_no);

    if (text_s == NULL)
    {
	*comment_age = 0;
	*limit = 0;
	return FALSE;
    }

    *comment_age = ldifftime(current_time.tv_sec, text_s->creation_time);
    *limit = default_save;

    for (nmisc = text_s->no_of_misc, misc = text_s->misc_items;
	 nmisc > 0;
	 --nmisc, ++misc)
    {
	switch (misc->type)
	{
	case recpt:
	case cc_recpt:
	case bcc_recpt:
	    in_comm_group = FALSE;
	    if (cached_conf_exists(misc->datum.recipient))
	    {
		tmp = day_to_sec * cached_get_keep_commented(
		    misc->datum.recipient);
		if (tmp > *limit)
		    *limit = tmp;
	    }
	    break;

	case comm_to:
	case footn_to:
	    in_comm_group = (misc->datum.text_link == parent);
	    break;

	case comm_in:
	case footn_in:
	    in_comm_group = FALSE;
	    has_comments = TRUE;
	    break;
	    
	case loc_no:
	case rec_time:
	case sent_by:
	    break;
	    
	case sent_at:
	    if (in_comm_group)
		*comment_age = ldifftime(current_time.tv_sec,
					 misc->datum.sent_at);
	    break;

#ifndef COMPILE_CHECKS
	default:
#endif
	case unknown_info:
	    restart_kom("find_comment_limit_and_age(): Illegal misc-item.\n");
	}
    }

    return has_comments;
}

static Bool
saved_by_keep_commented(Text_no text_no,
			Text_stat *text_s)
{
    unsigned short nmisc;
    Misc_info 	  *misc;
    double         comment_age;
    double         parent_limit;
    double         limit;

    if (!find_comment_limit_and_age(text_no, text_s, 0,
				    &comment_age, &parent_limit))
	return FALSE;

    for (nmisc = text_s->no_of_misc, misc = text_s->misc_items;
	 nmisc > 0;
	 --nmisc, ++misc)
    {
	switch (misc->type)
	{
	case comm_in:
	case footn_in:
	    find_comment_limit_and_age(misc->datum.text_link, NULL, text_no,
				       &comment_age, &limit);
	    if (limit < parent_limit)
		limit = parent_limit;

	    if (comment_age < limit)
		return TRUE;
	    break;
	    
	case recpt:
	case cc_recpt:
	case bcc_recpt:
	case comm_to:
	case footn_to:
	case loc_no:
	case rec_time:
	case sent_by:
	case sent_at:
	    break;

#ifndef COMPILE_CHECKS
	default:
#endif
	case unknown_info:
	    restart_kom("saved_by_keep_commented(): Illegal misc-item.\n");
	}
    }

    return FALSE;
}

static Bool
saved(Text_no    text_no,
      Text_stat *text_s)
{
    double age;

    if (text_s->no_of_marks > 0)
	return TRUE;

    age = ldifftime(current_time.tv_sec, text_s->creation_time);
    if (age < default_save)
	return TRUE;

    if (saved_by_aux(text_s))
	return TRUE;

    if (saved_by_recipient(text_s, age))
	return TRUE;

    if (saved_by_comment(text_s))
	return TRUE;

    if (saved_by_keep_commented(text_no, text_s))
	return TRUE;

    return FALSE;
}


/*
 * Delete old texts.
 *
 * Return value is TRUE if there is nothing more to do right now,
 * FALSE if this function should be called again as soon as the
 * server has some time over.
 */

static Bool
garb_text(void)
{
    static long	   deleted_texts = 0;
    Text_stat	  *text_s;

    if (param.garb_enable == FALSE)
	return TRUE;

    if (last_checked == 0)
	kom_log("MSG: garb started.\n");

    tell_cache_garb_text(1);
    last_checked = traverse_text( last_checked );

    if (last_checked == 0)
    {
	kom_log("MSG: garb ready. %lu texts deleted.\n",
		(unsigned long)deleted_texts);
	tell_cache_garb_text(0);
#ifdef DEBUG_CALLS
	async_garb_ended(deleted_texts);
#endif
	deleted_texts = 0;
	return TRUE;
    }
    
    if ((text_s = cached_get_text_stat(last_checked)) == NULL)
    {
	kom_log("ERROR: garb_text(): Can't get text-stat.\n");
	tell_cache_garb_text(0);
	return FALSE;
    }

    if (!saved(last_checked, text_s))
    {
	VBUG(("garb_text: deleting %lu\n", last_checked));
	do_delete_text(last_checked, text_s);
	deleted_texts++;
    }

    tell_cache_garb_text(0);
    return FALSE;
}

static void *
garb_callback(oop_source *source,
	      struct timeval UNUSED(tv),
	      void *UNUSED(user))
{
    int rv = 0;
    
    set_time();
    if (garb_text() == FALSE)
	if (server_idle())
	    rv = setup_timer(&garb_timer, param.garbtimeout);
	else
	    rv = setup_timer(&garb_timer, param.garb_busy_postponement);
    else
 	rv = setup_timer(&garb_timer, param.garb_interval);

    if (rv < 0)
	kom_log("gettimeofday failed: %s\n", strerror(errno));

    source->on_time(source, garb_timer, garb_callback, NULL);
    return OOP_CONTINUE;
}

void
start_garb_thread(oop_source *src)
{
    stop_garb_thread(src);

    garb_timer = OOP_TIME_NOW;
    src->on_time(src, garb_timer, garb_callback, NULL);
    garb_timer_running = TRUE;
}

void
stop_garb_thread(oop_source *src)
{
    if (garb_timer_running)
    {
	src->cancel_time(src, garb_timer, garb_callback, NULL);
	garb_timer_running = FALSE;
    }
}

#ifdef DEBUG_CALLS
Success
start_garb(void)
{
    CHK_CONNECTION(FAILURE);
    kom_log("MSG: garb restarted.\n");
    last_checked = 0;
    start_garb_thread(oop_sys_source(kom_server_oop_src));
    return OK;
}
#endif
