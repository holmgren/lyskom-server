/*
 * $Id: manipulate.h,v 0.45 2003/08/23 16:38:15 ceder Exp $
 * Copyright (C) 1991-1994, 1996-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: manipulate.h,v 0.45 2003/08/23 16:38:15 ceder Exp $
 *
 * manipulate.h
 *
 * Skapad av ceder n}gon g}ng efter 1990-05-25, men f|re 1990-07-05.
 */

/*
 * Definition of macros used in services.c and manipulate.c
 */

/* pers_no of the connected person. (0    if not logged in) */
#define ACTPERS (active_connection->pers_no)

/* person  of the connected person. (NULL if not logged in) */
#define ACT_P	(active_connection->person)

#define CHK_CONNECTION(errortype)               \
do {                                            \
    if (active_connection == NULL)              \
    {                                           \
        err_stat = 0;                           \
        kom_errno = KOM_INTERNAL_ERROR;         \
        return errortype;                       \
    }                                           \
} while (0)

#define CHK_BOOL(val, errortype)                \
do {                                            \
    if (val != 0 && val != 1)                   \
    {                                           \
        err_stat = 0;                           \
        kom_errno = KOM_BAD_BOOL;               \
        return errortype;                       \
    }                                           \
} while (0)

/* many functions can only be used if ACTPERS is logged in */
#define CHK_LOGIN(errortype)                    \
do {                                            \
    if ( !active_connection || !ACTPERS )       \
    {					        \
        err_stat  = 0;                          \
	kom_errno = KOM_LOGIN;		        \
	return errortype;		        \
    }					        \
} while (0)


/* Conference 0 does never exist. */

#define CONF_ZERO(conf, errortype)	\
do {					\
    if ( (conf) == 0 )			\
    {					\
        err_stat  = 0;                  \
	kom_errno = KOM_CONF_ZERO;	\
	return errortype;		\
    }					\
} while (0)
    

/* Check that a conference exists */

#define CHK_EXIST(conf, errortype)	\
do {					\
    CONF_ZERO(conf, errortype);		\
    if ( !cached_conf_exists( conf ))	\
    {					\
        err_stat  = conf;               \
	kom_errno = KOM_UNDEF_CONF;	\
	return errortype;		\
    }					\
} while (0)


/* do a cached_get_person_stat */

#define GET_P_STAT(p_stat_p, pers_no, failure)				\
do {									\
    if ( ((p_stat_p) = cached_get_person_stat( pers_no )) == NULL)	\
    {									\
	return (failure);						\
    }									\
} while (0)

#define VOID_GET_P_STAT(p_stat_p, pers_no)				\
do {									\
    if ( ((p_stat_p) = cached_get_person_stat( pers_no )) == NULL)	\
    {									\
	return;								\
    }									\
} while (0)

/* do a cached_get_conf_stat */

#define GET_C_STAT(c_stat_p, conf_no, failure)				\
do {									\
    if ( ((c_stat_p) = cached_get_conf_stat( conf_no )) == NULL)	\
    {									\
	return (failure);						\
    }									\
} while (0)

#define VOID_GET_C_STAT(c_stat_p, conf_no)				\
do {									\
    if ( ((c_stat_p) = cached_get_conf_stat( conf_no )) == NULL)	\
    {									\
	return;								\
    }									\
} while (0)

/* do a cached_get_text_stat */

#define GET_T_STAT(t_stat_p, text_no, failure)				\
do {									\
    if ( ((t_stat_p) = cached_get_text_stat( text_no )) == NULL)	\
    {									\
	return (failure);						\
    }									\
} while (0)

#define VOID_GET_T_STAT(t_stat_p, text_no)				\
do {									\
    if ( ((t_stat_p) = cached_get_text_stat( text_no )) == NULL)	\
    {									\
	return;								\
    }									\
} while (0)


/*
 * ENA returns TRUE if ACTPERS has his privtype bit set, and has
 * enabled to at least req_lev.
 */

#define ENA(privtype, req_lev)                                          \
                (active_connection &&                                   \
                 active_connection->ena_level >=(req_lev) &&            \
                 (ACT_P)->privileges.privtype)

#define ENA_C(conn, privtype, req_lev)                                  \
                (conn->ena_level >=(req_lev) &&                         \
                (conn)->person &&                                       \
                ((conn)->person)->privileges.privtype)

#define HAVE_PRIV(pers, privtype) (pers && pers->privileges.privtype)


/*
 * Add a misc item to a text_status.
 * The item is put last on the list. Thus this macro should not be used
 * to add rec_time.
 *
 *	Text_stat * text_stat_pointer	Textstatus to modify
 *	enum info_type type_of_misc	Type om misc_item to add
 *		    tag_name		Tagname in union info_datum
 *		    value_of_misc	Value to set tag_name to.
 */
#define ADD_MISC(text_stat_pointer, type_of_misc, tag_name, value_of_misc) \
{									\
    /* Allocate space */						\
    text_stat_pointer->misc_items					\
    = srealloc(text_stat_pointer->misc_items,				\
	       (++(text_stat_pointer->no_of_misc)) * sizeof(Misc_info));\
									\
    /* Set type */							\
    text_stat_pointer->misc_items[ text_stat_pointer->no_of_misc-1 ]	\
    .type = type_of_misc;						\
									\
    /* Set value */							\
    text_stat_pointer->misc_items[ text_stat_pointer->no_of_misc-1 ]	\
    .datum.tag_name = value_of_misc;					\
}


/*
 * A value of the following type are returned from access_perm(),
 * which is used to see how much viewer is allowed to read/modify the
 * data of a given conference.
 */

enum access {
    error,			/* The conf doesn't exist or other error. */
    none,			/* A secret conference. */
    read_protected,		/* An rd_prot conference. */
    limited,			/* An open conference. */
    member,			/* ACTPERS is a member of the conference. */
    unlimited			/* ACTPERS is supervisor of the conference.  */
};

/* Return the highest access level that viewer_conn has to victim, but
   don't bother checking for higher level of access than
   wanted_access.  Specify as low wanted_access as possible when
   calling this function, since that might speed upp the processing
   considerably.  */
extern enum access
access_perm(Conf_no victim,
	    const Connection *viewer_conn,
	    enum access wanted_access);

/* Return true if VIEWER_CONN has at least the access level
   WANTED_ACCESS to VICTIM. */
extern Bool
has_access(Conf_no victim,
	   const Connection *viewer_conn,
	   enum access wanted_access);

/* Return VICTIM if the user user logged in via VIEWER_CONN is allowed
   to see it, or 0 if it is a secret conference that the user isn't
   allowed to see.  */

extern Conf_no
filter_conf_no(Conf_no victim,
	       const Connection *viewer_conn);

/* Visibility of a membership. */
enum memb_visibility
{
    /* No visibility at all. */
    mv_none,

    /* The membership is visible.  However, the information about read
       texts should be censored. */
    mv_censor_unread,

    /* Full visibility.  The unread_is_secret bit should be
       ignored. */
    mv_full,
};


/* Compute the visibility of a membership.  MEMBER is a member of
   CONF_NO, and the membership type i TYPE.  VIEWER_CONN wants to know
   stuff about the membership.  If it is known that the person logged
   on at VIEWER_CONN is a supervisor of the MEMBER,
   IS_SUPERVISOR_OF_MEMBER should be set (for a small performance
   gain).  Similarly for IS_SUPERVISOR_OF_CONF. */

extern enum memb_visibility
membership_visible(const Connection *viewer_conn,
		   Pers_no member,
		   Conf_no conf_no,
		   Membership_type type,
		   Bool is_supervisor_of_member,
		   Bool is_supervisor_of_conf);

/* Set err_stat to CONF_NO.  Set kom_errno to ERRCODE if VIEWER_CONN
   is allowed to know that the conference exists.  Otherwise, set
   kom_errno to KOM_UNDEF_CONF. */
extern void
set_conf_errno(const Connection *viewer_conn,
	       Conf_no conf_no,
	       enum kom_err errcode);


/*
 * Functions which manipulate the CONFERENCE struct.
 */

extern Success
chk_passwd( Password   pwd,
	   const String     s );


/*
 * Locate the Member struct in CONF_C for person PERS_NO
 */

extern Member *
locate_member(Pers_no      pers_no,
	      Conference * conf_c);


/*
 * Return TRUE if viewer is a supervisor to CONF.
 */

Bool
is_supervisor(Conf_no conf,
	      const Connection *viewer);

extern Bool		
is_strictly_supervisor(Conf_no       conf,
                       Pers_no	     viewer,
                       const Person *viewer_p); /* May be NULL */


/*
 * This function is called whenever a person leaves a conf,
 * i e when he change_conference():s or logout():s.
 */

extern void
leave_conf(Connection *conn);


/*
 * Change presentation of a conference. If text_no is 0, there will be
 * no presentation.
 */

extern Success
do_set_presentation(Conf_no 	 conf_no,
		    Conference * conf_c,
		    Text_no	 text_no);


/*
 * Change motd of a conference. If text_no is 0, there will be
 * no motd.
 */

extern Success
do_set_etc_motd(Conf_no      conf_no,
		Conference * conf_c,
		Text_no      text_no);




/*
 * Functions that manupulate the PERSON struct
 */

/*
 * Find the data about PERS_P:s membership in CONF_NO.
 * Return NULL if not found
 */

extern Membership *
locate_membership(Conf_no       conf_no,
		  const Person *pers_p);


/*
 * Check if any read_ranges can be extended due to deleted texts.
 * Returns true if any change was performed.  If that happens, the
 * caller must call mark_person_as_changed().
 */
extern Bool
adjust_read(Membership *m,
	    const Conference *conf);

/*
 * Delete a person. (The mailbox is not deleted);.
 */

extern Success
do_delete_pers (Pers_no pers_no);


/*
 * Functions which work on CONFERENCES and/or PERSONS.
 */

/*
 * Add a member to a conference. All errorchecking should already
 * be done when this function is called. The person must not already
 * be a member of the conference. It is _not_ an error to make WHERE bigger
 * than the number of conferences the person is a member in.
 */

extern void
do_add_member(Conf_no	     conf_no,  /* Conference to add a new member to. */
	      Conference   * conf_c,   /* Conf. status. Must NOT be NULL.  */
	      Pers_no	     pers_no,  /* Person to be added. */
	      Person	   * pers_p,   /* Pers. status. Must NOT be NULL. */
              Pers_no        added_by, /* Person doing the adding */
	      unsigned char  priority, /* Priority to assign to this conf */
	      unsigned short where,    /* Sequence number in the list */
              Membership_type * type,   /* Membership type */  
              Bool           fake_priority
              );


/*
 * Return TRUE if NAME is a legal name.  Set kom_errno otherwize.
 */

extern Bool
legal_name( String name );


/*
 * Return TRUE if NAME is not already used
 */

extern Bool
unique_name( const String name, Conf_no conf_no );


/*
 * Delete a member from a conference.
 * No checks are made on the parameters.
 * The dynamically allocated areas conf_c->members.members and
 * pers_p->confs are NOT reallocated since they will anyhow sooner or later
 * be flushed from core.
 */

extern Success
do_sub_member(Conf_no	   conf_no, /* Conf to delete member from. */
	      Conference * conf_c,  /* May be NULL */
	      Member     * mbr,     /* May be NULL */
	      Pers_no	   pers_no, /* Person to be deleted. */
	      Person	 * pers_p,  /* May be NULL */
	      Membership * mship);   /* Pointer to the persons membership in
				       conf., or NULL if not known. */


/*
 * Functions which deal with TEXTS
 */

/*
 * Check if the person logged in on CONN is allowed to read a text.
 * Returns TRUE if he is allowed to read it.
 */

extern Bool
text_read_access(const Connection *conn,
                 Text_no           text_no,
		 const Text_stat  *text_stat);



/*
 * Check if ACTPERS has sent this text as a footnote to parent.
 */
extern Bool
is_footn_sender(Text_stat * text_s,
		Text_no     parent);


extern void
forced_leave_conf(Pers_no pers_no,
		  Conf_no conf_no);


extern void
register_jubel(Pers_no pno,	/* See text.c */
	       Text_no divis,
	       Text_no tno,
	       Bool public);

extern void
free_all_jubel(void);
