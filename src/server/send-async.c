/*
 * $Id: send-async.c,v 0.50 2003/08/23 16:38:14 ceder Exp $
 * Copyright (C) 1991, 1993-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * send-async.c -- Send messages about events to all connected clients.
 *
 * Written by Per Cederqvist 1990-07-22--23
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#include <setjmp.h>
#include "timewrap.h"
#include <sys/types.h>

#include "misc-types.h"
#include "s-string.h"
#include "kom-types.h"
#include "com.h"
#include "async.h"
#include "connections.h"
#include "internal-connections.h"
#include "async.h"
#include "send-async.h"
#include "prot-a-send-async.h"
#include "lyskomd.h"
#include "log.h"
#include "param.h"
#include "ldifftime.h"
#include "kom-errno.h"
#include "manipulate.h"
#include "server-time.h"
#include "timeval-util.h"

void
async_new_text_old(struct connection *cptr,
                   Text_no    	  text_no, 
                   Text_stat         *text_s)
{
    if (!param.send_async_messages)
	return;

    switch(cptr->protocol)
    {
    case 0:
	break;
    case 'A':
	if (handshake_ok(cptr, 0))
	    prot_a_async_new_text_old(cptr, text_no, text_s);
	break;
    default:
	restart_kom("async_new_text(): bad protocol.\n");
	break;
    }
}

    
void
async_new_text(struct connection *cptr,
               Text_no    	  text_no, 
               Text_stat         *text_s)
{
    if (!param.send_async_messages)
	return;

    switch(cptr->protocol)
    {
    case 0:
	break;
    case 'A':
	if (handshake_ok(cptr, 0))
	    prot_a_async_new_text(cptr, text_no, text_s);
	break;
    default:
	restart_kom("async_new_text(): bad protocol.\n");
	break;
    }
}

    
    
	    
void
async_i_am_on(Who_info info)
{
    Connection *cptr;
    Session_no i = 0;
    Who_info filtered;

    if (!param.send_async_messages)
	return;

    while ( (i = traverse_connections(i)) != 0 )
    {
	cptr = get_conn_by_number(i);

	filtered = info;
	filtered.working_conference = filter_conf_no(
	    info.working_conference, cptr);

	switch(cptr->protocol)
	{
	case 0:			/* Not yet logged on. */
	    break;
	case 'A':
	    if (handshake_ok(cptr, 0))
		prot_a_async_i_am_on(cptr, filtered);
	    break;
	default:
	    restart_kom("async_i_am_on(): bad protocol.\n");
	    break;
	}
    }
}

void
async_logout(Pers_no pers_no, 
	     Session_no session_no)
{
    Connection *cptr;
    Session_no i = 0;

    if (!param.send_async_messages)
	return;

    while ( (i = traverse_connections(i)) != 0)
    {
	cptr = get_conn_by_number(i);

	if ( cptr == NULL )
	{
	    kom_log("async_logout(): cptr == NULL\n");
	    return;
	}

	switch(cptr->protocol)
	{
	case 0:
	    break;
	case 'A':
	    if (handshake_ok(cptr, 0))
		prot_a_async_logout(cptr, pers_no, session_no);
	    break;
	default:
	    restart_kom("async_logout(): bad protocol.\n");
	    break;
	}
    }
}



void
async_new_name(Conf_no 	     conf_no,
	       const String  old_name,
	       const String  new_name)
{
    Connection *cptr;
    Session_no i = 0;

    if (!param.send_async_messages)
	return;

    while ( (i = traverse_connections(i)) != 0)
    {
	cptr = get_conn_by_number(i);

	if ( cptr == NULL )
	{
	    kom_log("async_new_name(): cptr == NULL\n");
	    return;
	}

	switch(cptr->protocol)
	{
	case 0:
	    break;
	case 'A':
	    /* Check that cptr has enough privileges to know
	       anything about the conference. */
            if (handshake_ok(cptr, 0)
		&& (has_access(conf_no, cptr, read_protected)
		    || ENA_C(cptr, admin, 2)    /* OK -- Uses ENA_C */
		    || ENA_C(cptr, wheel, 8))) /* OK -- Uses ENA_C */
            {
                prot_a_async_new_name(cptr, conf_no, old_name, new_name);
            }
	    break;
		
	default:
	    restart_kom("async_new_name(): bad protocol.\n");
	    break;
	}
    }
}




#if 0    
conf_deleted
conf_created
#endif

void
async_sync_db(void)
{
    Connection *cptr;
    Session_no i = 0;

    if (!param.send_async_messages)
	return;

    while ( (i = traverse_connections(i)) != 0 )
    {
	cptr = get_conn_by_number(i);

	if ( cptr == NULL )
	{
	    kom_log("async_sync_db(): cptr == NULL\n");
	    return;
	}

	switch(cptr->protocol)
	{
	case 0:
	    break;
	case 'A':
	    if (handshake_ok(cptr, ignore_dns))
		prot_a_async_sync_db(cptr);
	    break;
	default:
	    restart_kom("async_sync_db(): bad protocol.\n");
	    break;
	}
    }
}


extern void
async_forced_leave_conf (struct connection *cptr,
			 Conf_no 	   conf_no)
{
    if (!param.send_async_messages)
	return;

    switch(cptr->protocol)
    {
    case 0:
	break;
    case 'A':
	if (handshake_ok(cptr, 0))
	    prot_a_async_forced_leave_conf(cptr, conf_no);
	break;
    default:
	restart_kom("async_forced_leave_conf(): bad protocol.\n");
	break;
    }
}

void
async_login(Pers_no	pers_no,
	    int		client_no)
{
    Connection *cptr;
    Session_no i = 0;

    if (!param.send_async_messages)
	return;

    while ( (i = traverse_connections(i)) != 0)
    {
	cptr = get_conn_by_number(i);

	if ( cptr == NULL )
	{
	    kom_log("async_login(): cptr == NULL\n");
	    return;
	}

	switch(cptr->protocol)
	{
	case 0:
	    break;
	case 'A':
	    if (handshake_ok(cptr, 0))
		prot_a_async_login(cptr, pers_no, client_no);
	    break;
		
	default:
	    restart_kom("async_login(): bad protocol.\n");
	    break;
	}
    }
}

void
async_rejected_connection(void)
{
    Connection *cptr;
    Session_no i = 0;
    static struct timeval last_time = {0, 0};

    if (!param.send_async_messages)
	return;

    if (timeval_nonzero(last_time)
	&& timeval_diff_sec(current_time, last_time) < 60)
    {
	return;
    }

    last_time = current_time;
	
    while ( (i = traverse_connections(i)) != 0)
    {
	cptr = get_conn_by_number(i);

	if ( cptr == NULL )
	{
	    kom_log("async_rejected_connections(): cptr == NULL\n");
	    return;
	}

	switch(cptr->protocol)
	{
	case 0:
	    break;
	case 'A':
	    if (handshake_ok(cptr, ignore_dns))
		prot_a_async_rejected_connection(cptr);
	    break;
		
	default:
	    restart_kom("async_rejected_connection(): bad protocol.\n");
	    break;
	}
    }
}


/*
 * Returns failure if no message was sent.
 */
Success
async_send_message(Pers_no recipient,
		   Pers_no sender,
		   String  message,
                   Bool    force_message)
{
    return async_send_group_message(recipient,
                                    recipient,
                                    sender,
                                    message,
                                    force_message);
}

Success
async_send_group_message(Pers_no recipient,
			 Conf_no group_recipient,
			 Pers_no sender,
			 String  message,
                         Bool    force_message)
{
    Connection *cptr;
    Session_no i = 0;
    Success retval = FAILURE;
    Bool tmp = FALSE;

    kom_errno = KOM_MESSAGE_NOT_SENT;
    
    if (!param.send_async_messages)
    {
        err_stat = 0;
        kom_errno = KOM_FEATURE_DISABLED;
	return FAILURE;
    }

    while ( (i = traverse_connections(i)) != 0)
    {
	cptr = get_conn_by_number(i);

	if ( cptr == NULL )
	{
	    kom_log("async_send_message(): cptr == NULL\n");
            err_stat = 0;
            kom_errno = KOM_INTERNAL_ERROR;
	    return FAILURE;
	}

	switch(cptr->protocol)
	{
	case 0:
	    break;
	case 'A':
	    if ((recipient == 0
		 || (recipient == cptr->pers_no && recipient != 0 ))
		&& handshake_ok(cptr, ignore_dns))
	    {
                if (force_message)
                {
                    tmp = cptr->want_async[ay_send_message];
                    cptr->want_async[ay_send_message] = TRUE;
                }
		prot_a_async_send_message(cptr, group_recipient,
					  sender, message);
                if (force_message)
                    cptr->want_async[ay_send_message] = tmp;
		retval = OK;
	    }
	    break;
		
	default:
	    restart_kom("async_send_message(): bad protocol.\n");
	    break;
	}
    }
    
    return retval;
}


void
async_deleted_text(struct connection *cptr,
                   Text_no    	  text_no, 
                   Text_stat         *text_s)
{
    if (!param.send_async_messages)
	return;

    switch(cptr->protocol)
    {
    case 0:
	break;
    case 'A':
	if (handshake_ok(cptr, 0))
	    prot_a_async_deleted_text(cptr, text_no, text_s);
	break;
    default:
	restart_kom("async_deleted_text(): bad protocol.\n");
	break;
    }
}


void
async_new_recipient(struct connection   *cptr,
                    Text_no              text_no,
                    Conf_no              conf_no,
                    enum info_type       type)
{
    if (!param.send_async_messages)
        return;

    switch(cptr->protocol)
    {
    case 0:
        break;
    case 'A':
        if (handshake_ok(cptr, 0))
            prot_a_async_new_recipient(cptr, text_no, conf_no, type);
        break;
    default:
        restart_kom("async_new_recipient(): bad protocol.\n");
        break;
    }
}


void
async_sub_recipient(struct connection   *cptr,
                    Text_no              text_no,
                    Conf_no              conf_no,
                    enum info_type       type)
{
    if (!param.send_async_messages)
        return;

    switch(cptr->protocol)
    {
    case 0:
        break;
    case 'A':
        if (handshake_ok(cptr, 0))
            prot_a_async_sub_recipient(cptr, text_no, conf_no, type);
        break;
    default:
        restart_kom("async_sub_recipient(): bad protocol.\n");
        break;
    }
}


void
async_new_membership(struct connection   *cptr,
                     Pers_no pers_no,
                     Conf_no conf_no)
{
    if (!param.send_async_messages)
        return;

    switch(cptr->protocol)
    {
    case 0:
        break;
    case 'A':
        if (handshake_ok(cptr, 0))
            prot_a_async_new_membership(cptr, pers_no, conf_no);
        break;
    default:
        restart_kom("async_new_membership(): bad protocol.\n");
        break;
    }
}

void
async_new_user_area(Pers_no person,
		    Text_no old_user_area,
		    Text_no new_user_area)
{
    Connection *cptr;
    Session_no i = 0;
     
    if (!param.send_async_messages)
	return;
     
    while ((i = traverse_connections(i)) != 0)
    {
	cptr = get_conn_by_number(i);
     
	switch(cptr->protocol)
	{
	case 0:
	    /* No protocol specified yet */
	    break;
	case 'A':
	    /* Check that connection is logged on and allowed to see
	       the user-area of PERSON.  Check the want_async here
	       to avoid the potentially expensive call to access_perm. */
	    if (handshake_ok(cptr, 0)
		&& cptr->want_async[ay_new_user_area] == TRUE
		&& has_access(person, cptr, unlimited))
	    {
		prot_a_async_new_user_area(cptr, person, old_user_area,
					   new_user_area);
	    }
	    break;
	default:
	    restart_kom("async_new_user_area(): bad protocol.\n");
	    break;
	}
    }
}

void
async_new_presentation(Connection *cptr,
		       Conf_no conf_no,
		       Text_no old_presentation,
		       Text_no new_presentation)
{
    if (!param.send_async_messages)
	return;
     
    switch(cptr->protocol)
    {
    case 0:
	/* No protocol specified yet */
	break;
    case 'A':
	if (handshake_ok(cptr, 0)
	    && cptr->want_async[ay_new_presentation] == TRUE)
	{
	    prot_a_async_new_presentation(cptr, conf_no, old_presentation,
					  new_presentation);
	}
	break;
    default:
	restart_kom("async_new_presentation(): bad protocol.\n");
	break;
    }
}


void
async_new_motd(Connection *cptr,
	       Conf_no conf_no,
	       Text_no old_motd,
	       Text_no new_motd)
{
    if (!param.send_async_messages)
	return;
     
    switch(cptr->protocol)
    {
    case 0:
	/* No protocol specified yet */
	break;
    case 'A':
	if (handshake_ok(cptr, 0) && cptr->want_async[ay_new_motd] == TRUE)
	    prot_a_async_new_motd(cptr, conf_no, old_motd, new_motd);
	break;
    default:
	restart_kom("async_new_motd(): bad protocol.\n");
	break;
    }
}

void
async_text_aux_changed(Connection    *cptr,
		       Text_no        text_no,
		       Aux_item_list *aux_list,
		       unsigned long  highest_old_aux)
{
    if (!param.send_async_messages)
	return;
     
    switch(cptr->protocol)
    {
    case 0:
	/* No protocol specified yet */
	break;
    case 'A':
	if (handshake_ok(cptr, 0) && cptr->want_async[ay_text_aux_changed])
	    prot_a_async_text_aux_changed(cptr, text_no, aux_list,
					  highest_old_aux);
	break;
    default:
	restart_kom("async_text_aux_changed(): bad protocol.\n");
	break;
    }
}


#ifdef DEBUG_CALLS
void
async_garb_ended(int no_deleted)
{
    Connection *cptr;
    Session_no i = 0;
     
    if (!param.send_async_messages)
	return;
     
    while ((i = traverse_connections(i)) != 0)
    {
	cptr = get_conn_by_number(i);
     
	switch(cptr->protocol)
	{
	case 0:
	    /* No protocol specified yet */
	    break;
	case 'A':
	    /* Check that connection is logged on.  */
	    if (handshake_ok(cptr, 0))
		prot_a_async_garb_ended(cptr, no_deleted);
	    break;
	default:
	    restart_kom("async_garb_ended(): bad protocol.\n");
	    break;
	}
    }
}

#endif
