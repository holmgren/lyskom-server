#
# $Id: prot-a-parse-arg-c.awk,v 0.31 2003/08/23 16:38:15 ceder Exp $
# Copyright (C) 1991-1994, 1996-1999, 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 
#
# $Id: prot-a-parse-arg-c.awk,v 0.31 2003/08/23 16:38:15 ceder Exp $
BEGIN {
    printf("/* Don't edit this file - it is generated automatically");
    printf(" from\n   prot-a-parse-arg-c.awk and fncdef.txt */\n\n");
    printf("#ifdef HAVE_CONFIG_H\n");
    printf("#  include <config.h>\n");
    printf("#endif\n");
    printf("#include <sys/types.h>\n");
    printf("#include <sys/socket.h>\n");
    printf("#include <stdarg.h>\n");
    printf("#include <setjmp.h>\n");
    printf("#include \"timewrap.h\"\n");
    printf("\n");
    printf("#include \"oop.h\"\n");
    printf("\n");
    printf("#include \"s-string.h\"\n");
    printf("#include \"kom-types.h\"\n");
    printf("#include \"com.h\"\n");
    printf("#include \"isc-interface.h\"\n");
    printf("#include \"async.h\"\n");
    printf("#include \"connections.h\"\n");
    printf("#include \"isc-parse.h\"\n");
    printf("#include \"server/smalloc.h\"\n");
    printf("#include \"prot-a.h\"\n");
    printf("#include \"prot-a-parse.h\"\n");
    printf("#include \"prot-a-parse-arg.h\"\n");
    printf("#include \"kom-config.h\"\n");
    printf("#include \"param.h\"\n\n");
}
$1 == "#ifdef" {
    printf("#ifdef %s\n", $2);
    next;
}
$1 == "#endif" {
    printf("#endif\n");
    next;
}
$1 != "#" && $1 != "" {
    printf("void\nprot_a_parse_arg_%s(Connection *client)\n{\n", $3);
    printf("    switch( client->fnc_parse_pos )\n");
    printf("    {\n");
    num=0;
    pos=0;
    string=0;
    c_string=0;
    for ( i = 4; i <= NF; i++)
    {
	if ( $i == ":" )
	    break;

	printf("    case %d:\n", pos);
	if ( $i == "num" )
	    printf("\tclient->num%d = prot_a_parse_long(client);\n", num++);
	else if ( $i == "c_string" )
	   printf("\tprot_a_parse_string(client, &client->c_string%d, %s);\n",\
		   c_string++, $(++i));
	else if ( $i == "string" )
	    printf("\tprot_a_parse_string(client, &client->string%d, %s);\n",\
		   string++, $(++i));
	else if ( $i == "priv_bits" )
	    printf("\tprot_a_parse_priv_bits(client, &client->priv_bits);\n");
	else if ( $i == "conf_type" )
	    printf("\tprot_a_parse_conf_type(client, &client->conf_type);\n");
	else if ( $i == "membership_type" )
	    printf("\tprot_a_parse_membership_type(client, &client->membership_type);\n");
	else if ( $i == "c_misc_info_p" )
	{
	    printf("\tif ( parse_nonwhite_char(client) != '{' )\n");
	    printf("\t    longjmp(parse_env, KOM_PROTOCOL_ERR);\n");
	    printf("\tif ( client->num%d > %s )\n", num-1, $(++i));
	    printf("\t    longjmp(parse_env, KOM_PROTOCOL_ERR);\n");
	    printf("\n");
	    printf("\tclient->c_misc_info_p = ");
	    printf("smalloc(client->num%d\n", num-1);
	    printf("\t\t\t\t\t* sizeof(Misc_info));\n");
	    printf("\tclient->array_parse_index = 0;\n");
	    printf("\tclient->fnc_parse_pos = %d;\n", ++pos);
	    printf("    case %d:\n", pos);
	    printf("\twhile( client->array_parse_index < ");
	    printf("client->num%d )\n", num-1);
	    printf("\t{\n");
	    printf("\t    prot_a_parse_misc_info(client,");
	    printf(" &client->c_misc_info_p");
	    printf("[ client->array_parse_index ]);\n");
	    printf("\t    ++client->array_parse_index;\n");
	    printf("\t}\n");
	    printf("\tclient->fnc_parse_pos = %d;\n", ++pos);
	    printf("    case %d:\n", pos);
	    printf("\tif ( parse_nonwhite_char(client) != '}' )\n");
	    printf("\t    longjmp(parse_env, KOM_PROTOCOL_ERR);\n");
	}
        else if ( $i == "aux_item" )
          printf("\tprot_a_parse_aux_item(client, &client->aux_item);\n");
	else if ( $i == "aux_item_list" )
          printf("\tprot_a_parse_aux_item_list(client, &client->aux_item_list, %s);\n", $(++i));
        else if ( $i == "misc_info_list" )
          printf("\tprot_a_parse_misc_info_list(client, &client->misc_info_list, %s);\n", $(++i));
	else if ( $i == "time_date" )
	    printf("\tprot_a_parse_time_date(client, &client->time);\n");
        else if ( $i == "info" )
            printf("\tprot_a_parse_info(client, &client->info);\n");
        else if ( $i == "num_list" )
            printf("\tprot_a_parse_num_list(client, &client->num_list, %s);\n",
		   $(++i));
        else if ( $i == "pers_flags" )
            printf("\tprot_a_parse_pers_flags(client, &client->pers_flags);\n");
        else if ( $i == "read_range_list" )
	    printf("\tprot_a_parse_read_range_list(client, " \
		   "&client->read_range_list, %s);\n", $(++i));
	else
	    printf("#error in prot-a-parse-arg-c.awk: not ready yet.\n");

	if ( i != NF && $(i+1) != ":")
	    printf("\tclient->fnc_parse_pos = %d;\n", ++pos);
    }

    printf("    default:\n");
    printf("\tclient->fnc_parse_pos = 0;\n");
    printf("    }\n");
    printf("}\n\n");
}
END {
    printf("/* end of this auto generated file. */\n");
}
