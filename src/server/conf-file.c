/*
 * $Id: conf-file.c,v 1.34 2003/08/23 16:38:18 ceder Exp $
 * Copyright (C) 1994-1995, 1998-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/* 
 * Read configuration files.
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#include "timewrap.h"
#include <assert.h>
#include <ctype.h>
#ifdef HAVE_STRING_H
#  include <string.h>
#else
#  ifdef HAVE_STRINGS_H
#    include <strings.h>
#  endif
#endif
#ifndef HAVE_STRCHR
#  define strchr index
#endif
#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#endif

#include "s-string.h"
#include "server/smalloc.h"
#include "log.h"
#include "misc-types.h"
#include "kom-types.h"
#include "conf-file.h"
#include "lyskomd.h"
#include "eintr.h"
#include "timeval-util.h"

#define MAXLINE 1001

static int *assignment_count = NULL;
static int  npar = 0;

static void
init_init(const struct parameter *par)
{
    int ix;

    for (npar = 0; par[npar].name != NULL; npar++)
	if (par[npar].tp->ctor != NULL)
	    par[npar].tp->ctor(&par[npar]);

    assert (assignment_count == NULL);
    assignment_count = smalloc(npar * sizeof (*assignment_count));
    
    for (ix = 0; ix < npar; ix++)
	assignment_count[ix] = 0;
}

/* Check that each parameter is assigned at least as many times
   as the struct parameter says.  Assign the default value if it
   is not assigned at all. */
static void
assign_defaults(const struct parameter *par,
		int *err)
{
    int ix;

    for (ix = 0; ix < npar; ix++)
    {
	if (assignment_count[ix] < par[ix].min_assignments)
	{
	    kom_log ("Parameter %s only assigned %d times (%d times needed)\n",
                     par[ix].name, assignment_count[ix], par[ix].min_assignments);
	    (*err)++;
	}
	else if (assignment_count[ix] == 0)
	{
	    if ((*par[ix].tp->assigner)(par[ix].default_val, &par[ix]) != OK)
	    {
		kom_log ("default assigner failed for %s\n", par[ix].name);
		(*err)++;
	    }
	}
    }
}

/* Perform the assignments on one line. 
   Returns EOF on end-of-file, 0 otherwise. 
   A log message is printed, and (*err) is incremented,
   if errors are detected. */
static int
configure_line(FILE *fp,
	       const struct parameter *par,
	       int *err)
{
    char line[MAXLINE];
    char *start;
    char *val;
    char *end;
    int   found;
    int   ix;

    if (fgets(line, MAXLINE, fp) == NULL)
	return EOF;

    /* Separate the line into its two parts.  Set up ``start'' to point 
       to the beginning of the variable name, ``val'' to point to the
       beginning of the value, and remove the trailing newline (using
       ``end''). */
    for (start = line; *start == ' ' || *start == '\t'; start++)
	;
    switch (*start)
    {
    case '#':			/* Comment line */
	return 0;
    case '\0':			/* Too long */
	kom_log ("line too long (max %d chars allowed): %s\n", MAXLINE-1, line);
	(*err)++;
	return 0;
    case '\n':			/* Empty line */
	return 0;
    }
    
    val = strchr(line, ':');
    if (val == NULL)
    {
	/* Remove any trailing newline to avoid a blank line in the log. */
	end = strchr(line, '\n');
	if (end != NULL)
	    *end = '\0';

	kom_log("missing colon: %s\n", line);
	(*err)++;
	return 0;
    }
    *val = '\0';		/* NUL-terminate key */
    for (val++; *val == ' ' || *val == '\t'; val++)
	;
    switch (*val)
    {
    case '\0':
	kom_log ("line too long (max %d chars allowed): %s\n", MAXLINE-1, line);
	(*err)++;
	return 0;
    }

    end = strchr(val, '\n');
    if (end == NULL)
    {
	kom_log ("line too long (max %d chars allowed): %s\n", MAXLINE-1, line);
	(*err)++;
	return 0;
    }	
    *end = '\0';

    /* Set the variable which match this line. */
    found = 0;
    for (ix = 0; ix < npar; ix++)
    {
	if (!strcmp(start, par[ix].name))
	{
	    found++;
	    if (assignment_count[ix] >= par[ix].max_assignments
		&& par[ix].max_assignments != -1)
	    {
		(*err)++;
		kom_log ("variable already assigned %d times: %s\n", 
		     assignment_count[ix], line);
	    }
	    else
	    {
		assignment_count[ix]++;
		if ((*par[ix].tp->assigner)(val, &par[ix]) != OK)
		{
		    kom_log ("assigner for %s failed\n", par[ix].name);
		    (*err)++;
		}
	    }
	}
    }

    if (found != 1)
    {
	kom_log ("line matches %d times: %s\n", found, line);
	(*err)++;
	return 0;
    }
    return 0;
}

Success
read_config(const char *config_file, 
	    const struct parameter *par)
{
    FILE *fp;
    int errs = 0;

    init_init(par);

    fp = i_fopen(config_file, "r");
    if (fp == NULL)
    {
	free_config(par);
	sfree(assignment_count);
	assignment_count = 0;
	restart_kom("cannot open config file %s\n", config_file);
    }

    while (configure_line(fp, par, &errs) != EOF)
	;
    i_fclose(fp);
    assign_defaults(par, &errs);

    sfree(assignment_count);
    assignment_count = 0;

    if (errs)
	return FAILURE;
    else
	return OK;
}

void
free_config(const struct parameter *par)
{
    int i;

    for (i = 0; par[i].name != NULL; ++i)
        if (par[i].tp->dtor != NULL)
            (*par[i].tp->dtor)(&par[i]);
}


static Success
check_trailer(const char *val,
	      const char *end)
{
    if (end == val)
	return FAILURE;
    while (*end == ' ' || *end == '\t') /* Ignore trailing whitespace. */
	end++;
    if (*end != '\0')
	return FAILURE;
    return OK;
}


static Success
convert_to_long(const char *val,
		long *res)
{
    char *end;

    *res = strtol(val, &end, 10);
    return check_trailer(val, end);
}


static Success
convert_to_ulong(const char *val,
		 unsigned long *res)
{
    char *end;

    *res = strtoul(val, &end, 10);
    return check_trailer(val, end);
}


static Success
assign_text_no(const char *val, 
	       const struct parameter *par)
{
    if (val != NULL)
    {
	unsigned long res;

	if (convert_to_ulong(val, &res) != OK)
	    return FAILURE;
	
	*(Text_no*)par->value = res;

	/* Check for overflow. */
	if (*(Text_no*)par->value != res)
	    return FAILURE;
    }
    return OK;
}

static Success
assign_conf_no(const char *val, 
	       const struct parameter *par)
{
    if (val != NULL)
    {
	long res;

	if (convert_to_long(val, &res) != OK)
	    return FAILURE;
	
	*(Conf_no*)par->value = res;
	
	/* Check for overflow. */
	if (*(Conf_no*)par->value != res)
	    return FAILURE;
    }
    return OK;
}

static Success
assign_int(const char *val, 
	   const struct parameter *par)
{
    if (val != NULL)
    {
	long res;

	if (convert_to_long(val, &res) != OK)
	    return FAILURE;
	
	*(int*)par->value = res;
	
	/* Check for overflow. */
	if (*(int*)par->value != res)
	    return FAILURE;
    }
    return OK;
}

static Success
assign_ulong(const char *val,
             const struct parameter *par)
{
    if (val != NULL)
    {
	unsigned long res;

	if (convert_to_ulong(val, &res) != OK)
	    return FAILURE;

	*(unsigned long*)par->value = res;

	/* No need to check for overflow. */
    }
    return OK;
}

static Success
assign_uint(const char *val,
	    const struct parameter *par)
{
    if (val != NULL)
    {
	unsigned long res;

	if (convert_to_ulong(val, &res) != OK)
	    return FAILURE;

	*(unsigned int*)par->value = res;

	/* Check for overflow. */
	if (*(unsigned int*)par->value != res)
	    return FAILURE;
    }
    return OK;
}

static void
ctor_string(const struct parameter *par)
{
    *(char**)par->value = NULL;
}

static void
dtor_string(const struct parameter *par)
{
    if (*(char**)par->value != NULL)
    {
	sfree(*(char**)par->value);
	*(char**)par->value = NULL;
    }
}

static Success
assign_string(const char *val, 
	      const struct parameter *par)
{
    if (*(char**)par->value != NULL)
	sfree(*(char**)par->value);

    if (val == NULL)
	*(char**)par->value = NULL;
    else
    {
	*(char**)par->value = smalloc(strlen(val) + 1);
	strcpy(*(char**)par->value, val);
    }
    return OK;
}

static Success
assign_bool(const char *val, 
	    const struct parameter *par)
{
    if (val == NULL 
	||!strcmp(val, "false") 
	|| !strcmp(val, "off")
	|| !strcmp(val, "no")
	|| !strcmp(val, "0"))
    {
	*(Bool*)par->value = FALSE;
    }
    else if (!strcmp(val, "true") 
	|| !strcmp(val, "on")
	|| !strcmp(val, "yes")
	|| !strcmp(val, "1"))
    {
	*(Bool*)par->value = TRUE;
    }
    else
    {
	return FAILURE;
    }

    return OK;
}


static Success
assign_double(const char *val, 
	      const struct parameter *par)
{
    char *tail = NULL;

    if (val != NULL)
    {
	errno = 0;
	*(double*)par->value = strtod(val, &tail);
	if (errno || tail == val || *tail != '\0')
	    return FAILURE;
    }
    return OK;
}

struct suffix_conversion {
    const char *suffix;
    double factor;
};

static const struct suffix_conversion suffix_table[] = {
    {"seconds",  1.0},
    {"second",   1.0},
    {"sec",      1.0},
    {"s",        1.0},

    {"minutes", 60.0},
    {"minute",  60.0},
    {"min",     60.0},

    {"hours", 3600.0},
    {"hour",  3600.0},
    {"h",     3600.0},

    {"days", 86400.0},
    {"day",  86400.0},
    {"d",    86400.0},

    {"milliseconds", 0.001},
    {"millisecond",  0.001},
    {"milli",        0.001},
    {"ms",           0.001},
    
    {"microseconds", 0.000001},
    {"microsecond",  0.000001},
    {"micro",        0.000001},
    {"us",           0.000001},
    {"u",            0.000001},
    
    {"kiloseconds",     1000.0},
    {"megaseconds",  1000000.0},

    {"microfortnights", 1.2096},
    {"microfortnight",  1.2096},
    
    {NULL, 0.0},
};

static Success
assign_timeval(const char *val, 
	       const struct parameter *par)
{
    char *tail = NULL;
    double number;
    double err;
    const char *suffix;
    const struct suffix_conversion *s;
    struct timeval tv;

    assert(par->default_suffix != NULL);

    if (val != NULL)
    {
	errno = 0;
	number = strtod(val, &tail);
	if (errno || tail == val)
	    return FAILURE;

	if (*tail != '\0')
	    suffix = tail;
	else
	    suffix = par->default_suffix;

	while (isspace((int)(unsigned char)*suffix))
	    ++suffix;

	for (s = suffix_table; s->suffix != NULL; ++s)
	    if (!strcmp(s->suffix, suffix))
		break;

	if (s->suffix == NULL)
	{
	    kom_log("Bad suffix for parameter %s\n", par->name);
	    return FAILURE;
	}

	number *= s->factor;

	if (number < 0)
	{
	    kom_log("Negative values not supported for parameter %s\n",
		    par->name);
	    return FAILURE;
	}

	tv.tv_sec = number;
	tv.tv_usec = (number - (time_t)number) * 1000000 + 0.5;
	err = (tv.tv_sec + 1.0e-6 * tv.tv_usec) - number;
	if (err < 0)
	    err = -err;
	if ((err > 1.0e-6 && err > 1.0e-6 * number)
	    || tv.tv_sec < 0
	    || tv.tv_usec < 0
	    || tv.tv_usec >= 1000000)
	{
	    kom_log("Overflow for parameter %s (%s)\n", par->name, val);
	    return FAILURE;
	}
	*(struct timeval*)par->value = tv;
    }
    return OK;
}


static void
ctor_ipport_list(const struct parameter *par)
{
    struct ipport_list *val = par->value;
    val->size = 0;
    val->entries = NULL;
}

static void
dtor_ipport_list(const struct parameter *par)
{
    int i;

    struct ipport_list *val = par->value;
    for (i = 0; i < val->size; i++)
    {
	sfree(val->entries[i].ipaddr);
	sfree(val->entries[i].port);
    }
    sfree(val->entries);
    val->entries = NULL;
    val->size = 0;
}


static Success
assign_ipport_list(const char *val, 
		   const struct parameter *par)
{
    struct ipport_list *list = par->value;
    struct ipport_entry *entry;
    const char *host_start = NULL;
    const char *host_end = NULL;
    const char *port_start = val;

    if (val[0] == '[')
    {
	/* IPv6 address. */
#ifndef USE_INET6
	kom_log("IPv6 address found in \"Listen:\" directive, but"
		"IPv6 support not enabled in this build.\n");
	return FAILURE;
#else
	host_start = val + 1;
	host_end = strchr(val, ']');
	if (host_end == NULL)
	{
	    kom_log("Unterminated IPv6 address: %s\n", val);
	    return FAILURE;
	}
	if (*(host_end+1) != ':')
	{
	    kom_log("IPv6 address not followed by colon and port: %s\n", val);
	    return FAILURE;
	}
	port_start = host_end + 2;
#endif
    }
    else if ((host_end = strchr(val, ':')) != NULL)
    {
	/* IPv4 address or DNS name. */
	host_start = val;
	port_start = host_end + 1;
    }

    list->entries = srealloc(list->entries,
			     (list->size + 1) * sizeof(*list->entries));
    entry = &list->entries[list->size];
    list->size++;
    if (host_start == NULL)
	entry->ipaddr = NULL;
    else
    {
	entry->ipaddr = smalloc(host_end - host_start + 1);
	memcpy(entry->ipaddr, host_start, host_end - host_start);
	entry->ipaddr[host_end - host_start] = '\0';
    }

    if (*port_start == '\0')
    {
	entry->port = smalloc(strlen(par->default_suffix) + 1);
	strcpy(entry->port, par->default_suffix);
    }
    else
    {
	entry->port = smalloc(strlen(port_start) + 1);
	strcpy(entry->port, port_start);
    }

    return OK;
}


const struct datatype cf_text_no = {
    assign_text_no, NULL, NULL,
};

const struct datatype cf_conf_no = {
    assign_conf_no, NULL, NULL,
};

const struct datatype cf_int = {
    assign_int, NULL, NULL,
};

const struct datatype cf_ulong = {
    assign_ulong, NULL, NULL,
};

const struct datatype cf_uint = {
    assign_uint, NULL, NULL,
};

const struct datatype cf_string = {
    assign_string, ctor_string, dtor_string,
};

const struct datatype cf_bool = {
    assign_bool, NULL, NULL,
};

const struct datatype cf_double = {
    assign_double, NULL, NULL,
};

const struct datatype cf_timeval = {
    assign_timeval, NULL, NULL,
};

const struct datatype cf_ipport_list = {
    assign_ipport_list, ctor_ipport_list, dtor_ipport_list,
};
