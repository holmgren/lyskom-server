/*
 * $Id: kom-types.h,v 0.64 2003/08/29 10:39:48 ceder Exp $
 * Copyright (C) 1990-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 *  KOM-types.h  --  Types used by both server and client of LysKOM
 *
 *
 *  Copyright (C) 1990-1999, 2001-2003	Lysator Computer Club,
 *			Linkoping University,  Sweden
 *
 *  Everyone is granted permission to copy, modify and redistribute
 *  this code, provided the people they give it to can.
 *
 *
 *  Filecreator: Thomas Bellman
 *		  Lysator Computer Club
 *		  Linkoping University
 *		  Sweden
 *
 *  email:	  Bellman@Lysator.LiU.SE
 */



#ifndef  KOM_TYPES_H_ALREADY_INCLUDED
#define  KOM_TYPES_H_ALREADY_INCLUDED


#include <sys/types.h>
#include <time.h>
#include "s-string.h"


/* Some objects in LysKOM need a type field.
   These are the valid values */

enum object_type
{
    NO_OBJECT_TYPE,		/* No object at all             */
    TEXT_OBJECT_TYPE,		/* Text object                  */
    CONF_OBJECT_TYPE,		/* Conference object            */
    INFO_OBJECT_TYPE,		/* System info object           */
};




typedef unsigned short Pers_no;
typedef unsigned short Conf_no;
typedef unsigned long  Text_no;
typedef unsigned long  Local_text_no;
typedef unsigned long  Session_no;


#define	MAX_PERS_NO		((Pers_no) USHRT_MAX)
#define MAX_CONF_NO		((Conf_no) USHRT_MAX)
#define MAX_TEXT_NO		((Text_no) ULONG_MAX)
#define	MAX_LOCAL_TEXT_NO	((Local_text_no) ULONG_MAX)


#define PASSWD_LEN 64
typedef char   Password[PASSWD_LEN];
typedef off_t  Text_index;
typedef time_t Time;


/*
 * The privilige bits:  says what priviliges a person has.  E g
 *  if he is allowed to read texts he normally shouldn't be allowed
 *  to read. See file doc/Protocol-A.texi.
 */
typedef	struct {
	unsigned int	wheel	: 1;
	unsigned int	admin	: 1;
	unsigned int	statistic : 1;
	unsigned int	create_pers: 1;
	unsigned int	create_conf: 1;
	unsigned int	change_name: 1;
	unsigned int	flg7    : 1;	/* For future use. */
        unsigned int	flg8	: 1;
	unsigned int	flg9    : 1;
	unsigned int	flg10   : 1;
	unsigned int	flg11   : 1;
	unsigned int	flg12	: 1;
	unsigned int	flg13	: 1;
	unsigned int	flg14	: 1;
	unsigned int	flg15	: 1;
	unsigned int	flg16	: 1;
} Priv_bits;

/*
 * Flags in the Person struct.
 */
typedef struct {
    unsigned int  unread_is_secret : 1;/* FALSE if everyone is allowed to ask
					  how many unread texts you have. */
    unsigned int  flg2	: 1;
    unsigned int  flg3	: 1;
    unsigned int  flg4	: 1;
    unsigned int  flg5	: 1;
    unsigned int  flg6	: 1;
    unsigned int  flg7	: 1;
    unsigned int  flg8	: 1;
} Personal_flags;


/* See file doc/Protocol-A.texi */
enum info_type {
    recpt     = 0,	/* 0 Recipient (conference) */
    cc_recpt  = 1,	/* 1 Carbon Copy recipient (extra kopia) */
    comm_to   = 2,	/* 2 Comment to (text) */
    comm_in   = 3,	/* 3 Commented in (text) */
    footn_to  = 4,	/* 4 This is a footnote to (text) */
    footn_in  = 5,	/* 5 Footnote to this text in (text) */
    loc_no    = 6,	/* 6 Sequence number within conference */
    rec_time  = 7,	/* 7 Received at (time) */
    sent_by   = 8,	/* 8 Sent by (person) */
    sent_at   = 9,	/* 9 Sent at (time) */
    bcc_recpt = 15,	/* 15 BCC recipient (for kannedom) */

    unknown_info = 4711 /* Internal use only. Unknown misc item encountered */
};

union info_datum {
    Conf_no       recipient;    /* recpt, cc_recpt, bcc_recpt */
    Text_no	  text_link;    /* comm_to, comm_in, footn_to, footn_in */
    Local_text_no local_no;	/* loc_no */
    Time	  received_at;  /* rec_time */
    Pers_no	  sender;	/* sent_by */
    Time	  sent_at;	/* sent_at */
    enum info_type unknown_type; /* unknown_info */
};


/*  This struct contains miscellaneous information about a text.  */
typedef	struct {
	enum  info_type	 type;
	union info_datum datum;
} Misc_info;


typedef struct {
    unsigned short      no_of_misc;
    Misc_info         * misc;
} Misc_info_list;


typedef struct {
    unsigned int        deleted : 1; /* This item has been deleted */
    unsigned int        inherit : 1; /* Copy to comments */
    unsigned int        secret : 1; /* Don't show anyone */
    unsigned int        hide_creator : 1; /* Don't show the creator */
    unsigned int        dont_garb : 1; /* Don't garb object */
    unsigned int        reserved3 : 1;
    unsigned int        reserved4 : 1;
    unsigned int        reserved5 : 1;
} Aux_item_flags;
   

typedef struct {
    enum object_type target_type;
    unsigned long    target_item;
    union {
        Conf_no     conf;
        Text_no     text;
    } target_object;
} Aux_item_link;


typedef struct {
    unsigned long       aux_no;
    Pers_no             creator;
    Time                sent_at;
    Aux_item_flags      flags;
    unsigned long       inherit_limit;
    unsigned long       tag;
    String              data;
    Aux_item_link       linked_item;
} Aux_item;

typedef struct {
    unsigned short      length;
    Aux_item          * items;
} Aux_item_list;


/*  Fields of this type is supposed to tell the garbage collector
 *  which texts it should remove first.
 */
typedef unsigned long	Garb_nice;


/*  Struct for text status  */
typedef struct {
	Time		  creation_time;
        long              file_pos; /* Start of the text in the text file. */
	Pers_no		  author;
	unsigned short	  no_of_lines;
	String_size	  no_of_chars;
	unsigned short	  no_of_marks;	/* Including syntetic marks. */
	unsigned short	  no_of_misc;	/* Recipients, times, comments, ... */
        unsigned long     highest_aux;
	Misc_info	* misc_items;	/* List of miscellaneous info. */
        Aux_item_list     aux_item_list;
} Text_stat;


typedef	struct {
	unsigned int	rd_prot	: 1;	/* !(Can anyone become a member?) */
	unsigned int	original : 1;	/* Comments forbidden? */
	unsigned int	secret : 1;	/* Secret conference? */
				/* Note: if a letterbox is secret it
				 * will be very hard for that person
				 * to log in, since he can't map his
				 * name to a pers_no. He must either
				 * know his pers_no, or have another
				 * identity which is his supervisor.
				 */
	unsigned int	letter_box : 1;	/* Is this a letter box? */
	unsigned int    allow_anon : 1; /* do we accept anonymous messages */
        unsigned int	forbid_secret : 1; /* do we allow secret members */
	unsigned int	reserved2 : 1;
	unsigned int	reserved3 : 1;
} Conf_type;


typedef struct {
    unsigned int invitation : 1;
    unsigned int passive : 1;
    unsigned int secret : 1;
    unsigned int passive_message_invert : 1;
    unsigned int reserved2 : 1;
    unsigned int reserved3 : 1;
    unsigned int reserved4 : 1;
    unsigned int reserved5 : 1;
} Membership_type;


typedef struct {
    Pers_no		member;
    Pers_no             added_by; 
    Time                added_at;
   Membership_type     type;
} Member;


/* Struct for marks */
typedef struct {
	Text_no		text_no;
	unsigned char	mark_type;	/* It's up to the clients to decide the
					   meaning of this field. */
} Mark;



/*  Information about a person's membership in a conference  */

struct read_range {
    Local_text_no  first_read;
    Local_text_no  last_read;
};

struct read_range_list {
    unsigned short     length;
    struct read_range *ranges;
};

typedef struct {
    unsigned long   position;
    Conf_no		conf_no;
    unsigned char	priority;	/* Interrupt priority */
    Time		last_time_read;	/* Updated every time a text in this
					   conference is marked as read.  */
    unsigned int	no_of_read_ranges;
    struct read_range * read_ranges;    /* Sorted in ascending order. */
    Pers_no             added_by;
    Time                added_at;
    Membership_type type;

    /* read_ranges is cleared before it reaches the Protocol A output
       layer in certain situations, even though that layer wants to
       emit the last-text-read element.  This happens for example when
       get-membership [99] is called with want-read-texts == 0.  This
       variable maintains the last-text-read value. */
    Bool skip_read_texts;

} Membership;


/* Some structs to handle variable-sized arrays. */

typedef struct {
	unsigned short	  no_of_marks;
	Mark	* marks;		/* Pointer to an array of marks. */
} Mark_list;


typedef struct {
    unsigned short no_of_confs;
    Conf_no  *conf_nos;
} Conf_no_list;


typedef struct {
	unsigned short	  no_of_members;
	Member	* members;
} Member_list;


/*  A list of conference numbers, also telling if it is a
 *  mailbox (i e a person) or an ordinary conference.	*/
/* Delete this ugly variant as soon as possible! */
typedef struct {
	unsigned long		  no_of_conf_nos;
	Conf_no		* conf_nos;
	Conf_type	* type_of_conf;
} Conf_list_old;
/* This is the way it should look! */
typedef struct {
	Conf_no   conf_no;
	Conf_type type;
} Micro_conf;

typedef struct {
	unsigned long       no_of_confs;
	Micro_conf * confs;
} Conf_list;


/*  A list of person numbers  */
typedef struct {
	unsigned long		  no_of_persons;
	Pers_no		* persons;
} Pers_list;

typedef struct {
	unsigned short	      no_of_confs;
	Membership  * confs;
} Membership_list;

typedef struct {
	int		       length;
        long                  *data;
} Number_list;


/* The Info struct */
typedef struct {
	long		version;
	Conf_no		conf_pres_conf; /* Presentation of new confs */
	Conf_no		pers_pres_conf;	/* Presentation of new persons */
	Conf_no		motd_conf; 	/* Conf that receive motds */
	Conf_no		kom_news_conf;  /* News about kom */
	Text_no		motd_of_lyskom; /* To be displayed after login */
        unsigned long   highest_aux_no; /* Last aux-item number */
        Aux_item_list   aux_item_list;  /* System aux items */
	/* and maybe more... */
} Info;
 
struct l2g_block_info;

/* Nothing outside local-to-global.c should access the contents of a
   Local_to_global directly.  Use the accessor functions declared in
   local-to-global.h instead.  */
typedef struct {
    int num_blocks;
    Local_text_no first_unused;
    struct l2g_block_info * blocks;
} Local_to_global;

typedef struct {
    const Local_to_global *l2g;
    const struct l2g_block_info *binfo;
    int                arrindex;
    Local_text_no      beginval;       /* First index in the search */
    Local_text_no      endval;	       /* Last index in the search + 1 */

    int                search_ended;   /* 1 if finished, 0 otherwise */
    Local_text_no      lno;	       /* The Local_text_no to use */
    Text_no            tno;	       /* The Text_no to use */
} L2g_iterator;

typedef struct {
    const Local_to_global *l2g;
    const struct l2g_block_info *binfo;
    int                arrindex;
    Local_text_no      beginval;       /* First index in the search */
    Local_text_no      endval;	       /* Last index in the search + 1 */

    int                search_ended;   /* 1 if finished, 0 otherwise */
    Local_text_no      lno;	       /* The Local_text_no to use */
    Text_no            tno;	       /* The Text_no to use */
} L2g_reverse_iterator;

/* This struct holds the information that is needed to output a
   Text-Mapping.  Please note that the protocol A Text-Mapping data
   type looks very different from this structure.  */
typedef struct {
    Local_text_no          first;
    unsigned long          no_of_texts;
    const Local_to_global *l2g;
} Text_mapping;

/* This struct holds the information that is needed to output a
   Text-Mapping in the reverse order.  Please note that the protocol A
   Text-Mapping data type looks very different from this
   structure.  */
typedef struct {
    Local_text_no          ceiling;
    unsigned long          no_of_texts;
    const Local_to_global *l2g;
} Text_mapping_reverse;

/* For performance reasons, sort the fields in increasing size.
  The comments about number of bits are true on a Sun Sparc-2, not
  necessarily anything else.  The code should work anyhow. */
typedef	struct {
        /* 8-bit quantities */
	Conf_type	type;		/* secret, rd_prot etc */

	/* 16-bit quantities */
	Pers_no		creator;
	Conf_no		supervisor;	/* Supervisor conference. */
	Conf_no		permitted_submitters;	/* People who are allowed
					   to submit texts to this conf. 
					   0 -> anyone may submit. */
	Conf_no		super_conf;	/* Send unallowed submissions to
					   the super_conf. 
					   0 -> unallowed submissions bounce */

	/* 32-bit quantities */
	Time		creation_time;
	Text_no		presentation;
	Time		last_written;	/* Time of latest text in this conf. */
	Text_no		msg_of_day;	/* Message to be displayed when this
					   conf is referenced by the user. */
	Garb_nice	nice;		/* How long do texts in this
					   conf live? */
        Garb_nice       keep_commented; /* New comments protect texts */
        Garb_nice       expire;         /* When do we auto-kill this */
	String		name;		/* Name of conference */
        unsigned long   highest_aux;
        Aux_item_list   aux_item_list;
	Member_list	members;	/* List of members in conf */
	Local_to_global texts;		/* List of texts */
} Conference;



typedef struct {
	String		name;		/* Name of conference */
	Conf_type	type;		/* secret, rd_prot etc */
        Conf_no         supervisor;     /* Supervisor conference */
	Local_text_no	highest_local_no; /* highest local text no */
	Garb_nice	nice;	        /* Number of days to live */
	Garb_nice	keep_commented; /* New comments protect texts */
} Small_conf;

typedef struct {
	String      name;
	Conf_type   type;
	Conf_no     conf_no;
} Conf_z_info;

typedef struct {
	unsigned short    no_of_confs;
	Conf_z_info     * confs;
} Conf_z_info_list;

/*  Struct for persons  */
typedef struct {
	Text_no		user_area;	/* Misc info the clients might want to
					   store. 0 = not used. */
	Priv_bits	privileges;
	Personal_flags	flags;
	Time		last_login; 	/* Or logout */
	unsigned long	total_time_present; /* Number of seconds. */
	unsigned long	sessions;	/* Number of sessions */
	unsigned long	created_lines;	/* No. of created lines (statistics) */
	unsigned long	created_bytes;	/* No. of created bytes (statistics) */
	unsigned long	read_texts;	/* No. of read texts (statistics) */
	unsigned long	no_of_text_fetches; /* (statistics) */
	unsigned short	created_persons; /* (statistics) */
	unsigned short	created_confs;	/* (statistics) */
	String          username;	/* User-name & hostname */
	Local_to_global created_texts;
	Mark_list	marks;		/* List of marked texts */
	Membership_list conferences;	/* List of conferences the person is
					 * a member in. */
	Password	pwd;		/* Encrypted password */
} Person;


/*
 * This struct is returned from the 'who_is_on' call.
 */
typedef struct {
	Pers_no		person;
	String		what_am_i_doing;
	Conf_no		working_conference;
} Who_info_old;

typedef struct {
	Pers_no		person;
	String		what_am_i_doing;
	String		username;           /* Userid and hostname. */
	Conf_no		working_conference;
	Session_no	session_no;         /* Serial number of connection. */
} Who_info;

typedef struct {
	Pers_no		person;
	String		what_am_i_doing;
	String		username;           /* Userid and hostname. */
	String		ident_user;	     /* According to Ident protocol. */
	String		hostname; 	     /* According to TCP/IP. */
	Conf_no		working_conference;
	Session_no	session_no;         /* Serial number of connection. */
} Who_info_ident;

typedef struct {
	Pers_no		person;
	String		what_am_i_doing;
	String		username;            /* Userid and hostname. */
	Conf_no		working_conference;
	Session_no	session;             /* Serial number of connection. */
	Time		connection_time;     /* Not logintime. */
	unsigned long		idle_time; 	     /* Seconds. */
} Session_info;

typedef struct {
    unsigned int  invisible : 1;
    unsigned int  user_active_used : 1;
    unsigned int  user_absent : 1;
    unsigned int  reserved3 : 1;
    unsigned int  reserved4 : 1;
    unsigned int  reserved5 : 1;
    unsigned int  reserved6 : 1;
    unsigned int  reserved7 : 1;
} Session_flags;
    
typedef struct {
	Session_no     session;
	Pers_no        person;
	Conf_no        working_conference;
	unsigned long  idle_time;
	Session_flags  flags;
	String         what_am_i_doing;
} Dynamic_session_info;

typedef struct {
	long		          no_of_sessions;
    	Dynamic_session_info	* sessions;
} Dynamic_session_info_list;	

typedef struct {
	String  username;
	String  hostname;
	String  ident_user;
	Time    connection_time;
} Static_session_info;

typedef struct {
	Pers_no		person;
	String		what_am_i_doing;
	String		username;            /* Userid and hostname,
						according to the client. */
	String		ident_user;	     /* According to Ident protocol. */
	String		hostname; 	     /* According to TCP/IP. */
	Conf_no		working_conference;
	Session_no	session;             /* Serial number of connection. */
	Time		connection_time;     /* Not logintime. */
	unsigned long   idle_time; 	     /* Seconds. */
} Session_info_ident;

typedef struct {
	unsigned short priority;
    	unsigned short weight;
} Scheduling_info;

typedef struct {
	int		  no_of_persons;
    	Who_info_old	* info;
} Who_info_list_old;	


typedef struct {
	int		  no_of_persons;
    	Who_info	* info;
} Who_info_list;	


typedef struct {
	int		  no_of_persons;
    	Who_info_ident	* info;
} Who_info_ident_list;

typedef struct {
        unsigned long protocol_version;
        String        server_name;
        String        server_version;
} Version_info;

typedef struct {
    float   average;
    float   ascent_rate;
    float   descent_rate;
} Stats;

typedef struct {
    int    no_of_stats;
    Stats *stats;
} Stats_list;

typedef struct {
    int     no_of_stats;
    String *stat_names;
    Number_list intervals;
} Stats_description;

typedef struct {
    time_t  boot_time;
    time_t  save_time;
    String  db_status;
    Text_no existing_texts;
    Text_no highest_text_no;
    Conf_no existing_confs;
    Conf_no existing_persons;
    Conf_no highest_conf_no;
} Static_server_info;

#ifdef DEBUG_CALLS
typedef struct {
    int arena;
    int ordblks;
    int smblks;
    int hblks;
    int hblkhd;
    int usmblks;
    int fsmblks;
    int uordblks;
    int fordblks;
    int keepcost;
} Memory_info;
#endif

typedef struct {
        unsigned long protocol_version;
        const char *server_name;
        const char *server_version;
} Version_info_internal;

#endif	/*  ifndef  KOM_TYPES_H_ALREADY_INCLUDED__  */
