/*
 * $Id: smalloc.h,v 0.13 2003/08/23 16:38:21 ceder Exp $
 * Copyright (C) 1991-1992, 1994-1995, 1998-1999, 2002-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

/*
 * Wrappers around malloc()/realloc()/free() that never returns NULL.
 */

/*
 * "safe" malloc. Handles the case when malloc returns NULL.  smalloc
 * will never return on failure.  Will always return a distinct
 * pointer, even when size is 0.  The memory allocated memory must be
 * returned to sfree() or srealloc(), not to free()
 */
extern void *
smalloc(size_t size);


extern void
sfree(void * ptr);	/* it is legal to sfree a NULL pointer */

extern void *
srealloc(void * ptr, size_t size); /* Never fails. It is legal to
				      realloc the NULL ptr. */

/*
 * Allocate temporary memory, which is automatically freed after this
 * atomic call.
 */
void *
tmp_alloc(unsigned long size);

/*
 * Free all core which is allocated with tmp_alloc(). This is called from
 * end_of_atomic().
 */
void
free_tmp(void);

/*
 * Free all memory used internally in tmp_alloc().
 */
void
free_all_tmp(void);


/*
 * Write statistics of memory usage.
 */

void
dump_smalloc_counts(FILE *stat_file);
