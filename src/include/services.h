/*
 * $Id: services.h,v 0.71 2003/08/25 17:14:06 ceder Exp $
 * Copyright (C) 1991-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 *  services.h  --  All the services the LysKOM server makes available
 *		    for clients.
 *
 *  These functions match the requests defined in doc/Protocol-A.texi.
 *  See the descriptions in that file for documentation.
 *
 *  Created by ceder 1990-03-23
 */


#ifndef  SERVICES_H_ALREADY_INCLUDED
#define  SERVICES_H_ALREADY_INCLUDED



/*
 * Session control
 */

extern  Success
login_old (Pers_no	person,
	   const String	passwd);

extern  Success
login (Pers_no		person,
       const String	passwd,
       Bool		invisible);

extern  Success
logout (void);	/* can never fail */

/* Change Conference */
extern  Success
change_conference (Conf_no conference);


/* Change name of a person or conference. */
extern  Success
change_name (Conf_no	  conf_no,
	     const String new_name);

extern  Success
change_what_i_am_doing (String  what_am_i_doing);

/* Client version: one call to set, two to retrieve. */
extern  Success
set_client_version (const String client_name,
		    const String client_version);

extern  Success
get_client_name (Session_no session_no,
		 String *result);

extern  Success
get_client_version (Session_no session_no,
		    String *result);

extern  Success
set_scheduling(Session_no session_no,
	       unsigned short priority,
	       unsigned short weight);

/* Get info about a session */

extern  Success
get_session_info  (Session_no session_no,
		   Session_info *result);

extern  Success
get_session_info_ident  (Session_no session_no,
			 Session_info_ident *result);

extern  Success
get_static_session_info (Session_no session_no,
			 Static_session_info *result);

extern  Success
get_scheduling(Session_no session_no,
	       Scheduling_info *result);

/*
 * Disconnect a session. You can disconnect your own session (even if
 * you are not logged in) and any session where you are supervisor of
 * the user that is logged in on that session.
 */
extern  Success
disconnect (Session_no session_no);

/*
 * Returns your session number
 */
extern  Success
who_am_i (Session_no *session_no);

/*
 * Set state in the session
 */

extern  Success
user_active (void);

extern  Success
set_connection_time_format(int use_utc);
    


/******************************
*     Person-related calls    *
******************************/

/*
 *  Create a new person.  Returns 0 if any error occured.
 */
extern  Pers_no
create_person_old (const String  name,
		   const String  passwd);

extern Pers_no
create_person (const String name,
	       const String passwd,
	       Personal_flags flags,
	       Aux_item_list *conf_aux);


extern Success
set_pers_flags(Pers_no pers_no,
               Personal_flags flags);

/* Obsolete call; use get_person_stat instead. */
extern  Success
get_person_stat_old (Pers_no		  person,
		     int		  mask,
		     Person		* result);

extern  Success
get_person_stat (Pers_no	  person,
		 Person		* result);

extern  Success
get_created_texts (Pers_no		  person,
		   Local_text_no	  first,
		   unsigned long	  no_of_texts,
		   L2g_iterator 	 *created_texts);

extern  Success
map_created_texts (Pers_no	  person,
		   Local_text_no  first,
		   unsigned long  no_of_texts,
		   Text_mapping  *created_texts);

extern  Success
map_created_texts_reverse(Pers_no	 person,
			  Local_text_no  local_no_ceiling,
			  unsigned long  no_of_texts,
			  Text_mapping_reverse *created_texts);

extern  Success
get_membership_old (Pers_no		  person,
		    unsigned short	  first,
		    unsigned short	  no_of_confs,
		    Bool		  want_read_texts,
		    Membership_list	* memberships);

extern  Success
get_membership_10(Pers_no           person,
		  unsigned short    first,
		  unsigned short    no_of_confs,
		  Bool		    want_read_texts,
		  Membership_list * memberships);

extern  Success
get_membership(Pers_no           person,
	       unsigned short    first,
	       unsigned short    no_of_confs,
	       Bool              want_read_ranges,
	       unsigned long     max_ranges,
	       Membership_list * memberships);

extern  Success
set_priv_bits (Pers_no	 person,
	       Priv_bits privileges);


/*  Set the password of PERSON to NEW_PWD.  OLD_PWD is the password
 *  of the person who does the set.  This is not necessarily the
 *  same as the one who gets it set.	*/
extern  Success
set_passwd (Pers_no	   person,
	    const String   old_pwd,
	    const String   new_pwd);


/* You can query for unread texts without logging in. */
extern  Success
query_read_texts_old (Pers_no	     pers_no,
		      Conf_no	     conf_no,
		      Membership   * result  );

extern  Success
query_read_texts_10(Pers_no      pers_no,
		    Conf_no      conf_no,
		    Membership * result);

extern  Success
query_read_texts(Pers_no        pers_no,
		 Conf_no        conf_no,
		 Bool           want_read_ranges,
		 unsigned long  max_ranges,
		 Membership    *result);



extern Success
get_unread_confs(Pers_no       pers_no,
		 Conf_no_list *result);

extern Success
set_user_area(Pers_no	pers_no,
	      Text_no	user_area);


/****************************************
*	Conference-related calls	*
****************************************/


extern  Conf_no
create_conf_old (const String	  name,
		 Conf_type	  type);

extern Conf_no
create_conf (const String name,
	     Conf_type type,
	     Aux_item_list *aux);

extern Success
modify_conf_info (Conf_no conf_no,
		  Number_list *items_to_delete,
		  Aux_item_list *items_to_add);


/*  Delete a conference.  Also used to delete persons.  */
extern  Success
delete_conf (Conf_no	conf);


/*
 *  Map conference name to number.  Returns a list of the conferences
 *  that match the name NAME. Can be done without logging in.
 * This should be phased out.
 */
extern  Success
lookup_name (const String   name,
	     Conf_list_old *result);

extern Success
lookup_z_name (const String name,
	       int want_persons,
	       int want_confs,
	       Conf_z_info_list *result);

/* Use these two lookup-calls instead of lookup_name */
extern  Success
lookup_person (const String  pattern,
	       Conf_no_list  *result);

extern  Success
lookup_conf (const String  pattern,
	     Conf_no_list  *result);

/* Two functions for matching regexps. */
extern  Success
re_lookup_person (const String  regexp,
		  Conf_no_list  *result);

extern  Success
re_lookup_conf (const String  regexp,
		Conf_no_list  *result);

extern  Success
re_z_lookup (const String      regexp,
	     int               want_persons,
	     int	       want_confs,
	     Conf_z_info_list *result);

extern Success
get_collate_table (String * result);

extern  Success
get_conf_stat_older (Conf_no	 conf_no,
		     int	 mask,
		     Conference *result);

extern  Success
get_conf_stat_old (Conf_no		  conf_no,
		   Conference		* result);

extern  Success
get_conf_stat (Conf_no		  conf_no,
	       Conference	* result);

extern Success
get_uconf_stat (Conf_no		  conf_no,
		Small_conf	* result);

extern  Success
get_members (Conf_no	      conf,
	     unsigned short   first,
	     unsigned short   no_of_members,
	     Member_list    * members);

extern  Success
get_members_old (Conf_no	  conf,
		 unsigned short   first,
		 unsigned short   no_of_members,
		 Member_list	* members	);



/* add_member is also used to change the priority of a conference */
extern  Success
add_member (Conf_no		conf_no,
	    Pers_no		pers_no,
	    unsigned char	priority,
	    unsigned short	where, /* Range of where is [0..] */
	    Membership_type   * type);

extern  Success
add_member_old (Conf_no		conf_no,
		Pers_no		pers_no,
		unsigned char	priority,
		unsigned short	where	); /* Range of where is [0..] */

extern Success
set_membership_type (Pers_no          pers_no,
		     Conf_no          conf_no,
		     Membership_type *type   );


extern  Success
sub_member (Conf_no	conf_no,
	    Pers_no	pers_no);


/*
 * Tell the server that I want to mark/unmark texts as read so that I
 * get no_of_unread unread texts in conf_no.
 *
 * The new alternative function marks last_read as the last read local
 * text. Use this one to avoid race conditions.
 */

extern  Success
set_unread (Conf_no   conf_no,
	    Text_no   no_of_unread);

extern  Success
set_last_read (Conf_no       conf_no,
	       Local_text_no last_read);


/*
 * set_presentation and set_etc_motd also does some magic with the
 * no_of_marks field in the Text_stat structure of the old and new text.
 */ 

extern  Success
set_presentation (Conf_no   conf_no,
		  Text_no   text_no); /* 0 to delete pres. */

extern  Success
set_etc_motd (Conf_no	  conf_no,
	      Text_no	  text_no);


extern  Success
set_supervisor (Conf_no	  conf_no,
		Conf_no	  admin	);

extern  Success
set_permitted_submitters (Conf_no	  conf_no,
			  Conf_no	  perm_sub);

extern  Success
set_super_conf (Conf_no	  conf_no,
		Conf_no	  super_conf);

extern  Success
set_conf_type (Conf_no	  conf_no,
	       Conf_type  type	);
extern  Success
set_garb_nice (Conf_no	  conf_no,
	       Garb_nice  days); /* number of days */

extern  Success
set_expire (Conf_no	  conf_no,
	    Garb_nice	  expire); /* number of days */


extern Success
set_keep_commented(Conf_no	conf_no,
                   Garb_nice	keep_commented);	/* number of days */

extern Success
first_unused_conf_no(Conf_no *result);

/*
 * Return next/previous existing text-no.
 */

extern  Success
find_next_conf_no(Conf_no start, Conf_no *result);

extern  Success
find_previous_conf_no(Conf_no start, Conf_no *result);

/********************************
*     Calls to handle marks     *
********************************/


extern  Success
get_marks (Mark_list *result);



/*
 *  Will fail if the user is not allowed to read the text.
 */
/* This function uses mark_type==0 to delete the mark. */
extern  Success
mark_text_old (Text_no	  	  text,
	       unsigned char	  mark_type);


/* Using this function, you can set mark_type==0. */
extern  Success
mark_text (Text_no	  text,
	   unsigned char  mark_type);


/* Remove a mark, reporting an error if you had not marked the text. */
extern  Success
unmark_text (Text_no	  text);




/*******************************
*     Calls to handle texts    *
*******************************/


extern  Success
get_text (Text_no	  text,
	  String_size	  start_char,
	  String_size	  end_char,
	  String	* result);


extern  Success
get_text_stat (Text_no    text,
	       Text_stat *result);

extern  Success
get_text_stat_old (Text_no    text,
		   Text_stat *result);

extern  Success
mark_as_read(Conf_no		conference,
	     const Number_list *texts);	/* Local_text_no */

extern  Success
mark_as_unread(Conf_no         conference,
	       Local_text_no   lno);


extern  Success
set_read_ranges(Conf_no conference,
		const struct read_range_list *read_ranges);


/* Returns 0 on error */
extern  Text_no
create_text (const String     message,
	     Misc_info_list * misc,
	     Aux_item_list  * aux);

extern Text_no
create_text_old (const String     message,
		 Misc_info_list	* misc	    );

/* Returns 0 on error.  */
extern  Text_no
create_anonymous_text (const String      message,
		       Misc_info_list  * misc,
		       Aux_item_list    *aux);

extern  Text_no
create_anonymous_text_old (const String      message,
			   Misc_info_list  * misc);

extern Success
delete_text( Text_no text_no);

extern Success
modify_text_info(Text_no        text,
		 Number_list   *items_to_delete, 
		 Aux_item_list *aux);


extern  Success
add_recipient (Text_no	      text_no,
	       Conf_no	      conf_no,
	       enum info_type type);

extern  Success
sub_recipient (Text_no	  text_no,
	       Conf_no	  conf_no);


extern  Success
add_comment (Text_no	  comment,
	     Text_no 	  comment_to);


/*
 *  Make the text COMMENT to not be a comment to text COMMENT_TO
 */
extern  Success
sub_comment (Text_no	  comment,
	     Text_no	  comment_to);

extern Success
add_footnote (Text_no	footnote,
	      Text_no 	footnote_to);

extern Success
sub_footnote (Text_no	footnote,
	      Text_no	parent);

extern Success
first_unused_text_no(Text_no *result);

extern  Success
get_map (Conf_no	  conf_no,
	 Local_text_no	  first_local_no,
	 unsigned long	  no_of_texts,
	 L2g_iterator    *result);

extern Success
local_to_global (Conf_no       conf_No,
		 Local_text_no first_local_no,
		 unsigned long no_of_texts,
		 Text_mapping *result);
extern Success
local_to_global_reverse(Conf_no       conf_No,
			Local_text_no local_no_ceiling,
			unsigned long no_of_texts,
			Text_mapping_reverse *result);


/*
 *  Ask what the server thinks the time is. This
 *  might differ if on two different machines.
 */
extern  Success
get_time (time_t *t);


/*
 *  Gets the last text before a given time.
 */

extern  Success
get_last_text (struct tm *clk, Text_no *result);

/*
 * Return next/previous existing text-no.
 */

extern  Success
find_next_text_no (Text_no start, Text_no *result);

extern  Success
find_previous_text_no (Text_no start, Text_no *result);


/*
 * Who is logged on now?
 */
extern  Success
who_is_on_old (Who_info_list_old	* result);

extern  Success
who_is_on (Who_info_list	* result);

extern  Success
who_is_on_ident (Who_info_ident_list *result);

extern  Success
who_is_on_dynamic (int want_visible,
		   int want_invisible,
		   long active_last,
		   Dynamic_session_info_list *result);

/*
 *  Return various information about the server
 */
extern  Success
get_info_old (Info *result);

extern  Success
get_info (Info *result);

extern  Success
get_version_info (Version_info *result);


/*
 * Privileged calls.
 */

extern Success
set_info(Info *info);

extern Success
modify_system_info(Number_list      *items_to_delete,
		   Aux_item_list    *items_to_add);

extern Success
set_motd_of_lyskom (Text_no motd);

/*
 * Set ena_level. 0 means don't use any privileges.
 */
extern Success
enable (unsigned char ena_level);

/*
 * Make LysKOM sync its files.
 */
extern Success
sync_kom (void);

/*
 * Close LysKOM.
 */
extern Success
shutdown_kom (int exit_val);

/*
 * Send a message to all clients. This is obsoleted by send_message(),
 * but will remain for compatibility reasons.
 */
extern Success
broadcast (const String message);

/*
 * Send a message to a person, or all persons. If recipient == 0 all
 * connections will receive the message.
 */
extern Success
send_message (Conf_no recipient,
	      const String message);

/*
 * Selection of asynchronous messages
 */

extern Success
accept_async(Number_list *accept_list);

extern Success
query_async(Number_list *result);

/*
 * Aux-items
 */

extern Success
query_predefined_aux_items(Number_list *result);


/*
 * Measuread statistical numbers.
 */

extern Success
get_stats_description(Stats_description *result);

extern Success
get_stats(const String what,
	  Stats_list *result);

/*
 * Boot-time information.
 */

extern Success
get_boottime_info(Static_server_info *result);


#ifdef DEBUG_CALLS
extern Success
get_memory_info(Memory_info *result);

extern Success
set_marks(Text_no text_no, unsigned long no_of_marks);

extern Success
backdate_text(Text_no text_no,
	      unsigned long seconds);

/* Modify the "sent_at" item.  It must already exist. */
extern Success
backdate_comment_link(Text_no parent,
		      Text_no child,
		      unsigned long seconds);

extern Success
start_garb(void);

extern Success
cache_sync_start(void);

extern Success
cache_sync_finish(void);

extern Success
dump_cfg_timevals(void);

extern Success
server_sleep(int secs);

#endif
    
#endif	/*  _SERVICES_H_ALREADY_INCLUDED__  */
