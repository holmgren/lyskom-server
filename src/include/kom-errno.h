/*
 * $Id: kom-errno.h,v 0.28 2003/08/23 16:38:21 ceder Exp $
 * Copyright (C) 1991-1994, 1996-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * kom_errno.h
 *
 * Created by ceder 1990-04-18
 *
 * The values that kom_errno can take.
 */


#ifndef  KOM_ERRNO_ALREADY_INCLUDED
#define  KOM_ERRNO_ALREADY_INCLUDED

enum kom_err
{
    KOM_NO_ERROR = 0,		/* No error has occured */
    KOM_NOT_IMPL = 2,		/* Not implemented yet */
    KOM_OBSOLETE = 3,		/* No longer implemented */
    KOM_PWD = 4,			/* Wrong/illegal password */
    KOM_LONG_STR = 5,		/* String too long */
    KOM_LOGIN = 6,			/* Not logged in. */
    KOM_LOGIN_DISALLOWED = 7,	/* System is in 'singel-user mode' */
    KOM_CONF_ZERO = 8,		/* Attempt to use conference number 0. */
    KOM_UNDEF_CONF = 9,		/* Undefined or secret conference */
    KOM_UNDEF_PERS = 10,		/* Undefined or secret person */
    KOM_ACCESS = 11,		 	/* No 'read/write permission' */
    KOM_PERM = 12,			/* No permission */
    KOM_NOT_MEMBER = 13,             /* Not member in conf */
    KOM_NO_SUCH_TEXT = 14,		/* No such global text_no, or no access */
    KOM_TEXT_ZERO = 15,		/* Can't use text no 0 */
    KOM_NO_SUCH_LOCAL_TEXT = 16,	/* No such local text_no */
    KOM_LOCAL_TEXT_ZERO = 17,	/* Can't use local text no 0 */
    KOM_BAD_NAME = 18,		/* Too short/long or contains illegal chars */
    KOM_INDEX_OUT_OF_RANGE = 19,	/*  */
    KOM_CONF_EXISTS = 20,		/* Already exists */
    KOM_PERS_EXISTS = 21,		/* Already exists */
    KOM_SECRET_PUBLIC = 22,		/* Cannot be secret and !rd_prot */
    KOM_LETTER_BOX = 23,		/* Cannot change letter_box flag */
    KOM_LDB_ERR = 24,		/* Database is corrupted. */
    KOM_ILL_MISC = 25,		/* Illegal misc field.
				   err_stat holds field no */

    KOM_ILLEGAL_INFO_TYPE = 26,	/* Info_type parameter was illegal. This
				   means that there is a bug in the client. */
    KOM_ALREADY_RECIPIENT = 27,	/* Already recipient to this text. */
    KOM_ALREADY_COMMENT = 28,	/* Already comment to this text. */
    KOM_ALREADY_FOOTNOTE = 29,	/* Already footnote to this text. */
    KOM_NOT_RECIPIENT = 30,		/* Not recipient */
    KOM_NOT_COMMENT = 31,		/* Not comment to this text. */
    KOM_NOT_FOOTNOTE = 32,		/* Not footnote to this text. */
    KOM_RECIPIENT_LIMIT = 33,	/* Too many recipients */
    KOM_COMM_LIMIT = 34,		/* Too many comments */
    KOM_FOOT_LIMIT = 35,		/* Too many footnotes */
    KOM_MARK_LIMIT = 36,		/* Too many marks. */
    KOM_NOT_AUTHOR = 37,		/* Only the author may add footnotes or
				   delete texts. */
    KOM_OUT_OF_MEMORY = 39,		/* Couldn't get memory for result */
    KOM_CLIENT_IS_CRAZY = 41,	/* The client used an illegal call sequence. */
    KOM_UNDEF_SESSION = 42,	/* No such session exists. */
    KOM_REGEX_ERROR = 43,	/* Regexp compilation failed. */
    KOM_NOT_MARKED = 44,	/* Attempt to unmark an unmarked text. */
    KOM_TEMPFAIL = 45,		/* Try again later. */
    KOM_LONG_ARRAY = 46,	/* Too long array supplied. */
    KOM_ANON_REJECTED = 47,	/* Anonymous text not allowed in conference. */
    KOM_ILL_AUX = 48,           /* Bad misc item */
    KOM_AUX_PERM = 49,          /* No permission to set aux */
    KOM_UNKNOWN_ASYNC = 50,     /* Accepting an unknown async message */
    KOM_INTERNAL_ERROR = 51,    /* Internal server error */
    KOM_FEATURE_DISABLED = 52,  /* Server feature is disabled */
    KOM_MESSAGE_NOT_SENT = 53,  /* Message not sent (no recipient found) */
    KOM_INVALID_MEMBERSHIP_TYPE = 54, /* Invalid membership type */
    KOM_INVALID_RANGE = 55, /* Invalid range (low >= high) */
    KOM_INVALID_RANGE_LIST = 56, /* Invalid range list (not properly sorted) */
    KOM_UNDEFINED_MEASUREMENT = 57, /* Bad measurement name. */
    KOM_PRIORITY_DENIED = 58,	/* You cannot lower your priority that much. */
    KOM_WEIGHT_DENIED = 59,	/* You cannot increase your weight that much.*/
    KOM_WEIGHT_ZERO = 60,	/* You cannot increase your weight that much.*/
    KOM_BAD_BOOL = 61,          /* A Bool must be 0 or 1. */
    KOM_num_errs		/* End marker */
};


extern enum kom_err kom_errno;


extern unsigned long err_stat;	/* Additional information about the error */

/*
 * print a short description of the error to kom_errout (from config.h).
 */
  
void
kom_perror(void);


#endif  /* _KOM_ERRNO_ALREADY_INCLUDED__ */
