# This sed script removes known warnings for the benefit of Xenofarm.
# Placed in the public domain by Per Cederqvist 2003-08-23.

# NOTE: the "?" and "|" operators are not portable.  Don't use them.

# FIXME (bug 976): Lots of warnings from regex.c.
/regex\.c/d

# glibc-2.1.2/gcc-2.95.2 on gratia.
/\/usr\/include\/inttypes.h:427: warning: function returns an aggregate/d
/\/usr\/include\/inttypes.h:428: warning: function call has aggregate value/d
/\/usr\/include\/bits\/string2.h:.*: warning: pointer of type `void \*' used in arithmetic/d

# Linux on alpha. egcs-2.91.66. asmodean.lysator.liu.se as of 2003-07-16.
/\/usr\/include\/inttypes.h:342: warning: function returns an aggregate/d
/\/usr\/include\/inttypes.h:343: warning: function call has aggregate value/d

# egcs-2.91.66 produces this false warning. asmodean.lysator.liu.se as
# of 2003-07-16.
/aux-item-def-scan.c:711: warning: `yy_cp' might be used uninitialized in this function/d

# The Intel C++ compiler doesn't like the -M options that
# automake-1.7.2 uses to create dependency information.
/icc: Command line warning: ignoring option '-M'; no argument required/d
/icc: Command line warning: no action performed for specified object file(s)/d

# flex-2.5.4 generates some unused stuff.
/aux-item-def-scan.c:.*: warning: label `find_rule' defined but not used/d
/aux-item-def-scan.c:.*: warning: `yy_flex_realloc' defined but not used/d
/aux-item-def-scan.c:.*: warning: `yyunput' defined but not used/d

# Quoted from src/server/getopt.h:
#   /* Many other libraries have conflicting prototypes for getopt, with
#      differences in the consts, in stdlib.h.  To avoid compilation
#      errors, only prototype getopt for the GNU C library.  */
/getopt\.h:104: warning: function declaration isn't a prototype/d

# When compiling on a system that doesn't use the GNU C library,
# getopt.c and getopt1.c uses non-prototype declarations of several
# library functions.
#
# Examples: AIX 4.2 on hal.lysator.liu.se, AIX 4.3 on
# dupond.lysator.liu.se.
/getopt\.c:.*: warning: function declaration isn't a prototype/d
/getopt1\.c:.*: warning: function declaration isn't a prototype/d

# This is a harmless comparison of the return value of strlen() and
# a difference between two pointers.  Warning created on AIX 4.3 with
# gcc 3.2 (castafiore.lysator.liu.se).
/getopt\.c:461: warning: comparison between signed and unsigned/d

# This is a false warning from gcc 3.2 (seen oon AIX 4.3,
# castafiore.lysator.liu.se).
/getopt\.c:451: warning: `indfound' might be used uninitialized in this function/d

# On AIX 4.3, with the /usr/vac/bin/cc compiler, sig_atomic_t is
# apparently a volatile type.  The lyskom-server code uses "volatile
# sig_atomic_t", since many systems exists where sig_atomic_t isn't
# volatile, and this results in the warnings below.
# Seen on dupond.lysator.liu.se.
/"sys\.c", line .*: 1506-112 (W) Duplicate type qualifier "volatile" ignored\./d
/"signal\.c", line .*: 1506-112 (W) Duplicate type qualifier "volatile" ignored\./d


# Linux on ia64. gcc-2.96. soyokaze.roxen.com as of 2003-01-18.
/\/usr\/include\/bits\/socket\.h:271: warning: cast increases required alignment of target type/d

# Linux on ia64. gcc-2.96. gosroth.roxen.com as of 2003-07-16.
/\/usr\/include\/bits\/socket\.h:268: warning: cast increases required alignment of target type/d

# Linux on alpha. egcs-2.91.66. asmodean.lysator.liu.se as of 2003-07-16.
/\/usr\/include\/bits\/socket\.h:261: warning: cast increases required alignment of target type/d

# Linux on ia64 with the ecc compiler.  soyokaze.roxen.com as of 2003-01-18.
/warning #688: "format" is an unrecognized __declspec attribute/d

# HP-UX 11.0 with gcc and without GAS does not support -g.
# taylor.lysator.liu.se as of 2003-07-16.
/cc1: warning: -g is only supported when using GAS on this processor,/d
/cc1: warning: -g option disabled/d
/as: warning 2: Unknown option "--traditional-format" ignored\./d

# Assigning SIG_IGN to sa_handler gives this warning on some systems:
# Solaris 2.7 with gcc 2.95.2: stanly as of 2003-07-31.
# Solaris 2.8 with gcc 2.95.3: lenin as of 2003-07-31.
/ramkomd\.c:156: warning: function declaration isn't a prototype/d

# Solaris 2.7 with gcc 2.95.2.
# stanly as of 2003-07-31.
/\/usr\/include\/sys\/vnode\.h:382: warning: declaration of `free' shadows global declaration/d

# Including <math.h> gives this warning on Linux.  gcc-2.96, libc-2.2.5.
# paragon as of 2003-07-31.
/\/usr\/include\/bits\/mathinline\.h:.*: warning: comparing floating point with == or != is unsafe/d

# Including <math.h> gives these warning on Linux.  egcs-2.91.66.
# asmodean as of 2003-07-31.
/\/usr\/include\/bits\/mathinline\.h:.*: warning: no previous prototype for `__fdim'/d
/\/usr\/include\/bits\/mathinline\.h:.*: warning: no previous prototype for `fdim'/d
/\/usr\/include\/bits\/mathinline\.h:.*: warning: no previous prototype for `__fdimf'/d
/\/usr\/include\/bits\/mathinline\.h:.*: warning: no previous prototype for `fdimf'/d

# -Wwrite-strings causes this warning on HP-UX 11.0.
# fonda.roxen.com as of 2003-07-31.
/prot-a-output\.c:.*: warning: passing arg 3 of `snprintf' discards qualifiers from pointer target type/d

# Lenin has a dubious float.h header as of 2003-08-03.
/\/usr\/include\/limits\.h:165: warning: `DBL_MAX' redefined/d
/\/i\/gcc\/2\.95\.3\/lib\/gcc-lib\/i386-pc-solaris2\.8\/2\.95\.3\/include\/float\.h:63: warning: this is the location of the previous definition/d
/\/usr\/include\/limits\.h:167: warning: `DBL_MIN' redefined/d
/\/i\/gcc\/2\.95\.3\/lib\/gcc-lib\/i386-pc-solaris2\.8\/2\.95\.3\/include\/float\.h:54: warning: this is the location of the previous definition/d
/\/usr\/include\/limits\.h:170: warning: `FLT_MAX' redefined/d
/\/i\/gcc\/2\.95\.3\/lib\/gcc-lib\/i386-pc-solaris2\.8\/2\.95\.3\/include\/float\.h:35: warning: this is the location of the previous definition/d
/\/usr\/include\/limits\.h:171: warning: `FLT_MIN' redefined/d
/\/i\/gcc\/2\.95\.3\/lib\/gcc-lib\/i386-pc-solaris2\.8\/2\.95\.3\/include\/float\.h:26: warning: this is the location of the previous definition/d
