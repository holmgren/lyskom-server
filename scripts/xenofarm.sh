#!/bin/sh
# Perform a test compilation of the LysKOM server under Xenofarm.
# Copyright (C) 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/.

# To understand this file, you should look at the xenofarm file 
# projects/lyskom-server/source-transform.sh (see <URL:http://cvs.
# lysator.liu.se/viewcvs/viewcvs.cgi/xenofarm/projects/lyskom-server/
# source-transform.sh?cvsroot=xenofarm>).  It will attempt to unpack
# the distribution, configure it, compile, run tests, and collect
# information in the "r" directory.
#
# How can this file unpack the distribution when it is part of the
# distribution?  Read source-transform.sh and find out.
#
# Why is this file present here, and not in the Xenofarm CVS module?
# Because whenever anything here changes, we probably want to re-run
# the tests on the Xenofarm, and committing anything to the
# lyskom-server CVS module will trigger the creation of a source
# distribution.

cfgargs=
makeargs=
while [ $# -gt 0 ]
do
  case "$1" in
      --cfg)    shift; cfgargs="$1"; shift;;
      --make)   shift; makeargs="$1"; shift;;
      *)        echo $0: unsupported argument $1 >&2; exit 1;;
  esac
done

rm -rf r
mkdir r
exec > r/packlog.txt 2> r/packwarn.txt

# For some unknown reason, crond on asmodean has file descriptor 21
# open to /dev/null.  This interferes with the test suite, so we close
# that file descriptor.  It isn't portable to manipulate file
# descriptor 10 and above from a shell script, so do this only on the
# affected host (that is running bash).
if test "`hostname`" = asmodean.lysator.liu.se
then
    exec 21<&-
fi


VERS=`echo lyskom-server*tar.gz|sed s/lyskom-server-//|sed s/.tar.gz//`
BASE=lyskom-server-$VERS

timeecho () {
    echo `TZ=UTC date|awk '{print $6 "-" $2 "-" $3 " " $4}'\
	|sed -e s/Jan/01/ -e s/Feb/02/ -e s/Mar/03/ -e s/Apr/04/ \
	     -e s/May/05/ -e s/Jun/06/ -e s/Jul/07/ -e s/Aug/08/ \
	     -e s/Sep/09/ -e s/Oct/10/ -e s/Nov/11/ -e s/Dec/12/ `: "$@"
}

log () {
    echo "$@" >> r/mainlog.txt
    TZ=GMT LC_ALL=C date >> r/mainlog.txt
}

logstart () {
    log "BEGIN $1"
}

logpass () {
    log "PASS"
}

logfail () {
    log "FAIL"
}

logwarn () {
    log "WARN $1"
}

dotask () {
    important="$1"
    task="$2"
    warnfunc="$3"
    cmd="$4"
    if test $status = good
    then
	logstart $task
        timeecho Begin $task
        if sh -c "$cmd" > r/${task}log.txt 2>&1
        then
	    if [ -z "$warnfunc" ]
	    then
	        logpass
	    else
	        $warnfunc
	    fi
        else
	    timeecho FAIL: $task
	    if [ $important = 1 ]
	    then
	        status=${task}-failed
	    fi
	    logfail
        fi
    else
	echo status $status makes it impossible to perform this step \
	    > r/${task}log.txt
    fi
}

cfgwarn () {
    egrep -i 'warning|\(w\)' r/cfglog.txt \
    > r/cfgwarn.txt
    warnings=`wc -l < r/cfgwarn.txt`
    if test $warnings -gt 0
    then
	logwarn $warnings
    else
	rm r/cfgwarn.txt
	logpass
    fi
}

makewarn () {
    egrep -i 'warning|\(w\)' r/makelog.txt \
    | sed -f $BASE/scripts/warnings.sed \
    > r/makewarn.txt

    # Special case for a suspect gcc installation on fonda.roxen.com
    # as of 2003-07-16.
    if grep '/i/gcc/2\.95\.2/lib/gcc-lib/hppa1\.1-hp-hpux11\.00/2\.95\.2/include/stddef\.h:165: warning: this is the location of the previous definition' r/makewarn.txt >/dev/null 2>&1
    then
	grep -v '/usr/include/.*warning: `__size_t. redefined' r/makewarn.txt \
	| grep -v '2\.95\.2/include/stddef.h:165: warning: this is the' \
	| grep -v '/i/gcc/.*/string\.h:.*conflicting types for built-in func' \
	> r/makewarn2.txt
	mv r/makewarn2.txt r/makewarn.txt
    fi
    
    warnings=`wc -l < r/makewarn.txt`
    if test $warnings -gt 0
    then
	logwarn $warnings
    else
	rm r/makewarn.txt
	logpass
    fi
}

ckprgwarn () {
    egrep -i 'warning|\(w\)|error' r/ckprglog.txt \
|grep -v 'WARNING: Couldn.t find the global config file.' \
|grep -v 'WARNING: Couldn.t find tool init file' \
    > r/ckprgwarn.txt
    warnings=`wc -l < r/ckprgwarn.txt`
    if test $warnings -gt 0
    then
	egrep -i 'error' r/ckprgwarn.txt \
	> r/ckprgfail.txt
	if test `wc -l < r/ckprgfail.txt` -gt 0
	then
	    logfail
	else
	    rm r/ckprgfail.txt
	    logwarn $warnings
	fi
    else
	rm r/ckprgwarn.txt
	logpass
    fi
}


pfx=`pwd`/pfx

status=good

echo 'FORMAT 2' > r/mainlog.txt
ulimit -c unlimited

dotask 1 "unzip"  "" "gzip -d $BASE.tar.gz"
dotask 1 "unpack" "" "tar xf $BASE.tar"
dotask 1 "cfg"    "cfgwarn" "cd $BASE && ./configure -C --prefix=$pfx $cfgargs"
dotask 0 "fds"    "" "cd $BASE/src/server/testsuite && make check-testfd"
dotask 0 "id_cc"  "" "cd $BASE && make ident-cc"
dotask 0 "id_tx"  "" "cd $BASE/doc && make ident-makeinfo"
dotask 1 "make"   "makewarn" "cd $BASE && make $makeargs"
dotask 0 "sigjmp" "" "cd $BASE/src/server/testsuite && make check-test-sigjmp"

#
# "make check" requirements
#

pdfok=true
dviok=true
checkprgok=true

# We need "runtest".
if (runtest -V) 2>&1
then
    :
else 
    echo runtest not found >> r/ckprglog.txt
    checkprgok=false
fi

# runtest on taylor is broken.
if test "`hostname`" = taylor && test "`domainname`" = lysator.liu.se
then
    echo runtest is broken on taylor >> r/ckprglog.txt
    checkprgok=false
fi

# We need "pdftex"
if (pdftex --version) 2>&1
then
    :
else 
    echo pdftex not found >> r/pdflog.txt
    pdfok=false
fi

# We need "texi2dvi"
if (texi2dvi --version) 2>&1
then
    :
else 
    echo texi2dvi not found >> r/dvilog.txt
    dviok=false
fi

# We need "tex"
if (tex --version) 2>&1
then
    :
else 
    echo tex not found >> r/dvilog.txt
    dviok=false
fi

# No use to run pdf and dvi if check-doc fails.
oldstatus=$status

dotask 1 "ckdoc" "" "cd $BASE/doc && make check-doc"

if $dviok
then
    dotask 0 "dvi" "" "cd $BASE/doc && make dvi"
fi

if $pdfok
then
    dotask 0 "pdf" "" "cd $BASE/doc && make pdf"
fi

# Restore status: check is useful even if check-doc failed.
status=$oldstatus

if $checkprgok
then
    dotask 0 "ckprg" "" "cd $BASE/src && make check"
fi

dotask 1 "install" "" "cd $BASE && make install"

# FIXME: run distcheck.
# FIXME: compare the contents of the distcheck-generated tar file
# with the one we distributed.

# Collect stuff.

timeecho Collecting results
logstart pack

find pfx -type f -print | sort > r/installedfiles.txt
if test `wc -l < r/installedfiles.txt` -eq 0
then
    rm r/installedfiles.txt
fi

mv $BASE/config.cache r/configcache.txt
mv $BASE/config.log r/configlog.txt
mv $BASE/src/libraries/libisc-new/config.log r/iscconfiglog.txt
mv $BASE/src/libraries/liboop/config.log r/oopconfiglog.txt
mv $BASE/src/libraries/adns/config.log r/adnsconfiglog.txt
mv $BASE/src/libraries/adns/src/config.h r/adnsconfig-h.txt
mv $BASE/config.h r/config-h.txt
for file in $BASE/src/server/testsuite/*.log
do
  if test -f $file
  then
      mv $file r/`basename $file`.txt
  fi
done
find $BASE -name core\* -print > r/corefiles.txt
if test `wc -l < r/corefiles.txt` -eq 0
then
    rm r/corefiles.txt
else
    # FIXME (bug 1066): Since the test suite produces a core file this
    # is not a good thing to do.
    : echo core files found >&2
fi
env > r/environ.txt
echo $PATH > r/path.txt
makeinfo --version > r/makeinfo.txt
type makeinfo >> r/makeinfo.txt 2>&1

mv buildid.txt r/buildid.txt

warnings=`wc -l < r/packwarn.txt`
if test $warnings -gt 0
then
    logwarn $warnings
else
    rm r/packwarn.txt
    logpass
fi

echo END >> r/mainlog.txt

(cd r && tar cf - *) > xenofarm_result.tar
gzip -1 xenofarm_result.tar

exit 0
