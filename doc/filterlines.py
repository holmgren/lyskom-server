#!/usr/bin/env python
# Remove lines matching any line of several input files.

# Copyright (C) 2001-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Read and remember each line from all the file names given as arguments.
# Then, read stdin, remove any line present in the input files, and
# print the rest to stdout.

import sys

def add_contents(filename, m):
    f = open(filename, "r")
    while 1:
        line = f.readline()
        if line == "":
            break
        if line[-1] == "\n":
            line = line[:-1]
        m[intern(line)] = None
    f.close()

def filter_file(f, m):
    while 1:
        line = f.readline()
        if line == "":
            break
        if line[-1] == "\n":
            line = line[:-1]
        if not m.has_key(line):
            print line

def main(files):
    m = {}
    for fn in files:
        add_contents(fn, m)
    filter_file(sys.stdin, m)

if __name__ == '__main__':
    main(sys.argv)
