# 
# $Id: aux-items.conf,v 1.33 2003/08/28 20:36:21 kent Exp $
# Copyright (C) 1994-2003  Lysator Academic Computer Association.
# 
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
# 
# Please report bugs at http://bugzilla.lysator.liu.se/. 
# 
#
# If you make an addition to this file that you feel is useful
# you should contact the LysKOM developers to reserve a number
# and to get the change incorporated into the server distribution.
#
# The file format is documented in lyskomd.info.


#
# Content type of a text (MIME type)
#

1 : content-type (text)
{
    author-only     = true;
    one-per-person  = true;
    secret          = false;
    hide-creator    = false;
    dont-garb       = false;
    inherit         = false;
    inherit-limit   = 1;
    validate        = "^[^/]*/[^/]*(;.*)?$";
}


#
# Quick reply to a text
#

2 : fast-reply (text)
{
    secret          = false;
    hide-creator    = false;
    dont-garb       = false;
    inherit         = false;
    inherit-limit   = 1;
}


#
# Cross reference from one object to another
#

3 : cross-reference (text, conference, letterbox)
{
    dont-garb       = false;
    inherit         = false;
    inherit-limit   = 1;
    validate        = "^[CTP][0-9]+( .*)?$";
}


#
# Request for no comments to a text
#

4 : no-comments (text)
{
    secret          = false;
    hide-creator    = false;
    author-only     = true;
    dont-garb       = false;
    inherit         = false;
    inherit-limit   = 1;
    one-per-person  = true;
    validate        = "^$";
}


#
# Request for personal comments only
#

5 : personal-comment (text)
{
    secret          = false;
    hide-creator    = false;
    author-only     = true;
    dont-garb       = false;
    inherit         = false;
    inherit-limit   = 1;
    one-per-person  = true;
    validate        = "^$";
}


#
# Request for read confirmation
#

6 : request-confirmation (text)
{
    secret          = false;
    one-per-person  = true;
    author-only     = true;
    dont-garb       = false;
    inherit         = false;
    inherit-limit   = 1;
    hide-creator    = false;
    validate        = "^$";
}


#
# Read confirmation (creator has read the text)
#

7 : read-confirm (text)
{
    secret          = false;
    hide-creator    = false;
    dont-garb       = false;
    inherit         = false;
    inherit-limit   = 1;
    permanent       = true;
    validate        = "^$";
}


#
# Request redirect of texts from one conference to another or to e-mail
#

8 : redirect (conference, letterbox)
{
    dont-garb       = false;
    supervisor-only = true;
    owner-delete    = true;
    validate        = "^(LysKOM|E-mail):";
    inherit	    = false;
    inherit-limit   = 1;
    secret 	    = false;
    hide-creator    = false;
}


#
# Graphics in compface format
#

9 : x-face (conference, letterbox, server)
{
    secret          = false;
    hide-creator    = false;
    supervisor-only = true;
    owner-delete    = true;
    dont-garb       = false;
    inherit         = false;
    inherit-limit   = 1;
}


#
# Alternate name (subject or name) selected by the creator
#

10 : alternate-name (text, conference, letterbox)
{
    dont-garb       = false;
    inherit         = false;
}


#
# PGP signature of a text (created with pgp -fsba)
#

11 : pgp-signature (text)
{
    permanent       = true;
    hide-creator    = false;
    dont-garb       = false;
    inherit         = false;
    inherit-limit   = 1;
}


#
# Public key of a person
#

12 : pgp-public-key (letterbox)
{
    author-only     = true;
    owner-delete    = true;
    hide-creator    = false;
    dont-garb       = false;
    inherit         = false;
    inherit-limit   = 1;
}


#
# E-mail address of a person or conference
#

13 : e-mail-address (conference, letterbox, server)
{
    author-only     = true;
    hide-creator    = false;
    owner-delete    = true;
    dont-garb       = false;
    inherit         = false;
    inherit-limit   = 1;
}

#
# FAQ in Text
#

14 : faq-text (conference, letterbox, server)
{
    author-only     = true;
    hide-creator    = false;
    secret          = false;
    inherit         = false;
    unique-data	    = true;
    owner-delete    = true;
    inherit-limit   = 1;
    add-trigger     = link-faq();
    validate        = "^[1-9][0-9]*$";
    validate        = existing-readable-text();
}

#
# Name of the creating software
#

15 : creating-software (text)
{
    author-only     = true;
    permanent       = true;
    one-per-person  = true;
    hide-creator    = false;
    dont-garb       = false;
    inherit         = false;
}

#
# Name of actual author.
#

16 : mx-author (create text)
{
    author-only     = true;
    one-per-person  = true;
    inherit	    = false;
    hide-creator    = false;
    dont-garb       = false;
    secret          = false;
}

#
# From header of an imported e-mail
#

17 : mx-from (text)
{
    author-only     = true;
    one-per-person  = false;
    inherit	    = false;
    hide-creator    = false;
    dont-garb       = false;
    secret          = false;
}

#
# Reply-To header of an imported e-mail
#

18 : mx-reply-to (text)
{
    author-only     = true;
    one-per-person  = false;
    inherit	    = false;
    hide-creator    = false;
    dont-garb       = false;
    secret          = false;
}

#
# To header of an imported or to-be-exported e-mail
#

19 : mx-to (text)
{
    author-only     = false;
    one-per-person  = false;
    inherit	    = false;
    hide-creator    = false;
    secret          = false;
    dont-garb       = false;
}

#
# CC header of an imported or to-be-exported e-mail
#

20 : mx-cc (text)
{
    author-only     = false;
    one-per-person  = false;
    inherit	    = false;
    hide-creator    = false;
    secret          = false;
    dont-garb       = false;
}

#
# Date header of an imported e-mail
#

21 : mx-date (text)
{
    author-only     = true;
    one-per-person  = true;
    inherit         = false;
    hide-creator    = false;
    secret          = false;
    dont-garb       = false;
    validate        = "^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]( [-+][0-9][0-9][0-9][0-9])?$";
}

#
# Message-ID header of an imported e-mail
#

22 : mx-message-id (text)
{
    author-only     = true;
    one-per-person  = true;
    inherit         = false;
    hide-creator    = false;
    secret          = false;
    dont-garb       = false;
}

#
# In-Reply-To header of an imported e-mail
#

23 : mx-in-reply-to (text)
{
    author-only     = true;
    one-per-person  = false;
    inherit         = false;
    hide-creator    = false;
    secret          = false;
    dont-garb       = false;
}

#
# All mail headers
#

24 : mx-misc (text)
{
    author-only     = true;
    one-per-person  = true;
    inherit         = false;
    hide-creator    = false;
    secret          = false;
    dont-garb       = false;
}

#
# Mail filtering or something like that
# Nobody quite knows how this will be used, so DON'T!
#

25 : mx-allow-filter (conference, letterbox) disabled
{
    author-only     = true;
    one-per-person  = false;
    inherit         = false;
    hide-creator    = false;
    secret          = false;
    dont-garb       = false;
}

#
# Mail rejection 
# Nobody quite knows how this will be used, so DON'T!
#

26 : mx-reject-forward (conference, letterbox) disabled
{
    author-only     = true;
    one-per-person  = false;
    inherit         = false;
    hide-creator    = false;
    secret          = false;
    dont-garb       = false;
}

#
# Notify user about comments to text
#

27 : notify-comments (letterbox)
{
    author-only     = true;
    inherit	    = false;
    hide-creator    = false;
    dont-garb       = false;
    validate        = "^[0-9]+$";
    validate        = existing-readable-text();
}

#
# Mirror of FAQ-text
#

28 : faq-for-conf (text)
{
    system-only     = true;
    dont-garb       = true;
}

#
# Conferences that new members should be added to automatically.
#

29 : recommended-conf (server)
{
    secret          = false;
    hide-creator    = false;
    dont-garb	    = false;
    inherit         = false;
    validate        = "^[0-9]+(| [0-9]+(| [01]+))$";
}

#
# Allowed content-types
#

30 : allowed-content-type (conference, letterbox, server)
{
    supervisor-only = true;
    owner-delete    = true;
    secret          = false;
    hide-creator    = false;
    dont-garb       = false;
    inherit         = false;
    validate        = "^[0-9]+ [^/ ]*/[^/ ]*$";
}

#
# The canonical name of a LysKOM server
#

31 : canonical-name (server)
{
    supervisor-only = true;
    one-per-person  = true;
    secret          = false;
    hide-creator    = false;
    dont-garb       = false;
    inherit         = false;
    validate        = "^[---A-Za-z0-9.]+(:[0-9]+)?$";
}

#
# The name of the list imported to this conference
#

32 : mx-list-name (conference)
{
    author-only     = true;
    one-per-person  = true;
    owner-delete    = true;
    secret          = false;
    hide-creator    = false;
    dont-garb       = false;
    inherit         = false;
    validate        = "^[^@]+@[^@]+$";
}

#
# Where comments should be sent if the author isn't a member of any
# of the recipients.  0 means no special action.
#

33 : send-comments-to (letterbox)
{
    author-only     = true;
    one-per-person  = true;
    owner-delete    = true;
    secret          = false;
    hide-creator    = false;
    validate	    = "^(0|[1-9][0-9]*)( (0|1|15))?$";
}

#
# Text should be readable without login.
#

34 : world-readable (text)
{
    author-only     = true;
    one-per-person  = true;
    owner-delete    = true;
    secret          = false;
    dont-garb       = false;
    hide-creator    = false;
    validate	    = "^$";
    inherit         = false;
    inherit-limit   = 1;
}

#
# Should importers refuse to add this conference as a recipient?
#

35 : mx-refuse-import (conference, letterbox)
{
    supervisor-only = true;
    owner-delete    = true;
    secret          = false;
    hide-creator    = false;
    validate        = "^(all|spam|html)$";
    dont-garb       = false;
    inherit         = false;
    one-per-person  = false;
}

#
# The text number of the mail that this mime part belongs to
#

10100 : mx-mime-belongs-to (text)
{
    author-only     = true;
    inherit         = false;
    secret          = false;
    hide-creator    = false;
    dont-garb       = false;
    validate        = "^[0-9]+$";
    validate        = existing-readable-text();
}

#
# The text number of a mime part that belongs to this mail
#

10101 : mx-mime-part-in (text)
{
    author-only     = true;
    inherit         = false;
    secret          = false;
    hide-creator    = false;
    dont-garb       = false;
    validate        = "^[0-9]+$";
    validate        = existing-readable-text();
}

#
# All the mime headers for this imported text
#

10102 : mx-mime-misc (text)
{
    author-only     = true;
    inherit         = false;
    secret          = false;
    hide-creator    = false;
    dont-garb       = false;
    one-per-person  = true;
}

#
# The envelope sender of this imported text
#

10103 : mx-envelope-sender (text)
{
    author-only     = true;
    one-per-person  = false;
    inherit         = false;
    secret          = false;
    hide-creator    = false;
    dont-garb       = false;
}

#
# The file name of this mime part
#

10104 : mx-mime-file-name (text)
{
    author-only     = true;
    inherit         = false;
    secret          = false;
    hide-creator    = false;
    dont-garb       = false;
}
